-- Function: eg_cxc_traslado_fintra(character varying)

-- DROP FUNCTION eg_cxc_traslado_fintra(character varying);

CREATE OR REPLACE FUNCTION eg_cxc_traslado_fintra(usuario character varying)
  RETURNS boolean AS
$BODY$
  
DECLARE
	/*
	Funcion para crear cxc a fintra en selectrik
	*/
	
	--Constantes de la factura.
	numCxC text:='';
	_tipoDocumento text:='';
	_cuentaTemporalPuente text:='';
	_hcFactura text:='';
	_conceptoFactura text:='';
	_terceroFintra text:='';
	_PeriodoCte text:='';
	
	recordFacturas record;
	crearFacturaFintra boolean:=false;
	rs boolean :=FALSE;
 
BEGIN
	--1.)Llenamos las constantes del proceso::....
	_PeriodoCte = replace(substring(now(),1,7),'-','')::varchar;
	_tipoDocumento := (select valor from  constante where  dstrct = 'FINV' and  codigo ='TIPO_DOCUMENTO_FAC' and reg_status = '' );
	_cuentaTemporalPuente:= (select valor from  constante where  dstrct = 'FINV' and  codigo ='CUENTA_TEMPORAL_TRASLADO_FACTURA_CLIENTE' and reg_status = '' );
	_hcFactura := (select valor from  constante where  dstrct = 'FINV' and  codigo ='HC_FACTURA_TRASLADO_FINTRA' and reg_status = '' );
	_conceptoFactura := (select valor from  constante where  dstrct = 'FINV' and  codigo ='CONCEPTO_CXC_FINTRA' and reg_status = '' );
	_terceroFintra := (select valor from  constante where  dstrct = 'FINV' and  codigo ='NIT_FINTRA' and reg_status = '' );

	--2.)Buscamos las facturas a trasladar segun la marca de la factura::...	
		
			FOR recordFacturas IN (
						select * from con.factura a 
						INNER JOIN opav.ofertas o ON a.ref1=o.num_os AND a.referencia_1=o.id_solicitud                 
						where	a.dstrct = 'FINV' 
						and a.tipo_documento = 'FAC' 
						and a.documento like 'NM%' 
						and o.tipo_proyecto NOT IN ('TPR00006','TPR00008')
						and a.reg_status != 'A' 
						and a.nc_traslado = 'S' 
						and a.numero_nc != ''
						and a.factura_traslado = '' 
						and a.cxc_traslado='N'

						)
				
			LOOP
			     
				--1.)Generar Cabecera de la cxc
				numCxC := eg_serie_cxc_traslado();
				INSERT INTO con.factura(
					    reg_status, dstrct, tipo_documento, documento, nit, codcli, concepto, 
					    fecha_factura, fecha_vencimiento, fecha_ultimo_pago, fecha_impresion, 
					    descripcion, observacion, valor_factura, valor_abono, valor_saldo, 
					    valor_facturame, valor_abonome, valor_saldome, valor_tasa, moneda, 
					    cantidad_items, forma_pago, agencia_facturacion, agencia_cobro, 
					    zona, clasificacion1, clasificacion2, clasificacion3, transaccion, 
					    transaccion_anulacion, fecha_contabilizacion, fecha_anulacion, 
					    fecha_contabilizacion_anulacion, base, last_update, user_update, 
					    creation_date, creation_user, fecha_probable_pago, flujo, rif, 
					    cmc, usuario_anulo, formato, agencia_impresion, periodo, valor_tasa_remesa, 
					    negasoc, num_doc_fen, obs, pagado_fenalco, corficolombiana, tipo_ref1, 
					    ref1, tipo_ref2, ref2, dstrct_ultimo_ingreso, tipo_documento_ultimo_ingreso, 
					    num_ingreso_ultimo_ingreso, item_ultimo_ingreso, fec_envio_fiducia, 
					    nit_enviado_fiducia, tipo_referencia_1, referencia_1, tipo_referencia_2, 
					    referencia_2, tipo_referencia_3, referencia_3, nc_traslado, fecha_nc_traslado, 
					    tipo_nc, numero_nc, factura_traslado, factoring_formula_aplicada, 
					    nit_endoso, devuelta, fc_eca, fc_bonificacion, indicador_bonificacion, 
					    fi_bonificacion, endoso_fenalco)
				    VALUES ('', 'FINV',_tipoDocumento, numCxC, _terceroFintra,  get_codnit(_terceroFintra), 'MU',
					    recordFacturas.fecha_factura, recordFacturas.fecha_vencimiento, '0099-01-01 00:00:00'::timestamp without time zone, now()::date, 
					    recordFacturas.descripcion, '',recordFacturas.valor_factura, 0.0,recordFacturas.valor_factura, 
					    recordFacturas.valor_factura, 0.0, recordFacturas.valor_factura, 1.000000, 'PES', 
					    1,'CREDITO','OP','OP', 
					    '','','','', 0, 
					    0, '0099-01-01 00:00:00'::timestamp without time zone, '0099-01-01 00:00:00'::timestamp without time zone, 
					    '0099-01-01 00:00:00'::timestamp without time zone, 'COL', '0099-01-01 00:00:00'::timestamp without time zone, '', 
					    NOW(), usuario,'0099-01-01 00:00:00'::timestamp without time zone, 'S', '', 
					    _hcFactura, '', '', 'OP', '', 0.000000, 
					    '', '0', '0', '', '', recordFacturas.tipo_ref1, 
					    recordFacturas.ref1, recordFacturas.tipo_ref2, recordFacturas.ref2, '', '', 
					    '', 0, '0099-01-01 00:00:00'::timestamp without time zone, 
					    '',recordFacturas.tipo_referencia_1,recordFacturas.referencia_1, recordFacturas.tipo_referencia_2, 
					    recordFacturas.referencia_2, '','', 'N', '0099-01-01 00:00:00'::timestamp without time zone, 
					    '', '', '', 'N', 
					    '', '', '','', '', 
					    '', 'N');
					
					
				--2.)Generar detalle de la cxc
				INSERT INTO con.factura_detalle(
					    reg_status, dstrct, tipo_documento, documento, item, nit, concepto, 
					    numero_remesa, descripcion, codigo_cuenta_contable, cantidad, 
					    valor_unitario, valor_unitariome, valor_item, valor_itemme, valor_tasa, 
					    moneda, last_update, user_update, creation_date, creation_user, 
					    base, auxiliar, valor_ingreso, tipo_documento_rel, transaccion, 
					    documento_relacionado, tipo_referencia_1, referencia_1, tipo_referencia_2, 
					    referencia_2, tipo_referencia_3, referencia_3)
				    VALUES ('', 'FINV', _tipoDocumento, numCxC, 1,  _terceroFintra, '01', 
					    '', recordFacturas.descripcion, _cuentaTemporalPuente, 1, 
					    recordFacturas.valor_factura, recordFacturas.valor_factura, recordFacturas.valor_factura,recordFacturas.valor_factura, 1, 
					    'PES', '0099-01-01 00:00:00'::timestamp without time zone, '', NOW(), usuario, 
					    'COL', '', 0, '', 0, 
					    '', '', '', '', 
					    '', '', '');
					
				--3.)Actulizamos la factura iterada para marcarla como procesada
				UPDATE con.factura set cxc_traslado='S' WHERE documento=recordFacturas.documento and  tipo_documento=recordFacturas.tipo_documento; 
				

				--4.)..::Realiza factura en la bd de fintra llamando un procedimiento en selectrik::..			
				select into crearFacturaFintra salida from  dblink('dbname=fintra port=5432 host=localhost user=postgres password=bdversion17','SELECT con.eg_cxc_pm_fintra('''||usuario||''','''||recordFacturas.documento||''') as salida') AS ct (salida boolean);
				raise notice 'crearFacturaFintra: %',crearFacturaFintra;
				/*
				--5.) ..::CONTABILIZO LA CXC::..
				
				_grupo_transaccion = 0;
				SELECT INTO _grupo_transaccion nextval('con.comprobante_grupo_transaccion_seq');
			     
				--(Cabecera)
				INSERT INTO con.comprobante(
					    reg_status, dstrct, tipodoc, numdoc, grupo_transaccion, sucursal, 
					    periodo, fechadoc, detalle, tercero, total_debito, total_credito, 
					    total_items, moneda, fecha_aplicacion, aprobador, last_update, 
					    user_update, creation_date, creation_user, base, usuario_aplicacion, 
					    tipo_operacion, moneda_foranea, vlr_for, ref_1, ref_2)
				    VALUES ('', 'FINV', 'FAC', numCxC, _grupo_transaccion, 'OP', 
					    _PeriodoCte, now(), 'CONT FAC '||numCxC, recordManifiestos.identificacion, recordManifiestos.valor, recordManifiestos.valor, 
					    2, 'PES', '0099-01-01 00:00:00', Usuario, now(),
					    Usuario, now(), Usuario, 'COL', Usuario, 
					    '003', '', 0.00, '', '');
				
				--(Detalle)
				--1
				_transaccion = 0;
				SELECT INTO _transaccion nextval('con.comprodet_transaccion_seq');
				
				INSERT INTO con.comprodet(
					    reg_status, dstrct, tipodoc, numdoc, grupo_transaccion, transaccion, 
					    periodo, cuenta, auxiliar, detalle, valor_debito, valor_credito, 
					    tercero, documento_interno, last_update, user_update, creation_date, 
					    creation_user, base, tipodoc_rel, documento_rel, abc, vlr_for, 
					    tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2, 
					    tipo_referencia_3, referencia_3)
				    VALUES ('', 'FINV', 'FAC', numCxC, _grupo_transaccion, _transaccion, 
					    _PeriodoCte, '13802806', 'CUENTA AUXILIAR', recordManifiestos.transportadora, recordManifiestos.valor, 0.00, 
					    recordManifiestos.identificacion, 'FAC', now(), Usuario, now(), 
					    Usuario, 'COL', 'ICA', RsInfoIA.num_ingreso, '', 0.00, 
					    '', '', '', '',
					    '', '');
					
				--2
				_transaccion = 0;
				SELECT INTO _transaccion nextval('con.comprodet_transaccion_seq');
				
				INSERT INTO con.comprodet(
					    reg_status, dstrct, tipodoc, numdoc, grupo_transaccion, transaccion, 
					    periodo, cuenta, auxiliar, detalle, valor_debito, valor_credito, 
					    tercero, documento_interno, last_update, user_update, creation_date, 
					    creation_user, base, tipodoc_rel, documento_rel, abc, vlr_for, 
					    tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2, 
					    tipo_referencia_3, referencia_3)
				    VALUES ('', 'FINV', 'FAC', numCxC, _grupo_transaccion, _transaccion, 
					    _PeriodoCte, '13802805', 'CUENTA AUXILIAR', 'CXC CORRIDA TRANSPORTADORAS', 0.00, recordManifiestos.valor,
					    recordManifiestos.identificacion, 'FAC', now(), Usuario, now(), 
					    Usuario, 'COL', 'FAC', numCxC, '', 0.00, 
					    '', '', '', '', 
					    '', '');
				
				--ACTUALIZAR FACTURA CxC - Para que no se contabilice en el proceso normal.
				update con.factura
				set 
				   transaccion = _grupo_transaccion, 
				   fecha_contabilizacion = now()
				where documento = numCxC;						    
				--------------------------------------------------------------------------------------------------------------------------				
				*/

				rs :=TRUE;
			END LOOP;		

	RETURN  rs;

END $BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION eg_cxc_traslado_fintra(character varying)
  OWNER TO postgres;
