ALTER TABLE solicitud_aval ADD COLUMN compra_cartera BOOLEAN DEFAULT FALSE;
ALTER TABLE solicitud_aval ADD COLUMN activo_fijo BOOLEAN DEFAULT FALSE;
ALTER TABLE solicitud_aval ADD COLUMN capital_trabajo BOOLEAN DEFAULT FALSE;
ALTER TABLE solicitud_aval ADD COLUMN cuota_maxima NUMERIC(11,2) DEFAULT 0;

ALTER TABLE solicitud_persona ADD COLUMN extracto_correspondecia BOOLEAN DEFAULT FALSE;
ALTER TABLE solicitud_persona ADD COLUMN extracto_email BOOLEAN DEFAULT TRUE;
ALTER TABLE solicitud_persona ADD COLUMN parentesco_deudor_solidario VARCHAR(20) DEFAULT '';

ALTER TABLE solicitud_negocio ADD COLUMN activos NUMERIC(11,2) DEFAULT 0;
ALTER TABLE solicitud_negocio ADD COLUMN pasivos NUMERIC(11,2) DEFAULT 0;

ALTER TABLE solicitud_negocio ADD COLUMN tiene_socio VARCHAR(2) DEFAULT 'N';
ALTER TABLE solicitud_negocio ADD COLUMN porcentaje_participacion NUMERIC(5,2) DEFAULT 0;
ALTER TABLE solicitud_negocio ADD COLUMN nombre_socio VARCHAR(50) DEFAULT '';
ALTER TABLE solicitud_negocio ADD COLUMN identificacion_socio VARCHAR(15) DEFAULT '';
ALTER TABLE solicitud_negocio ADD COLUMN direccion_socio VARCHAR(100) DEFAULT '';
ALTER TABLE solicitud_negocio ADD COLUMN telefono_socio VARCHAR(20) DEFAULT '';
ALTER TABLE solicitud_negocio ADD COLUMN tipo_actividad VARCHAR(20) DEFAULT '';
ALTER TABLE solicitud_negocio ADD COLUMN local VARCHAR(20) DEFAULT '';
ALTER TABLE solicitud_negocio ADD COLUMN camara_comercio VARCHAR(2) DEFAULT 'N';
ALTER TABLE solicitud_negocio ADD COLUMN tipo_negocio VARCHAR(20) DEFAULT '';

ALTER TABLE solicitud_referencias ADD COLUMN referencia_comercial VARCHAR(20) DEFAULT '';