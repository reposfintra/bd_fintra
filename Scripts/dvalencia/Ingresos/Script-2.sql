select * from con.ingreso_detalle where reg_status = '' and (num_ingreso, documento) in 
(SELECT  
						      ing.num_ingreso,
						      idet.documento 
						      --,neg.cod_neg, ing.periodo
						FROM con.ingreso ing
						INNER JOIN con.ingreso_detalle idet ON (ing.num_ingreso=idet.num_ingreso AND ing.tipo_documento=idet.tipo_documento)
						LEFT JOIN con.factura fac on (fac.documento=idet.documento AND 	fac.tipo_documento=idet.tipo_doc AND fac.negasoc !='')
						left join negocios neg on neg.cod_neg = fac.negasoc
						WHERE branch_code in ('SUPEREFECTIVO','BANCOLOMBIA','BANCO OCCIDENTE'
										     ,'BCO COLPATRIA','CAJA TESORERIA','CAJA UNIATONOMA'
										     ,'FID COLP RECFEN','FENALCO ATLANTI','BANCOLMBIA MC','EFECTY')
						AND bank_account_no in ('SUPEREFECTIVO','CA','CC','CTE 802027144','BARRANQUILLA'
											,'UNIATONOMA','FIDCOLP REC FENALCO','CORFICOLOMBIANA'
									,'MICROCREDITO','EFECTY','CAJA')
						AND idet.reg_status=''
						AND idet.cuenta not in ('23050128','23809904')
						AND idet.num_ingreso in  ('IC311765','IC314061','IC314157','IC315378','IC315690')
						and idet.documento != ''
--						and idet.periodo = '201809'
						--and neg.cod_neg = '' --IN ('MC08898')
				       --and COALESCE(idet.procesado_ica,'N') = 'N'
						--AND idet.procesado_ica = 'S'
						AND ing.fecha_consignacion::DATE > '2016-12-01'::DATE
						-- neg.cod_neg not in (select negocio_reestructuracion from rel_negocios_reestructuracion)
						--and neg.cod_neg  in (select negocio_reestructuracion from rel_negocios_reestructuracion)
						AND idet.nitcli IN (SELECT cod_cli FROM negocios WHERE estado_neg='T' group by cod_cli)
						--AND idet.periodo='201809'
						--and neg.id_convenio in(35,16)
						--AND idet.nitcli IN (SELECT cod_cli FROM negocios WHERE estado_neg='T' group by cod_cli)
						GROUP BY
						      idet.periodo,
						      ing.branch_code,
						      ing.bank_account_no,
						      ing.num_ingreso,
						      ing.tipo_documento,
						      ing.creation_date,
						      idet.nitcli,
						      ing.fecha_consignacion,
						      idet.cuenta,
						      idet.documento,
						      idet.tipo_doc,
						      idet.creation_user,
						      idet.procesado_ica,
						      fac.num_doc_fen,
						      idet.descripcion,
						      neg.id_convenio
						      --,neg.cod_neg, ing.periodo
						ORDER BY
						ing.num_ingreso,
						ing.fecha_consignacion,
						idet.periodo,
						branch_code
						,documento desc 						
						)						
order by periodo, num_ingreso


IC229733
IC229907
IC230237


('MI78597','CA64346')
('IC241358','IC241365','IC241629','IC241951','IC243491','IC243860','IC244226')


select periodo,* from con.ingreso
--UPDATE con.ingreso SET branch_code = 'SUPEREFECTIVO', bank_account_no='SUPEREFECTIVO'
where num_ingreso in ('IC315692')

MC07959
MC08005
MC08080
MC08457
MC07935


select * from con.ingreso_detalle where num_ingreso in  ('IC312852')

select * from con.factura where negasoc = 'MC05849' order by documento

select negasoc,documento from con.factura where documento in ('MC0707708')

select * from con.ingreso_detalle where documento in (select documento from con.factura where negasoc in ('MC07935'))

select sp_uneg_negocio_name('FA37343')

select * from rel_negocios_reestructuracion where negocio_reestructuracion in ('MC12489',
'MC11326',
'MC11327',
'MC08898',
'MC10350',
'MC12939',
'MC13393',
'MC13105',
'MC08168')

select * from con.factura where documento = 'R0035996'

select * from con.factura_detalle where documento = 'R0035996'

select i.num_ingreso, vlr_ingreso, negasoc, negocio_reestructuracion
from con.ingreso i
inner join con.ingreso_detalle id on id.num_ingreso=i.num_ingreso
left join con.factura f on f.documento=id.documento
left join rel_negocios_reestructuracion r on r.negocio_reestructuracion=f.negasoc
where i.num_ingreso in ('IC283276',
'IC284211',
'IC284246',
'IC284683',
'IC284694',
'IC284698',
'IC284699',
'IC284700',
'IC284722',
'IC285103',
'IC287101',
'IC288947')


select * from con.comprodet where numdoc = 'IC261352'

select * from con.ingreso_detalle where documento in (select documento from con.factura where negasoc = 'MC05074')
and reg_status = ''
			
		SELECT
			erx.tipo_documento, erx.num_ingreso, agencia--, erx.negocio, neg.creation_date
		FROM
			--con.eg_recaudo_xunidad('201707', 'LIBRANZA') erx
			tem.eg_recaudo_xunidad('201809', 'MICROCREDITO') erx
		INNER JOIN
			negocios neg on(neg.cod_neg=erx.negocio)
		INNER JOIN
			convenios con on(con.id_convenio=neg.id_convenio)
		WHERE
			erx.num_ingreso ilike'IC%' AND
			erx.negocio not ilike 'TF%' --and
			--erx.negocio  in (select negocio_reestructuracion from rel_negocios_reestructuracion)
			--and NUM_INGRESO IN('IC247815','IC247707')
		group by
			erx.tipo_documento,erx.num_ingreso, agencia

			
		SELECT   un.descripcion, neg.cod_neg FROM con.factura fac
		INNER JOIN negocios neg ON (neg.cod_neg=fac.negasoc)
		INNER JOIN rel_unidadnegocio_convenios rneg on (rneg.id_convenio=neg.id_convenio)
		INNER JOIN unidad_negocio un on (un.id=rneg.id_unid_negocio)
		WHERE documento='FG1856705' AND tipo_documento IN ('FAC','NDC') AND un.id in (1,2,3,4,6,7,8,9,10,13,21,22,30,31);
			
		select * from con.ingreso_detalle where num_ingreso = 'IC237114'	

SELECT
				ing.num_ingreso,
				ing.cuenta,
				ing.tipo_documento,
				ing.creation_date,
				ing.periodo,
				ing.fecha_consignacion,
				ing.descripcion,
				ing.nitcli,
				0 as valor_debito,
				valor_ingreso as valor_credito,
				ing.cuenta_banco,
				ing.cuota,
				ing.negocio,
				ing.documento
			FROM
				--con.eg_recaudo_xunidad_xingreso('201705', 'LIBRANZA', 'IC239084')  ing
				con.eg_recaudo_xunidad_xingreso('201809', '', 'IC316414')  ing
			WHERE
				NUM_INGRESO='IC316414' AND valor_ingreso!=0

				
				
select 
	*
	from 
		con.homolacion_interface 
	where 
		proceso='RECAUDO'  
		--tipo_doc_fintra=tipo_doc_fintra_ and 
		and cuenta_fintra in ('13050921','28150533','I010300014170') 
		--agencia_fintra like coalesce(agencia_fintra_,'%');

select * from con.factura where documento = 'MI99100'
select * from con.homolacion_interface where proceso = 'RECAUDO_AUTOM' and cuenta_fintra like '16%'


	SELECT un.descripcion, neg.cod_neg, neg.creation_date  FROM con.factura fac
			INNER JOIN negocios neg ON (neg.cod_neg=fac.negasoc)
			INNER JOIN rel_unidadnegocio_convenios rneg on (rneg.id_convenio=neg.id_convenio)
			INNER JOIN unidad_negocio un on (un.id=rneg.id_unid_negocio)
			WHERE documento='FG2054102' AND tipo_documento IN ('FAC','NDC') AND un.id in (1,2,3,4,6,7,8,9,10,21,22);