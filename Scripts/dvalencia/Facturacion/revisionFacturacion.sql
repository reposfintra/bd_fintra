select
		'2018-11-30'::date as vencimiento_rop,
		''::varchar as venc_mayor,
		f.id_convenio::numeric, negasoc::varchar, f.nit::varchar,
		''::varchar as nom_cli,
		''::varchar as direccion,
		''::varchar as barrio,
		''::varchar as ciudad,
		''::varchar as departamento,
		''::varchar as agencia,
		''::varchar as linea_producto,
		count(0)-1::numeric as total_cuotas_vencidas,
		''::varchar as cuotas_pendientes,
		('2018-10-16'-min(fecha_vencimiento)::DATE)::numeric as min_dias_ven,
		null::date as fecha_ultimo_pago,
		0::numeric as subtotal_det,
		0::numeric as total_sanciones,
		0::numeric as total_descuento,
		0::numeric as total_det,
		0::numeric as total_abonos,
		''::varchar as observaciones,
		''::varchar as msg_paguese_antes,
		''::varchar as msg_estado,
		0::numeric as capital,
		0::numeric as int_cte,
		0::numeric as seguros,
		0::numeric as int_mora,
		0::numeric as gxc,
		0::numeric as dscto_cap,
		0::numeric as dscto_seguro,
		0::numeric as dscto_int_cte,
		0::numeric as dscto_int_mora,
		0::numeric as dscto_gxc,
		0::numeric as subtotal_corriente,
		0::numeric as subtotal_vencido,
		0::numeric as subtotalneto,
		0::numeric as descuentos,
		0::numeric as total,
		''::varchar as est_comercio
		FROM con.foto_ciclo_pagos f
		WHERE  f.reg_status = ''
			and f.dstrct = 'FINV'
			and f.tipo_documento in ('FAC','NDC')
			and f.id_convenio in (select id_convenio from rel_unidadnegocio_convenios where id_unid_negocio =2)
			and f.valor_saldo > 0
			and f.id_ciclo =185
			and ( (select financia_aval from negocios where cod_neg = f.negasoc) = 't' or ( (select financia_aval from negocios where cod_neg = f.negasoc) = 'f' and (select negocio_rel from negocios where cod_neg = f.negasoc) = '' ) )
			and (select negocio_rel_seguro from negocios where cod_neg = f.negasoc) = ''
			and (select negocio_rel_gps from negocios where cod_neg = f.negasoc) = ''
			and f.negasoc in ('FA38165') --('FA18901','FA18900')
			--and f.fecha_vencimiento <= Ciclo.fecha_fin
			and substring(f.documento,1,2) not in ('CP','FF')  and f.negasoc not in (select cod_neg from extractos_generados where unidad_negocio = 2 and periodo = 201811 and num_ciclo = 1)
			GROUP BY f.id_convenio, f.negasoc,f.nit 
			
			
			
SELECT --DISTINCT ON (f.num_doc_fen)
		f.negasoc as negocio,
		f.documento,
		f.num_doc_fen as cuota,
		sum(f.valor_factura) as valor_factura,
		sum(f.valor_abono) as valor_abono,
		sum(f.valor_saldo) as valor_saldo
		--sum(f.valor_saldo) + SP_TieneCuotaManejo(periodo_corriente, f.negasoc, f.num_doc_fen) as valor_saldo
		--f.fecha_factura,
		--f.fecha_vencimiento
	FROM con.foto_ciclo_pagos f
	WHERE   f.reg_status = ''
		and f.dstrct = 'FINV'
		and f.id >= 15612255
		and f.tipo_documento in ('FAC','NDC')
		--and substring(f.documento,char_length(f.documento)-1,char_length(f.documento)) !='00'
		and f.valor_saldo > 0
		and f.negasoc = 'FA38165'
	--	and f.id_convenio in (select id_convenio from rel_unidadnegocio_convenios where id_unid_negocio = und_negocio)
		and substring(f.documento,1,2) not in ('CP','FF','CM')
		and f.fecha_vencimiento <= '2018-10-16'
		--and replace(substring(f.fecha_vencimiento,1,7),'-','')::numeric <= periodo_corriente
		and SUBSTRING(f.documento,8,2) !='00'
		and f.periodo_lote ='201811'
	GROUP BY f.documento, f.num_doc_fen,f.negasoc --,f.num_doc_fen --,f.fecha_factura,f.fecha_vencimiento
	ORDER BY f.num_doc_fen
			
	select * from con.factura where negasoc ='FA38165' and valor_saldo>0;
	
	403321 v_f
	304441 v_a
	98880  v_s

	select * from con.factura_detalle where documento ='FG2383508';  304.441
	CAPITAL				:	381221.00
    INTERESES			:   6100.00  
    CUOTA-ADMINISTRACION :	16000.00  