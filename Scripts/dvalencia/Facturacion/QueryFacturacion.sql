SELECT 2 as und_neg, * FROM sp_ver_extracto_fenalco_ciclo_pagos (2 , '201811', 1, '2018-10-16'::date, 'Visualizar', 'DVALENCIA') as extracto (vencimiento_rop date, venc_mayor varchar, id_convenio numeric, negasoc varchar, nit varchar, nom_cli varchar, direccion varchar, barrio varchar, 
		ciudad varchar, departamento varchar, agencia varchar, linea_producto varchar, total_cuotas_vencidas numeric, cuotas_pendientes varchar, min_dias_ven numeric, fecha_ultimo_pago date, subtotal_det numeric, total_sanciones numeric, 
		total_descuento numeric, total_det numeric, total_abonos numeric, observaciones varchar, msg_paguese_antes varchar, msg_estado varchar, capital numeric, int_cte numeric, seguro numeric, int_mora numeric,
		gxc numeric,dscto_cap numeric, dscto_seguro numeric, dscto_int_cte numeric, dscto_int_mora numeric, dscto_gxc numeric, subtotal_corriente numeric, subtotal_vencido numeric, subtotalneto numeric, descuentos numeric, total numeric, est_comercio varchar)
            where extracto.venc_mayor in ('1- CORRIENTE','2- 1 A 30','3- ENTRE 31 Y 60','4- ENTRE 61 Y 90','5- ENTRE 91 Y 120','6- ENTRE 121 Y 180','7- ENTRE 180 Y 360') order by venc_mayor

select num_doc_fen,valor_factura,valor_abono,valor_saldo,* from con.factura where negasoc = 'FA37954' and valor_saldo>0 ;

FACTURA : 2.264.873,2
VALOR ABONO : 0.00
VALOR SALDO : 2.264.873,2

SELECT --DISTINCT ON (f.num_doc_fen)
	f.negasoc as negocio,
	f.documento,
	f.num_doc_fen as cuota,
	sum(f.valor_factura) as valor_factura,
	sum(f.valor_abono) as valor_abono,
	sum(f.valor_saldo) as valor_saldo
	--sum(f.valor_saldo) + SP_TieneCuotaManejo(periodo_corriente, f.negasoc, f.num_doc_fen) as valor_saldo
	--f.fecha_factura,
	--f.fecha_vencimiento
FROM con.foto_ciclo_pagos f
WHERE   f.reg_status = ''
	and f.dstrct = 'FINV'
	and f.id >= 15612473
	and f.tipo_documento in ('FAC','NDC')
	--and substring(f.documento,char_length(f.documento)-1,char_length(f.documento)) !='00'
	and f.valor_saldo > 0
	and f.negasoc ='FA37954' 
	and f.id_convenio in (select id_convenio from rel_unidadnegocio_convenios where id_unid_negocio = 2)
	and substring(f.documento,1,2) not in ('CP','FF','CM')
	and f.fecha_vencimiento <= '2018-11-07'
--	and SUBSTRING(f.documento,8,2) !='00'
	and f.periodo_lote = '201811'
GROUP BY f.documento, f.num_doc_fen,f.negasoc --,f.num_doc_fen --,f.fecha_factura,f.fecha_vencimiento
ORDER BY f.num_doc_fen


SELECT negocio, cedula, prefijo, cuota, fecha_factura, fecha_vencimiento, sum(valor_saldo) as valor_saldo, ('2018-10-16'-fecha_vencimiento::DATE) AS dias_vencidos,prioridad_pago FROM (
	SELECT
		f.negasoc as negocio,
		f.nit AS cedula,
		f.documento,
		fr.descripcion as prefijo,
		f.num_doc_fen as cuota,
		f.fecha_factura,
		f.fecha_vencimiento,
		fr.valor_unitario as valor_saldo,
		cf.prioridad_pago
	FROM con.foto_ciclo_pagos f, con.factura_detalle fr, conceptos_facturacion cf 
	WHERE   f.documento = fr.documento and cf.descripcion2=fr.descripcion
		and f.reg_status = ''
		and f.dstrct = 'FINV'
		and f.id >= 15612473
		and f.tipo_documento in ('FAC','NDC')
		and fr.reg_status = ''
		and fr.dstrct = 'FINV'
		and fr.tipo_documento in ('FAC','NDC')
		and f.valor_saldo > 0
		and f.negasoc = 'FA37954' 
		and f.num_doc_fen = '1'
		and f.codcli != 'CL00201'
		and f.fecha_vencimiento <= '2018-11-07'
		and substring(f.documento,1,2) not in ('CP','FF','CM')
		and f.periodo_lote = '201811'
	) as c
GROUP BY negocio, cedula, prefijo, cuota, fecha_vencimiento, fecha_factura, dias_vencidos,prioridad_pago
ORDER BY prioridad_pago asc;

select * from con.foto_ciclo_pagos f where documento='FG2367800' and periodo_lote='201811';

select * from con.factura_detalle where documento='FG2367800';
select * from conceptos_facturacion;

1	234049.20	0.00	234049.20
