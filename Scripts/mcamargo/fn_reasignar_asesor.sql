-- Function: fn_reasignar_asesor(text[], character varying, character varying);

-- DROP FUNCTION fn_reasignar_asesor(text[], character varying, character varying);

/*******************Descripcion***********************
*Funcion para reasignar persolicitudes a los asesores*
********************Descripcion**********************/

CREATE OR REPLACE FUNCTION fn_reasignar_asesor(
    _num_solicitud text[],
    _usuario character varying,
    _asesor character varying)
  RETURNS text AS
$BODY$
DECLARE

_pre_solicitudes record;

begin
	
	
	  
                FOR _pre_solicitudes IN  
                
                SELECT 
		                psc.numero_solicitud,
		                psc.identificacion,
		                psc.primer_nombre||' '||psc.primer_apellido as nombre,
		               	psc.asesor,
		               	psc.monto_credito,
		               	c.nombre AS convenio,
		               	psc.estado_sol,
		               	psc.creation_date::date
		        FROM apicredit.pre_solicitudes_creditos psc
		        INNER JOIN convenios c ON c.id_convenio=psc.id_convenio
		        WHERE psc.numero_solicitud=ANY (_num_solicitud::varchar[]) ORDER BY psc.producto
		        
				LOOP
					
				update apicredit.pre_solicitudes_creditos 
				set asesor=_asesor,
				user_update=_usuario,
				last_update=now() 
                where numero_solicitud=_pre_solicitudes.numero_solicitud;
                
                INSERT INTO apicredit.traza_asignacion_asesores
                (numero_solicitud, identificacion, nombre, asesor_origen, asesor_actual, creation_user, creation_date)
                VALUES 
                (_pre_solicitudes.numero_solicitud, _pre_solicitudes.identificacion, _pre_solicitudes.nombre, _pre_solicitudes.asesor, _asesor, _usuario, now());
                
                
                INSERT INTO public.negocios_trazabilidad
				(dstrct, numero_solicitud, usuario, fecha, comentarios, concepto) 
				VALUES
				('FINV', _pre_solicitudes.numero_solicitud, _usuario, now(),'Se Asigna solicitud al asesor: '||_asesor, 'ASIGNACION' );

							   
				END LOOP;
					
			  

			RETURN 'OK';

END $BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION fn_reasignar_asesor(text[], character varying, character varying)
  OWNER TO postgres;
