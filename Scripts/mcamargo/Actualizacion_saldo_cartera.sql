-- Function: fn_reporte_saldo_cartera(character varying,character varying)

-- DROP FUNCTION fn_reporte_saldo_cartera(character varying,character varying);

CREATE OR REPLACE FUNCTION fn_reporte_actualizacion_saldo_cartera(_periodo character varying,_unid_negocio character varying)
  RETURNS SETOF record AS
$BODY$
DECLARE
  _record_facturas record;
  _record_cxc record;
  _vlr_capital numeric:=0;
  _total_vlr_capital numeric:=0;
  _cuotas int:=0;
  _saldo_factura numeric:=0;  
  _total_factura numeric:=0;
 
 

begin
	
	
	

			   FOR _record_facturas IN 	select 
			   							'802022016'::varchar as intermediario,
										fac.nit::varchar as nit,
										neg.cod_neg::varchar as cod_negocio,
										0::numeric as saldo_capital_corte,
										0::numeric as total_obligacion,
										fac.creation_date::date as fecha_corte,
										0::varchar as cuotas_pendientes,										
										fac.fecha_vencimiento::date as inicio_mora,
										'2018-11-01'::date as fecha_cancelacion,
										''::varchar as estado,
										neg.num_pagare::varchar as pagare,
										fac.documento::varchar,
										fac.creation_date::date,
										fac.fecha_ultimo_pago::date
										from negocios neg 
										inner join  con.foto_cartera  fac on neg.cod_neg=fac.negasoc
										inner join rel_unidadnegocio_convenios u on (u.id_convenio = neg.id_convenio) 
										where fac.nit not in ('9003517362','8020196905') 
										and fac.periodo_lote=_periodo and fac.reg_status!='A' and  u.id_unid_negocio= _unid_negocio
										
			
				loop
				
				
						SELECT  into _cuotas COUNT(documento)
						FROM con.foto_cartera 
						WHERE reg_status != 'A'
			            AND dstrct = 'FINV'
			            AND tipo_documento IN ('FAC')
			            AND valor_saldo> 0
			            AND fecha_vencimiento::DATE <= _record_facturas.creation_date::date
			            and periodo_lote=_periodo  
			            and negasoc=_record_facturas.cod_negocio;
						
						SELECT  into _record_cxc *
						FROM con.foto_cartera fac
						WHERE  fac.negasoc= _record_facturas.cod_negocio
						AND fac.documento = _record_facturas.documento
						and periodo_lote=_periodo
						and fac.valor_saldo>0
						AND fac.reg_status !='A';						
				
						
					if (_record_cxc.valor_saldo>0) then 
					
						SELECT into _vlr_capital coalesce(facdet.valor_unitario,0) 
							FROM con.foto_cartera fac
							INNER JOIN con.factura_detalle as facdet on (fac.documento=facdet.documento)
							WHERE fac.negasoc= _record_facturas.cod_negocio
							AND fac.documento = _record_facturas.documento
							and periodo_lote=_periodo
							and fac.valor_saldo>0
							AND fac.reg_status !='A'
							AND facdet.descripcion in ('CAPITAL');
						
						
							if (_record_cxc.valor_saldo<_vlr_capital) then 
							_total_vlr_capital=_record_cxc.valor_saldo;							
							else 
							_total_vlr_capital=_vlr_capital;
							end if;
					
														
							
							if (_cuotas>0) then
							
										
								if (_record_cxc.valor_saldo>_vlr_capital and _record_cxc.fecha_vencimiento::date >_record_facturas.creation_date::date ) then 
								
									_total_factura=_vlr_capital;	
								
								elsif  (_record_cxc.valor_saldo<_vlr_capital and _record_cxc.fecha_vencimiento::date >_record_facturas.creation_date::date )then
								
									_total_factura=_record_cxc.valor_saldo;	
								
								elsif (_record_cxc.valor_saldo<_vlr_capital and _record_cxc.fecha_vencimiento::date <_record_facturas.creation_date::date)then
									
									_total_factura=_record_cxc.valor_saldo;
									
								else 
								
									_total_factura=_record_cxc.valor_saldo;
								
								end if;
					
							else
							
							_total_factura=_total_vlr_capital;
							
							end if;
							
							
					else 
					
					_total_vlr_capital=0;
					_total_factura=0;
						
					end if; 
						
						raise notice '_total_vlr_capital: %',_total_vlr_capital;
						raise notice '_total_factura: %',_total_factura;
						raise notice '_cuotas: %',_cuotas;
						
						_record_facturas.saldo_capital_corte:=_total_vlr_capital;
						_record_facturas.total_obligacion:=_total_factura;
						_record_facturas.cuotas_pendientes:=_cuotas;	
					
						

					RETURN NEXT _record_facturas;
				
				  END LOOP;

END;$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION fn_reporte_actualizacion_saldo_cartera(character varying,character varying)
  OWNER TO postgres;
