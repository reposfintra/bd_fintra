select * from negocios where cod_neg='MC16520'

--eliminamos contablemente las notas de las polizas

delete from con.comprobante where numdoc in (select documento from fin.cxp_items_doc where documento in (select documento from fin.cxp_doc where documento_relacionado='MP13943'))

delete from con.comprodet where numdoc in (select documento from fin.cxp_items_doc where documento in (select documento from fin.cxp_doc where documento_relacionado='MP13943'))

--eliminamos contablemente las cxps generadas

delete from con.comprobante where numdoc in (select documento from fin.cxp_items_doc where documento in (select documento from fin.cxp_doc where documento_relacionado='MC16520'))

delete from con.comprodet where numdoc in (select documento from fin.cxp_items_doc where documento in (select documento from fin.cxp_doc where documento_relacionado='MC16520'))

--eliminamos notacreditos operatva
delete from fin.cxp_items_doc where documento in (select documento from fin.cxp_doc where documento_relacionado='MP13943')

delete from fin.cxp_doc where documento_relacionado='MP13943'

--eliminamos notacreditos cxps operativa
delete from fin.cxp_items_doc where documento in (select documento from fin.cxp_doc where documento_relacionado='MC16520')

delete from fin.cxp_doc where documento_relacionado='MC16520'

--eliminamos egresos 
delete from con.comprodet where numdoc='EG63273'

delete from con.comprobante where numdoc='EG63273'

delete from egresodet where document_no='EG63273'

delete from egreso where document_no='EG63273'

--eliminamos cartera

delete from con.comprodet where numdoc in (select documento from con.factura where negasoc='MC16520')

delete from con.comprobante where numdoc in (select documento from con.factura where negasoc='MC16520')

delete from con.factura_detalle where documento in ( select documento from con.factura where negasoc='MC16520')

delete from con.factura where negasoc='MC16520'

--eliminamos diferidos
select * from con.comprobante where numdoc in (select cod from ing_fenalco where codneg='MC16520')

delete from ing_fenalco where codneg='MC16520'

select actividad,estado_neg,* from negocios where cod_neg='MC16520'

select * from negocios_trazabilidad where cod_neg='MC16520'



SE DESISTE NEGOCIO SOLICITADO  POR HENRY FERRER A TRAVEZ DEL TICKET no 6911 

INSERTAR TRAZABILIDAD
INSERT INTO negocios_trazabilidad(
		    reg_status, dstrct, numero_solicitud, actividad, usuario, fecha, cod_neg, comentarios)
		VALUES ('', 'FINV',134603 , 'FOR', 'MCAMARGO', NOW(),  'MC16520', 'NEGOCIO DESISTIDO POR EL CLIENTE SOLICITADO  POR HENRY FERRER  TICKET no 6911 ');


MP13943-FZ1
MP13943-FZ2
EG63273
MP13943