create table cuentas_endoso_diferidos (
		id                      serial primary key,
		reg_status 				varchar(1) not null default ''::varchar,
		id_convenio             integer not null,
		tipo_documento 			varchar(8) not null default ''::varchar,
		cuenta 					varchar(15) not null default ''::varchar,
		descripcion 			varchar(100) not null default ''::varchar,
		cta_ingreso_dif			varchar(15) not null default ''::varchar,
		creation_user			varchar(20) not null default ''::varchar,
		creation_date 			timestamp without time zone NOT NULL DEFAULT now()
);

truncate table cuentas_endoso_diferidos

select * from cuentas_endoso_diferidos


INSERT INTO public.cuentas_endoso_diferidos
( reg_status, id_convenio, tipo_documento, cuenta, descripcion, cta_ingreso_dif, creation_user, creation_date)
VALUES( '', 55, 'CA', '16252338', 'CTA CAT MICRO', 'I12345678', 'MCAMARGO', now());

INSERT INTO public.cuentas_endoso_diferidos
( reg_status, id_convenio, tipo_documento, cuenta, descripcion, cta_ingreso_dif, creation_user, creation_date)
VALUES( '', 55, 'CM', '16252336', 'CTA CUOTA ADMON MICRO', 'I12345678', 'MCAMARGO', now());

INSERT INTO public.cuentas_endoso_diferidos
( reg_status, id_convenio, tipo_documento, cuenta, descripcion, cta_ingreso_dif, creation_user, creation_date)
VALUES( '', 55, 'MI', '16252335', 'CTA INTERES MICRO', 'I12345678', 'MCAMARGO', now());


 '{16252302,16252338,16252336,16252335}'; --1:=cartera, 2:=cat ,3:= cuota admon, 4:= interes,
 				CA     CM        MI


select * from rel_unidadnegocio_convenios where id_unid_negocio=1


SELECT cuenta FROM cuentas_endoso_diferidos WHERE id_convenio=10 and tipo_documento='CA';


 
 