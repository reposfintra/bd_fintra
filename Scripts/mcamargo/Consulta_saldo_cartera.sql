select * from (select intermediario,
nit, 
cod_negocio,
sum (saldo_capital_corte) as saldo_capital_corte,
sum(total_obligacion) as total_obligacion,
fecha_corte,
cuotas_pendientes,
case
	when cuotas_pendientes>0
	then min(inicio_mora::date) end as inicio_mora,
case 
	when sum (saldo_capital_corte)=0 
	then max(fecha_ultimo_pago) end as fecha_cancelacion,
case
	when cuotas_pendientes =0 and sum (saldo_capital_corte)!=0 then 'V' 
	when cuotas_pendientes >0 and sum (saldo_capital_corte)!=0 then 'M' 
	when cuotas_pendientes =0 and sum (saldo_capital_corte) =0 then 'C' 
	end as estado,
pagare 
from  fn_reporte_actualizacion_saldo_cartera('201811'::varchar,30::varchar) as cartera 
(
intermediario varchar,
nit varchar,
cod_negocio varchar,
saldo_capital_corte numeric,
total_obligacion numeric,
fecha_corte date,
cuotas_pendientes varchar,
inicio_mora date,
fecha_cancelacion date,
estado varchar,
pagare varchar,
documento varchar,
creation_date date,
fecha_ultimo_pago date
)group by intermediario,
nit, 
cod_negocio,
fecha_corte,
fecha_cancelacion,
cuotas_pendientes,
estado,
pagare,
creation_date)rs
where rs.estado in ('M','V') 
or (rs.estado='C'
	and replace(substring(rs.fecha_cancelacion,1,7),'-','')= replace(substring(rs.fecha_corte,1,7),'-','')::int-1)
	
	
	select * from consultas where creador='MCAMARGO'

