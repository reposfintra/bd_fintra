/*Se crea Schema apoteosys*/
create schema apoteosys;

/*Creamos la tabla 
 Ejecutar el siguiente archivo 
 Ruta:fintra/apoteosys/Tables/apoteosys.control_contabilizacion_apoteosys.sql

*/

/*Realizamos el Insert de los datos en la tabla*/

INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(1, '', 'FINV', 'A-ENDEUDAMIENTO', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(2, '', 'FINV', 'B-INVERSIONISTAS', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(3, '', 'FINV', 'C-MICROCREDITO', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(4, '', 'FINV', 'D-LIBRANZA', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(5, '', 'FINV', 'E-EDUCATIVO', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(6, '', 'FINV', 'F-CONSUMO', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(7, '', 'FINV', 'G-AUTOMOTOR', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(8, '', 'FINV', 'H-FIANZA', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(9, '', 'FINV', 'I-DIFERIDOS', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(10, '', 'FINV', 'J-ENDOSO', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(11, '', 'FINV', 'K-CAUSACION_INT', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(12, '', 'FINV', 'L-RECAUDO', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');
INSERT INTO apoteosys.control_contabilizacion_apoteosys
(id, reg_status, dstrct, descripcion, empresa, creation_date, creation_user, last_update, user_update)
VALUES(13, '', 'FINV', 'M-LOGISTICA', 'FINTRA', '2018-10-22 12:09:16.610', 'MCAMARGO', '2018-10-22 12:09:16.610', 'MCAMARGO');

