

--['Numero Solicitud','Identificacion','Nombre','Asesor','Convenio','Estado','Fecha Creacion','Valor']
CREATE TABLE APICREDIT.TRAZA_ASIGNACION_ASESORES
(
  id SERIAL PRIMARY KEY,
  reg_status character varying(1) default '' NOT NULL,
  numero_solicitud character varying(15) default '' NOT NULL,
  identificacion character varying(15) default '' NOT NULL,
  nombre character varying(80) default '' NOT NULL,
  asesor_origen character varying(20) default '' NOT NULL,
  asesor_actual character varying(20) default '' NOT NULL,
  creation_user character varying(20) NOT NULL DEFAULT ''::character varying,
  creation_date timestamp without time zone NOT NULL DEFAULT now()
)

DROP TABLE APICREDIT.traza_ASIGNACION_ASESORES

SELECT * FROM APICREDIT.TRAZA_ASIGNACION_ASESORES

INSERT INTO apicredit.asignacion_asesores
(numero_solicitud, identificacion, nombre, asesor_anterior, asesor_actual, creation_user, creation_date)
VALUES(?, ?, ?, ?, ?, ?, now());

SELECT * FROM negocios_trazabilidad WHERE numero_solicitud=123739


SELECT * FROM detalle_poliza_negocio WHERE cod_neg='122476'