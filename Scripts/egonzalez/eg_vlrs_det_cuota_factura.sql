﻿CREATE INDEX index_fact_v1 ON con.factura USING btree (reg_status,dstrct,documento,tipo_documento,negasoc,valor_saldo);
CREATE INDEX index_fact_v2 ON con.factura USING btree (reg_status,SUBSTRING(documento,1,2),tipo_documento,negasoc,num_doc_fen);
CREATE INDEX index_cmc ON con.cmc_doc USING btree (cmc,tipodoc);
CREATE INDEX index_neg ON negocios USING btree (cod_neg,cod_cli,estado_neg);
CREATE INDEX index_sa ON solicitud_aval USING btree (cod_neg,id_convenio);
CREATE INDEX index_sa2 ON solicitud_aval USING btree (cod_neg);
CREATE INDEX idx_ftocartera_masivo_anomes_v2 ON con.foto_cartera USING btree (periodo_lote,ano, mes);


-- Function: eg_vlrs_det_cuota_factura(codneg text, concepto character varying, _documento varchar ,_cuota varchar);

-- DROP FUNCTION eg_vlrs_det_cuota_factura(codneg text, concepto character varying, _documento varchar ,_cuota varchar);

CREATE OR REPLACE FUNCTION eg_vlrs_det_cuota_factura(codneg text, concepto character varying, _documento varchar ,_cuota varchar)
  RETURNS numeric AS
$BODY$
DECLARE
 
  _valor numeric:=0.00;
  _recordFactura record;
  _recordLiquidador record;
  _esquema varchar:='';



BEGIN

SELECT INTO _recordLiquidador cod_neg, item, fecha, dias, saldo_inicial, capital, interes,
       valor, saldo_final, reg_status, creation_date, no_aval, capacitacion,
       cat, seguro, interes_causado, fch_interes_causado, documento_cat,
       custodia, remesa, causar, dstrct, tipo, cuota_manejo, cuota_manejo_causada,
              fch_cuota_manejo_causada, causar_cuota_admin
      FROM documentos_neg_aceptado  
      WHERE cod_neg=codneg AND reg_status='' and item=_cuota;

    --validacion documento
IF SUBSTRING(_documento,1,2) IN ('CM','MI','CA','TS') THEN  
  RETURN 0.00;
ELSIF (concepto ='CAPITAL') THEN
  RETURN _recordLiquidador.capital;
    ELSIF (concepto = 'SEGURO') THEN
      RETURN _recordLiquidador.seguro;
END IF;

_esquema:=COALESCE((SELECT esquema FROM tem.negocios_facturacion_old WHERE cod_neg=codneg UNION ALL SELECT esquema FROM tem.negocios_facturacion_old_fenalco WHERE cod_neg=codneg),'N');
--RAISE NOTICE 'esquema : %',_esquema;
IF(_esquema='S')THEN

 IF concepto='INTERES' THEN
 
  SELECT INTO _valor coalesce(sum(valor_saldo),0.00) as saldo
  FROM con.factura fac
     WHERE  fac.negasoc= codneg
     AND fac.num_doc_fen= _cuota
     AND fac.tipo_documento='FAC'
     AND SUBSTRING(fac.documento,1,2) IN ('MI','TS')
     AND fac.reg_status !='A' ;
   
 ELSIF concepto='CAT' THEN
 
  SELECT INTO _valor coalesce(sum(valor_saldo),0.00) as saldo
     FROM con.factura fac
     WHERE  fac.negasoc=codneg
     AND fac.tipo_documento='FAC'
     AND fac.num_doc_fen=  _cuota
     AND SUBSTRING(fac.documento,1,2) IN ('CA')
     AND fac.reg_status !='A';
     
 ELSIF concepto='CUOTA-ADMINISTRACION'  THEN
 
  SELECT INTO _valor coalesce(sum(valor_saldo),0.00) as saldo
     FROM con.factura fac
     WHERE  fac.negasoc= codneg
     AND fac.num_doc_fen= _cuota
     AND fac.tipo_documento='FAC'
     AND SUBSTRING(fac.documento,1,2) IN ('CM')
     AND fac.reg_status !='A' ;
     
 END IF;

    ELSE
   
     _valor:=CASE WHEN concepto='INTERES' THEN _recordLiquidador.interes
         WHEN concepto='CAT' THEN _recordLiquidador.cat
         WHEN concepto='CUOTA-ADMINISTRACION' THEN _recordLiquidador.cuota_manejo
        ELSE 0   END;

END IF;
     
    RETURN _valor;

END;$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION eg_vlrs_det_cuota_factura(text, character varying,varchar, varchar)
  OWNER TO postgres;