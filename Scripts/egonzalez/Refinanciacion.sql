--## Refinanciacion tipo (A). Querys

--1) Query que busca los negocios en cartera.
SELECT f.nit AS indentificacion,
        get_nombc(f.nit) AS nombre,
        f.negasoc AS negocio,
        n.vr_negocio AS valor_negocio,
        n.nro_docs AS total_cuotas,
        c.agencia,
        sum(valor_saldo) AS saldo_cartera,
        max(current_date-f.fecha_vencimiento) AS dias_mora,
        COALESCE(e.esquema,'N') AS esquema
     FROM con.factura f
     INNER JOIN negocios n ON n.cod_neg=f.negasoc AND n.cod_cli=f.nit 
     INNER JOIN convenios c ON c.id_convenio=n.id_convenio
     LEFT JOIN tem.negocios_facturacion_old e ON e.cod_neg=f.negasoc
     WHERE f.nit=92522530
        AND n.estado_neg='T'
        AND f.reg_status=''
        AND f.tipo_documento='FAC'
        AND f.valor_saldo > 0
     ---   AND periodo_lote='201905'
        AND eg_altura_mora_periodo(f.negasoc::character varying, (TO_CHAR(current_date,'YYYYMM')::integer), 1::integer, 0::integer) IN ('2- 1 A 30','3- ENTRE 31 Y 60','4- ENTRE 61 Y 90')
     GROUP BY 
         indentificacion,
         nombre,
         negocio,
         valor_negocio,
         total_cuotas,
         agencia,
         esquema   
       --  having max(current_date-f.fecha_vencimiento) <= 90 
       
         SELECT TO_CHAR(current_date,'YYYYMM')::integer
        
         SELECT eg_altura_mora_periodo('MC15750'::character varying, 201905::integer, 1::integer, 0::integer)
         
SELECT
	CASE WHEN maxdia >= 365 THEN '8- MAYOR A 1 AÑO'
	     WHEN maxdia >= 181 THEN '7- ENTRE 180 Y 360'
	     WHEN maxdia >= 121 THEN '6- ENTRE 121 Y 180'
	     WHEN maxdia >= 91 THEN '5- ENTRE 91 Y 120'
	     WHEN maxdia >= 61 THEN '4- ENTRE 61 Y 90'
	     WHEN maxdia >= 31 THEN '3- ENTRE 31 Y 60'
	     WHEN maxdia >= 1 THEN '2- 1 A 30'
	     WHEN maxdia <= 0 THEN '1- CORRIENTE'
	     WHEN maxdia is null  THEN '1- CORRIENTE'
	     ELSE '0' END AS rango
	FROM (  SELECT
			max(sp_fecha_corte_foto(substring(periodo_lote,1,4),substring(periodo_lote,5)::integer)::date-(fecha_vencimiento)) as maxdia
			FROM con.foto_cartera fra
			WHERE fra.dstrct = 'FINV'   
			AND fra.reg_status=''
			AND periodo_lote = '201905'
			AND fra.valor_saldo > 0
			AND fra.negasoc = 'MC15750'
			AND fra.tipo_documento in ('FAC','NDC')
			AND substring(fra.documento,1,2) not in ('CP','FF','DF')
			AND fra.ano=2019
			AND fra.mes=5
	--	and fra.id >=_id_min
	) 


--2) Query liquidacion de saldo

-- Function: eg_saldos_refinanciacion_negocios(character varying)

-- DROP FUNCTION eg_saldos_refinanciacion_negocios(character varying,character VARYING);

CREATE OR REPLACE FUNCTION eg_saldos_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING)
  RETURNS SETOF record AS
$BODY$
DECLARE
  listaFacturas record;
  filaLiquidador record;
  saldo_factura record;
  fechaAnterior date;
  sumaConceptos NUMERIC;
  tasaIm  NUMERIC;
  tasaIg  NUMERIC;
  query varchar;

BEGIN
	
  IF _tipo_refinanciacion='A' THEN 
  
     query:='SELECT   
				 fac.documento::varchar as documento,
				 fac.negasoc::varchar as negocio,
				 fac.num_doc_fen::varchar as cuota,
				 fac.fecha_vencimiento::date as fecha_vencimiento,	
				 (now()::date-(fac.fecha_vencimiento))::numeric as dias_mora,
				 COALESCE(e.esquema,''N'') AS esquema, 
				 ''''::varchar as estado,
				 0::numeric as valor_saldo_capital,
				 0::numeric as valor_saldo_mi,
				 0::numeric as valor_saldo_ca,
				 0::numeric as valor_cm,
                 0::numeric as valor_seguro,
                 0::numeric as valor_saldo_cuota,
				 0::numeric as IxM,
				 0::numeric as GaC,
				 0::numeric as suma_saldos	
			FROM con.factura as fac 
			INNER JOIN con.factura_detalle as facdet on (fac.documento=facdet.documento AND fac.tipo_documento=facdet.tipo_documento AND fac.nit=facdet.nit)
		    LEFT  JOIN tem.negocios_facturacion_old e ON e.cod_neg=fac.negasoc
			WHERE fac.negasoc='''|| _cod_neg||'''
			AND fac.documento LIKE ''MC%'' 
			AND valor_saldo > 0 
			AND fac.reg_status !=''A''
			AND facdet.descripcion=''CAPITAL''	
            AND to_char(fac.fecha_vencimiento,''YYYYMM'')::INTEGER <=TO_CHAR(CURRENT_DATE,''YYYYMM'')::INTEGER
			order by  fac.num_doc_fen::numeric ';
  
  ELSIF   _tipo_refinanciacion='B' THEN 
  
  	 query:='SELECT   
				 fac.documento::varchar as documento,
				 fac.negasoc::varchar as negocio,
				 fac.num_doc_fen::varchar as cuota,
				 fac.fecha_vencimiento::date as fecha_vencimiento,	
				 (now()::date-(fac.fecha_vencimiento))::numeric as dias_mora,
				 COALESCE(e.esquema,''N'') AS esquema, 
				 ''::varchar as estado,
				 0::numeric as valor_saldo_capital,
				 0::numeric as valor_saldo_mi,
				 0::numeric as valor_saldo_ca,
				 0::numeric as valor_cm,
                 0::numeric as valor_seguro,
                 0::numeric as valor_saldo_cuota,
				 0::numeric as IxM,
				 0::numeric as GaC,
				 0::numeric as suma_saldos	
			FROM con.factura as fac 
			INNER JOIN con.factura_detalle as facdet on (fac.documento=facdet.documento AND fac.tipo_documento=facdet.tipo_documento AND fac.nit=facdet.nit)
		    LEFT  JOIN tem.negocios_facturacion_old e ON e.cod_neg=fac.negasoc
			WHERE fac.negasoc= '''|| _cod_neg||'''
			AND fac.documento LIKE ''MC%'' 
			AND valor_saldo > 0 
			AND fac.reg_status !=''A''
			AND facdet.descripcion=''CAPITAL''	
			order by  fac.num_doc_fen::numeric ';
  
  ELSE 
    -- RETURN NEXT NULL;
  END IF;
  
     RAISE NOTICE 'sql : % ', query;
 
   FOR listaFacturas IN EXECUTE query
	LOOP
	    
		sumaConceptos=0;
        tasaIm=0;
		tasaIg=0;
		
        SELECT INTO saldo_factura * FROM eg_detalle_saldo_facturas_mc(listaFacturas.negocio, listaFacturas.cuota::integer);

		IF (listaFacturas.dias_mora > 0) THEN 
			/* ***********************
			* Estado de la factura   *
			**************************/
			listaFacturas.estado='VENCIDO';
			listaFacturas.valor_saldo_capital:=saldo_factura.saldo_capital;
			listaFacturas.valor_saldo_mi :=saldo_factura.saldo_interes;
			listaFacturas.valor_saldo_ca :=saldo_factura.saldo_cat;
			listaFacturas.valor_cm :=saldo_factura.saldo_cuota_manejo;
			listaFacturas.valor_seguro:=saldo_factura.saldo_seguro;
			listaFacturas.valor_saldo_cuota := saldo_factura.saldo_factura;
			sumaConceptos=saldo_factura.saldo_factura;

			/* *******************************************
			* Buscamos tasa del negocio. por el convenio *
			**********************************************/
			
			SELECT INTO tasaIm c.tasa_usura FROM negocios as n
			INNER JOIN convenios as c on (c.id_convenio=n.id_convenio)
			WHERE n.cod_neg =listaFacturas.negocio;
           
			 IF (FOUND) THEN
				listaFacturas.IxM:= round((((sumaConceptos * tasaIm)/100 ) / 30) * listaFacturas.dias_mora );
			END IF;
			
			/* *******************************
			* Gastos de Cobranza por factura *
			**********************************/	
			SELECT INTO tasaIg max(porcentaje) FROM sanciones_condonaciones 
			WHERE id_unidad_negocio = 1 
			AND id_conceptos_recaudo in (2,4,6) 
			AND id_tipo_acto = 1 
			AND categoria='GAC'
			AND periodo = replace(substring(now(),1,7),'-','')
			AND listaFacturas.dias_mora between dias_rango_ini and dias_rango_fin
            GROUP BY id_conceptos_recaudo
			ORDER by id_conceptos_recaudo ;
			
			 IF (FOUND) THEN
				listaFacturas.GaC =round((sumaConceptos * tasaIg) ::numeric /100) ; 
			END IF;

            /* *******************
			 ** Suma saldos *****
			 *********************/
            listaFacturas.suma_saldos=sumaConceptos + listaFacturas.IxM +listaFacturas.GaC ;                     
			fechaAnterior =listaFacturas.fecha_vencimiento;
			
		ELSIF (to_char(listaFacturas.fecha_vencimiento,'YYYYMM')::INTEGER =to_char(current_date,'YYYYMM')) THEN 	
		    listaFacturas.estado='CORRIENTE';	
		    listaFacturas.valor_saldo_capital:=saldo_factura.saldo_capital;
			listaFacturas.valor_saldo_mi :=saldo_factura.saldo_interes;
			listaFacturas.valor_saldo_ca :=saldo_factura.saldo_cat;
			listaFacturas.valor_cm :=saldo_factura.saldo_cuota_manejo;
			listaFacturas.valor_seguro:=saldo_factura.saldo_seguro;
			listaFacturas.valor_saldo_cuota := saldo_factura.saldo_factura;
		    listaFacturas.suma_saldos :=saldo_factura.saldo_factura;
		ELSE 

		    /* ***********************
		     * Estado de la factura   *
		     **************************/
		     listaFacturas.estado='FUTURO';	
             SELECT INTO filaLiquidador * FROM documentos_neg_aceptado d   where d.cod_neg=listaFacturas.negocio and d.item =listaFacturas.cuota ;
		
		     listaFacturas.valor_saldo_ca=filaLiquidador.cat;
    	     listaFacturas.suma_saldos= listaFacturas.valor_saldo_capital + filaLiquidador.cat;
             
		END IF;


	RETURN NEXT listaFacturas;

   END LOOP; 

END;$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION eg_saldos_refinanciacion_negocios(character VARYING,character VARYING)
  OWNER TO postgres;
   
-- Table: administrativo.saldos_refinanciacion_negocios

-- DROP TABLE administrativo.saldos_refinanciacion_negocios;

CREATE TABLE administrativo.saldos_refinanciacion_negocios
(
  id serial,
  reg_status character varying(1) NOT NULL DEFAULT ''::character varying,
  dstrct character varying(4) NOT NULL DEFAULT 'FINV'::character varying,
  documento character varying,
  negocio character varying,
  cuota character varying,
  fecha_vencimiento date,
  dias_mora numeric,
  esquema character varying,
  estado character varying,
  valor_saldo_capital numeric,
  valor_saldo_mi numeric,
  valor_saldo_ca numeric,
  valor_cm numeric,
  valor_seguro numeric,
  valor_saldo_cuota numeric,
  ixm numeric,
  gac numeric,
  suma_saldos NUMERIC,
  creation_date timestamp without time zone DEFAULT now(),
  creation_user character varying(10) DEFAULT ''::character varying,
  last_update timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
  user_update character varying(10) NOT NULL DEFAULT ''::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE administrativo.saldos_refinanciacion_negocios
  OWNER TO postgres;

 ALTER TABLE administrativo.saldos_refinanciacion_negocios ADD COLUMN key_ref character VARYING  NOT NULL DEFAULT ''::character VARYING;
  
 SELECT     documento,
            negocio, 
            cuota,
            fecha_vencimiento,
            dias_mora,
            esquema,
            estado,
            valor_saldo_capital,
            valor_saldo_mi,
            valor_saldo_ca,
            valor_cm,
            valor_seguro,
            valor_saldo_cuota,
            IxM,
            GaC,
            suma_saldos            
FROM eg_saldos_refinanciacion_negocios('MC17868','A') as factura (
documento varchar, 
negocio varchar, 
cuota varchar, 
fecha_vencimiento date,
dias_mora numeric, 
esquema varchar,
estado varchar, 
valor_saldo_capital numeric,
valor_saldo_mi numeric, 
valor_saldo_ca numeric, 
valor_cm numeric,
valor_seguro numeric, 
valor_saldo_cuota numeric,
IxM numeric,
GaC numeric, 
suma_saldos numeric);


--#Query 3 proyeccion de liquidacion.
--


-- Function: eg_proyeccion_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING, _valor_pago NUMERIC );

-- DROP FUNCTION eg_proyeccion_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING, _valor_pago NUMERIC );

CREATE OR REPLACE FUNCTION eg_proyeccion_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING, _valor_pago NUMERIC )
  RETURNS SETOF tem.proyeccion_negocios_refinanciacion AS
$BODY$
DECLARE
  _maxFechaPago date ;
  _maxCuota integer:=0;
  _facturasFuturas record;
  _contadorCuotas  integer:=1;
  _saldo_aplicacion NUMERIC:=_valor_pago;
  _flag boolean:=TRUE;
  _rs tem.proyeccion_negocios_refinanciacion;

BEGIN
    --## Eliminamos tabla temporal 
    TRUNCATE TABLE tem.proyeccion_negocios_refinanciacion;
    
	--## Buscamos la fecha de pago de la ultima cuota.
	_maxFechaPago:=(SELECT MAX(fecha)::date AS max_fecha_vencimiento FROM documentos_neg_aceptado WHERE cod_neg=_cod_neg AND reg_status='');
	_maxCuota :=(SELECT count(0) AS max_cuota FROM documentos_neg_aceptado WHERE cod_neg=_cod_neg AND reg_status='');
	--##Recorremos las cuotas futuras 
	 FOR _facturasFuturas IN (
	 						   SELECT 
								       f.documento,
									   dna.cod_neg,
								       dna.item::integer,
								       dna.fecha::date,
								       dna.capital,
								       dna.interes,
								       dna.cat,
								       dna.cuota_manejo,
								       f.valor_saldo AS valor_cuota_sin_aplicacion,
								       f.valor_saldo AS valor_cuota,								      
								       (current_date-f.fecha_vencimiento::date) AS dias_vencidos,
								       CASE WHEN to_char(f.fecha_vencimiento::date,'YYYYMM')::integer =to_char(current_date,'YYYYMM')::integer THEN 'CORRIENTE'
								             WHEN (current_date-f.fecha_vencimiento::date) > 0 THEN 'VENCIDO'
								            WHEN (current_date-f.fecha_vencimiento::date) < 0 THEN 'FUTURO'
								       END AS estado,
								       'N'::varchar AS color,
								       'N'::varchar AS aplicado
								FROM documentos_neg_aceptado dna
								INNER JOIN con.factura f ON f.negasoc=dna.cod_neg AND dna.item=f.num_doc_fen
								WHERE dna.cod_neg=_cod_neg 
								AND DNA.reg_status=''
								AND f.valor_saldo >0
								AND f.reg_status=''
								AND f.tipo_documento='FAC'
								AND to_char(f.fecha_vencimiento,'YYYYMM')::INTEGER >TO_CHAR(CURRENT_DATE,'YYYYMM')::INTEGER
								ORDER BY f.num_doc_fen::integer			
								)
								UNION ALL
							   (
								SELECT 
								       f.documento,
									   dna.cod_neg,
								       dna.item::integer,
								       dna.fecha::date ,
								       dna.capital,
								       dna.interes,
								       dna.cat,
								       dna.cuota_manejo,
								       f.valor_saldo AS valor_cuota_sin_aplicacion,
								       f.valor_saldo AS valor_cuota,
								       (current_date-f.fecha_vencimiento::date) AS dias_vencidos,
								        CASE WHEN to_char(f.fecha_vencimiento::date,'YYYYMM')::integer =to_char(current_date,'YYYYMM')::integer THEN 'CORRIENTE'
								             WHEN (current_date-f.fecha_vencimiento::date) > 0 THEN 'VENCIDO'
								            WHEN (current_date-f.fecha_vencimiento::date) < 0 THEN 'FUTURO'
								       END AS estado,
								       'S'::varchar AS color,
								       'N'::varchar AS aplicado
								FROM documentos_neg_aceptado dna
								INNER JOIN con.factura f ON f.negasoc=dna.cod_neg AND dna.item=f.num_doc_fen
								WHERE dna.cod_neg=_cod_neg
								AND DNA.reg_status=''
								AND f.valor_saldo >0
								AND f.reg_status=''
								AND f.tipo_documento='FAC'
								AND to_char(f.fecha_vencimiento,'YYYYMM')::INTEGER <=TO_CHAR(CURRENT_DATE,'YYYYMM')::INTEGER
								ORDER BY f.num_doc_fen::integer desc
								)
	
     LOOP 
     				IF _facturasFuturas.estado IN  ('CORRIENTE','VENCIDO')THEN   
     				
     					IF _contadorCuotas =1 THEN 
     						_contadorCuotas :=_maxCuota;
     					END IF;
     				   _contadorCuotas:=_contadorCuotas+1;
     				   _facturasFuturas.item := _contadorCuotas;
     				   _maxFechaPago := (_maxFechaPago + '1 month'::INTERVAL)::date;
     				   _facturasFuturas.fecha := _maxFechaPago;
     				   _facturasFuturas.dias_vencidos:=(current_date-_maxFechaPago::date) ;
     				   _facturasFuturas.estado ='FUTURO';
     				   
     				ELSE 
     			
     				   _contadorCuotas:=_facturasFuturas.item; 
     				END IF; 
     				INSERT INTO tem.proyeccion_negocios_refinanciacion(documento,cod_neg,item,fecha,capital,
  		 											interes,cat,cuota_manejo,valor_cuota,dias_vencidos,estado,color,aplicado,valor_cuota_sin_aplicacion)
  		 			VALUES (_facturasFuturas.documento,_facturasFuturas.cod_neg,_facturasFuturas.item,_facturasFuturas.fecha,_facturasFuturas.capital,
  		 					_facturasFuturas.interes,_facturasFuturas.cat,_facturasFuturas.cuota_manejo,_facturasFuturas.valor_cuota,_facturasFuturas.dias_vencidos,
  		 					_facturasFuturas.estado,_facturasFuturas.color,_facturasFuturas.aplicado, _facturasFuturas.valor_cuota_sin_aplicacion);
     			
     END LOOP; 
				
    --Simulacion del pago sobre saldo 
    
     FOR _facturasFuturas IN SELECT documento,
     								cod_neg,
     								item,
     								fecha,
     								capital,
  		 							interes,
  		 							cat,
  		 							cuota_manejo,
  		 							valor_cuota_sin_aplicacion,
  		 							valor_cuota,
  		 							dias_vencidos,
  		 							estado,
  		 							color,
  		 							aplicado
  		 					  FROM  tem.proyeccion_negocios_refinanciacion WHERE cod_neg =_cod_neg ORDER BY item DESC
    LOOP
                  IF _flag THEN 
                        IF _saldo_aplicacion !=0 THEN 
                  		   _saldo_aplicacion:=_facturasFuturas.valor_cuota - _saldo_aplicacion;          
		    			   IF _saldo_aplicacion > 0  THEN 
		                   		_facturasFuturas.valor_cuota=_saldo_aplicacion;  
		                   		_facturasFuturas.aplicado='P';
		                   		_flag:=FALSE;
		                   ELSIF _saldo_aplicacion < 0 THEN 
		                   	    _facturasFuturas.valor_cuota=0.00;
		                   	    _facturasFuturas.aplicado='T';
		                   		_saldo_aplicacion:=_saldo_aplicacion*-1;
		                   ELSE 
		                        _facturasFuturas.valor_cuota=0.00;
		                        _facturasFuturas.aplicado='T';
		                        _saldo_aplicacion:=0.00;
		                        _flag:=FALSE;
		                   END IF;
		                END IF; 
                   END IF;
                   
                   _rs:=_facturasFuturas;
                   RETURN NEXT _rs;
    				
    END LOOP;



END;$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION eg_proyeccion_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING, _valor_pago NUMERIC )
  OWNER TO postgres;
  

-- Table: administrativo.proyeccion_refinanciacion_negocios

-- DROP TABLE administrativo.proyeccion_refinanciacion_negocios;

CREATE TABLE administrativo.proyeccion_refinanciacion_negocios
(
  id serial,
  reg_status character varying(1) NOT NULL DEFAULT ''::character varying,
  dstrct character varying(4) NOT NULL DEFAULT 'FINV'::character varying,
  documento character varying(10),
  cod_neg character varying(15),
  item integer,
  fecha date,
  capital moneda,
  interes moneda,
  cat moneda,
  cuota_manejo moneda,
  valor_cuota_sin_aplicacion moneda,
  valor_cuota moneda,
  valor_extracto numeric,
  dias_vencidos integer,
  estado text,
  color character varying(1),
  aplicado character varying(1),
  creation_date timestamp without time zone DEFAULT now(),
  creation_user character varying(10) DEFAULT ''::character varying,
  last_update timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
  user_update character varying(10) NOT NULL DEFAULT ''::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE administrativo.proyeccion_refinanciacion_negocios
  OWNER TO postgres;
 
 ALTER TABLE administrativo.proyeccion_refinanciacion_negocios ADD COLUMN key_ref character VARYING  NOT NULL DEFAULT ''::character VARYING;
  
  
SELECT  
    documento,
	cod_neg,
	item,
	fecha,
	capital,
	interes,
	cat,
	cuota_manejo,
	valor_cuota_sin_aplicacion,
	valor_cuota,
	(valor_cuota_sin_aplicacion-valor_cuota) AS valor_extracto,
	dias_vencidos,
	estado,
	color,
	aplicado	
FROM eg_proyeccion_refinanciacion_negocios('MC12810'::character VARYING, 'A'::character VARYING, 21500::NUMERIC ) ORDER BY item::integer;


SELECT  
    cod_neg, 
    documento,
	(valor_cuota_sin_aplicacion-valor_cuota) AS capital,
	0.00::NUMERIC AS interes,
	0.00::NUMERIC AS intXmora,
	0.00::NUMERIC AS gac,
	0.00::NUMERIC AS total,
	0.00::NUMERIC AS porcentanje		
FROM eg_proyeccion_refinanciacion_negocios('MC12810'::character VARYING, 'A'::character VARYING, 21500::NUMERIC )  WHERE aplicado IN ('P','T') ORDER BY item::integer;


-- Table: tem.proyeccion_negocios_refinanciacion

-- DROP TABLE tem.proyeccion_negocios_refinanciacion;

CREATE TABLE tem.proyeccion_negocios_refinanciacion
(
  documento character varying(10),
  cod_neg character varying(15),
  item integer,
  fecha date,
  capital moneda,
  interes moneda,
  cat moneda,
  cuota_manejo moneda,
  valor_cuota_sin_aplicacion moneda,
  valor_cuota moneda,
  dias_vencidos integer,
  estado text,
  color character varying(1),
  aplicado character varying(1)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem.proyeccion_negocios_refinanciacion
  OWNER TO postgres;

  
  
-- Function: eg_generar_extracto_refinanciacion(text[], character varying)

-- DROP FUNCTION eg_generar_extracto_refinanciacion(text[], character varying);

CREATE OR REPLACE FUNCTION eg_generar_extracto_refinanciacion(in_array text[], usuario character varying)
  RETURNS text AS
$BODY$
DECLARE

 negocioIn varchar:='';
 documento varchar:='';
 capital numeric:=0;
 interes numeric:=0;
 intXmora numeric:=0;
 gac numeric:=0;
 totalIn numeric:=0; 
 _cuota varchar:='';

 id_concepto_rec record;
 sumaConceptos numeric:=0;
 _cod_rop_real integer:=0;
 UnidadNeg integer:=0;
 periodoRop varchar:=0;
 vcto_rop date;
 ClienteRec record;
 sqlExtractoDetalle varchar:='';
 retorno varchar:='';
_porcentaje numeric:=0;
_valor_concepto varchar:='';
_aplicarDes varchar:='';
_validarROP boolean:=true;

BEGIN
	FOR i IN 1 .. (array_upper(in_array, 1))
	LOOP
	   
	   RAISE NOTICE 'indice i: % indice j: % negocio : %',i,1, in_array[i][1]; --negocio
	   RAISE NOTICE 'indice i: % indice j: % documento : %',i,2, in_array[i][2]; --documento
	   RAISE NOTICE 'indice i: % indice j: % capital : %',i,3, in_array[i][3]; --capital
	   RAISE NOTICE 'indice i: % indice j: % interes : %',i,4, in_array[i][4]; --interes
	   RAISE NOTICE 'indice i: % indice j: % intXmora : %',i,5, in_array[i][5]; --intXmora
	   RAISE NOTICE 'indice i: % indice j: % gac : %',i,6, in_array[i][6]; --gac
	   RAISE NOTICE 'indice i: % indice j: % total : %',i,7, in_array[i][7]; --total
	   RAISE NOTICE 'indice i: % indice j: % porcentanje : %',i,8, in_array[i][8]; --porcentanje            
              
	   negocioIn :=in_array[i][1];
	   documento :=in_array[i][2];
	   capital :=replace(in_array[i][3],',','');
	   interes :=replace(in_array[i][4],',','');
	   intXmora :=replace(in_array[i][5],',','');
	   gac :=replace(in_array[i][6],',','');
	   totalIn :=replace(in_array[i][7],',',''); 
	   
	   _cuota := (SELECT num_doc_fen FROM  con.factura f WHERE f.documento= in_array[i][2] AND f.reg_status='' AND f.tipo_documento='FAC' );

       IF(_validarROP)THEN 
                
            _validarROP:=false;
            UnidadNeg := sp_uneg_negocio(negocioIn)::numeric;	
            periodoRop := replace(substring(now(),1,7),'-','');  
            vcto_rop := now()::date + '2 day'::interval;

             /* *********************************************
		     ******** INFORMACION BASICA DEL CLIENTE ********/
            SELECT INTO ClienteRec nit,nomcli,direccion,ciudad FROM cliente WHERE nit = (SELECT COD_CLI FROM negocios where cod_neg = negocioIn);
                  
                      
		    INSERT INTO recibo_oficial_pago (cod_rop, id_unidad_negocio, periodo_rop, vencimiento_rop, negocio,cedula,nombre_cliente,
						    direccion,ciudad,cuotas_vencidas,cuotas_pendientes, dias_vencidos, subtotal, total_sanciones, 
						    total_descuentos, total,total_abonos, creation_date, creation_user, last_update, user_update, observacion)
		     VALUES('',UnidadNeg,periodoRop,vcto_rop,negocioIn,ClienteRec.nit,ClienteRec.nomcli,ClienteRec.direccion,ClienteRec.ciudad,'0','0/0','0',0,0,0,0,0,now(),usuario,now(),usuario,'')
		     RETURNING id INTO _cod_rop_real  ;
                   
		     retorno:=_cod_rop_real;
             raise notice '_cod_rop_real: %',_cod_rop_real;
                     
       END IF;

                
      	 FOR j IN 1 .. (array_upper(in_array, 2)-1) LOOP                         
                        
		  	   _porcentaje:= in_array[i][8]::numeric;          
	           IF(j > 2 AND j < 7)THEN
	             _valor_concepto:= replace(in_array[i][j],',',''); 
					IF(j=3)THEN--capital--CAP
	                     SELECT INTO id_concepto_rec * FROM  sp_concepto_unidad(sp_uneg_negocio(in_array[i][1])::numeric,'CAP') as coco(id_concepto integer, descrip varchar); 
	                    _porcentaje:=100; 		
					END IF;
					IF(j=4)THEN--interes--INT  
					   raise notice 'INT %',in_array[i][1];                              
					   SELECT INTO id_concepto_rec * FROM  sp_concepto_unidad(sp_uneg_negocio(in_array[i][1])::numeric,'INT') as coco(id_concepto integer, descrip varchar); 
					   _porcentaje:=100;
					END IF;
					IF(j=5)THEN--intXmora--IXM     
					    raise notice 'IXM % ',in_array[i][1];	
	                    SELECT INTO id_concepto_rec * FROM  sp_concepto_unidad(sp_uneg_negocio(in_array[i][1])::numeric,'IXM') as coco(id_concepto integer, descrip varchar); 
						 _porcentaje:=100;						
					END IF;
					IF(j=6)THEN--gac-GAC
					   raise notice 'GAC % ',in_array[i][1];
	                   SELECT INTO id_concepto_rec * FROM  sp_concepto_unidad(sp_uneg_negocio(in_array[i][1])::numeric,'GAC') as coco(id_concepto integer, descrip varchar);
					   _porcentaje:=100;							    
					END IF;
	
	                RAISE NOTICE '_valor_concepto : %',_valor_concepto;
	                
	                IF _valor_concepto >0.00 THEN 
					sqlExtractoDetalle:=sqlExtractoDetalle||'INSERT INTO detalle_rop (id_rop, id_conceptos_recaudo, descripcion, cuota, dias_vencidos, fecha_factura_padre, 
																			  fecha_vencimiento_padre, fecha_ultimo_pago, items, valor_concepto, valor_descuento, valor_ixm, 
																			  valor_descuento_ixm, valor_gac, valor_descuento_gac, valor_abono, valor_saldo, creation_date, creation_user, negocio,porcentaje_cta_inicial) 
															  VALUES (_current_codrop,'||id_concepto_rec.id_concepto||', '''||id_concepto_rec.descrip||''', '''||_cuota||''', ''0'', ''0099-01-01'', 
																''0099-01-01'','''',1,'|| _valor_concepto||', 0, 0, 0, 0, 0, 0, '|| _valor_concepto||', now(), '''||usuario||''','''||in_array[i][1]||''','||_porcentaje||');';
			       END IF;  
			END IF;
        END LOOP;
	END LOOP;

   /* **********************************************************
   ************REMPLAZAMOS EL ID_ROP POR EL DE LA CABEZERA******/ 
   sqlExtractoDetalle:=REPLACE(sqlExtractoDetalle,'_current_codrop',_cod_rop_real);
   execute sqlExtractoDetalle;

   /* ***********************************************************
   ************ACTUALIZAMOS LA CABECERA DEL RECIBO OFICIAL*******/
   SELECT INTO sumaConceptos SUM(valor_concepto) FROM detalle_rop WHERE id_rop =_cod_rop_real; 
       UPDATE  recibo_oficial_pago SET
               cod_rop = OVERLAY('REF0000000' PLACING _cod_rop_real FROM 11 - length(_cod_rop_real) FOR length(_cod_rop_real)),
	  		   subtotal=sumaConceptos,
               total=sumaConceptos
       WHERE   id=_cod_rop_real;
      
    /**********************************************************************
     * Actualizamos control de refinanciacion con el id_rop				  *
     ***********************************************************************/
      UPDATE administrativo.control_refinanciacion_negocios SET id_rop=_cod_rop_real 
       WHERE id=(SELECT max(id) FROM administrativo.control_refinanciacion_negocios WHERE cod_neg=negocioIn AND estado='P');
     
       
	
	RETURN retorno;

EXCEPTION 

	WHEN foreign_key_violation THEN
		RAISE EXCEPTION 'La institución no puede ser borrada, existen dependencias para este registro.';
		retorno:='FAIL' ;
		return retorno;
        
	WHEN unique_violation THEN
		RAISE EXCEPTION 'Error Insertando en la bd, ya existe en la base de datos.';
		retorno:='FAIL' ;
		return retorno; 

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION eg_generar_extracto_refinanciacion(text[], character varying)
  OWNER TO postgres;
  
SELECT eg_generar_extracto_refinanciacion('{{"MC12810","MC1121911",21499.00,0,0,0,0,0},{"MC12810","MC1121912",78501.00,0,0,0,0,0}}' , 'EGONZALEZ') AS numero_extracto;

SELECT * FROM detalle_rop WHERE id_rop LIKE '%355090%'

'{{"MC12810","MC1121911",21499.00,0,0,0,0,0},{"MC12810","MC1121912",78501.00,0,0,0,0,0}}'

select 217468.00-138967.00

SELECT asesor,creation_date,estado_sol,etapa,* FROM apicredit.pre_solicitudes_creditos WHERE entidad='EDUCATIVO' AND estado_sol='R' ORDER BY creation_date;

--'{{"MC12810","MC000001",1,2,4,5,6,8},{"MC45612","MC000001",1,2,4,5,6,8}}'

-- Sequence: key_refinanciacion_id_seq

-- DROP SEQUENCE key_refinanciacion_id_seq;

CREATE SEQUENCE key_refinanciacion_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 100
  CACHE 1;
ALTER TABLE key_refinanciacion_id_seq
  OWNER TO postgres;
GRANT ALL ON SEQUENCE key_refinanciacion_id_seq TO postgres;

SELECT nextval('key_refinanciacion_id_seq');


-- Function: eg_guardar_refinanciacion_negocio(character varying, character varying, numeric, character varying, numeric)

-- DROP FUNCTION eg_guardar_refinanciacion_negocio(character varying, character varying, numeric, character varying, numeric);

CREATE OR REPLACE FUNCTION eg_guardar_refinanciacion_negocio(
    _cod_neg character varying,
    _tipo_refinanciacion character varying,
    _valor_pago numeric,
    _usuario character varying,
    _porc_negociacion numeric)
  RETURNS text AS
$BODY$
DECLARE

  retorno varchar:='OK'; 
  _estado varchar:='';
  _contador integer:=0;
  _saldo_a_refinanciar NUMERIC:=0;
  _identificador varchar;
  _key int4;

BEGIN
	
	_estado:=COALESCE((SELECT estado FROM administrativo.control_refinanciacion_negocios WHERE cod_neg=_cod_neg AND estado !='S'),'');
	_contador:=COALESCE((SELECT count(0) FROM administrativo.control_refinanciacion_negocios WHERE cod_neg=_cod_neg AND estado ='S'),0);
    PERFORM * FROM negocios WHERE cod_neg =_cod_neg AND estado_neg='T';
	IF FOUND THEN 
			IF  _contador <=2 THEN 
					IF _tipo_refinanciacion='A' THEN 
					
								IF _estado !='P' THEN 
								
									_key:=nextval('key_refinanciacion_id_seq');
									_identificador:=OVERLAY('PAA0000000' PLACING _key FROM 11 - length(_key) FOR length(_key));
								
									--GUARDAMOS LOS SALDOS DE LA REFINACIACION 
									INSERT INTO administrativo.saldos_refinanciacion_negocios(documento, negocio, cuota, fecha_vencimiento, 
																					            dias_mora, esquema, estado, valor_saldo_capital, valor_saldo_mi, 
																					            valor_saldo_ca, valor_cm, valor_seguro, valor_saldo_cuota, ixm, 
																					            gac, suma_saldos,creation_user,key_ref)
									      SELECT documento,
									             negocio, 
									             cuota,
									             fecha_vencimiento,
									             dias_mora,
									             esquema,
									             estado,
									             valor_saldo_capital,
									             valor_saldo_mi,
									             valor_saldo_ca,
									             valor_cm,
									             valor_seguro,
									             valor_saldo_cuota,
									             IxM,
									             GaC,
									             suma_saldos,
									             _usuario,
									             _identificador
									FROM eg_saldos_refinanciacion_negocios(_cod_neg,_tipo_refinanciacion) as factura (
									documento varchar, 
									negocio varchar, 
									cuota varchar, 
									fecha_vencimiento date,
									dias_mora numeric, 
									esquema varchar,
									estado varchar, 
									valor_saldo_capital numeric,
									valor_saldo_mi numeric, 
									valor_saldo_ca numeric, 
									valor_cm numeric,
									valor_seguro numeric, 
									valor_saldo_cuota numeric,
									IxM numeric,
									GaC numeric, 
									suma_saldos numeric); 
									
									--GARDAMOR LA PROYECCION DEL LA REFINACIACION
									INSERT INTO administrativo.proyeccion_refinanciacion_negocios(
														            documento, cod_neg, item, fecha, capital, 
														            interes, cat, cuota_manejo, valor_cuota_sin_aplicacion, valor_cuota, 
														            valor_extracto, dias_vencidos, estado, color, aplicado, creation_user,key_ref)
									SELECT  
										    documento,
											cod_neg,
											item,
											fecha,
											capital,
											interes,
											cat,
											cuota_manejo,
											valor_cuota_sin_aplicacion,
											valor_cuota,
											(valor_cuota_sin_aplicacion-valor_cuota) AS valor_extracto,
											dias_vencidos,
											estado,
											color,
											aplicado,
											_usuario,
											_identificador
									FROM eg_proyeccion_refinanciacion_negocios(_cod_neg, _tipo_refinanciacion, _valor_pago) ORDER BY item::integer;
									
								   UPDATE negocios SET refinanciado='P' WHERE cod_neg=_cod_neg;
								   
								   INSERT INTO administrativo.control_refinanciacion_negocios(cod_neg,estado,creation_user,valor_a_pagar,porc_negociacion,key_ref)
								   VALUES(_cod_neg,'P',_usuario,_valor_pago,_porc_negociacion,_identificador);
								   
							  ELSE 
							  	  retorno:='NEGOCIO EN PROCESO DE REFINANCIACION TIPO A.';
							  END IF;
					       
				   END IF; 
			ELSE 
					retorno:='MAXIMO INTENTOS PERMITIDOS SUPERADO.';
			END IF;
	  ELSE 
	  			retorno:='NEGOCIO NO EXISTE EN LA BD.';
	  END IF;
	
  RETURN retorno;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION eg_guardar_refinanciacion_negocio(character varying, character varying, numeric, character varying, numeric)
  OWNER TO postgres;

  
-- Table: administrativo.control_refinanciacion_negocios

-- DROP TABLE administrativo.control_refinanciacion_negocios;

CREATE TABLE administrativo.control_refinanciacion_negocios
(
  id serial NOT NULL,
  reg_status character varying(1) NOT NULL DEFAULT ''::character varying,
  dstrct character varying(4) NOT NULL DEFAULT ''::character varying,
  cod_neg character varying(15) NOT NULL DEFAULT ''::character varying,
  estado character varying(1) NOT NULL DEFAULT ''::character varying,
  creation_date timestamp without time zone NOT NULL DEFAULT now(),
  creation_user character varying(10) NOT NULL DEFAULT ''::character varying,
  last_update timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
  user_update character varying(10) NOT NULL DEFAULT ''::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE administrativo.control_refinanciacion_negocios
  OWNER TO postgres;

 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN valor_a_pagar NUMERIC  NOT NULL DEFAULT 0.00::NUMERIC;
 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN porc_negociacion NUMERIC  NOT NULL DEFAULT 0.00::NUMERIC;
 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN id_rop INT4  NOT NULL DEFAULT 0.00::INT4;
 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN key_ref character VARYING  NOT NULL DEFAULT ''::character VARYING;
 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN nota_ajuste character VARYING  NOT NULL DEFAULT ''::character VARYING;

  

SELECT * FROM administrativo.proyeccion_refinanciacion_negocios  WHERE cod_neg='MC12810';
SELECT * FROM administrativo.saldos_refinanciacion_negocios WHERE negocio='MC12810';
SELECT * FROM administrativo.control_refinanciacion_negocios WHERE cod_neg='MC12810'; --357718
  
SELECT eg_guardar_refinanciacion_negocio('MC12810'::character VARYING, 'A'::Character VARYING, 0::NUMERIC,'EGONZALEZ'::character VARYING,0);

--delete FROM administrativo.proyeccion_refinanciacion_negocios WHERE cod_neg IN ('MC12810');
--delete FROM administrativo.saldos_refinanciacion_negocios WHERE negocio IN ('MC12810') ;
--delete FROM administrativo.control_refinanciacion_negocios WHERE cod_neg  IN ('MC12810') ;

ALTER TABLE negocios ADD COLUMN refinanciado character varying(1) NOT NULL DEFAULT 'N'::character VARYING ;


SELECT
   ctrl.creation_user AS gestor,
   ctrl.creation_date::date AS fecha_negociacion,
   ctrl.valor_a_pagar AS valor_negociacion,
   ctrl.cod_neg AS negocio,
   n.cod_cli AS cedula,
   get_nombc(n.cod_cli) AS nombre,
   eg_altura_mora_periodo(ctrl.cod_neg::character varying, (TO_CHAR( ctrl.creation_date::date,'YYYYMM')::integer), 1::integer, 0::integer) AS rango_mora,
   ctrl.porc_negociacion,
   COALESCE(max(pm.fecha_pago),'0101-01-01') AS fecha_pago,
   COALESCE(sum(pm.valor_aplicar_neto),0.00) AS valor_pagado,
   COALESCE(pm.banco,'-') as banco,
   cn.agencia,
   CASE WHEN ctrl.estado='P' THEN 'Pendiente'
        WHEN ctrl.estado='S' THEN 'Aplicado'
   END AS estado,
   ctrl.nota_ajuste,
   COALESCE(pm.num_ingreso,'-'), 
   ctrl.key_ref,
   pm.lote_pago,
   pm.aplica_refinanciacion
FROM administrativo.control_refinanciacion_negocios  ctrl 
INNER JOIN negocios n ON n.cod_neg=ctrl.cod_neg
INNER JOIN convenios cn ON cn.id_convenio=n.id_convenio 
LEFT JOIN  tem.pago_masivo__  pm  ON ctrl.cod_neg=pm.negasoc AND logica_aplicacion='PLAN_AL_DIA'
WHERE  ctrl.estado='S'  AND PM.lote_pago='LD00014'
GROUP BY 
gestor,
fecha_negociacion,
valor_negociacion,
negocio,
cedula,
nombre,
porc_negociacion,
pm.banco,
agencia,
ctrl.estado,
ctrl.nota_ajuste,
COALESCE(pm.num_ingreso,'-'),
ctrl.key_ref,
pm.lote_pago,
pm.aplica_refinanciacion

SELECT * FROM administrativo.saldos_refinanciacion_negocios WHERE key_ref='PAA0000191'; --236.618
SELECT * FROM administrativo.proyeccion_refinanciacion_negocios WHERE key_ref='PAA0000191'; --236.618
SELECT * FROM administrativo.control_refinanciacion_negocios WHERE key_ref='PAA0000191'; --236.618


SELECT * FROM con.factura WHERE negasoc='MC15485';

SELECT * FROM negocios WHERE cod_neg='MC16333';

SELECT (substring(py.documento,1,length(py.documento)-2)||CASE WHEN py.item < 10 THEN '0'||py.item ELSE py.item::varchar END ) AS new_documento,
       py.fecha,
       py.item,
       py.cod_neg,
       py.documento,
       cmc.cuenta
 FROM administrativo.proyeccion_refinanciacion_negocios py 
 INNER JOIN con.factura f ON f.documento= py.documento AND f.negasoc=py.cod_neg
 INNER JOIN con.cmc_doc cmc ON cmc.cmc=f.cmc AND f.tipo_documento=cmc.tipodoc
 WHERE py.cod_neg=_negocio AND py.color='S' AND  f.valor_saldo >0 
	