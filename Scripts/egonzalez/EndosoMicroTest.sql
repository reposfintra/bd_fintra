
-- Function: tem.eg_comprobante_endoso(character varying)

-- DROP FUNCTION tem.eg_comprobante_endoso(character varying, _lote_endoso integer);

CREATE OR REPLACE FUNCTION tem.eg_comprobante_endoso(usuario character VARYING, _lote_endoso integer,_reversion varchar )
  RETURNS text AS
$BODY$

DECLARE

retorno text:='FALSE';

RsFacturas record;
recordCuentas record;


numeroComprobante text;

CtaCmc varchar := '';
_tercero varchar := '';
_endosadoa varchar := '';
_cuentasEndoso varchar[]  := '{16252102,16252138,16252136,16252135}'; --1:=cartera, 2:=cat ,3:= cuota admon, 4:= interes,
_cmcEndoso varchar[] :='{CL,CA, AL}'; --1: cmc Fiducia, 2: cmc Fintra atl , 3 :: CORD Y SUC
_cmcReverso varchar:='';
_record_diferido record ;
_grupo_transaccion numeric:=0;
_transaccion numeric:=0;
_cta_aux_dif varchar :='';
_endosado VARCHAR :='S';

BEGIN

	--CREAMOS LA CABECERA DEL COPROBANTE DIARIO.
	numeroComprobante := con.serie_comprobante_cen();
	_grupo_transaccion = 0;
	SELECT INTO _grupo_transaccion nextval('con.comprobante_grupo_transaccion_seq');
    
    raise notice 'numeroComprobante: %', numeroComprobante;
 
	INSERT INTO con.comprobante(
		    reg_status, dstrct, tipodoc, numdoc, grupo_transaccion, sucursal,
		    periodo, fechadoc, detalle, total_debito, total_credito,
		    total_items, moneda, fecha_aplicacion, aprobador, last_update,
		    user_update, creation_date, creation_user, base, usuario_aplicacion,
		    tipo_operacion, moneda_foranea, vlr_for, ref_1, ref_2)
	    VALUES ('', 'FINV','CDIAR', numeroComprobante, _grupo_transaccion, 'OP',
		    replace(substring(now(),1,7),'-',''),now()::date,'COMPROBANTE DIARIO DE ENDOSO MICROCREDITO', 0, 0,
		    0, 'PES', NOW()::DATE, usuario, '0099-01-01 00:00:00'::TIMESTAMP,
		    '', NOW(), usuario, 'COL', usuario,
		    'GRAL', '', 0.00, '', ''); --FALTA EL NIT LOS ITEMS Y VALOR DEBITO Y CREDITO

	--AGRUPAMOS LOS COMPROBANTES PENDIENTES POR REALIZAR POR LINEA NEGOCIO Y TIPO CARTERA(ESTE LOOP EN TEORIA ES DE UNA ITERACION)
	FOR RsFacturas IN
	
		SELECT ctr.*,cmc.cuenta AS cuenta_cartera, cmc.cmc,c.agencia
		FROM administrativo.control_endosofiducia ctr
		INNER JOIN con.factura fac ON fac.documento=ctr.documento AND fac.tipo_documento='FAC' AND fac.num_doc_fen=ctr.cuota AND fac.nit=ctr.nit_cliente
		INNER JOIN con.cmc_doc cmc ON fac.cmc=cmc.cmc AND cmc.tipodoc=fac.tipo_documento
		INNER JOIN negocios n ON n.cod_neg=fac.negasoc 
		INNER JOIN convenios c ON c.id_convenio=n.id_convenio
		WHERE ctr.reg_status='' 
			AND estado_proceso=''
			AND num_comprobante=''
			AND fecha_cdiar='0099-01-01 00:00:00'::timestamp
			AND lote_endoso= _lote_endoso 
		ORDER BY negocio, documento, cuota
		
	LOOP

			--BUSCAMOS LAS CUENTAS DEL DEBITO DEL COMPROBANTE (FENALCO ATLANTICO O BOLIVAR)
			_tercero = RsFacturas.nit_cliente;
			_endosadoa = RsFacturas.endosar_en;
		 
			/*-- CUANDO ES DESENDOSO
			if ( RsFacturas.endosar_en = '' ) THEN
			
				SELECT INTO recordCuentas * from administrativo.proceso_endoso where reg_status='' and custodiada_por = _endosadoa; --RsFacturas.endosar_en;
				_endosadoa = '8020220161';
				reversion = 'S';
			
				--FENALCO ATL
				if (RsFacturas.id_unidad_negocio in (2,3,4,30,31)) then
					_cuentasFenalco = '13050902';
					_cmcFenalco = 'FA';
				end if;
			
				--FENALCO BOL
				if (RsFacturas.id_unidad_negocio in (8,10)) then
					_cuentasFenalco = '13050521';
					_cmcFenalco = 'NB';
				end if;		
			 
			else 
		
				_endosadoa = RsFacturas.endosar_en;
				raise notice 'Entra aqui: %', 'Entro';
			    SELECT INTO recordCuentas * FROM  administrativo.proceso_endoso WHERE  reg_status='' and custodiada_por = _endosadoa; --RsFacturas.endosar_en;
			
			end if;*/
	
	
		   --CREAMOS EL DEBITO DEL COMPROBANTE DIARIO.
		if (_reversion = 'S') THEN
		
			_endosado:='N';
		
			IF RsFacturas.agencia= 'ATL' THEN 
			   	_cmcReverso:= _cmcEndoso[2];
			ELSE 
			   _cmcReverso:= _cmcEndoso[3];
			END IF; 
			SELECT INTO CtaCmc cuenta FROM con.cmc_doc WHERE cmc=_cmcReverso AND tipodoc='FAC';	
			
			_transaccion = 0;
			SELECT INTO _transaccion nextval('con.comprodet_transaccion_seq');	
			INSERT INTO con.comprodet(
					    reg_status, dstrct, tipodoc, numdoc, grupo_transaccion, transaccion,
					    periodo, cuenta, auxiliar, detalle, valor_debito, valor_credito,
					    tercero, documento_interno, last_update, user_update, creation_date,
					    creation_user, base, tipodoc_rel, documento_rel, abc, vlr_for,
					    tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,
					    tipo_referencia_3, referencia_3)
				    VALUES ('', 'FINV','CDIAR', numeroComprobante, _grupo_transaccion, _transaccion,
					    replace(substring(now(),1,7),'-',''),CtaCmc, _tercero, 'CONTABILIZACION DEBITO CDIAR ENDOSO-REVERSION', RsFacturas.valor_saldo_trasladado, 0.00,
					    RsFacturas.nit_cliente, 'CDIAR', '0099-01-01 00:00:00'::TIMESTAMP, Usuario, now(),
					    Usuario, 'COL', 'FAC', RsFacturas.documento, '', 0.00,
					    'NEG', RsFacturas.negocio, '', '',
					    'TPE', RsFacturas.unidad_negocio);
		else
	
			_transaccion = 0;
			SELECT INTO _transaccion nextval('con.comprodet_transaccion_seq');
	
			INSERT INTO con.comprodet(
					    reg_status, dstrct, tipodoc, numdoc, grupo_transaccion, transaccion,
					    periodo, cuenta, auxiliar, detalle, valor_debito, valor_credito,
					    tercero, documento_interno, last_update, user_update, creation_date,
					    creation_user, base, tipodoc_rel, documento_rel, abc, vlr_for,
					    tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,
					    tipo_referencia_3, referencia_3)
				    VALUES ('', 'FINV','CDIAR', numeroComprobante, _grupo_transaccion, _transaccion,
					    replace(substring(now(),1,7),'-',''), _cuentasEndoso[1], _tercero, 'NACIMIENTO CARTERA - ENDOSO', RsFacturas.valor_saldo_trasladado, 0.00,
					    RsFacturas.nit_cliente, 'CDIAR', '0099-01-01 00:00:00'::TIMESTAMP, Usuario, now(),
					    Usuario, 'COL', 'FAC', RsFacturas.documento, '', 0.00,
					    'NEG', RsFacturas.negocio, '', '',
					    'TPE', RsFacturas.unidad_negocio);
	
		end if;
	
		--CREAMOS EL CREDITO DEL COMPROBANTE...
		_transaccion = 0;
		SELECT INTO _transaccion nextval('con.comprodet_transaccion_seq');
		INSERT INTO con.comprodet(
			    reg_status, dstrct, tipodoc, numdoc, grupo_transaccion, transaccion,
			    periodo, cuenta, auxiliar, detalle, valor_debito, valor_credito,
			    tercero, documento_interno, last_update, user_update, creation_date,
			    creation_user, base, tipodoc_rel, documento_rel, abc, vlr_for,
			    tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,
			    tipo_referencia_3, referencia_3)
		    VALUES ('', 'FINV','CDIAR', numeroComprobante, _grupo_transaccion, _transaccion,
			    replace(substring(now(),1,7),'-',''), RsFacturas.cuenta_cartera, _tercero, 'CANCELA CARTERA - ENDOSO '||RsFacturas.nombre_cliente,0.00,RsFacturas.valor_saldo_trasladado,
			    RsFacturas.nit_cliente, 'CDIAR', '0099-01-01 00:00:00'::TIMESTAMP, Usuario, now(),
			    Usuario, 'COL', 'FAC', RsFacturas.documento, '', 0.00,
			    'NEG', RsFacturas.negocio, '', '',
			    'TPE', RsFacturas.unidad_negocio);
	
			
		--bloque para los diferidos
		  FOR _record_diferido IN  select   a.dstrct,
									        a.tipodoc as tipo_doc,
									        a.nit as tercero,
									        a.cod as numdoc,
									        a.valor as valor,
									        a.base,
									        a.cmc as hc,
									        a.fecha_doc,
									        substr( a.fecha_doc,1,4 ) || substr( a.fecha_doc,6,2 ) as periodo,
									        cmc.sigla_comprobante,
									        cmc.cuenta,
									        cmc.dbcr,
									        ctas.nombre_largo as descrip,
									        ng.cod_neg AS negocio,
									        'NEG' AS tipodocRel,
									        coalesce(cfc.aplica_iva,'N') as aplica_iva,
									        ng.id_convenio,
									        ''::VARCHAR AS cuenta_fiducia_dif
									    from
									        ing_fenalco a
									    JOIN  negocios ng on (ng.cod_neg = a.codneg)
									    LEFT JOIN con.tipo_docto tdoc  on (tdoc.dstrct = a.dstrct  and tdoc.codigo_interno = a.tipodoc)
									    LEFT JOIN con.cmc_doc cmc on (cmc.dstrct = tdoc.dstrct  and cmc.tipodoc = tdoc.codigo and cmc.cmc = a.cmc)
									    LEFT JOIN con.cuentas ctas  on (cmc.cuenta=ctas.cuenta)
									    LEFT JOIN negocios n  on (n.cod_neg = a.codneg)
									    LEFT join convenios c on (c.id_convenio = n.id_convenio)
									    LEFT join convenios_cargos_fijos cfc on (c.id_convenio=cfc.id_convenio and cfc.activo = true and cfc.tipodoc=a.tipodoc AND cfc.reg_status='')
									    where  a.dstrct = 'FINV'
								        and a.reg_status = ''
								        AND A.tipodoc IN ('MI','CM')
								       -- and a.periodo !=''
								        AND a.codneg = RsFacturas.negocio
								        AND cuota= RsFacturas.cuota 
								        order by a.tipodoc,a.fecha_doc,cuenta  
		  LOOP
		  
			  -- ESTO MIENTRAS SE DEFINEN LAS TABLAS   
			  IF _record_diferido.tipo_doc ='MI' THEN 
				   _record_diferido.cuenta_fiducia_dif:= _cuentasEndoso[4];
			  ELSIF _record_diferido.tipo_doc ='CA' THEN 
				  _record_diferido.cuenta_fiducia_dif:= _cuentasEndoso[2];
			  ELSIF _record_diferido.tipo_doc ='CM' THEN 
				  _record_diferido.cuenta_fiducia_dif:= _cuentasEndoso[3];
			  END IF;
		  		
		  	  IF (_reversion = 'S') THEN
		  	  	  _cta_aux_dif := _record_diferido.cuenta;
		  	 	  _record_diferido.cuenta:= _record_diferido.cuenta_fiducia_dif;
		  	 	  _record_diferido.cuenta_fiducia_dif :=_cta_aux_dif;	
		  	 	  _endosado:='N';
		      END IF;
		      
			  --DEBITO
			  INSERT INTO con.comprodet(
				    reg_status, dstrct, tipodoc, numdoc, grupo_transaccion, transaccion,
				    periodo, cuenta, auxiliar, detalle, valor_debito, valor_credito,
				    tercero, documento_interno, last_update, user_update, creation_date,
				    creation_user, base, tipodoc_rel, documento_rel, abc, vlr_for,
				    tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,
				    tipo_referencia_3, referencia_3)
			    VALUES ('', 'FINV','CDIAR', numeroComprobante, _grupo_transaccion, nextval('con.comprodet_transaccion_seq'),
				    replace(substring(now(),1,7),'-',''), _record_diferido.cuenta, _tercero, 'DIFERIDO '||_record_diferido.descrip||' - ENDOSO '||RsFacturas.nombre_cliente,_record_diferido.valor,0.00,
				    RsFacturas.nit_cliente, 'CDIAR', '0099-01-01 00:00:00'::TIMESTAMP, Usuario, now(),
				    Usuario, 'COL', 'FAC', _record_diferido.numdoc, '', 0.00,
				    'NEG', RsFacturas.negocio, '', '',
				    'TPE', RsFacturas.unidad_negocio);				   
			
				
				--1:=cartera, 2:=cat ,3:= cuota admon, 4:= interes,
				
				--CREDITO
				INSERT INTO con.comprodet(
					    reg_status, dstrct, tipodoc, numdoc, grupo_transaccion, transaccion,
					    periodo, cuenta, auxiliar, detalle, valor_debito, valor_credito,
					    tercero, documento_interno, last_update, user_update, creation_date,
					    creation_user, base, tipodoc_rel, documento_rel, abc, vlr_for,
					    tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,
					    tipo_referencia_3, referencia_3)
				    VALUES ('', 'FINV','CDIAR', numeroComprobante, _grupo_transaccion, nextval('con.comprodet_transaccion_seq'),
					    replace(substring(now(),1,7),'-',''),  _record_diferido.cuenta_fiducia_dif, _tercero, 'DIFERIDO FIDUCIA - ENDOSO '||RsFacturas.nombre_cliente,0.00,_record_diferido.valor,
					    RsFacturas.nit_cliente, 'CDIAR', '0099-01-01 00:00:00'::TIMESTAMP, Usuario, now(),
					    Usuario, 'COL', 'FAC', _record_diferido.numdoc, '', 0.00,
					    'NEG', RsFacturas.negocio, '', '',
					    'TPE', RsFacturas.unidad_negocio );
					   
			     --MARCAMOS EL DIFERIDO COMO ENDOSADO
			     UPDATE ing_fenalco SET endosado=_endosado WHERE codneg= RsFacturas.negocio AND cuota= RsFacturas.cuota AND cod=_record_diferido.numdoc AND reg_status='';
			    
		  END LOOP; 
	
		if (_reversion = 'S') then
			UPDATE con.factura set corficolombiana='N', cmc = _cmcEndoso[2] WHERE documento=RsFacturas.documento and tipo_documento IN ('FAC','ND');
		else
			UPDATE con.factura set corficolombiana='S', cmc = _cmcEndoso[1] WHERE documento=RsFacturas.documento and tipo_documento IN ('FAC','ND');
		end if;
	
	END LOOP;

	--ACTUALIZAMOS LA CABECERA DEL COMPROBANTE....
	UPDATE con.comprobante SET
		tercero = _endosadoa,
		total_debito = (SELECT sum(valor_debito) FROM con.comprodet WHERE numdoc=numeroComprobante AND reg_status='' ),
		total_credito = (SELECT sum(valor_credito) FROM con.comprodet WHERE numdoc=numeroComprobante AND reg_status='' ),
		total_items = (SELECT count(0) FROM con.comprodet WHERE numdoc=numeroComprobante AND reg_status='' )
	WHERE numdoc=numeroComprobante AND reg_status='' ;

	--VALIDAMOS QUE EL COMPROBANTE ESTE CUADRADO...
	IF ( NOT exists( SELECT * FROM con.comprobante c where c.numdoc = numeroComprobante and tipodoc = 'CDIAR' and total_debito =(SELECT sum(valor_debito) FROM con.comprodet WHERE numdoc=c.numdoc AND reg_status='') AND total_credito = (SELECT sum(valor_credito) FROM con.comprodet WHERE numdoc=c.numdoc AND reg_status='') ) ) THEN

		DELETE FROM con.comprobante  where numdoc= numeroComprobante and tipodoc = 'CDIAR';
		DELETE FROM con.comprodet  where numdoc= numeroComprobante and tipodoc = 'CDIAR';
		retorno:='LO SENTIMOS EL COMPROBANTE '||numeroComprobante||'  FUE ELIMINADO POR VALORES DEBITO Y CREDITO ERRADOS.';

	ELSE

		--MARCAMOS LAS FACTURAS COMO PROCESADAS
		UPDATE administrativo.control_endosofiducia
			SET estado_proceso='P', num_comprobante=numeroComprobante, fecha_cdiar=now(), user_update=usuario, last_update=now()
		WHERE estado_proceso=''
			AND fecha_cdiar='0099-01-01 00:00:00'::timestamp
			AND reg_status=''
			AND linea_negocio=RsFacturas.linea_negocio
			AND endosar_en=RsFacturas.endosar_en
		    AND lote_endoso=_lote_endoso;

		retorno:='TRUE';

	END IF;

	RETURN retorno;

EXCEPTION

	WHEN foreign_key_violation THEN RAISE EXCEPTION 'NO EXISTEN DEPENDENCIAS FORANEAS PARA ALGUNOS REGISTROS.';
	WHEN  null_value_not_allowed THEN RAISE EXCEPTION 'VALOR NULO NO PERMITIDO';

END;

$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION tem.eg_comprobante_endoso(character VARYING, _lote_endoso integer,_reversion varchar )
  OWNER TO postgres; 
  
  



--DROP FUNCTION  tem.eg_guardar_facturas_endoso(_usuario CHARACTER VARYING,_endosar_en VARCHAR,_cartera_en VARCHAR,_unidad_negocio INTEGER,_reverso VARCHAR)

CREATE OR REPLACE FUNCTION tem.eg_guardar_facturas_endoso(_usuario CHARACTER VARYING,_endosar_en VARCHAR,_cartera_en VARCHAR,_unidad_negocio INTEGER, _reverso VARCHAR)
  RETURNS text AS
$BODY$
DECLARE

_facturasEndoso RECORD;
_lote_endoso integer:=0;
_recordUnidadNegocio record;

BEGIN
	_lote_endoso:=get_lote_endoso('LOTE_ENDOSO'); 
    SELECT INTO _recordUnidadNegocio * FROM unidad_negocio WHERE id=_unidad_negocio;
   
   		FOR _facturasEndoso IN 							
									SELECT
										''::varchar as cartera_en,
										sp_uneg_negocio(negasoc)::varchar as id_uneg_negocio,
										sp_uneg_negocio_name(negasoc)::varchar as uneg_negocio,
										nit::varchar as nit_cliente,
										get_nombc(f.nit)::varchar as nombre_cliente,
										f.codcli::varchar,
										negasoc::varchar as negocio,
										eg_tipo_negocio(negasoc)::varchar as tipo_negocio,
										f.documento::varchar,
										f.num_doc_fen::varchar as cuota,
										f.fecha_vencimiento::date,
										(now()::date-f.fecha_vencimiento::date)::numeric as dias_vencidos,
										f.valor_factura::numeric,
										f.valor_abono::numeric,
										f.valor_saldo::numeric
									FROM con.factura f
									     inner join negocios as neg on (neg.cod_neg=f.negasoc and neg.estado_neg IN ('T','A','D'))
									     inner join rel_unidadnegocio_convenios as run on (run.id_convenio=neg.id_convenio)
									     inner join unidad_negocio as un on (run.id_unid_negocio=un.id and ref_4 = 'MIROCREDITO')
									     inner join con.cmc_doc AS cmc on (cmc.cmc=f.cmc and cmc.tipodoc=f.tipo_documento)
									     left join administrativo.control_endosofiducia as ctrl on (ctrl.documento = f.documento and ctrl.negocio = f.negasoc and ctrl.endosar_en != '') 
									WHERE f.reg_status=''
										AND f.dstrct ='FINV'
										AND substring(f.documento,1,2)  in ('MC')
										AND f.valor_saldo > 0
										AND CASE WHEN _reverso='S' THEN TRUE ELSE  ctrl.documento is NULL  END 
										and negasoc IN (SELECT codneg FROM tem.endoso_micro_20190524) 
										--and negasoc not in (select codneg from ing_fenalco where periodo != '' and reg_status = '' group by codneg)
		   LOOP
		   
		   			RAISE NOTICE '_facturasEndoso : %',_facturasEndoso;
		   		
			   		 INSERT INTO administrativo.control_endosofiducia(
			                        reg_status, dstrct, lote_endoso, endosar_en, anteriormente_en, fecha_corte, 
			                        fecha_endoso, linea_negocio, id_unidad_negocio, unidad_negocio, 
			                        nit_cliente, nombre_cliente, codcli, negocio, tipo_negocio, documento, 
			                        cuota, fecha_vencimiento, dias_vencidos, valor_factura_trasladado, 
			                        valor_abono_trasladado, valor_saldo_trasladado, estado_proceso, creation_date, creation_user)
			                VALUES ('', 'FINV', _lote_endoso, _endosar_en, _cartera_en, now()::date,
			                        now(), _recordUnidadNegocio.ref_4 , _recordUnidadNegocio.id, _recordUnidadNegocio.descripcion, 
			                        _facturasEndoso.nit_cliente, _facturasEndoso.nombre_cliente, _facturasEndoso.codcli, _facturasEndoso.negocio, _facturasEndoso.tipo_negocio, _facturasEndoso.documento, 
			                        _facturasEndoso.cuota, _facturasEndoso.fecha_vencimiento, _facturasEndoso.dias_vencidos, _facturasEndoso.valor_factura, 
			                        _facturasEndoso.valor_abono, _facturasEndoso.valor_saldo, '', now(), _usuario);
		   
		   END LOOP;
		   

   RETURN _lote_endoso;
	
END
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION tem.eg_guardar_facturas_endoso(_usuario CHARACTER VARYING,_endosar_en VARCHAR,_cartera_en VARCHAR,_unidad_negocio INTEGER, _reverso VARCHAR)
  OWNER TO postgres;


--VALIDACIONES.
SELECT * FROM administrativo.control_endosofiducia WHERE creation_user='JTORRES' AND creation_date::DATE = NOW()::DATE 

--GENERAR ENDOSO
SELECT tem.eg_guardar_facturas_endoso('JTORRES'::VARCHAR,'8001444676','8020220161',1,'N'::VARCHAR); --RETORNA UN LOTE 
SELECT tem.eg_comprobante_endoso('JTORRES', 232::integer,'N'::varchar );-- EL ULTIMO PARAMETRO N: endoso S: desemdoso

SELECT tem.eg_guardar_facturas_endoso('JTORRES'::VARCHAR,'8020220161','8001444676',1,'S'::VARCHAR); --RETORNA UN LOTE
185

SELECT negocio,cuota,valor_saldo_trasladado,num_comprobante,* FROM administrativo.control_endosofiducia WHERE lote_endoso=185 ORDER BY negocio, cuota::INTEGER;
SELECT tem.eg_comprobante_endoso('JTORRES', 185::integer,'S'::varchar );



