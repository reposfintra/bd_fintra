select 
    periodo,
	num_ingreso,
	fecha_consignacion::date, 
	sum(round(vlr_ingreso)), 
	COUNT(0) 
from con.ingreso 
where periodo !='' 
and periodo::int between 201809 and 201809 
--and fecha_consignacion between '2017-01-01'::date and '2018-08-31'
and bank_account_no='SUPEREFECTIVO' 
and reg_status='' 
group by periodo,fecha_consignacion ,num_ingreso
order by fecha_consignacion
--order by num_ingreso
--order by periodo::int

300.450.527
300.450.527

300,450,473


select * from con.ingreso_detalle where num_ingreso='IA533971';
select * from con.ingreso where num_ingreso='IA533971';


--R0033976


select 
	periodo,
	documento,
	descripcion,
	valor_factura, 
	creation_date,
	cmc 
from 
con.factura a
where 
cmc='SE' 
and periodo between '201809' and '201809' 
and reg_status='' 
and descripcion ilike '%SUPEREFECTIVO%'  
order by documento 


select * from 

select nextval('con.interfaz_secuencia_oper_apoteosys');

select negocio_reestructuracion,* 
from rel_negocios_reestructuracion re 
inner join negocios neg  on re.negocio_reestructuracion=neg.cod_neg
where 
negocio_reestructuracion like 'MC%' 
--and negocio_reestructuracion 
and re.creation_date::DATE >'2017-01-01'::DATE
and neg.estado_neg ='T';

select * from CON.factura where negasoc ='MC13056'

select * from (
	(
	select 
	    'OPER'::VARCHAR as tipo_concepto,
	    'MCRE'::VARCHAR as concepto, 
	   -- nextval('con.interfaz_secuencia_oper_apoteosys') as numero_documento,
		fac.fecha_contabilizacion::date as fecha_del_comprobante,
		'13050501'::VARCHAR as cuenta16,
		''::varchar as cuenta26,
		'A1111F14201'::varchar as centro_costo,	
	    fac.nit as tercero,
	    'CXCO'::VARCHAR as documento_soporte, 
		fac.documento as numero_documento_soporte,
		'1'::varchar as numero_vencimiento,
		''::varchar as numero_de_cuotas,
		fac.fecha_contabilizacion::date as fecha_emision,
	    fac.fecha_vencimiento::VARCHAR,
	    ''::VARCHAR as fecha_de_descuento,
	    ''::varchar as valor_descuento,
	    ''::varchar as porcentaje_descuento,
	    fac.negasoc as referencia,
	    ''::varchar as valor_iva,
	    ''::varchar as valor_retencion,
	    ''::varchar as valor_base_iva_rete,
	    ''::varchar as fecha_tasa_cambio,
	    0::integer as valor_debito_moneda_origen,
	    0::integer as valor_credito_moneda_origen,
	    fac.valor_factura as debito_moneda_local,
		0.00::integer as credito_moneda_local,
		'CARTERA MICROCREDITO - NEGOCIO REESTRUCTURADO'::VARCHAR as observacion,
		'FINT'::VARCHAR as contabilidad
	from con.factura fac
	inner join con.cmc_doc cmc on (cmc.cmc=fac.cmc and fac.tipo_documento=cmc.tipodoc)
	inner join negocios neg on (neg.cod_neg=fac.negasoc) 
	where negasoc in  ('MC10899') and
	neg.estado_neg='T' and
	substring(fac.documento,1,2) ='MC' and 
	fac.reg_status=''
	order by fac.fecha_vencimiento
	)
	union all 
	(
	select 
		'OPER'::VARCHAR as tipo_concepto,
	    'MCRE'::VARCHAR as concepto, 
	  --  nextval('con.interfaz_secuencia_oper_apoteosys') as numero_documento,
		fac.fecha_contabilizacion::date as fecha_del_comprobante,
		case when fdet.descripcion='SEGURO' then '28150502'
			 when fdet.descripcion='INTERES' then '27050501'
			 when fdet.descripcion='CAT' then '27051001'
		end::VARCHAR AS	cuenta16,
		''::varchar as cuenta26,
		'A1111F14201'::varchar as centro_costo,	
	    fac.nit as tercero,
	    'CXPA'::VARCHAR as documento_soporte, 
		fac.documento as numero_documento_soporte,
		'1'::varchar as numero_vencimiento,
		''::varchar as numero_de_cuotas,
		fac.fecha_contabilizacion::date as fecha_emision,
	    fac.fecha_vencimiento::VARCHAR,
	    ''::VARCHAR as fecha_de_descuento,
	    ''::varchar as valor_descuento,
	    ''::varchar as porcentaje_descuento,
	    fac.negasoc as referencia,
	    ''::varchar as valor_iva,
	    ''::varchar as valor_retencion,
	    ''::varchar as valor_base_iva_rete,
	    ''::varchar as fecha_tasa_cambio,
	    0::integer as valor_debito_moneda_origen,
	    0::integer as valor_credito_moneda_origen,
	    0::integer as debito_moneda_local,
		fdet.valor_unitario::numeric as credito_moneda_local,
		fdet.descripcion::VARCHAR as observacion,
		'FINT'::VARCHAR as contabilidad
	from con.factura fac
	inner join con.factura_detalle fdet on (fdet.documento=fac.documento and fac.tipo_documento=fdet.tipo_documento and fac.nit=fdet.nit)
	inner join con.cmc_doc cmc on (cmc.cmc=fac.cmc and fac.tipo_documento=cmc.tipodoc)
	inner join negocios neg on (neg.cod_neg=fac.negasoc) 
	where negasoc in   ('MC10899') and
	neg.estado_neg='T' and
	substring(fac.documento,1,2) ='MC' and 
	fdet.descripcion in ('SEGURO','INTERES','CAT') and 
	fac.reg_status=''
	order BY fac.fecha_vencimiento
	)
	union all 
	(
	select 	
		'OPER'::VARCHAR as tipo_concepto,
	    'MCRE'::VARCHAR as concepto, 
	   -- nextval('con.interfaz_secuencia_oper_apoteosys') as numero_documento,
		fac.fecha_contabilizacion::date as fecha_del_comprobante,
		'11050612'::VARCHAR as cuenta16,
		''::varchar as cuenta26,
		'A1111F14201'::varchar as centro_costo,	
	    fac.nit as tercero,
	    'EGRE'::VARCHAR as documento_soporte, 
		fac.negasoc as numero_documento_soporte,
		''::varchar as numero_vencimiento,
		''::varchar as numero_de_cuotas,
		fac.fecha_contabilizacion::date as fecha_emision,
	    ''::VARCHAR AS fecha_vencimiento,
	    ''::VARCHAR as fecha_de_descuento,
	    ''::varchar as valor_descuento,
	    ''::varchar as porcentaje_descuento,
	    fac.negasoc as referencia,
	    ''::varchar as valor_iva,
	    ''::varchar as valor_retencion,
	    ''::varchar as valor_base_iva_rete,
	    ''::varchar as fecha_tasa_cambio,
	    0::integer as valor_debito_moneda_origen,
	    0::integer as valor_credito_moneda_origen,
	    0::integer as debito_moneda_local,
		SUM(fdet.valor_unitario)::numeric as credito_moneda_local,
		'CARTERA TEMPORAL MICRO - NEGOCIO REESTRUCTURADO'::VARCHAR as observacion,
		'FINT'::VARCHAR as contabilidad
	from con.factura fac
	inner join con.factura_detalle fdet on (fdet.documento=fac.documento and fac.tipo_documento=fdet.tipo_documento and fac.nit=fdet.nit)
	inner join con.cmc_doc cmc on (cmc.cmc=fac.cmc and fac.tipo_documento=cmc.tipodoc)
	inner join negocios neg on (neg.cod_neg=fac.negasoc) 
	where negasoc in   ('MC10899')and
	neg.estado_neg='T' and
	substring(fac.documento,1,2) ='MC' and 
	fdet.descripcion ='CAPITAL' and 
	fac.reg_status=''
	group by 
	fac.nit,
	fac.negasoc,
	fac.periodo,
	fac.cmc,
	fdet.codigo_cuenta_contable,
	fdet.descripcion,
	fac.tipo_documento,
	fac.fecha_contabilizacion::date
	)
)plantilla order by referencia,documento_soporte,OBSERVACION,fecha_vencimiento
	

	select negasoc,substring(documento,1,2) as prefijo,
		   count(*)
		   --(select count(0) from con.factura_detalle where documento=f.documento) 
	from con.factura f
	where negasoc IN (select negocio_reestructuracion
						from rel_negocios_reestructuracion re 
						inner join negocios neg  on re.negocio_reestructuracion=neg.cod_neg
						where 
					--	negocio_reestructuracion like 'MC%' 
						negocio_reestructuracion in ('MC09921','MC10899','MC14988')
						and re.creation_date::DATE >'2017-01-01'::DATE
						and neg.estado_neg ='T'
						)
	--and documento like 'MC%'
    group by negasoc,substring(documento,1,2)--,f.documento
	order by negasoc
	
	
	
select nitcli,* from con.ingreso where num_ingreso='IC269438';
select * from con.ingreso_detalle where num_ingreso='IC269438';
select * from con.factura where num_ingreso_ultimo_ingreso='IC269438';

select * from caja_ingreso;

select negasoc from con.factura where nit='32675257';

	
	select valor_saldo,fecha_ultimo_pago,* from con.factura where negasoc='MC10899' order by documento; 
	
	select * from ing_fenalco where codneg='MC10899'
	
	select * from con.factura where documento='MC0941801'; 170431.00
	
	select * from con.factura_detalle where documento='MC0941805';
	
	select * from con.factura_detalle where 
	descripcion in ('INTERES','CAT','CUOTA-ADMINISTRCION')
	and documento in (select documento from con.factura where negasoc='MC10350' and documento like 'MC%' order by documento);
	

	select periodo,* from con.ingreso_detalle where factura in ('CA64346','MC0601004','MC0601005','MI78597');
		
	select * from con.factura where documento='R0036376';
	52,209,708
