--drop function tem.refinanciacion_creditos(usuario character VARYING, _id_unidad_neg integer,_negocio CHARACTER VARYING, _cuotas INTEGER,_fecha_sys date , _fecha_pago date, _ciudad varchar  )

CREATE OR REPLACE FUNCTION tem.refinanciacion_creditos(usuario character VARYING, _id_unidad_neg integer,_negocio CHARACTER VARYING, _cuotas INTEGER,_fecha_sys date , _fecha_pago date, _ciudad varchar  )
  RETURNS varchar AS
$BODY$
DECLARE

 _retorno VARCHAR:='FALSE';
 _saldo_refinaciacion NUMERIC:=0.00;
 _liquidacionNegocio record;
 _rs rs_detalle_saldo_facturas;

BEGIN


	  
	  --VALIDAMOS QUE ESTE AL DIA EL NEGOCIO
--	  IF eg_altura_mora_periodo(_negocio::character varying, 0::integer, 8::integer, 0::integer) !='1- CORRIENTE' THEN 	  		
--	       RETURN 'EL NEGOCIO NO SE ENCUENTRA AL DIA';	  
--	  END IF;
	   
	  FOR _liquidacionNegocio IN 
	   							SELECT 
	   								item,
	   								capital,
	   								interes,
	   								cat,
	   								seguro,
	   								causar_cuota_admin 
	   							FROM  documentos_neg_aceptado 
	   							WHERE cod_neg=_negocio AND reg_status='' 	
	   LOOP
	    
	      IF _id_unidad_neg=1  THEN
	    		SELECT INTO _rs * FROM eg_detalle_saldo_facturas_mc(_negocio,_liquidacionNegocio.item::integer);
	      END IF; 
	      
	      _saldo_refinaciacion:= _saldo_refinaciacion+_rs.saldo_capital;	    
	   	-- RAISE NOTICE '_liquidacionNegocio : %',_liquidacionNegocio;	   	   
	   END LOOP;	   
	   
	  -- eg_detalle_saldo_facturas_mc(codneg text, cuota integer)
	  -- eg_detalle_saldo_facturas_fe_fc(codneg text, cuota integer)
	   
	   IF _saldo_refinaciacion > 0 THEN 
	   
	    RAISE NOTICE '_saldo_refinaciacion : %',_saldo_refinaciacion;
	    
	    
	    /* *************************************
		 * Insert documentos_neg_aceptado  *****
		 **************************************/
		INSERT INTO documentos_neg_aceptado(
			      cod_neg, item, fecha, dias, saldo_inicial, capital, interes,CAT,valor, saldo_final, creation_date,seguro,custodia, remesa,cuota_manejo,valor_aval)
			(
			 SELECT  --	
			    _negocio AS negocio,
				item AS cuota ,
				fecha,
				dias,
				saldo_inicial,
				capital,
				round(interes) AS intereses,
				round(cat) AS cat,
				round(valor) AS valor_cuota,
				saldo_final,
				now(),
				seguro,
				COALESCE(custodia,0.00) AS custodia,
				COALESCE(remesa,0.00) AS remesa,
				cuota_manejo,
				COALESCE(valor_aval,0.00) AS valor_aval
			FROM  apicredit.eg_simulador_liquidacion_micro_fecha(_saldo_refinaciacion::numeric, _cuotas::integer, 
                                                             		   _fecha_pago::date, 'CTFCPV'::varchar,_ciudad::varchar, 
                                                              		   _fecha_sys::date,'N'::VARCHAR) 
			);
	   		
	   	
                                                                 
       INSERT INTO documentos_neg_aceptado(
			      cod_neg, item, fecha, dias, saldo_inicial, capital, interes,CAT,valor, saldo_final, creation_date,seguro,custodia, remesa,cuota_manejo,valor_aval)
			(                                                          
     	SELECT --	cod_negocio_nuevo,
             	'MC15923' AS NEGOCIO ,            
				(item::INTEGER+3) AS cuota ,
				fecha,
				dias,
				saldo_inicial,
				capital,
				round(interes) AS intereses,
				round(cat) AS cat,
				round(valor) AS valor_cuota,
				saldo_final,
				now(),
				seguro,
				COALESCE(custodia,0.00) AS custodia,
				COALESCE(remesa,0.00) AS remesa,
				cuota_manejo,
				COALESCE(valor_aval,0.00) AS valor_aval
            FROM  apicredit.eg_simulador_liquidacion_micro_fecha((8304145+1020164)-830168::numeric, 45::integer, 
                                                                 '2019-02-12'::date, 'CTFCPV'::varchar,'BQ'::varchar, 
                                                                 '2019-01-28'::date,'N'::VARCHAR) 
 			 )                                                                 
                 
 			
 		
--                                                                 
       
	   END IF ;
	   
	RETURN _retorno;
EXCEPTION

	WHEN foreign_key_violation THEN RAISE EXCEPTION 'NO EXISTEN DEPENDENCIAS FORANEAS PARA ALGUNOS REGISTROS.';
	WHEN  null_value_not_allowed THEN RAISE EXCEPTION 'VALOR NULO NO PERMITIDO';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION  tem.refinanciacion_creditos(usuario character VARYING, _id_unidad_neg integer,_negocio character VARYING,_cuotas INTEGER)
  OWNER TO postgres; 
  
  
SELECT   tem.refinanciacion_creditos('EDGARGM87'::VARCHAR,1::integer,'MC15923'::VARCHAR);

SELECT cod_neg,interes,cuota_manejo,cat,seguro FROM TEM.backup_refinaciacion WHERE cod_neg='MC15923'; 


SELECT *  FROM documentos_neg_aceptado WHERE cod_neg='MC15923' AND reg_status='A'; ORDER BY item::INTEGER;

SELECT * FROM ing_fenalco WHERE codneg='MC15923';

SELECT * FROM con.comprobante WHERE numdoc IN ('CA500577603','CM500905203','MI500577603');


SELECT 
       bank_account_no,
       branch_code,
       vlr,
       fecha_cheque,
       (SELECT documento_relacionado FROM fin.cxp_doc WHERE cheque=document_no) AS negocio
       ,*
FROM egreso WHERE document_no='EG66689'; --group  by  bank_account_no,branch_code;

SELECT * FROM egresodet WHERE document_no='EG66689';
SELECT * FROM con.com


	


--1.Generar a nueva liquidacion
-- Anular diferidos 
--2.Cancelar cartera anterior core y apoteosy
--3.Generar nueva cartera 

  SELECT estado FROM logproceso WHERE proceso = 'PyG' ORDER BY fecha_inicial DESC LIMIT 1
  
  SELECT * FROM apicredit.pre_solicitudes_creditos WHERE numero_solicitud=143457;
  
  
  SELECT * FROM solicitud_aval WHERE cod_neg='MC17957';
  
  SELECT * FROM administrativo.pasivo_vacacional WHERE cc_empleado='1052071969';
  
  SELECT * FROM administrativo.novedades WHERE cc_empleado='1052071969';
