-- Table: con.ciclos_facturacion

-- DROP TABLE tem.comprobantes_ajustes;

CREATE TABLE tem.comprobantes_ajustes
(
  ia_documento character varying(20) NOT NULL DEFAULT ''::character varying,
  documento character varying(20) NOT NULL DEFAULT ''::character varying,
  fecha_ingreso character varying(20) NOT NULL DEFAULT ''::character varying,
  fecha_vencimiento character varying(20) NOT NULL DEFAULT ''::character varying,
  nitcli character varying(20) NOT NULL DEFAULT ''::character varying,
  agencia character varying(10) NOT NULL DEFAULT ''::character varying,
  valor_deb integer,
  valor_cre integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem.comprobantes_ajustes
  OWNER TO postgres;

select * from con.ingreso_detalle where num_ingreso='IC237605'