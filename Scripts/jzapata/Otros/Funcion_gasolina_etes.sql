-- Function: con.interfaz_fintra_ut_apoteosys_gasolina()

-- DROP FUNCTION con.interfaz_fintra_ut_apoteosys_gasolina();

CREATE OR REPLACE FUNCTION con.interfaz_fintra_ut_apoteosys_gasolina()
  RETURNS text AS
$BODY$

DECLARE

 /************************************************
  *DESCRIPCION: ESTA FUNCION TOMA LAS PLANILLAS Y
  *OBTIENE LOS ANTICIPOS POR GASOLINA Y CONTRUYE EL ASIENTO
  *CONTABLE QUE MAS ADELANTE SE TRASLADARA A APOTEOSYS.
  *DOCUMENTACION:=
  *AUTOR:=@JZAPATA
  *FECHA CREACION:=2018-12-12
  *LAST_UPDATE
  *DESCRIPCION DE CAMBIOS Y FECHA
  ************************************************/

 REC_OS RECORD;
 REC_AGA RECORD;
 REC_PLA RECORD;
 REC_TER RECORD;
 SECUENCIA_TR INTEGER;
 CONSEC INTEGER:=1;
 FECHADOC_ TEXT:='';
 CORRIDA TEXT:='';
 SW TEXT:='N';
 _ARRCUENTASAGA VARCHAR[] :='{}';
 MCTYPE CON.TYPE_INSERT_MC;


 BEGIN

	--SE CONSULTA EL VECTOR DE CUENTAS Y SE ASIGNA
	_ARRCUENTASAGA:=ETES.GET_CUENTAS_PERFIL('AGA_GASOLINA');

	--SE ACTUALIZA LA CORRIDA EN LA TABLA CON.PLANILLAS_PROCESADAS_UT SI LA ENCUENTRA

	FOR REC_PLA IN

		select
			planilla
		from
			con.planillas_procesadas_ut
		where
			factura_cxc='' AND tipo_operacion='AGA'

	LOOP

		corrida:='';

		select
			INTO corrida a.cxc_corrida
		from
		(
		select
			cxc_corrida
		from
			etes.manifiesto_carga
		where
			planilla=REC_PLA.planilla
		union all
		select
			cxc_corrida
		from
			etes.manifiesto_reanticipos
		where
			planilla=REC_PLA.planilla
		)
		as a;

		raise notice 'Planilla: %',REC_PLA.planilla;
		raise notice 'Corrida: %',corrida;
		if(corrida!='') then
			update con.planillas_procesadas_ut set factura_cxc=corrida where planilla=REC_PLA.planilla;
		end if;

	END LOOP;

	--SE CONSULTAN LOS REGISTROS QUE SE VAN A TRANSPORTAR PARA GASOLINAS

	FOR REC_OS IN

		select PRODUCTO_SER.CODIGO_PROSERV,
					A.PLANILLA,
					A.CREATION_DATE,
					A.planilla AS FACTURA_CXP,
					A.CXC_CORRIDA AS FACTURA_CXC,
<<<<<<< HEAD
					egd.document_no aS EGRESO,
=======
>>>>>>> migracion
					'AGA' AS TIPO_OPERACION,
					tr.identificacion,
					tr.razon_social,
					a.fecha_contabilizacion
					from etes.manifiesto_carga a
		INNER join ETES.PRODUCTOS_SERVICIOS_TRANSP AS PRODUCTO_SER ON (a.ID_PROSERV=PRODUCTO_SER.ID)
<<<<<<< HEAD
		inner join egresodet egd on(egd.tipo_documento='FAP' and egd.documento=a.planilla)
=======
>>>>>>> migracion
		inner join etes.agencias ag on(ag.id=a.id_agencia)
		inner join etes.transportadoras tr on(tr.id=ag.id_transportadora)
		where A.REG_STATUS!='A' AND
					A.DSTRCT='FINV' AND
<<<<<<< HEAD
					A.TRANSACCION!=0 AND
					A.PERIODO_CONTABILIZACION!='' AND
					A.FECHA_CONTABILIZACION::DATE >= '2014-01-01' AND
					A.PERIODO_CONTABILIZACION ='201812' and --REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','') AND
					PRODUCTO_SER.CODIGO_PROSERV='ANT00001' /*and 
					(A.PLANILLA,a.planilla,egd.document_no, 'AGA') NOT IN(SELECT
																					PLANILLA,
																					factura_cxp,
																					egreso,
																					tipo_operacion
																				FROM
																				con.planillas_procesadas_ut
																				)*/
=======
					A.PERIODO = REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','') AND
					PRODUCTO_SER.CODIGO_PROSERV='ANT00001' and 
					(A.PLANILLA,a.planilla, 'AGA') NOT IN(SELECT
																					PLANILLA,
																					factura_cxp,
																					tipo_operacion
																				FROM
																				con.planillas_procesadas_ut
																				)
>>>>>>> migracion
					--and a.planilla='0236901'	
		union all			
		select
					PRODUCTO_SER.CODIGO_PROSERV,
					A.PLANILLA,
					A.CREATION_DATE,
					A.planilla AS FACTURA_CXP,
					A.CXC_CORRIDA AS FACTURA_CXC,
<<<<<<< HEAD
					egd.document_no AS EGRESO,
=======
>>>>>>> migracion
					'AGA' AS TIPO_OPERACION,
					tr.identificacion,
					tr.razon_social,
					a.fecha_contabilizacion
				from
					etes.manifiesto_reanticipos A
				INNER JOIN
					ETES.MANIFIESTO_CARGA AS B ON (A.ID_MANIFIESTO_CARGA=B.ID)
				INNER JOIN
					ETES.PRODUCTOS_SERVICIOS_TRANSP AS PRODUCTO_SER ON (B.ID_PROSERV=PRODUCTO_SER.ID)
				inner join 
<<<<<<< HEAD
					egresodet egd on(egd.tipo_documento='FAP' and egd.documento=a.planilla)
				inner join 
=======
>>>>>>> migracion
					etes.agencias ag on(ag.id=b.id_agencia)
				inner join 
					etes.transportadoras tr on(tr.id=ag.id_transportadora)
				where
					A.REG_STATUS!='A' AND
					A.DSTRCT='FINV' AND
<<<<<<< HEAD
					A.TRANSACCION!=0 AND
					A.PERIODO_CONTABILIZACION!='' AND
					A.FECHA_CONTABILIZACION::DATE >= '2014-01-01' AND
					A.PERIODO_CONTABILIZACION ='201812' and--REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','') AND
					PRODUCTO_SER.CODIGO_PROSERV='ANT00001' and 
					(A.PLANILLA,a.planilla,egd.document_no, 'AGA') NOT IN(SELECT
																				PLANILLA,
																				factura_cxp,
																				egreso,
=======
					replace(substring(A.fecha_reanticipo,1,7),'-','') = REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','') AND
					PRODUCTO_SER.CODIGO_PROSERV='ANT00001' and 
					(A.PLANILLA,a.planilla,'AGA') NOT IN(SELECT
																				PLANILLA,
																				factura_cxp,
>>>>>>> migracion
																				tipo_operacion
																			FROM
																			con.planillas_procesadas_ut
																			)
			--and planilla='9000243'
		ORDER BY 3, 2
/**
select con.interfaz_fintra_ut_apoteosys_gasolina();

select * from etes.manifiesto_carga where cxc_corrida='' and reg_status='';

select * from con.mc____ where procesado='N' and mc_____codigo____cpc____b is null;

<<<<<<< HEAD
delete from con.mc____ where MC_____CODIGO____TD_____B = 'OPER' and MC_____CODIGO____CD_____B = 'AET' and procesado='N' and mc_____numero____b=592312;
=======
SELECT * from con.mc____ where MC_____CODIGO____TD_____B = 'OPER' and MC_____CODIGO____CD_____B = 'AGA' and procesado='N';
>>>>>>> migracion

delete from con.planillas_procesadas_ut;

*/


	LOOP

		raise notice 'Planilla: %',REC_OS.planilla;

		SECUENCIA_TR:=0;
		CONSEC :=1;

		--SECUENCIA DE LA TRANSACCION
		SELECT INTO SECUENCIA_TR NEXTVAL('CON.INTERFAZ_SECUENCIA_OPER_APOTEOSYS');

		FOR REC_AGA IN

<<<<<<< HEAD
			SELECT
				 A.FECHADOC,
				 B.TIPODOC,
				 B.GRUPO_TRANSACCION,
				 B.PERIODO,
				 B.CUENTA,
				 CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE B.TERCERO END AS TERCERO,
				 --A.TERCERO,
				 B.VALOR_DEBITO,
				 B.VALOR_CREDITO,
				 B.DETALLE,
				 B.CREATION_DATE,
				 B.CREATION_USER,
				 B.LAST_UPDATE,
				 B.USER_UPDATE,
				 (CASE
=======
			select 
				a.fecha_creacion_anticipo as fechadoc,
				replace(substring(a.fecha_creacion_anticipo,1,7),'-','') as periodo,
				'FAC' as tipodoc, 
				_ARRCUENTASAGA[1] as cuenta, 
				CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE tr.identificacion END AS TERCERO,
				valor_planilla as valor_debito, 
				0 as valor_credito,
				D.NOMBRE as detalle,
				(CASE
				 WHEN D.TIPO_IDEN='CED' THEN 'CC'
				 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN D.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				 D.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 D.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 D.TELEFONO AS TERCER_TELEFONO1_B
			from 
				etes.manifiesto_carga a
			INNER join 
				ETES.PRODUCTOS_SERVICIOS_TRANSP AS PRODUCTO_SER ON (A.ID_PROSERV=PRODUCTO_SER.ID)
			inner join 
				etes.agencias ag on(ag.id=a.id_agencia)
			inner join 
				etes.transportadoras tr on(tr.id=ag.id_transportadora)
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=tr.identificacion)
			LEFT JOIN
				NIT D ON(D.CEDULA=C.NIT)
			LEFT JOIN
				CIUDAD E ON(E.CODCIU=D.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=tr.identificacion)
			where 
				a.planilla=rec_os.planilla
			union all
			select 
				a.fecha_creacion_anticipo as fechadoc,
				replace(substring(a.fecha_creacion_anticipo,1,7),'-','') as periodo,
				'AGA' as tipodoc, 
				_ARRCUENTASAGA[2] as cuenta, 
				CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE p.cod_proveedor END AS TERCERO,
				0 as valor_debito, 
				valor_descuentos_fintra as valor_credito,
				('AGA CREDITO '||a.planilla) as detalle,
				(CASE
				 WHEN D.TIPO_IDEN='CED' THEN 'CC'
				 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN D.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				 D.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 D.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 D.TELEFONO AS TERCER_TELEFONO1_B
			from 
				etes.manifiesto_carga a 
			INNER JOIN 
				etes.vehiculo AS v ON (a.id_vehiculo=v.id)	
			INNER JOIN 
				etes.propietario  AS p ON (v.id_propietario=p.id) 
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=p.cod_proveedor)
			LEFT JOIN
				NIT D ON(D.CEDULA=C.NIT)
			LEFT JOIN
				CIUDAD E ON(E.CODCIU=D.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=p.cod_proveedor)
			where 
				planilla=rec_os.planilla
			union all
			select 
				a.fecha_creacion_anticipo as fechadoc,
				replace(substring(a.fecha_creacion_anticipo,1,7),'-','') as periodo,
				'FAP' as tipodoc, 
				_ARRCUENTASAGA[3] as cuenta, 
				CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE p.cod_proveedor END AS TERCERO,
				0 as valor_debito, 
				valor_desembolsar as valor_credito,
				'AGA DETALLE' as detalle,
				(CASE
>>>>>>> migracion
				 WHEN D.TIPO_IDEN='CED' THEN 'CC'
				 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN D.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				 D.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 D.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 D.TELEFONO AS TERCER_TELEFONO1_B
<<<<<<< HEAD
			FROM
				CON.COMPROBANTE A
			INNER JOIN
				CON.COMPRODET B ON(B.DSTRCT=A.DSTRCT AND B.TIPODOC=A.TIPODOC AND B.NUMDOC=A.NUMDOC AND B.GRUPO_TRANSACCION=A.GRUPO_TRANSACCION)
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=B.TERCERO)
=======
			from 
			etes.manifiesto_carga a 
			INNER JOIN 
				etes.vehiculo AS v ON (a.id_vehiculo=v.id)
			INNER JOIN 
				etes.propietario  AS p ON (v.id_propietario=p.id) 
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=p.cod_proveedor)
>>>>>>> migracion
			LEFT JOIN
				NIT D ON(D.CEDULA=C.NIT)
			LEFT JOIN
				CIUDAD E ON(E.CODCIU=D.CODCIU)
			LEFT JOIN
<<<<<<< HEAD
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=B.TERCERO)
			WHERE
				A.DSTRCT='FINV' AND
				(A.TIPODOC, A.NUMDOC, B.CUENTA) =('AGA',REC_OS.planilla, _ARRCUENTASAGA[2]) OR
				(A.TIPODOC, A.NUMDOC, B.CUENTA) =('FAP',REC_OS.factura_cxp, _ARRCUENTASAGA[3]) OR
				(A.TIPODOC, B.DOCUMENTO_REL, B.CUENTA) =('FAC',REC_OS.planilla, _ARRCUENTASAGA[1])
				order by valor_debito desc,tipodoc
=======
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=p.cod_proveedor)
			where 
				planilla=rec_os.planilla
>>>>>>> migracion

		loop
		
			if(REC_AGA.tercer_nombexte__b is null)then
			
				select into REC_TER
					'CC' AS TERCER_CODIGO____TIT____B,
					 '' AS TERCER_DIGICHEQ__B,
					 nomcli AS TERCER_NOMBCORT__B,
					 nomcli AS TERCER_APELLIDOS_B,
					 nomcli AS TERCER_NOMBEXTE__B,
					 'RCOM' AS TERCER_CODIGO____TT_____B,
					 DIRECCION AS TERCER_DIRECCION_B,
					 '08001' AS TERCER_CODIGO____CIUDAD_B,
					 TELEFONO AS TERCER_TELEFONO1_B
				from 
					cliente 
				where 
					nit=REC_AGA.TERCERO;
				
				REC_AGA.TERCER_CODIGO____TIT____B := REC_TER.TERCER_CODIGO____TIT____B;
				REC_AGA.TERCER_DIGICHEQ__B := REC_TER.TERCER_DIGICHEQ__B;
				REC_AGA.TERCER_NOMBCORT__B := REC_TER.TERCER_NOMBCORT__B;
				REC_AGA.TERCER_APELLIDOS_B := REC_TER.TERCER_APELLIDOS_B;
				REC_AGA.TERCER_NOMBEXTE__B := REC_TER.TERCER_NOMBEXTE__B;
				REC_AGA.TERCER_CODIGO____TT_____B := REC_TER.TERCER_CODIGO____TT_____B;
				REC_AGA.TERCER_DIRECCION_B := REC_TER.TERCER_DIRECCION_B;
				REC_AGA.TERCER_CODIGO____CIUDAD_B := REC_TER.TERCER_CODIGO____CIUDAD_B;
				REC_AGA.TERCER_TELEFONO1_B := REC_TER.TERCER_TELEFONO1_B;			
			
			end if;

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(rec_os.fecha_contabilizacion,1,7),'-','')=REC_AGA.PERIODO THEN rec_os.fecha_contabilizacion::DATE ELSE con.sp_fecha_corte_mes(SUBSTRING(REC_AGA.PERIODO,1,4),SUBSTRING(REC_AGA.PERIODO,5,2)::INT)::DATE END ;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 6)='S')THEN
				MCTYPE.MC_____FECHEMIS__B=FECHADOC_::DATE;
				MCTYPE.MC_____FECHVENC__B=FECHADOC_::DATE;
			ELSE
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';

			END IF;

			MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
			MCTYPE.MC_____CODIGO____TD_____B := 'OPER' ;
			MCTYPE.MC_____CODIGO____CD_____B := 'AGA'  ;
			MCTYPE.MC_____SECUINTE__DCD____B := CONSEC  ;
			MCTYPE.MC_____FECHA_____B := CASE WHEN (REC_OS.fecha_contabilizacion::DATE > FECHADOC_::DATE AND REPLACE(SUBSTRING(REC_OS.fecha_contabilizacion,1,7),'-','')=REC_AGA.PERIODO)  THEN REC_OS.fecha_contabilizacion::DATE ELSE FECHADOC_::DATE END;
			--MCTYPE.MC_____FECHA_____B := REC_OS.CREATION_DATE::DATE  ;
			MCTYPE.MC_____NUMERO____B := SECUENCIA_TR  ;
			MCTYPE.MC_____SECUINTE__B := CONSEC  ;
			MCTYPE.MC_____REFERENCI_B := REC_OS.planilla  ;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING(REC_AGA.PERIODO,1,4)::INT  ;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING(REC_AGA.PERIODO,5,2)::INT  ;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF' ;
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 1)  ;
			MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 2)  ;
<<<<<<< HEAD
			MCTYPE.MC_____IDENTIFIC_TERCER_B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCERO)>10 THEN SUBSTR(REC_AGA.TERCERO,1,10) ELSE REC_AGA.TERCERO END;
=======
			MCTYPE.MC_____IDENTIFIC_TERCER_B := CASE WHEN REC_AGA.TERCER_CODIGO____TIT____B='NIT' and CHAR_LENGTH(REC_AGA.TERCERO)>9 THEN SUBSTR(REC_AGA.TERCERO,1,9) ELSE REC_AGA.TERCERO END;
>>>>>>> migracion
			MCTYPE.MC_____DEBMONORI_B := 0  ;
			MCTYPE.MC_____CREMONORI_B := 0 ;
			MCTYPE.MC_____DEBMONLOC_B := REC_AGA.VALOR_DEBITO::NUMERIC  ;
			MCTYPE.MC_____CREMONLOC_B := REC_AGA.VALOR_CREDITO::NUMERIC  ;
			MCTYPE.MC_____INDTIPMOV_B := 4  ;
			MCTYPE.MC_____INDMOVREV_B := 'N'  ;
			MCTYPE.MC_____OBSERVACI_B := REC_AGA.DETALLE ||' - Nit Transportadora: '||REC_OS.identificacion||' - Razon Social: '||REC_OS.razon_social;
			MCTYPE.MC_____FECHORCRE_B := rec_os.fecha_contabilizacion::TIMESTAMP  ;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN'  ;
			MCTYPE.MC_____FEHOULMO__B := rec_os.fecha_contabilizacion::TIMESTAMP  ;
			MCTYPE.MC_____AUTULTMOD_B := ''  ;
			MCTYPE.MC_____VALIMPCON_B := 0  ;
			MCTYPE.MC_____NUMERO_OPER_B := REC_OS.planilla;
			MCTYPE.TERCER_CODIGO____TIT____B := REC_AGA.TERCER_CODIGO____TIT____B  ;
			MCTYPE.TERCER_NOMBCORT__B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCER_NOMBCORT__B)>32 THEN SUBSTR(REC_AGA.TERCER_NOMBCORT__B,1,32) ELSE REC_AGA.TERCER_NOMBCORT__B END;
			MCTYPE.TERCER_NOMBEXTE__B := REC_AGA.TERCER_NOMBEXTE__B  ;
			MCTYPE.TERCER_APELLIDOS_B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCER_APELLIDOS_B)>32 THEN SUBSTR(REC_AGA.TERCER_APELLIDOS_B,1,32) ELSE REC_AGA.TERCER_APELLIDOS_B END;
			MCTYPE.TERCER_CODIGO____TT_____B := REC_AGA.TERCER_CODIGO____TT_____B  ;
			MCTYPE.TERCER_DIRECCION_B := CASE WHEN REC_AGA.TERCER_DIRECCION_B IS NULL OR  REC_AGA.TERCER_DIRECCION_B='' THEN 'CASA1' ELSE REC_AGA.TERCER_DIRECCION_B END ;
			MCTYPE.TERCER_CODIGO____CIUDAD_B := REC_AGA.TERCER_CODIGO____CIUDAD_B  ;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCER_TELEFONO1_B)>15 THEN SUBSTR(REC_AGA.TERCER_TELEFONO1_B,1,15) ELSE REC_AGA.TERCER_TELEFONO1_B END;
			MCTYPE.TERCER_TIPOGIRO__B := 1 ;
			MCTYPE.TERCER_CODIGO____EF_____B := ''  ;
			MCTYPE.TERCER_SUCURSAL__B := ''  ;
			MCTYPE.TERCER_NUMECUEN__B := ''  ;
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 3);
			MCTYPE.MC_____NUMDOCSOP_B := REC_OS.planilla;
			MCTYPE.MC_____NUMEVENC__B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 5)::INT;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 4)='S')THEN
				MCTYPE.MC_____NUMDOCSOP_B := REC_OS.planilla;
			ELSE
				MCTYPE.MC_____NUMDOCSOP_B := '';
			END IF;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 5)::INT=1)THEN
				MCTYPE.MC_____NUMEVENC__B := 1;
			ELSE
				MCTYPE.MC_____NUMEVENC__B := NULL;
			END IF;

			--FUNCION QUE TRANSACCION POR TIPO DE DOCUMENTO EN TABLA TEMPORAL EN FINTRA.
			SW:=CON.SP_INSERT_TABLE_MC( MCTYPE);
			CONSEC:=CONSEC+1;

		END LOOP;

		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE, 'LOGISTICA') ='N' THEN
			SW='N';
			CONTINUE;
		END IF;

		IF(SW='S')THEN
<<<<<<< HEAD
			/*insert into
				con.planillas_procesadas_ut(planilla, factura_cxp, factura_cxc, egreso, tipo_operacion, creation_date_planilla)
			values
				(REC_OS.planilla, REC_OS.factura_cxp, REC_OS.factura_cxc, REC_OS.egreso, 'AGA', REC_OS.creation_date);*/
=======
			insert into
				con.planillas_procesadas_ut(planilla, factura_cxp, factura_cxc, egreso, tipo_operacion, creation_date_planilla)
			values
				(REC_OS.planilla, REC_OS.factura_cxp, REC_OS.factura_cxc, '', 'AGA', REC_OS.creation_date);
>>>>>>> migracion
			SW:='N';
		END IF;

	END LOOP;

	RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION con.interfaz_fintra_ut_apoteosys_gasolina()
  OWNER TO postgres;

 
 /**
 SELECT con.interfaz_fintra_ut_apoteosys_gasolina()
 
select * from con.mc____ where procesado='N' order by mc_____numero____b, mc_____secuinte__b
 */
 
 