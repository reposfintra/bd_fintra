select * from etes.manifiesto_carga where planilla='0236901'

select * from fin.cxp_doc where documento='0236901'

select * from con.planillas_procesadas_ut where tipo_operacion='AGA' and procesado_pasivo_eds='N'

select * from con.mc____ where mc_____observaci_b like '%Nit Transportadora: 900668484%' and mc_____codigo____td_____b='CXPN' and procesado='R' and creation_date::timestamp='2019-01-18 16:19:11'::timestamp;

select * from con.mc____ where num_proceso=44316
44323

select --into REC_TRANS 
		distinct tr.identificacion, tr.razon_social 
from etes.manifiesto_carga a
		INNER join ETES.PRODUCTOS_SERVICIOS_TRANSP AS PRODUCTO_SER ON (A.ID_PROSERV=PRODUCTO_SER.ID)
		inner join etes.agencias ag on(ag.id=a.id_agencia)
		inner join etes.transportadoras tr on(tr.id=ag.id_transportadora)
		where planilla='0238343';

select 
				cxp.tipo_documento as tipodoc, 
				cxp.documento, 
				cxp.proveedor, 
				cxp.descripcion,
				cxp.periodo,
				cmc.cuenta, 
				cxp.vlr_neto as valor_debito, 
				0 as valor_credito,
				cxp.fecha_contabilizacion,
				(CASE
				 WHEN D.TIPO_IDEN='CED' THEN 'CC'
				 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN D.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				 D.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 D.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 D.TELEFONO AS TERCER_TELEFONO1_B
			from 
				fin.cxp_doc cxp 
			inner join 
				con.cmc_doc cmc on(cmc.cmc=cxp.handle_code and cmc.tipodoc=cxp.tipo_documento)
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=cxp.proveedor)
			LEFT JOIN
				NIT D ON(D.CEDULA=C.NIT)
			LEFT JOIN
				CIUDAD E ON(E.CODCIU=D.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=cxp.proveedor)
			where 
				tipo_documento='FAP' and documento='0236901'
			union all
				select 
				cxpi.tipo_documento as tipodoc, 
				cxpi.documento, 
				et.nit_estacion, 
				cxpi.descripcion, 
				cxpi.periodo,
				cmc.cuenta, 
				0 as valor_debito, 
				cxpi.vlr_neto as valor_credito, 
				cxpi.fecha_contabilizacion,
				(CASE
				 WHEN D.TIPO_IDEN='CED' THEN 'CC'
				 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN D.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				 D.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 D.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 D.TELEFONO AS TERCER_TELEFONO1_B
			from 
				fin.cxp_doc cxpi
			inner join 
				con.cmc_doc cmc on(cmc.cmc='EG' and cmc.tipodoc=cxpi.tipo_documento)
			inner join 
				etes.manifiesto_carga mc on(mc.planilla=cxpi.documento)
			inner join 
				etes.ventas_eds veds on(veds.id_manifiesto_carga=mc.id)
			inner join 
				etes.estacion_servicio et on(et.id=veds.id_eds)
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=et.nit_estacion)
			LEFT JOIN
				NIT D ON(D.CEDULA=C.NIT)
			LEFT JOIN
				CIUDAD E ON(E.CODCIU=D.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=et.nit_estacion)
			where tipo_documento='FAP' and documento='0236901'
			group by
			cxpi.tipo_documento, cxpi.documento, et.nit_estacion, cxpi.descripcion, cxpi.periodo, cmc.cuenta, cxpi.fecha_contabilizacion,
			cxpi.vlr_neto, D.TIPO_IDEN, C.DIGITO_VERIFICACION, D.NOMBRE1, D.NOMBRE2, D.APELLIDO1, 
			D.APELLIDO2, D.NOMBRE, C.GRAN_CONTRIBUYENTE, C.AGENTE_RETENEDOR, D.DIRECCION, E.CODIGO_DANE2, D.telefono

select * from nit where cedula='17973679'

select * from con.comprodet where numdoc='0236901'

select * from con.planillas_procesadas_ut where planilla='0236901'

select * from egresodet where document_no='AG314771'

select * from banco where branch_code='ESTACION'

select * from con.factura where documento='R0036945'

select * from etes.estacion_servicio

select * from etes.ventas_eds where id_manifiesto_carga=12419

-----------------------------------------------------------------------------------------------

select * from con.planillas_procesadas_ut where tipo_operacion='AGA' and procesado_egreso='N'

select 
				cxpi.tipo_documento as tipodoc, 
				cxpi.documento, 
				case when D.TIPO_IDEN='NIT' and CHAR_LENGTH(et.nit_estacion)>9 then SUBSTR(et.nit_estacion,1,9) else et.nit_estacion end as tercero, 
				cxpi.descripcion as detalle, 
				cxpi.periodo,
				cmc.cuenta, 
				cxpi.vlr_neto as valor_debito, 
				0 as valor_credito, 
				cxpi.fecha_contabilizacion,
				(CASE
				 WHEN D.TIPO_IDEN='CED' THEN 'CC'
				 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN D.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				 D.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 D.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 D.TELEFONO AS TERCER_TELEFONO1_B
			from 
				fin.cxp_doc cxpi
			inner join 
				con.cmc_doc cmc on(cmc.cmc='EG' and cmc.tipodoc=cxpi.tipo_documento)
			inner join 
				etes.manifiesto_carga mc on(mc.planilla=cxpi.documento)
			inner join 
				etes.ventas_eds veds on(veds.id_manifiesto_carga=mc.id)
			inner join 
				etes.estacion_servicio et on(et.id=veds.id_eds)
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=et.nit_estacion)
			LEFT JOIN
				NIT D ON(D.CEDULA=C.NIT)
			LEFT JOIN
				CIUDAD E ON(E.CODCIU=D.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=et.nit_estacion)
			where tipo_documento='FAP' and documento='0236901'
			group by
			cxpi.tipo_documento, cxpi.documento, et.nit_estacion, cxpi.descripcion, cxpi.periodo, cmc.cuenta, cxpi.fecha_contabilizacion,
			cxpi.vlr_neto, D.TIPO_IDEN, C.DIGITO_VERIFICACION, D.NOMBRE1, D.NOMBRE2, D.APELLIDO1, 
			D.APELLIDO2, D.NOMBRE, C.GRAN_CONTRIBUYENTE, C.AGENTE_RETENEDOR, D.DIRECCION, E.CODIGO_DANE2, D.telefono
			union all
			select 
			'EGR' as tipo_documento, 
			eg.document_no,
			case when D.TIPO_IDEN='NIT' and CHAR_LENGTH(et.nit_estacion)>9 then SUBSTR(et.nit_estacion,1,9) else et.nit_estacion end as tercero, 
			'EGRESO-'||eg.document_no as detalle,
			eg.periodo,
			bc.codigo_cuenta,
			0 as valor_debito,
			eg.vlr,
			cxp.fecha_contabilizacion,
			(CASE
				 WHEN D.TIPO_IDEN='CED' THEN 'CC'
				 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN D.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				 D.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 D.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 D.TELEFONO AS TERCER_TELEFONO1_B
			from egreso eg 
			inner join egresodet egd on(egd.dstrct=eg.dstrct and egd.branch_code=eg.branch_code and egd.bank_account_no=eg.bank_account_no and egd.document_no=eg.document_no)
			inner join fin.cxp_doc cxp on(cxp.documento=egd.documento and cxp.tipo_documento=egd.tipo_documento)
			inner join 
				banco bc on(bc.bank_account_no=eg.bank_account_no and bc.branch_code=eg.branch_code)
			inner join 
				etes.manifiesto_carga mc on(mc.planilla=egd.documento)
			inner join 
				etes.ventas_eds veds on(veds.id_manifiesto_carga=mc.id)
			inner join 
				etes.estacion_servicio et on(et.id=veds.id_eds)
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=et.nit_estacion)
			LEFT JOIN
				NIT D ON(D.CEDULA=C.NIT)
			LEFT JOIN
				CIUDAD E ON(E.CODCIU=D.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=et.nit_estacion)
			where egd.document_no='AG314771'
			group by
			eg.document_no, et.nit_estacion, eg.periodo, bc.codigo_cuenta, cxp.fecha_contabilizacion,
			eg.vlr, D.TIPO_IDEN, C.DIGITO_VERIFICACION, D.NOMBRE1, D.NOMBRE2, D.APELLIDO1, 
			D.APELLIDO2, D.NOMBRE, C.GRAN_CONTRIBUYENTE, C.AGENTE_RETENEDOR, D.DIRECCION, E.CODIGO_DANE2, D.telefono

			---------------------------------------------------------------------------------
			
			select * from egreso where document_no='AG314772'
			
			select * from banco where branch_code='ESTACION' and bank_account_no='GASOLINA' 
			
			select distinct mc_____referenci_b from con.mc____ where mc_____codigo____td_____b='CXPN' and mc_____observaci_b ilike '%- Nit Transportadora:%'
			
			select * from con.planillas_procesadas_ut where planilla='0236901'
			
			select * from fin.cxp_doc where documento='EDS0000376'
			
			select * from fin.cxp_items_doc_doc where documento='EDS0000376'