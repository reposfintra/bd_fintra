-- Function: con.interfaz_fintra_ut_apoteosys_gasolina_pago_eds()

-- DROP FUNCTION con.interfaz_fintra_ut_apoteosys_gasolina_pago_eds();

CREATE OR REPLACE FUNCTION con.interfaz_fintra_ut_apoteosys_gasolina_pago_eds()
  RETURNS text AS
$BODY$

DECLARE

 /************************************************
  *DESCRIPCION: ESTA FUNCION TOMA LAS PLANILLAS Y
  *OBTIENE EL PAGO A LA EDS DE GASOLINA 
  *Y CONTRUYE EL ASIENTO CONTABLE QUE MAS ADELANTE SE 
  *TRASLADARA A APOTEOSYS.
  *DOCUMENTACION:=
  *AUTOR:=@JZAPATA
  *FECHA CREACION:=2018-01-21
  *LAST_UPDATE
  *DESCRIPCION DE CAMBIOS Y FECHA
  ************************************************/

 REC_OS RECORD;
 REC_AGA RECORD;
 REC_PLA RECORD;
 REC_TER RECORD;
 SECUENCIA_TR INTEGER;
 CONSEC INTEGER:=1;
 FECHADOC_ TEXT:='';
 CORRIDA TEXT:='';
 SW TEXT:='N';
 _ARRCUENTASAGA VARCHAR[] :='{}';
 MCTYPE CON.TYPE_INSERT_MC;


 BEGIN

	--SE CONSULTAN LOS REGISTROS QUE SE VAN A TRANSPORTAR PARA GASOLINAS

	FOR REC_OS in
	
		select 
			b.cheque, 
			b.banco, 
			b.sucursal,
			tr.identificacion,
			tr.razon_social
		from 
			fin.cxp_items_doc a 
		inner join 
			fin.cxp_doc b on(b.tipo_documento=a.tipo_documento and b.documento=a.documento and b.proveedor=a.proveedor)
		inner join 
			etes.manifiesto_carga d on(d.planilla=a.planilla)
		inner join 
			etes.agencias ag on(ag.id=d.id_agencia)
		inner join 
			etes.transportadoras tr on(tr.id=ag.id_transportadora)
		where 
			a.tipo_documento='FAP' and 
			a.documento ilike 'EDS%' and 
			a.referencia_2='S' and 
			(coalesce(a.referencia_3,'N')='N' or a.referencia_3='N')
		group by 
			b.cheque, 
			b.banco, 
			b.sucursal,
			tr.identificacion,
			tr.razon_social
		--limit 1

/**
select con.interfaz_fintra_ut_apoteosys_gasolina_pago_eds();

select * from etes.manifiesto_carga where cxc_corrida='' and reg_status='';

select * from con.mc____ where procesado='N' AND mc_____codigo____pf_____b=2018 and	mc_____numero____period_b=11 order by mc_____numero____b, mc_____secuinte__b

delete from con.mc____ where procesado='N' and num_proceso=44333

SELECT mc_____numdocsop_b, mc_____debmonloc_b, mc_____cremonloc_b,* from con.mc____ where MC_____CODIGO____TD_____B = 'EGRN' AND mc_____numero____b=738671;

delete from con.planillas_procesadas_ut;

*/


	LOOP

		raise notice 'Cheque: %',REC_OS.cheque;

		SECUENCIA_TR:=0;
		CONSEC :=1;
	
		--SECUENCIA DE LA TRANSACCION
		SELECT INTO SECUENCIA_TR NEXTVAL('CON.INTERFAZ_SECUENCIA_EGRESO_APOTEOSYS');

		FOR REC_AGA IN

			select 
				'EGR' as tipodoc, 
				a.branch_code, 
				a.bank_account_no, 
				a.documento as doc_sop, 
				b.nit as tercero, 
				a.description as detalle, 
				b.printer_date as fecha, 
				d.cuenta, 
				a.vlr as valor_debito, 
				0 as valor_credito, 
				b.periodo,
			(CASE
				 WHEN f.TIPO_IDEN='CED' THEN 'CC'
				 WHEN f.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN f.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 e.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (f.NOMBRE1||' '||f.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (f.APELLIDO1||' '||f.APELLIDO2) AS TERCER_APELLIDOS_B,
				 f.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN e.GRAN_CONTRIBUYENTE='N' AND e.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN e.GRAN_CONTRIBUYENTE='N' AND e.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN e.GRAN_CONTRIBUYENTE='S' AND e.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN e.GRAN_CONTRIBUYENTE='S' AND e.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 f.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN g.CODIGO_DANE2!='' THEN g.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 f.TELEFONO AS TERCER_TELEFONO1_B
			from egresodet a 
			inner join egreso b on(b.branch_code=a.branch_code and b.bank_account_no=a.bank_account_no and b.document_no=a.document_no)
			inner join fin.cxp_doc c on(c.tipo_documento=a.tipo_documento and c.documento=a.documento)
			inner join con.cmc_doc d on(d.tipodoc=c.tipo_documento and d.cmc=c.handle_code)
			LEFT JOIN
				PROVEEDOR e ON(e.NIT=b.nit)
			LEFT JOIN
				NIT f ON(f.CEDULA=e.NIT)
			LEFT JOIN
				CIUDAD g ON(g.CODCIU=f.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=b.nit)
			where a.branch_code=rec_os.banco and a.bank_account_no=rec_os.sucursal and a.document_no=REC_OS.cheque
			union all
			select 
				'EGR' as tipodoc, 
				a.branch_code, 
				a.bank_account_no, 
				a.document_no as doc_sop, 
				a.nit, 
				a.payment_name as descripcion, 
				a.printer_date, 
				b.codigo_cuenta, 
				0 as valor_debito, 
				a.vlr as valor_credito, 
				a.periodo,
			(CASE
				 WHEN f.TIPO_IDEN='CED' THEN 'CC'
				 WHEN f.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN f.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 e.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (f.NOMBRE1||' '||f.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (f.APELLIDO1||' '||f.APELLIDO2) AS TERCER_APELLIDOS_B,
				 f.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN e.GRAN_CONTRIBUYENTE='N' AND e.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN e.GRAN_CONTRIBUYENTE='N' AND e.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN e.GRAN_CONTRIBUYENTE='S' AND e.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN e.GRAN_CONTRIBUYENTE='S' AND e.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 f.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN g.CODIGO_DANE2!='' THEN g.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 f.TELEFONO AS TERCER_TELEFONO1_B
			from egreso a
			inner join banco b on(b.branch_code=a.branch_code and b.bank_account_no=a.bank_account_no)
			LEFT JOIN
				PROVEEDOR e ON(e.NIT=a.nit)
			LEFT JOIN
				NIT f ON(f.CEDULA=e.NIT)
			LEFT JOIN
				CIUDAD g ON(g.CODCIU=f.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=a.nit)
			where a.branch_code=rec_os.banco and a.bank_account_no=rec_os.sucursal and a.document_no=rec_os.cheque

		loop
		
			if(REC_AGA.tercer_nombexte__b is null)then
			
				select into REC_TER
					'CC' AS TERCER_CODIGO____TIT____B,
					 '' AS TERCER_DIGICHEQ__B,
					 nomcli AS TERCER_NOMBCORT__B,
					 nomcli AS TERCER_APELLIDOS_B,
					 nomcli AS TERCER_NOMBEXTE__B,
					 'RCOM' AS TERCER_CODIGO____TT_____B,
					 DIRECCION AS TERCER_DIRECCION_B,
					 '08001' AS TERCER_CODIGO____CIUDAD_B,
					 TELEFONO AS TERCER_TELEFONO1_B
				from 
					cliente 
				where 
					nit=REC_AGA.TERCERO;
				
				REC_AGA.TERCER_CODIGO____TIT____B := REC_TER.TERCER_CODIGO____TIT____B;
				REC_AGA.TERCER_DIGICHEQ__B := REC_TER.TERCER_DIGICHEQ__B;
				REC_AGA.TERCER_NOMBCORT__B := REC_TER.TERCER_NOMBCORT__B;
				REC_AGA.TERCER_APELLIDOS_B := REC_TER.TERCER_APELLIDOS_B;
				REC_AGA.TERCER_NOMBEXTE__B := REC_TER.TERCER_NOMBEXTE__B;
				REC_AGA.TERCER_CODIGO____TT_____B := REC_TER.TERCER_CODIGO____TT_____B;
				REC_AGA.TERCER_DIRECCION_B := REC_TER.TERCER_DIRECCION_B;
				REC_AGA.TERCER_CODIGO____CIUDAD_B := REC_TER.TERCER_CODIGO____CIUDAD_B;
				REC_AGA.TERCER_TELEFONO1_B := REC_TER.TERCER_TELEFONO1_B;			
			
			end if;

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(REC_AGA.fecha,1,7),'-','')=REC_AGA.PERIODO THEN REC_AGA.fecha::DATE ELSE con.sp_fecha_corte_mes(SUBSTRING(REC_AGA.PERIODO,1,4),SUBSTRING(REC_AGA.PERIODO,5,2)::INT)::DATE END ;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 6)='S')THEN
				MCTYPE.MC_____FECHEMIS__B=FECHADOC_::DATE;
				MCTYPE.MC_____FECHVENC__B=FECHADOC_::DATE;
			ELSE
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';

			END IF;

			MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
			MCTYPE.MC_____CODIGO____TD_____B := 'EGRN' ;
			MCTYPE.MC_____CODIGO____CD_____B := 'EGLG'  ;
			MCTYPE.MC_____SECUINTE__DCD____B := CONSEC  ;
			MCTYPE.MC_____FECHA_____B := CASE WHEN (REC_AGA.fecha::DATE > FECHADOC_::DATE AND REPLACE(SUBSTRING(REC_AGA.fecha,1,7),'-','')=REC_AGA.PERIODO)  THEN REC_AGA.fecha::DATE ELSE FECHADOC_::DATE END;
			--MCTYPE.MC_____FECHA_____B := REC_OS.CREATION_DATE::DATE  ;
			MCTYPE.MC_____NUMERO____B := SECUENCIA_TR  ;
			MCTYPE.MC_____SECUINTE__B := CONSEC  ;
			MCTYPE.MC_____REFERENCI_B := REC_OS.cheque  ;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING(REC_AGA.PERIODO,1,4)::INT  ;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING(REC_AGA.PERIODO,5,2)::INT  ;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF' ;
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 1)  ;
			MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 2)  ;
			MCTYPE.MC_____IDENTIFIC_TERCER_B := CASE when CHAR_LENGTH(REC_AGA.TERCERO)>10 THEN SUBSTR(REC_AGA.TERCERO,1,10) ELSE REC_AGA.TERCERO END;
			MCTYPE.MC_____DEBMONORI_B := 0  ;
			MCTYPE.MC_____CREMONORI_B := 0 ;
			MCTYPE.MC_____DEBMONLOC_B := REC_AGA.VALOR_DEBITO::NUMERIC  ;
			MCTYPE.MC_____CREMONLOC_B := REC_AGA.VALOR_CREDITO::NUMERIC  ;
			MCTYPE.MC_____INDTIPMOV_B := 4  ;
			MCTYPE.MC_____INDMOVREV_B := 'N'  ;
			MCTYPE.MC_____OBSERVACI_B := REC_AGA.DETALLE||' - Nit Transportadora: '||REC_OS.identificacion||' - Razon Social: '||REC_OS.razon_social;
			MCTYPE.MC_____FECHORCRE_B := REC_AGA.fecha::TIMESTAMP  ;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN'  ;
			MCTYPE.MC_____FEHOULMO__B := REC_AGA.fecha::TIMESTAMP  ;
			MCTYPE.MC_____AUTULTMOD_B := ''  ;
			MCTYPE.MC_____VALIMPCON_B := 0  ;
			MCTYPE.MC_____NUMERO_OPER_B := REC_OS.cheque;
			MCTYPE.TERCER_CODIGO____TIT____B := REC_AGA.TERCER_CODIGO____TIT____B  ;
			MCTYPE.TERCER_NOMBCORT__B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCER_NOMBCORT__B)>32 THEN SUBSTR(REC_AGA.TERCER_NOMBCORT__B,1,32) ELSE REC_AGA.TERCER_NOMBCORT__B END;
			MCTYPE.TERCER_NOMBEXTE__B := REC_AGA.TERCER_NOMBEXTE__B  ;
			MCTYPE.TERCER_APELLIDOS_B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCER_APELLIDOS_B)>32 THEN SUBSTR(REC_AGA.TERCER_APELLIDOS_B,1,32) ELSE REC_AGA.TERCER_APELLIDOS_B END;
			MCTYPE.TERCER_CODIGO____TT_____B := REC_AGA.TERCER_CODIGO____TT_____B  ;
			MCTYPE.TERCER_DIRECCION_B := CASE WHEN REC_AGA.TERCER_DIRECCION_B IS NULL OR  REC_AGA.TERCER_DIRECCION_B='' THEN 'CASA1' ELSE REC_AGA.TERCER_DIRECCION_B END ;
			MCTYPE.TERCER_CODIGO____CIUDAD_B := REC_AGA.TERCER_CODIGO____CIUDAD_B  ;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCER_TELEFONO1_B)>15 THEN SUBSTR(REC_AGA.TERCER_TELEFONO1_B,1,15) ELSE REC_AGA.TERCER_TELEFONO1_B END;
			MCTYPE.TERCER_TIPOGIRO__B := 1 ;
			MCTYPE.TERCER_CODIGO____EF_____B := ''  ;
			MCTYPE.TERCER_SUCURSAL__B := ''  ;
			MCTYPE.TERCER_NUMECUEN__B := ''  ;
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 3);
			--MCTYPE.MC_____NUMDOCSOP_B := REC_OS.planilla;
			MCTYPE.MC_____NUMEVENC__B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 5)::INT;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 4)='S')THEN
				MCTYPE.MC_____NUMDOCSOP_B := REC_aga.doc_sop;
			ELSE
				MCTYPE.MC_____NUMDOCSOP_B := '';
			END IF;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 5)::INT=1)THEN
				MCTYPE.MC_____NUMEVENC__B := 1;
			ELSE
				MCTYPE.MC_____NUMEVENC__B := NULL;
			END IF;

			--FUNCION QUE TRANSACCION POR TIPO DE DOCUMENTO EN TABLA TEMPORAL EN FINTRA.
			SW:=CON.SP_INSERT_TABLE_MC( MCTYPE);
			CONSEC:=CONSEC+1;

		END LOOP;

		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE, 'LOGISTICA') ='N' THEN
			SW='N';
			CONTINUE;
		END IF;

		IF(SW='S')THEN
			
			update 
				fin.cxp_items_doc 
			set 
				referencia_3='S' 
			where 
				tipo_documento='FAP' and
				documento in(select documento from fin.cxp_doc where banco=rec_os.banco and sucursal=rec_os.sucursal and cheque=rec_os.cheque);
			
			SW:='N';
		END IF;

	END LOOP;

	RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION con.interfaz_fintra_ut_apoteosys_gasolina_pago_eds()
  OWNER TO postgres;

 
 /**
 SELECT con.interfaz_fintra_ut_apoteosys_gasolina_pago_eds()
 
select * from con.mc____ where procesado='N' order by mc_____numero____b, mc_____secuinte__b
 */
 
 