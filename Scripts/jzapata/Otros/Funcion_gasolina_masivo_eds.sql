-- Function: con.interfaz_fintra_ut_apoteosys_gasolina_pasivo_eds()

-- DROP FUNCTION con.interfaz_fintra_ut_apoteosys_gasolina_pasivo_eds();

CREATE OR REPLACE FUNCTION con.interfaz_fintra_ut_apoteosys_gasolina_pasivo_eds()
  RETURNS text AS
$BODY$

DECLARE

 /************************************************
  *DESCRIPCION: ESTA FUNCION TOMA LAS PLANILLAS Y
  *OBTIENE EL PASIVO DIFINITIVO DE LA EDS POR GASOLINA 
  *Y CONTRUYE EL ASIENTO CONTABLE QUE MAS ADELANTE SE 
  *TRASLADARA A APOTEOSYS.
  *DOCUMENTACION:=
  *AUTOR:=@JZAPATA
  *FECHA CREACION:=2018-12-12
  *LAST_UPDATE
  *DESCRIPCION DE CAMBIOS Y FECHA
  ************************************************/

 REC_OS RECORD;
 REC_AGA RECORD;
 REC_PLA RECORD;
 REC_TER RECORD;
 REC_TRANS RECORD;
 SECUENCIA_TR INTEGER;
 CONSEC INTEGER:=1;
 FECHADOC_ TEXT:='';
 CORRIDA TEXT:='';
 SW TEXT:='N';
 _ARRCUENTASAGA VARCHAR[] :='{}';
 MCTYPE CON.TYPE_INSERT_MC;


 BEGIN

	--SE CONSULTAN LOS REGISTROS QUE SE VAN A TRANSPORTAR PARA GASOLINAS

	FOR REC_OS in
	
		select 
			pput.planilla, cxp.documento 
		from 
			con.planillas_procesadas_ut pput
		inner join fin.cxp_items_doc cxp on(cxp.tipo_documento='FAP' and cxp.documento ilike 'EDS%' and cxp.planilla=pput.planilla)
		where 
			tipo_operacion='AGA' and 
			(coalesce(cxp.referencia_2,'N')='N' or cxp.referencia_2='')

/**
select con.interfaz_fintra_ut_apoteosys_gasolina_pasivo_eds();

select * from etes.manifiesto_carga where cxc_corrida='' and reg_status='';

select * from con.mc____ where procesado='N' AND mc_____codigo____pf_____b=2018 and	mc_____numero____period_b=11 order by mc_____numero____b, mc_____secuinte__b

delete from con.mc____ where procesado='N'

select * from con.mc____ where procesado='R' and num_proceso=44397

SELECT * from con.mc____ where MC_____CODIGO____TD_____B = 'OPER' and MC_____CODIGO____CD_____B = 'AGA' and procesado='N';

delete from con.planillas_procesadas_ut;

*/


	LOOP

		raise notice 'Planilla: %',REC_OS.planilla;

		SECUENCIA_TR:=0;
		CONSEC :=1;
	
		select into REC_TRANS 
		distinct tr.identificacion, tr.razon_social from etes.manifiesto_carga a
		INNER join ETES.PRODUCTOS_SERVICIOS_TRANSP AS PRODUCTO_SER ON (A.ID_PROSERV=PRODUCTO_SER.ID)
		inner join etes.agencias ag on(ag.id=a.id_agencia)
		inner join etes.transportadoras tr on(tr.id=ag.id_transportadora)
		where planilla=REC_OS.planilla;

		--SECUENCIA DE LA TRANSACCION
		SELECT INTO SECUENCIA_TR NEXTVAL('CON.INTERFAZ_SECUENCIA_CXP_APOTEOSYS');

		FOR REC_AGA IN

			select a.tipodoc, 
				a.documento, 
				a.proveedor as tercero, 
				a.descripcion as detalle, 
				a.periodo,
				a.cuenta, 
				round(sum(a.valor_debito)) as valor_debito, 
				a.valor_credito, 
				a.fecha_contabilizacion,
				a.TERCER_CODIGO____TIT____B,
				 a.TERCER_DIGICHEQ__B,
				 a.TERCER_NOMBCORT__B,
				 a.TERCER_APELLIDOS_B,
				 a.TERCER_NOMBEXTE__B,
				 a.TERCER_CODIGO____TT_____B,
				 a.TERCER_DIRECCION_B,
				 a.TERCER_CODIGO____CIUDAD_B,
				 a.TERCER_TELEFONO1_B from 
			(select 
				cxp2.tipo_documento as tipodoc, 
				cxp2.documento, 
				cxp2.proveedor, 
				cxp2.descripcion, 
				cxpi.periodo,
				cmc.cuenta, 
				cxpit.vlr-coalesce(cxpid.vlr,0) as valor_debito, 
				0 as valor_credito, 
				cxpi.fecha_contabilizacion::date as fecha_contabilizacion,
				(CASE
				 WHEN D.TIPO_IDEN='CED' THEN 'CC'
				 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN D.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				 D.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 D.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 D.TELEFONO AS TERCER_TELEFONO1_B
			from 
				fin.cxp_doc cxpi
			inner join
				fin.cxp_items_doc cxpit on(cxpit.documento=cxpi.documento and cxpit.tipo_documento=cxpi.tipo_documento and cxpit.proveedor=cxpi.proveedor)
			inner join 
				fin.cxp_doc cxp2 on(cxp2.tipo_documento='FAP' and cxp2.documento=cxpit.planilla)
			left join 
				fin.cxp_items_doc cxpid on (cxpid.proveedor=cxpi.proveedor and cxpid.tipo_documento='NC' and cxpid.documento ilike 'NC_%'||cxpi.documento||'%' and cxpid.descripcion ilike '%'||cxpit.planilla||'%')
			inner join 
				con.cmc_doc cmc on(cmc.cmc=cxp2.handle_code and cmc.tipodoc=cxp2.tipo_documento)
			inner join 
				etes.manifiesto_carga mc on(mc.planilla=cxpit.planilla)
			inner join 
				etes.ventas_eds veds on(veds.id_manifiesto_carga=mc.id)
			inner join 
				etes.estacion_servicio et on(et.id=veds.id_eds)
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=cxp2.proveedor)
			LEFT JOIN
				NIT D ON(D.CEDULA=C.NIT)
			LEFT JOIN
				CIUDAD E ON(E.CODCIU=D.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=cxp2.proveedor)
			where cxpi.tipo_documento='FAP' and cxpi.handle_code='EG' and cxpit.planilla=rec_os.planilla and cxpi.documento=rec_os.documento
			group by
			cxp2.tipo_documento, cxp2.documento, cxp2.proveedor, cxp2.descripcion, cxpi.periodo, cmc.cuenta, cxpit.vlr, cxpid.vlr, cxpi.fecha_contabilizacion::date,
			cxpi.vlr_neto, D.TIPO_IDEN, C.DIGITO_VERIFICACION, D.NOMBRE1, D.NOMBRE2, D.APELLIDO1, 
			D.APELLIDO2, D.NOMBRE, C.GRAN_CONTRIBUYENTE, C.AGENTE_RETENEDOR, D.DIRECCION, E.CODIGO_DANE2, D.telefono) as a
			group by a.tipodoc, 
				a.documento, 
				a.proveedor, 
				a.descripcion, 
				a.periodo,
				a.cuenta, 
				a.valor_credito, 
				a.fecha_contabilizacion,
				a.TERCER_CODIGO____TIT____B,
				 a.TERCER_DIGICHEQ__B,
				 a.TERCER_NOMBCORT__B,
				 a.TERCER_APELLIDOS_B,
				 a.TERCER_NOMBEXTE__B,
				 a.TERCER_CODIGO____TT_____B,
				 a.TERCER_DIRECCION_B,
				 a.TERCER_CODIGO____CIUDAD_B,
				 a.TERCER_TELEFONO1_B
			union all
				select 
				cxpi.tipo_documento as tipodoc, 
				cxpi.documento, 
				cxpi.proveedor, 
				cxpi.descripcion, 
				cxpi.periodo,
				cmc.cuenta, 
				0 as valor_debito, 
				round(cxpit.vlr-coalesce(cxpid.vlr,0)) as valor_credito, 
				cxpi.fecha_contabilizacion::date as fecha_contabilizacion,
				(CASE
				 WHEN D.TIPO_IDEN='CED' THEN 'CC'
				 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN D.TIPO_IDEN='NIT' THEN 'NIT' ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				 D.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 D.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 D.TELEFONO AS TERCER_TELEFONO1_B
			from 
				fin.cxp_doc cxpi
			inner join
				fin.cxp_items_doc cxpit on(cxpit.documento=cxpi.documento and cxpit.tipo_documento=cxpi.tipo_documento and cxpit.proveedor=cxpi.proveedor)
			left join 
				fin.cxp_items_doc cxpid on (cxpid.proveedor=cxpi.proveedor and cxpid.tipo_documento='NC' and cxpid.documento ilike 'NC_%'||cxpi.documento||'%' and cxpid.descripcion ilike '%'||cxpit.planilla||'%')
			inner join 
				con.cmc_doc cmc on(cmc.cmc='EG' and cmc.tipodoc=cxpi.tipo_documento)
			inner join 
				etes.manifiesto_carga mc on(mc.planilla=cxpit.planilla)
			inner join 
				etes.ventas_eds veds on(veds.id_manifiesto_carga=mc.id)
			inner join 
				etes.estacion_servicio et on(et.id=veds.id_eds)
			LEFT JOIN
				PROVEEDOR C ON(C.NIT=cxpi.proveedor)
			LEFT JOIN
				NIT D ON(D.CEDULA=C.NIT)
			LEFT JOIN
				CIUDAD E ON(E.CODCIU=D.CODCIU)
			LEFT JOIN
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=cxpi.proveedor)
			where cxpi.tipo_documento='FAP' and handle_code='EG' and cxpit.planilla=rec_os.planilla and cxpi.documento=rec_os.documento
			group by
			cxpi.tipo_documento, cxpi.documento, cxpi.proveedor, cxpi.descripcion, cxpi.periodo, cmc.cuenta, cxpit.vlr, cxpid.vlr, cxpi.fecha_contabilizacion::date,
			cxpi.vlr_neto, D.TIPO_IDEN, C.DIGITO_VERIFICACION, D.NOMBRE1, D.NOMBRE2, D.APELLIDO1, 
			D.APELLIDO2, D.NOMBRE, C.GRAN_CONTRIBUYENTE, C.AGENTE_RETENEDOR, D.DIRECCION, E.CODIGO_DANE2, D.telefono

		loop
		
			if(REC_AGA.tercer_nombexte__b is null)then
			
				select into REC_TER
					'CC' AS TERCER_CODIGO____TIT____B,
					 '' AS TERCER_DIGICHEQ__B,
					 nomcli AS TERCER_NOMBCORT__B,
					 nomcli AS TERCER_APELLIDOS_B,
					 nomcli AS TERCER_NOMBEXTE__B,
					 'RCOM' AS TERCER_CODIGO____TT_____B,
					 DIRECCION AS TERCER_DIRECCION_B,
					 '08001' AS TERCER_CODIGO____CIUDAD_B,
					 TELEFONO AS TERCER_TELEFONO1_B
				from 
					cliente 
				where 
					nit=REC_AGA.TERCERO;
				
				REC_AGA.TERCER_CODIGO____TIT____B := REC_TER.TERCER_CODIGO____TIT____B;
				REC_AGA.TERCER_DIGICHEQ__B := REC_TER.TERCER_DIGICHEQ__B;
				REC_AGA.TERCER_NOMBCORT__B := REC_TER.TERCER_NOMBCORT__B;
				REC_AGA.TERCER_APELLIDOS_B := REC_TER.TERCER_APELLIDOS_B;
				REC_AGA.TERCER_NOMBEXTE__B := REC_TER.TERCER_NOMBEXTE__B;
				REC_AGA.TERCER_CODIGO____TT_____B := REC_TER.TERCER_CODIGO____TT_____B;
				REC_AGA.TERCER_DIRECCION_B := REC_TER.TERCER_DIRECCION_B;
				REC_AGA.TERCER_CODIGO____CIUDAD_B := REC_TER.TERCER_CODIGO____CIUDAD_B;
				REC_AGA.TERCER_TELEFONO1_B := REC_TER.TERCER_TELEFONO1_B;			
			
			end if;

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(REC_AGA.fecha_contabilizacion,1,7),'-','')=REC_AGA.PERIODO THEN REC_AGA.fecha_contabilizacion::DATE ELSE con.sp_fecha_corte_mes(SUBSTRING(REC_AGA.PERIODO,1,4),SUBSTRING(REC_AGA.PERIODO,5,2)::INT)::DATE END ;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 6)='S')THEN
				MCTYPE.MC_____FECHEMIS__B=FECHADOC_::DATE;
				MCTYPE.MC_____FECHVENC__B=FECHADOC_::DATE;
			ELSE
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';

			END IF;

			MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
			MCTYPE.MC_____CODIGO____TD_____B := 'CXPN' ;
			MCTYPE.MC_____CODIGO____CD_____B := 'AG'  ;
			MCTYPE.MC_____SECUINTE__DCD____B := CONSEC  ;
			MCTYPE.MC_____FECHA_____B := CASE WHEN (REC_AGA.fecha_contabilizacion::DATE > FECHADOC_::DATE AND REPLACE(SUBSTRING(REC_AGA.fecha_contabilizacion,1,7),'-','')=REC_AGA.PERIODO)  THEN REC_AGA.fecha_contabilizacion::DATE ELSE FECHADOC_::DATE END;
			--MCTYPE.MC_____FECHA_____B := REC_OS.CREATION_DATE::DATE  ;
			MCTYPE.MC_____NUMERO____B := SECUENCIA_TR  ;
			MCTYPE.MC_____SECUINTE__B := CONSEC  ;
			MCTYPE.MC_____REFERENCI_B := REC_OS.planilla  ;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING(REC_AGA.PERIODO,1,4)::INT  ;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING(REC_AGA.PERIODO,5,2)::INT  ;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF' ;
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 1)  ;
			MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 2)  ;
			MCTYPE.MC_____IDENTIFIC_TERCER_B := CASE when CHAR_LENGTH(REC_AGA.TERCERO)>10 THEN SUBSTR(REC_AGA.TERCERO,1,10) ELSE REC_AGA.TERCERO END;
			MCTYPE.MC_____DEBMONORI_B := 0  ;
			MCTYPE.MC_____CREMONORI_B := 0 ;
			MCTYPE.MC_____DEBMONLOC_B := REC_AGA.VALOR_DEBITO::NUMERIC  ;
			MCTYPE.MC_____CREMONLOC_B := REC_AGA.VALOR_CREDITO::NUMERIC  ;
			MCTYPE.MC_____INDTIPMOV_B := 4  ;
			MCTYPE.MC_____INDMOVREV_B := 'N'  ;
			MCTYPE.MC_____OBSERVACI_B := REC_AGA.DETALLE||' - Nit Transportadora: '||REC_TRANS.identificacion||' - Razon Social: '||REC_TRANS.razon_social;
			raise notice 'REC_AGA.DETALLE: %',REC_AGA.DETALLE||' - Nit AGAportadora: '||REC_TRANS.identificacion||' - Razon Social: '||REC_TRANS.razon_social;
			raise notice 'MCTYPE.MC_____OBSERVACI_B: %',MCTYPE.MC_____OBSERVACI_B;
			MCTYPE.MC_____FECHORCRE_B := REC_AGA.fecha_contabilizacion::TIMESTAMP  ;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN'  ;
			MCTYPE.MC_____FEHOULMO__B := REC_AGA.fecha_contabilizacion::TIMESTAMP  ;
			MCTYPE.MC_____AUTULTMOD_B := ''  ;
			MCTYPE.MC_____VALIMPCON_B := 0  ;
			MCTYPE.MC_____NUMERO_OPER_B := REC_OS.planilla;
			MCTYPE.TERCER_CODIGO____TIT____B := REC_AGA.TERCER_CODIGO____TIT____B  ;
			MCTYPE.TERCER_NOMBCORT__B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCER_NOMBCORT__B)>32 THEN SUBSTR(REC_AGA.TERCER_NOMBCORT__B,1,32) ELSE REC_AGA.TERCER_NOMBCORT__B END;
			MCTYPE.TERCER_NOMBEXTE__B := REC_AGA.TERCER_NOMBEXTE__B  ;
			MCTYPE.TERCER_APELLIDOS_B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCER_APELLIDOS_B)>32 THEN SUBSTR(REC_AGA.TERCER_APELLIDOS_B,1,32) ELSE REC_AGA.TERCER_APELLIDOS_B END;
			MCTYPE.TERCER_CODIGO____TT_____B := REC_AGA.TERCER_CODIGO____TT_____B  ;
			MCTYPE.TERCER_DIRECCION_B := CASE WHEN REC_AGA.TERCER_DIRECCION_B IS NULL OR  REC_AGA.TERCER_DIRECCION_B='' THEN 'CASA1' ELSE REC_AGA.TERCER_DIRECCION_B END ;
			MCTYPE.TERCER_CODIGO____CIUDAD_B := REC_AGA.TERCER_CODIGO____CIUDAD_B  ;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(REC_AGA.TERCER_TELEFONO1_B)>15 THEN SUBSTR(REC_AGA.TERCER_TELEFONO1_B,1,15) ELSE REC_AGA.TERCER_TELEFONO1_B END;
			MCTYPE.TERCER_TIPOGIRO__B := 1 ;
			MCTYPE.TERCER_CODIGO____EF_____B := ''  ;
			MCTYPE.TERCER_SUCURSAL__B := ''  ;
			MCTYPE.TERCER_NUMECUEN__B := ''  ;
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 3);
			MCTYPE.MC_____NUMDOCSOP_B := REC_OS.planilla;
			MCTYPE.MC_____NUMEVENC__B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 5)::INT;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 4)='S')THEN
				MCTYPE.MC_____NUMDOCSOP_B := REC_AGA.documento;
			ELSE
				MCTYPE.MC_____NUMDOCSOP_B := '';
			END IF;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('GASOLINA', REC_AGA.TIPODOC, REC_AGA.CUENTA,'', 5)::INT=1)THEN
				MCTYPE.MC_____NUMEVENC__B := 1;
			ELSE
				MCTYPE.MC_____NUMEVENC__B := NULL;
			END IF;

			--FUNCION QUE TRANSACCION POR TIPO DE DOCUMENTO EN TABLA TEMPORAL EN FINTRA.
			SW:=CON.SP_INSERT_TABLE_MC( MCTYPE);
			CONSEC:=CONSEC+1;

		END LOOP;

		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE, 'LOGISTICA') ='N' THEN
			SW='N';
			CONTINUE;
		END IF;

		IF(SW='S')THEN
			
			update 
				fin.cxp_items_doc 
			set 
				referencia_2='S' 
			where 
				tipo_documento='FAP' and 
				documento =rec_os.documento and 
				planilla=rec_os.planilla;
			
			SW:='N';
		END IF;

	END LOOP;

	RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION con.interfaz_fintra_ut_apoteosys_gasolina_pasivo_eds()
  OWNER TO postgres;

 
 /**
 SELECT con.interfaz_fintra_ut_apoteosys_gasolina_pasivo_eds()
 
select * from con.mc____ where procesado='N' order by mc_____numero____b, mc_____secuinte__b
 */
 
 