-- Function: apoteosys.interfaz_pagos_notas_reversion_fiducia_apoteosys()

-- DROP FUNCTION apoteosys.interfaz_pagos_notas_reversion_fiducia_apoteosys();

CREATE OR REPLACE FUNCTION apoteosys.interfaz_pagos_notas_reversion_fiducia_apoteosys()
  RETURNS text AS
$BODY$

DECLARE
	r_ingreso record;
	r_asiento record;
	recordNeg record;
	INFOCLIENTE record;
	CUENTA_ASIENTO VARCHAR;
	SECUENCIA_GEN INTEGER;
	FECHADOC_ TEXT:='';
	MCTYPE CON.TYPE_INSERT_MC;
	SW TEXT:='N';
	CONSEC INTEGER:=1;
	_PROCESOHOM TEXT := 'REVERSION_IA';

--select apoteosys.interfaz_pagos_notas_reversion_fiducia_apoteosys()
--select * from con.mc_recaudo____  where procesado='N' and MC_____CODIGO____PF_____B='2019' and MC_____NUMERO____PERIOD_B IN(2,3)

BEGIN

	FOR r_ingreso IN

		select 
			a.*,
			case when con.interfaz_obtener_centro_costo_x_ingreso(a.num_ingreso,2) ilike 'MC%' and a.tipodoc='IF' then 'MI' else a.tipodoc end as tipodoc2
		from 
			(select 
				b.procesado_ica,
				a.reg_status, 
				a.periodo,
				a.cmc,
				a.tipo_documento, 
				a.num_ingreso, 
				case 
				when a.descripcion_ingreso ilike '%REVERS%INTERES%PAGO%TOTAL%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'AIPT'
				when a.descripcion_ingreso ilike '%REVERS%IF%PAGO%TOTAL%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'AIPT'
				when a.descripcion_ingreso ilike '%REVERS%INTERES%PAGO%ADELANTA%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'AIPT'
				when a.descripcion_ingreso ilike '%REVERS%IF%PAGO%ADELANTA%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'AIPT'
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMON%PAGO%TOTAL%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'ACPT' 
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMINISTRA%PAGO%TOTAL%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'ACPT' 
				when a.descripcion_ingreso ilike '%REVERS%CM%PAGO%TOTAL%'  and a.descripcion_ingreso not ilike '%CAUSAD%' then 'ACPT' 
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMON%PAGO%ADELANTA%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'ACPT' 
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMINISTRA%PAGO%ADELANTA%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'ACPT' 
				when a.descripcion_ingreso ilike '%REVERS%CM%PAGO%ADELANTA%'  and a.descripcion_ingreso not ilike '%CAUSAD%' then 'ACPT' 
				--when descripcion_ingreso ilike '%SE%APLICA%POR%CORREO%' then 'CMIF?'
				when a.descripcion_ingreso ilike '%REVERS%INTERE%CAUSADO%' then 'RIPT' 
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMON%CAUSAD%' then 'RCPT'
				when a.descripcion_ingreso ilike '%REVERS%CM%CAUSAD%' then 'RCPT'
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMON%TICIPA%' then 'ACPA'
				when a.descripcion_ingreso ilike '%REVERS%CM%TICIPA%' then 'ACPA'
				when a.descripcion_ingreso ilike '%REVERS%INTERES%TICIPA%' then 'AIPA'
				when a.descripcion_ingreso ilike '%REVERS%IF%TICIPA%' then 'AIPA'
				when a.descripcion_ingreso ilike '%SALDO%ULTIM%CUOTA%' then 'AASM'
				when a.descripcion_ingreso ilike '%REVERS%GAST%COBR%' then 'REGA'
				end as _CLASEDOC, 
				case 
				when a.descripcion_ingreso ilike '%REVERS%INTERES%PAGO%TOTAL%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'IF'
				when a.descripcion_ingreso ilike '%REVERS%IF%PAGO%TOTAL%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'IF'
				when a.descripcion_ingreso ilike '%REVERS%INTERES%PAGO%ADELANTA%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'IF'
				when a.descripcion_ingreso ilike '%REVERS%IF%PAGO%ADELANTA%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'IF'
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMON%PAGO%TOTAL%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'CM' 
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMINISTRA%PAGO%TOTAL%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'CM'
				when a.descripcion_ingreso ilike '%REVERS%CM%PAGO%TOTAL%'  and a.descripcion_ingreso not ilike '%CAUSAD%' then 'CM' 
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMON%PAGO%ADELANTA%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'CM' 
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMINISTRA%PAGO%ADELANTA%' and a.descripcion_ingreso not ilike '%CAUSAD%' then 'CM' 
				when a.descripcion_ingreso ilike '%REVERS%CM%PAGO%ADELANTA%'  and a.descripcion_ingreso not ilike '%CAUSAD%' then 'CM' 
				--when descripcion_ingreso ilike '%SE%APLICA%POR%CORREO%' then 'CMIF?'
				when a.descripcion_ingreso ilike '%REVERS%INTERE%CAUSADO%' then 'IF' 
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMON%CAUSAD%' then 'CM'
				when a.descripcion_ingreso ilike '%REVERS%CM%CAUSAD%' then 'CM'
				when a.descripcion_ingreso ilike '%REVERS%CUOTA%ADMON%TICIPA%' then 'CM'
				when a.descripcion_ingreso ilike '%REVERS%CM%TICIPA%' then 'CM'
				when a.descripcion_ingreso ilike '%REVERS%INTERES%TICIPA%' then 'IF'
				when a.descripcion_ingreso ilike '%REVERS%IF%TICIPA%' then 'IF'
				when a.descripcion_ingreso ilike '%SALDO%ULTIM%CUOTA%' then 'IA'
				when a.descripcion_ingreso ilike '%REVERS%GAST%COBR%' then 'GC'
				end as tipodoc, 
				a.descripcion_ingreso, 
				a.cuenta,
				a.nitcli, 
				a.fecha_ingreso, 
				a.vlr_ingreso 
			from 
				con.ingreso a
			inner join
				con.ingreso_detalle b on(b.tipo_documento=a.tipo_documento and b.num_ingreso=a.num_ingreso)
			where 
				a.reg_status='' and 
				a.tipo_documento='ICA' and 
				a.cmc!='LC' and 
				a.periodo=replace(substring(CURRENT_date,1,7),'-','')
				and b.procesado_ica='N'
				and b.cuenta='16252102'
				--and a.num_ingreso in('IA538450')
				--and b.factura ilike 'FG%'
				group by 
				b.procesado_ica,
				a.reg_status, 
				a.periodo,
				a.cmc,
				a.tipo_documento, 
				a.num_ingreso, 
				a.descripcion_ingreso, 
				a.cuenta,
				a.nitcli, 
				a.fecha_ingreso, 
				a.vlr_ingreso) as a
		where 
			a._CLASEDOC is not null
		order by num_ingreso
		--limit 1

	LOOP

		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.INTERFAZ_SECUENCIA_ICA_APOTEOSYS');

		FOR r_asiento IN

			select 
				a.tipo_documento,
				a.num_ingreso,
				a.creation_date,
				b.negasoc,
				d.cuenta as cuenta2,
				coalesce(c.cod,substring(d.descripcion_ingreso,1,8)) as documento,
				c.endosado,
				a.cuenta,
				a.valor_ingreso as valor_debito, 
				0 as valor_credito,
				a.nitcli,
						CASE    WHEN substr(c.cod,1,2)=cfc.prefijo AND c.tipodoc=cfc.tipodoc THEN coalesce(cfc.cuenta_diferido,'')
							WHEN substr(c.cod,1,2)=co.prefijo_diferidos AND c.tipodoc=(select tipo_documento from series where document_type=co.prefijo_diferidos) THEN coalesce(co.cuenta_diferidos,'')
							WHEN substr(c.cod,1,2)=co.prefijo_dif_fiducia AND c.tipodoc=(select tipo_documento from series where document_type=co.prefijo_dif_fiducia) THEN coalesce(co.cuenta_dif_fiducia,'')
							WHEN substr(c.cod,1,2)=co.prefijo_cuota_administracion_diferido AND c.tipodoc=(select tipo_documento from series where document_type=co.prefijo_cuota_administracion_diferido) THEN coalesce(co.cta_cuota_admin_diferido,'')
						   ELSE ''
						END  as cuenta_diferidos,
						co.cuenta_interes, 
						co.cuenta_cuota_administracion, 
						c.endosado,
						'1' as item
			from 
				con.ingreso_detalle a 
			inner join
				con.ingreso d on(d.tipo_documento=a.tipo_documento and d.num_ingreso=a.num_ingreso)
			left join 
				con.factura b on(b.tipo_documento=a.tipo_doc and b.documento=a.documento)
			left join 
				ing_fenalco c on(c.codneg=b.negasoc and c.tipodoc=r_ingreso.tipodoc2 and (substring(c.cod,char_length(c.cod)-1,2))::int+1=(substring(a.factura,char_length(a.factura)-1,2))::int)
			left join negocios ng on (ng.cod_neg = c.codneg)
			left join con.tipo_docto tdoc on (tdoc.dstrct = c.dstrct and tdoc.codigo_interno = c.tipodoc)
			left join con.cmc_doc cmc on (cmc.dstrct = tdoc.dstrct and cmc.tipodoc = tdoc.codigo and cmc.cmc = c.cmc)
			left join con.cuentas ctas on (cmc.cuenta=ctas.cuenta)
			left join negocios n on (n.cod_neg = c.codneg)
			left join convenios co on (co.id_convenio = n.id_convenio)
			left join convenios_cargos_fijos cfc on (co.id_convenio=cfc.id_convenio and cfc.activo = true and cfc.tipodoc=c.tipodoc)
			left JOIN rel_unidadnegocio_convenios ruc ON (co.id_convenio = ruc.id_convenio and ruc.id_unid_negocio in(30,31,14,12,13))
			left JOIN unidad_negocio uneg ON (uneg.id = ruc.id_unid_negocio)
			left JOIN con.cuentas cu1 on(cu1.cuenta=co.cuenta_interes)
			left JOIN con.cuentas cu2 on(cu2.cuenta=co.cuenta_cuota_administracion)
			where 
				a.reg_status='' and 
				a.tipo_documento='ICA' and 
				a.num_ingreso=r_ingreso.num_ingreso
			union all
			select 
				a.tipo_documento,
				a.num_ingreso,
				a.creation_date,
				b.negasoc,
				d.cuenta as cuenta2,
				coalesce(a.factura,substring(d.descripcion_ingreso,1,8)) as documento, 
				c.endosado,
				a.cuenta,
				0 as valor_debito, 
				a.valor_ingreso as valor_credito,
				a.nitcli,
				'' as cuenta_diferidos,
						'' as cuenta_interes, 
						'' as cuenta_cuota_administracion, 
						'' as endosado,
				'2' as item
			from 
				con.ingreso_detalle a 
			inner join
				con.ingreso d on(d.tipo_documento=a.tipo_documento and d.num_ingreso=a.num_ingreso)
			inner join 
				con.factura b on(b.tipo_documento=a.tipo_doc and b.documento=a.documento)
			left join 
				ing_fenalco c on(c.codneg=b.negasoc and c.tipodoc=r_ingreso.tipodoc2 and (substring(c.cod,char_length(c.cod)-1,2))::int+1=(substring(a.factura,char_length(a.factura)-1,2))::int)
			where 
				a.reg_status='' and 
				a.tipo_documento='ICA' and 
				a.num_ingreso=r_ingreso.num_ingreso

		LOOP

			SELECT INTO INFOCLIENTE
				'NIT' AS TIPO_DOC,
				'GCON' AS CODIGO,
				(CASE
				WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				ELSE '08001' END) AS CODIGOCIU,
				NOMCLI AS NOMBRE_CORTO,
				NOMCLI AS  NOMBRE,
				'' AS APELLIDOS,
				DIRECCION,
				TELEFONO
			FROM CLIENTE CL
			LEFT JOIN CIUDAD E ON(E.CODCIU=CL.CIUDAD)
			WHERE CL.NIT =  r_asiento.nitcli;

			SELECT INTO recordNeg un.descripcion, neg.cod_neg, neg.creation_date  FROM con.factura fac
			INNER JOIN negocios neg ON (neg.cod_neg=fac.negasoc)
			INNER JOIN rel_unidadnegocio_convenios rneg on (rneg.id_convenio=neg.id_convenio)
			INNER JOIN unidad_negocio un on (un.id=rneg.id_unid_negocio)
			WHERE documento=r_asiento.documento AND tipo_documento IN ('FAC','NDC') AND un.id in (1,2,3,4,6,7,8,9,10,21,22);
		
			-------FORMULA PARA SABER LA CUENTA A LA QUE DEBE IR-------
			if(r_asiento.item='1')then
				if(r_ingreso.tipodoc2 in('IF','MI'))then
					if(r_asiento.endosado='N')then
						CUENTA_ASIENTO:=r_asiento.cuenta_interes;--'27050901'
					else
						if(r_asiento.cuenta2 ilike 'I0%' or r_asiento.cuenta2='16252141') then
							CUENTA_ASIENTO:='16252141';
						else
							CUENTA_ASIENTO:='16252135';
						end if;
					end if;
				elsif(r_ingreso.tipodoc2 in('CM'))then
					if(r_asiento.endosado='N')then
						CUENTA_ASIENTO:=r_asiento.cuenta_cuota_administracion;--'27059602'
					else
						if(r_asiento.cuenta2 ilike 'I0%') then
							CUENTA_ASIENTO:='16252160';
						else
							CUENTA_ASIENTO:='16252136';
						end if;
					end if;
				elsif(r_ingreso.tipodoc2 in('IA','GC','IM'))then
					CUENTA_ASIENTO:=r_asiento.cuenta2;
				end if;
			else
				CUENTA_ASIENTO:=r_asiento.cuenta;
			end if;
			-----------------------------------------------------------

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(R_ASIENTO.CREATION_DATE::DATE,1,7),'-','')=R_INGRESO.PERIODO THEN R_ASIENTO.CREATION_DATE::DATE ELSE CON.SP_FECHA_CORTE_MES(SUBSTRING(R_INGRESO.PERIODO,1,4),SUBSTRING(R_INGRESO.PERIODO,5,2)::INT)::DATE END ;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, CUENTA_ASIENTO,'', 6)='S')then
				MCTYPE.MC_____FECHEMIS__B = FECHADOC_::DATE;
				--MCTYPE.MC_____FECHEMIS__B = recordNeg.CREATION_DATE::DATE;
				MCTYPE.MC_____FECHVENC__B = FECHADOC_::DATE;
			else
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';

			end if;

			MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
			MCTYPE.MC_____CODIGO____TD_____B := 'ICRN' ;
			MCTYPE.MC_____CODIGO____CD_____B := r_ingreso._CLASEDOC;
			MCTYPE.MC_____SECUINTE__DCD____B := CONSEC  ;
			MCTYPE.MC_____FECHA_____B := FECHADOC_::DATE  ;
			MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN  ;
			MCTYPE.MC_____SECUINTE__B := CONSEC  ;
			MCTYPE.MC_____REFERENCI_B := con.interfaz_obtener_centro_costo_x_ingreso(r_ingreso.num_ingreso,2);--recordNeg.cod_neg;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING(r_ingreso.PERIODO,1,4)::INT;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING(r_ingreso.PERIODO,5,2)::INT;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF' ;
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, CUENTA_ASIENTO,'', 1)  ;
			MCTYPE.MC_____CODIGO____CU_____B := con.interfaz_obtener_centro_costo_x_ingreso(r_ingreso.num_ingreso,1);
			MCTYPE.MC_____IDENTIFIC_TERCER_B :=  CASE WHEN (SUBSTRING(R_ASIENTO.nitcli,1,1) IN(8,9) AND CHAR_LENGTH(R_ASIENTO.nitcli)>9) and MCTYPE.MC_____CODIGO____CPC____B !='900703674' THEN SUBSTR(R_ASIENTO.nitcli,1,9) 
												WHEN MCTYPE.MC_____CODIGO____CPC____B='28150501' THEN '900703674'
												ELSE R_ASIENTO.nitcli END;
			MCTYPE.MC_____DEBMONORI_B := 0  ;
			MCTYPE.MC_____CREMONORI_B := 0 ;
			MCTYPE.MC_____DEBMONLOC_B := R_ASIENTO.VALOR_DEBITO::NUMERIC  ;
			MCTYPE.MC_____CREMONLOC_B := R_ASIENTO.VALOR_CREDITO::NUMERIC  ;
			MCTYPE.MC_____INDTIPMOV_B := 4  ;
			MCTYPE.MC_____INDMOVREV_B := 'N'  ;
			MCTYPE.MC_____OBSERVACI_B := r_ingreso.num_ingreso||' - '||r_ingreso.descripcion_ingreso;
			MCTYPE.MC_____FECHORCRE_B := R_ASIENTO.CREATION_DATE::TIMESTAMP  ;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN'  ;
			MCTYPE.MC_____FEHOULMO__B := R_ASIENTO.CREATION_DATE::TIMESTAMP  ;
			MCTYPE.MC_____AUTULTMOD_B := ''  ;
			MCTYPE.MC_____VALIMPCON_B := 0  ;
			MCTYPE.MC_____NUMERO_OPER_B := R_ASIENTO.num_ingreso;
			MCTYPE.TERCER_CODIGO____TIT____B := INFOCLIENTE.TIPO_DOC;
			MCTYPE.TERCER_NOMBCORT__B := SUBSTRING(INFOCLIENTE.NOMBRE_CORTO,1,32);
			MCTYPE.TERCER_NOMBEXTE__B := SUBSTRING(INFOCLIENTE.NOMBRE,1,64);
			MCTYPE.TERCER_APELLIDOS_B := INFOCLIENTE.APELLIDOS;
			MCTYPE.TERCER_CODIGO____TT_____B := INFOCLIENTE.CODIGO;
			MCTYPE.TERCER_DIRECCION_B := CASE WHEN CHAR_LENGTH(INFOCLIENTE.DIRECCION)>64 THEN SUBSTR(INFOCLIENTE.DIRECCION,1,64) ELSE INFOCLIENTE.DIRECCION END;
			MCTYPE.TERCER_CODIGO____CIUDAD_B := INFOCLIENTE.CODIGOCIU;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(INFOCLIENTE.TELEFONO)>15 THEN SUBSTR(INFOCLIENTE.TELEFONO,1,15) ELSE INFOCLIENTE.TELEFONO END;
			MCTYPE.TERCER_TIPOGIRO__B := 1 ;
			MCTYPE.TERCER_CODIGO____EF_____B := ''  ;
			MCTYPE.TERCER_SUCURSAL__B := ''  ;
			MCTYPE.TERCER_NUMECUEN__B := ''  ;
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, CUENTA_ASIENTO,'', 3);
			--MCTYPE.MC_____NUMDOCSOP_B := REC_OS.NUMERO_OPERACION;
			MCTYPE.MC_____NUMEVENC__B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, CUENTA_ASIENTO,'', 5)::INT;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, CUENTA_ASIENTO,'', 4)='S')then
				/*if(MCTYPE.MC_____CODIGO____CPC____B IN('23809605','28150501'))then
					MCTYPE.MC_____NUMDOCSOP_B := R_ASIENTO.num_ingreso;
				else*/
					MCTYPE.MC_____NUMDOCSOP_B := R_ASIENTO.DOCUMENTO;
--				end if;
			else
				MCTYPE.MC_____NUMDOCSOP_B := '';
			end if;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, CUENTA_ASIENTO,'', 5)::int=1)then
				MCTYPE.MC_____NUMEVENC__B := 1;
			else
				MCTYPE.MC_____NUMEVENC__B := null;
			end if;

			raise notice 'MCTYPE: %', MCTYPE;

			-- Insertamos en la tabla de Apoteosys
			--FUNCION QUE TRANSACCION POR TIPO DE DOCUMENTO EN TABLA TEMPORAL EN FINTRA.
			SW:=CON.SP_INSERT_TABLE_MC_RECAUDO____(MCTYPE);
			CONSEC:=CONSEC+1;
		END LOOP;

		CONSEC:=1;
		---------------------------------------------------------------------------

		--------------Revision de la transaccion-----------------
		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE, 'RECAUDO') ='N' THEN
			SW='N';

			--BORRAMOS EL COMPROBANTE DE EXT
			DELETE FROM CON.mc_recaudo____
			WHERE MC_____NUMERO____B = SECUENCIA_GEN AND MC_____CODIGO____CONTAB_B = 'FINT'
			 AND MC_____CODIGO____TD_____B = 'ICA' AND  MC_____CODIGO____CD_____B = r_ingreso._CLASEDOC;

			CONTINUE;
		END IF;

		-- ACTUALIZAMOS EL CAMPO DE APOTEOSYS DE LA CABECERA DEL CRéDITO PARA INDICAR QUE YA SE ENVíO
		IF(SW='S')THEN

			 UPDATE
				con.ingreso_detalle
			SET
				PROCESADO_ICA='S'
			WHERE
				TIPO_DOCUMENTO=r_ingreso.tipo_documento and
				num_ingreso=r_ingreso.num_ingreso;

			SW:='N';
		END IF;

		CONSEC:=1;
		---------------------------------------------------------------

	END LOOP;

	RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apoteosys.interfaz_pagos_notas_reversion_fiducia_apoteosys()
  OWNER TO postgres;
