--drop table tem.pago_masivo__; 

CREATE TABLE tem.pago_masivo__
(
negasoc character varying(20) NOT NULL,
nitcli character varying(15) NOT NULL,
unidad_negocio int,
tipo_pago character varying(10) NOT NULL,
Fecha_Pago date,
banco character varying(15) NOT NULL,
sucursal character varying(30) NOT NULL,
valor_aplicar_neto moneda,
usuario character varying(30) NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem.pago_masivo__
  OWNER TO postgres;
COMMENT ON TABLE tem.pago_masivo__
  IS 'TABLA TEMPORAL PARA PAGOS MASIVOS';
  
 select * from tem.pago_masivo__;

 delete from tem.pago_masivo__;
 
 INSERT INTO tem.pago_masivo__ (
                negasoc, nitcli, unidad_negocio, tipo_pago,
                Fecha_Pago, banco, sucursal, valor_aplicar_neto, usuario) 
            VALUES ('MC12977','1130609576','1','AAC','2018-12-15'::date,'SUPEREFECTIVO','SUPEREFECTIVO','250000','JZAPATA')
            
            

--1.) Calcular gastos de cobranza
--2.) Calculo del interes de mora
--3.) CAT
--4.) CUOTA ADMINSTRACION
--5.) POLIZA DE SEGURO 
--6.) INTERESES
--7.) CAPITAL 

select * from con.ingreso_detalle where num_ingreso='IC329112'

SELECT 
--INTO tasaIm 
c.tasa_interes,c.id_convenio FROM negocios as n
				INNER JOIN convenios as c on (c.id_convenio=n.id_convenio)
				WHERE n.cod_neg ='MC12977';

select * from con.cmc_doc where cmc='CA' and tipodoc='FAC'			
			
SELECT *
	  FROM 
	  --con.factura fra
	  tem.foto_factura_20181217 fra
	WHERE fra.dstrct = 'FINV'
	AND fra.valor_saldo > 0
	AND fra.reg_status = ''
	AND fra.negasoc = 'MC12977'
	AND fra.tipo_documento in ('FAC','NDC')
	AND substring(fra.documento,1,2) not in ('CP','FF','DF')
	order by fecha_vencimiento, documento desc;

--dias de mora->28
--interes de mora
-- 11 A 30 :=5 ;
-- 31 A 60 := 10;
-- > 60 :=20 ;
select round((((242318 * 3.5)/100 ) / 30) * 28 );

--gastos de cobranza
select round(242318 * 0.05);

select * from tem.comprobante_20181218
tem.comprodet_20181218

select * from con.comprobante where tipodoc='CDIAR' and numdoc ilike 'CEN%' and periodo>='201811'