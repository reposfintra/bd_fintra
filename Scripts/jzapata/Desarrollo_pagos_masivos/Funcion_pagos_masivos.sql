-- Function: tem.pagos_masivos_manuales()

-- DROP FUNCTION tem.pagos_masivos_manuales();

CREATE OR REPLACE FUNCTION tem.pagos_masivos_manuales()
  RETURNS text AS
$BODY$

declare

pagos_manuales_ record;
facturas record;
intereses_ record;
valor_interes_x_mora int := 0;
valor_gasto_cobranza int :=0;
total_valor_interes_x_mora int := 0;
total_valor_saldo_factura int :=0;
valor_saldo_factura int :=0;

begin
	
	for pagos_manuales_ in
		
		select 
			negasoc,
			nitcli,
			unidad_negocio,
			tipo_pago,
			fecha_pago,
			banco,
			sucursal,
			valor_aplicar_neto,
			usuario
		from 
			tem.pago_masivo__
		where 
			negasoc='MC12977'
	
	loop
		
		--SETEAMOS NUEVAMENTE LOS VALORES DE INTERESES X MORA Y GASTOS DE COBRANZA 
		valor_interes_x_mora := 0;
		valor_gasto_cobranza := 0;
		total_valor_interes_x_mora := 0;
		total_valor_saldo_factura :=0;
		
		--SE ASIGNA EL VALOR DEL PAGO A LA VARIABLE PARA SABER QUE SE PAGA
		valor_saldo_factura := pagos_manuales_.valor_aplicar_neto;
	
		for facturas_ in
		
			SELECT 
				tipo_documento,
				documento,
				nit,
				codcli,
				cmc.cuenta,
				fecha_factura,
				fecha_vencimiento,
				('2018-12-15'::date-fecha_vencimiento) as dias_vencidos,
				case when ('2018-12-15'::date-fecha_vencimiento) between 11 and 30 then 0.05
				when ('2018-12-15'::date-fecha_vencimiento) between 31 and 60 then 0.1
				when ('2018-12-15'::date-fecha_vencimiento) >60 then 0.2
				else 0
				end as porc_mora,
				descripcion,
				valor_factura,
				valor_abono,
				valor_saldo,
				periodo,
				negasoc
			FROM 
			  --con.factura fra
				tem.foto_factura_20181217 fra
			inner join con.cmc_doc cmc on(cmc.cmc=fra.cmc and cmc.tipodoc=fra.tipo_documento)
			WHERE 
				fra.dstrct = 'FINV'
				AND fra.valor_saldo > 0
				AND fra.reg_status = ''
				AND fra.negasoc = 'MC12977'
				AND fra.tipo_documento in ('FAC','NDC')
				AND substring(fra.documento,1,2) not in ('CP','FF','DF')
			order by fecha_vencimiento, documento desc
		
		loop
			
			--OBTENEMOS EL % DE INTERES A COBRAR POR MORA
			SELECT into interes_
				c.tasa_interes,
				c.id_convenio 
			FROM 
				negocios as n
			INNER JOIN convenios as c on (c.id_convenio=n.id_convenio)
			WHERE 
				n.cod_neg ='MC12977';
			
			--CALCULAMOS EL VALOR DEL INTERES POR MORA DE LA FACTURA
			valor_interes_x_mora := round((((facturas_.valor_saldo * intereses_.tasa_interes)/100 ) / 30) *  facturas_.dias_vencidos);
			total_valor_interes_x_mora := total_valor_interes_x_mora + valor_interes_x_mora;
			
			--CALCULAMOS LOS GASTOS DE COBRANZA SI ESTA VENCIDO DE LA FACTURA
			valor_gasto_cobranza := round(facturas_valor_saldo * facturas_.porc_mora);
			total_valor_gasto_cobranza := total_valor_gasto_cobranza + valor_gasto_cobranza;
			
			--OBTENEMOS EL SALDO A ABONAR A LA FACTURA DESPUES DE LOS DESCUENTOS POR MORA
			valor_saldo_factura:=valor_saldo_factura-(valor_interes_x_mora-valor_gasto_cobranza);
		
			--INSERTAMOS LOS REGISTROS DE LAS FACTURAS A ABONAR
			if(valor_saldo_factura>facturas_.valor_saldo)then
				insert into tem.prueba_ic() values();--	facturas_.valor_saldo
				valor_saldo_factura:=valor_saldo_factura-facturas_.valor_saldo;
			elsif(valor_saldo_factura<facturas_.valor_saldo)then
				insert into tem.prueba_ic() values();--valor_saldo_factura
				valor_saldo_factura:=0;
				exit;
			else
				insert into tem.prueba_ic() values();--valor_saldo_factura
				valor_saldo_factura:=0;
				exit;
			end if;			
		
		end loop;
		
		--SI EL VALOR DE LOS INTERESES ES MAYOR QUE 0 INSERTAMOS EN LA TABLA
		if(total_valor_interes_x_mora>0)then
			insert into tem.prueba_ic() values();--total_valor_interes_x_mora
		end if;
		
		--SI EL VALOR DE LOS GASTOS DE COBRANZA ES MAYOR QUE 0 INSERTAMOS EN LA TABLA
		if(total_valor_gasto_cobranza>0)then
			insert into tem.prueba_ic() values();--total_valor_gasto_cobranza
		end if;
	
	
		insert de la cabecera del ic
	
		
	
	end loop;
	
	
	return 'OK';
end;

$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION tem.pagos_masivos_manuales()
  OWNER TO postgres;