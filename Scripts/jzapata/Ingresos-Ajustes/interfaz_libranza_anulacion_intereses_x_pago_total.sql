-- Function: apoteosys.interfaz_libranza_anulacion_intereses_x_pago_total()

-- DROP FUNCTION apoteosys.interfaz_libranza_anulacion_intereses_x_pago_total();

CREATE OR REPLACE FUNCTION apoteosys.interfaz_libranza_anulacion_intereses_x_pago_total()
  RETURNS text AS
$BODY$

DECLARE
	r_ingreso record;
	r_asiento record;
	SECUENCIA_GEN INTEGER;
	FECHADOC_ TEXT:='';
	MCTYPE CON.TYPE_INSERT_MC;
	SW TEXT:='N';
	CONSEC INTEGER:=1;
	VALOR_ACUM_ numeric:=0.00;
	CUENTA_BANCO TEXT:='';
	cuota_ text:='';
	--_CLASEDOC TEXT := 'AIPT';
	_PROCESOHOM TEXT := 'RECAUDO_LIBRA';
	_NEGOCIO TEXT = '';

BEGIN


	FOR r_ingreso IN
--1). SE EJECUTA ESTE SELECT DEPENDIENDO LA UNIDAD DE NEGOCIO Y EL PERIODO (_periodo, _unidadNego)
--2). SE EJECUTA LA FUNCION COMPLETA

			select * from (select 
				a.tipo_documento,
				a.num_ingreso, 
				b.fecha_ingreso,
				'ATL'::varchar as agencia,
				case when b.descripcion_ingreso ilike '%INTERES%' then 'AIPT' 
				when b.descripcion_ingreso ilike '%SEGURO%' then 'ASPT' end as _CLASEDOC 
			from 
				con.ingreso_detalle a 
			inner join 
				con.ingreso b on(b.tipo_documento=a.tipo_documento and b.num_ingreso=a.num_ingreso)
			where 
				a.reg_status='' and 
				a.tipo_documento='ICA' and 
				a.num_ingreso ilike 'IA%' and 
				--b.periodo='201901' and 
				b.periodo=replace(substring(current_date,1,7),'-','') 
				--and b.num_ingreso in()
				and a.procesado_ica='N' 
				and b.cmc='LC'
				and a.factura!=''
			group by a.tipo_documento, a.num_ingreso, b.fecha_ingreso, b.descripcion_ingreso
			order by b.fecha_ingreso) as a
			where a._clasedoc is not null

/**
select apoteosys.interfaz_libranza_anulacion_intereses_x_pago_total();
select * from con.mc_recaudo____ where procesado='N'
**/

	LOOP

		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.interfaz_secuencia_ica_apoteosys');
		CONSEC:=1;

		FOR r_asiento in
		
		
			select 
				a.tipo_documento, 
				a.num_ingreso, 
				a.periodo,
				a.nitcli as nit_cliente, 
				b.fecha_consignacion,
			    b.fecha_ingreso,
			    b.branch_code,
			    b.bank_account_no,
			    b.descripcion_ingreso,
				0 as valor_debito, 
				a.valor_ingreso as valor_credito, 
				a.factura as documento_soporte, 
				a.fecha_factura, 
				d.cuenta, 
				a.descripcion,
				(CASE
				 WHEN g.TIPO_IDEN='CED' THEN 'CC'
				 WHEN g.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN g.TIPO_IDEN='' THEN 'CC'
				 WHEN g.TIPO_IDEN='NIT' THEN 'NIT'
				 ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 f.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (g.NOMBRE1||' '||g.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (g.APELLIDO1||' '||g.APELLIDO2) AS TERCER_APELLIDOS_B,
				 g.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN f.GRAN_CONTRIBUYENTE='N' AND f.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN f.GRAN_CONTRIBUYENTE='N' AND f.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN f.GRAN_CONTRIBUYENTE='S' AND f.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN f.GRAN_CONTRIBUYENTE='S' AND f.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 g.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN h.CODIGO_DANE2!='' THEN h.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 g.TELEFONO AS TERCER_TELEFONO1_B
			from 
				con.ingreso_detalle a
			inner join 
				con.ingreso b on(b.tipo_documento=a.tipo_documento and b.num_ingreso=a.num_ingreso)
			left join 
				con.factura c on(c.documento=a.factura)
			left join 
				con.cmc_doc d on(d.cmc=c.cmc and d.tipodoc=c.tipo_documento)
			LEFT JOIN 
				PROVEEDOR f ON(f.NIT=a.nitcli)
			LEFT JOIN 
				NIT g ON(g.CEDULA=f.NIT)
			LEFT JOIN 
				CIUDAD h ON(h.CODCIU=g.CODCIU)
			LEFT JOIN 
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=a.nitcli)
			where 
				a.num_ingreso=r_ingreso.num_ingreso
			union all
			select 
				a.tipo_documento, 
				a.num_ingreso, 
				a.periodo,
				--a.nitcli, 
				case when e.descripcion_ingreso ilike '%REV%INTERES%' then a.nitcli else '860028415' end as nitcli,
				e.fecha_consignacion,
			    e.fecha_ingreso,
			    e.branch_code,
			    e.bank_account_no,
			    e.descripcion_ingreso,
				a.valor_ingreso as valor_debito, 
				0 as valor_credito, 
				case when e.descripcion_ingreso ilike '%REV%INTERES%' then c.cod else a.factura end as descripcion, 
				a.fecha_factura, 
				e.cuenta, 
				b.descripcion,
				(CASE
				 WHEN g.TIPO_IDEN='CED' THEN 'CC'
				 WHEN g.TIPO_IDEN='RIF' THEN 'CE'
				 WHEN g.TIPO_IDEN='' THEN 'CC'
				 WHEN g.TIPO_IDEN='NIT' THEN 'NIT'
				 ELSE
				 'CC' END) AS TERCER_CODIGO____TIT____B,
				 f.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
				 (g.NOMBRE1||' '||g.NOMBRE2) AS TERCER_NOMBCORT__B,
				 (g.APELLIDO1||' '||g.APELLIDO2) AS TERCER_APELLIDOS_B,
				 g.NOMBRE AS TERCER_NOMBEXTE__B,
				 (CASE
				 WHEN f.GRAN_CONTRIBUYENTE='N' AND f.AGENTE_RETENEDOR='N' THEN 'RCOM'
				 WHEN f.GRAN_CONTRIBUYENTE='N' AND f.AGENTE_RETENEDOR='S' THEN 'RCAU'
				 WHEN f.GRAN_CONTRIBUYENTE='S' AND f.AGENTE_RETENEDOR='N' THEN 'GCON'
				 WHEN f.GRAN_CONTRIBUYENTE='S' AND f.AGENTE_RETENEDOR='S' THEN 'GCAU'
				 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
				 g.DIRECCION AS TERCER_DIRECCION_B,
				 (CASE
				 WHEN h.CODIGO_DANE2!='' THEN h.CODIGO_DANE2
				 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
				 g.TELEFONO AS TERCER_TELEFONO1_B
			from 
				con.ingreso_detalle a
			inner join 
				con.ingreso e on(e.tipo_documento=a.tipo_documento and e.num_ingreso=a.num_ingreso)
			left join 
				con.factura b on(b.documento=a.factura)
			left join 
				con.cmc_doc d on(d.cmc=b.cmc and d.tipodoc=b.tipo_documento)
			left join 
				ing_fenalco c on(c.codneg=b.negasoc and c.cuota=b.num_doc_fen)
			LEFT JOIN 
				PROVEEDOR f ON(f.NIT=case when e.descripcion_ingreso ilike '%REV%INTERES%' then a.nitcli else '860028415' end)
			LEFT JOIN 
				NIT g ON(g.CEDULA=f.NIT)
			LEFT JOIN 
				CIUDAD h ON(h.CODCIU=g.CODCIU)
			LEFT JOIN 
				CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=case when e.descripcion_ingreso ilike '%REV%INTERES%' then a.nitcli else '860028415' end)
			where 
				a.num_ingreso=r_ingreso.num_ingreso

		LOOP
			SELECT INTO _NEGOCIO CON.INTERFAZ_NEGOCIOXDOCUMENTO(r_asiento.documento_soporte);

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(R_ASIENTO.FECHA_INGRESO::DATE,1,7),'-','')=R_ASIENTO.PERIODO THEN R_ASIENTO.FECHA_INGRESO::DATE ELSE CON.SP_FECHA_CORTE_MES(SUBSTRING(R_ASIENTO.PERIODO,1,4),SUBSTRING(R_ASIENTO.PERIODO,5,2)::INT)::DATE END ;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, 'ING', R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 6)='S')then
				--MCTYPE.MC_____FECHEMIS__B = R_INGRESO.CREATION_DATE::DATE;
				MCTYPE.MC_____FECHEMIS__B = FECHADOC_::DATE;
				MCTYPE.MC_____FECHVENC__B = FECHADOC_::DATE;
			else
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';

			end if;

			MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
			MCTYPE.MC_____CODIGO____TD_____B := 'ICRN' ;
			MCTYPE.MC_____CODIGO____CD_____B := r_ingreso._CLASEDOC;
			MCTYPE.MC_____SECUINTE__DCD____B := CONSEC  ;
			MCTYPE.MC_____FECHA_____B := FECHADOC_::DATE  ;
			MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN  ;
			MCTYPE.MC_____SECUINTE__B := CONSEC  ;
			MCTYPE.MC_____REFERENCI_B := CASE WHEN _NEGOCIO IS NOT NULL THEN _NEGOCIO||';'||r_ingreso.NUM_INGRESO ELSE r_ingreso.NUM_INGRESO END;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING(R_ASIENTO.PERIODO,1,4)::INT;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING(R_ASIENTO.PERIODO,5,2)::INT;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF' ;
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, 'ING', R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 1)  ;
			raise notice '_PROCESOHOM: %',_PROCESOHOM;
			raise notice 'R_ASIENTO.CUENTA: %',R_ASIENTO.CUENTA;
			raise notice 'R_INGRESO.AGENCIA: %',R_INGRESO.AGENCIA;
			raise notice 'MCTYPE.MC_____CODIGO____CPC____B: %',MCTYPE.MC_____CODIGO____CPC____B;
			MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, 'ING', R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 2)  ;
			MCTYPE.MC_____IDENTIFIC_TERCER_B :=  CASE WHEN CHAR_LENGTH(R_ASIENTO.nit_cliente)>10 THEN SUBSTR(R_ASIENTO.nit_cliente,1,10) ELSE R_ASIENTO.nit_cliente END;
			MCTYPE.MC_____DEBMONORI_B := 0  ;
			MCTYPE.MC_____CREMONORI_B := 0 ;
			MCTYPE.MC_____DEBMONLOC_B := R_ASIENTO.VALOR_DEBITO::NUMERIC  ;
			MCTYPE.MC_____CREMONLOC_B := R_ASIENTO.VALOR_CREDITO::NUMERIC  ;
			MCTYPE.MC_____INDTIPMOV_B := 4  ;
			MCTYPE.MC_____INDMOVREV_B := 'N'  ;
			MCTYPE.MC_____OBSERVACI_B := R_ASIENTO.DESCRIPCION_INGRESO ||'- Ingreso: '||r_ingreso.num_ingreso ;
			MCTYPE.MC_____FECHORCRE_B := R_ASIENTO.FECHA_INGRESO::TIMESTAMP  ;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN'  ;
			MCTYPE.MC_____FEHOULMO__B := R_ASIENTO.FECHA_INGRESO::TIMESTAMP  ;
			MCTYPE.MC_____AUTULTMOD_B := ''  ;
			MCTYPE.MC_____VALIMPCON_B := 0  ;
			MCTYPE.MC_____NUMERO_OPER_B := R_ASIENTO.num_ingreso;
			MCTYPE.TERCER_CODIGO____TIT____B := R_ASIENTO.TERCER_CODIGO____TIT____B  ;
			MCTYPE.TERCER_NOMBCORT__B := R_ASIENTO.TERCER_NOMBCORT__B  ;
			MCTYPE.TERCER_NOMBEXTE__B := CASE WHEN CHAR_LENGTH(R_ASIENTO.TERCER_NOMBEXTE__B)>64 THEN SUBSTR(R_ASIENTO.TERCER_NOMBEXTE__B,1,64) ELSE R_ASIENTO.TERCER_NOMBEXTE__B END;
			MCTYPE.TERCER_APELLIDOS_B := R_ASIENTO.TERCER_APELLIDOS_B  ;
			MCTYPE.TERCER_CODIGO____TT_____B := R_ASIENTO.TERCER_CODIGO____TT_____B  ;
			MCTYPE.TERCER_DIRECCION_B := CASE WHEN CHAR_LENGTH(R_ASIENTO.TERCER_DIRECCION_B)>64 THEN SUBSTR(R_ASIENTO.TERCER_DIRECCION_B,1,64) ELSE R_ASIENTO.TERCER_DIRECCION_B END;
			MCTYPE.TERCER_CODIGO____CIUDAD_B := R_ASIENTO.TERCER_CODIGO____CIUDAD_B  ;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(R_ASIENTO.TERCER_TELEFONO1_B)>15 THEN SUBSTR(R_ASIENTO.TERCER_TELEFONO1_B,1,15) ELSE R_ASIENTO.TERCER_TELEFONO1_B END;
			MCTYPE.TERCER_TIPOGIRO__B := 1 ;
			MCTYPE.TERCER_CODIGO____EF_____B := ''  ;
			MCTYPE.TERCER_SUCURSAL__B := ''  ;
			MCTYPE.TERCER_NUMECUEN__B := ''  ;
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, 'ING', R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 3);
			--MCTYPE.MC_____NUMDOCSOP_B := REC_OS.NUMERO_OPERACION;
			MCTYPE.MC_____NUMEVENC__B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, 'ING', R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 5)::INT;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, 'ING', R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 4)='S')then
				if(MCTYPE.MC_____CODIGO____CPC____B IN('23809605','28150501'))then
					MCTYPE.MC_____NUMDOCSOP_B := R_ASIENTO.NEGOCIO;
				else
					MCTYPE.MC_____NUMDOCSOP_B := R_ASIENTO.DOCUMENTO_SOPORTE;
				end if;
			else
				MCTYPE.MC_____NUMDOCSOP_B := '';
			end if;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, 'ING', R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 5)::int=1)then
				MCTYPE.MC_____NUMEVENC__B := 1;
			else
				MCTYPE.MC_____NUMEVENC__B := null;
			end if;

			-- Insertamos en la tabla de Apoteosys
			--FUNCION QUE TRANSACCION POR TIPO DE DOCUMENTO EN TABLA TEMPORAL EN FINTRA.
			SW:=CON.SP_INSERT_TABLE_MC_RECAUDO____(MCTYPE);
			CONSEC:=CONSEC+1;

		END LOOP;

		---------------------------------------------------------------------------

		--------------Revision de la transaccion-----------------
		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE, 'RECAUDO') ='N' THEN
			SW='N';

			--BORRAMOS EL COMPROBANTE DE EXT
			DELETE FROM CON.mc_recaudo____
			WHERE MC_____NUMERO____B = SECUENCIA_GEN AND MC_____CODIGO____CONTAB_B = 'FINT'
			 AND MC_____CODIGO____TD_____B = 'INGN' AND  MC_____CODIGO____CD_____B = r_ingreso._CLASEDOC;

			CONTINUE;
		END IF;

		-- ACTUALIZAMOS EL CAMPO DE APOTEOSYS DE LA CABECERA DEL CRéDITO PARA INDICAR QUE YA SE ENVíO
		IF(SW='S')THEN

			UPDATE
				con.ingreso_detalle
			SET
				PROCESADO_ICA='S'
			WHERE
				TIPO_DOCUMENTO=r_ingreso.tipo_documento and
				num_ingreso=r_ingreso.num_ingreso;

			SW:='N';
		END IF;

		---------------------------------------------------------------

	END LOOP;

	RETURN 'OK';

END;

$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apoteosys.interfaz_libranza_anulacion_intereses_x_pago_total()
  OWNER TO postgres;