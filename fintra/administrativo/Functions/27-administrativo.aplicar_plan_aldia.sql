
--DROP FUNCTION administrativo.aplicar_plan_aldia(_usuario character VARYING, _negocio character VARYING, _key_ref character VARYING)

CREATE OR REPLACE FUNCTION administrativo.aplicar_plan_aldia(_usuario character VARYING, _negocio character VARYING, _key_ref character VARYING)
  RETURNS text AS
$BODY$

DECLARE
  
   _proyeccion_facturas record;
   _facturas_cartera record;
   _banco_record record;
   numero_ingreso_fg varchar; 
   _items_fg integer:=0;
   _saldosCartera record;
   _flag boolean:=TRUE;
  
BEGIN
	  
	  FOR  _proyeccion_facturas IN 
	  								SELECT (substring(py.documento,1,length(py.documento)-2)||CASE WHEN py.item < 10 THEN '0'||py.item ELSE py.item::varchar END ) AS new_documento,
								           py.fecha,
								           py.item,
								           py.cod_neg,
								           py.documento,
								           cmc.cuenta
								     FROM administrativo.proyeccion_refinanciacion_negocios py 
								     INNER JOIN con.factura f ON f.documento= py.documento AND f.negasoc=py.cod_neg
								     INNER JOIN con.cmc_doc cmc ON cmc.cmc=f.cmc AND f.tipo_documento=cmc.tipodoc
								     WHERE py.cod_neg=_negocio AND py.color='S' AND  f.valor_saldo >0 AND  py.key_ref=_key_ref
								     
	  LOOP 

	    --1.) Cabecera de la factura.
		INSERT INTO con.factura(
					            reg_status, dstrct, tipo_documento, documento, nit, codcli, concepto, 
					            fecha_factura, fecha_vencimiento, fecha_ultimo_pago, fecha_impresion, 
					            descripcion, observacion, valor_factura, valor_abono, valor_saldo, 
					            valor_facturame, valor_abonome, valor_saldome, valor_tasa, moneda, 
					            cantidad_items, forma_pago, agencia_facturacion, agencia_cobro, 
					            zona, clasificacion1, clasificacion2, clasificacion3, transaccion, 
					            transaccion_anulacion, fecha_contabilizacion, fecha_anulacion, 
					            fecha_contabilizacion_anulacion, base, last_update, user_update, 
					            creation_date, creation_user, fecha_probable_pago, flujo, rif, 
					            cmc, usuario_anulo, formato, agencia_impresion, periodo, valor_tasa_remesa, 
					            negasoc, num_doc_fen, obs, pagado_fenalco, corficolombiana, tipo_ref1, 
					            ref1, tipo_ref2, ref2, dstrct_ultimo_ingreso, tipo_documento_ultimo_ingreso, 
					            num_ingreso_ultimo_ingreso, item_ultimo_ingreso, fec_envio_fiducia, 
					            nit_enviado_fiducia, tipo_referencia_1, referencia_1, tipo_referencia_2, 
					            referencia_2, tipo_referencia_3, referencia_3, nc_traslado, fecha_nc_traslado, 
					            tipo_nc, numero_nc, factura_traslado, factoring_formula_aplicada, 
					            nit_endoso, devuelta, fc_eca, fc_bonificacion, indicador_bonificacion, 
					            fi_bonificacion, endoso_fenalco, endoso_fiducia, causacion_int_ms, 
					            fecha_causacion_int_ms, procesado)
			SELECT 
			       f.reg_status,f.dstrct,f.tipo_documento, _proyeccion_facturas.new_documento,
			       f.nit,f.codcli,f.concepto,current_date AS fecha_factura,
			       _proyeccion_facturas.fecha AS fecha_vencimiento,'0099-01-01 00:00:00'::timestamp AS fecha_ultimo_pago,f.fecha_impresion,f.descripcion,f.observacion,
			       f.valor_factura,f.valor_abono,f.valor_saldo,f.valor_facturame,f.valor_abonome,f.valor_saldome,
			       f.valor_tasa,f.moneda,f.cantidad_items,f.forma_pago,f.agencia_facturacion,f.agencia_cobro,f.zona,
			       f.clasificacion1,f.clasificacion2,f.clasificacion3,0::integer AS transaccion,f.transaccion_anulacion,
			       '0099-01-01 00:00:00'::timestamp AS fecha_contabilizacion,f.fecha_anulacion,f.fecha_contabilizacion_anulacion,f.base,'0099-01-01 00:00:00'::timestamp AS last_update,
			       ''::varchar AS user_update, current_timestamp AS creation_date, _usuario AS creation_user,f.fecha_probable_pago,f.flujo,f.rif,f.cmc,f.usuario_anulo,
			       f.formato,f.agencia_impresion,''::varchar AS periodo,f.valor_tasa_remesa,f.negasoc, _proyeccion_facturas.item AS num_doc_fen,'0'::varchar AS obs,f.pagado_fenalco,
			       f.corficolombiana,f.tipo_ref1,f.ref1,tipo_ref2,f.ref2,f.dstrct_ultimo_ingreso,f.tipo_documento_ultimo_ingreso,
			       f.num_ingreso_ultimo_ingreso,f.item_ultimo_ingreso,f.fec_envio_fiducia,f.nit_enviado_fiducia,f.tipo_referencia_1,
			       f.referencia_1,f.tipo_referencia_2,f.referencia_2,f.tipo_referencia_3,f.referencia_3,f.nc_traslado,f.fecha_nc_traslado,
			       f.tipo_nc,numero_nc,f.factura_traslado,f.factoring_formula_aplicada,f.nit_endoso,f.devuelta,f.fc_eca,f.fc_bonificacion,
			       f.indicador_bonificacion,f.fi_bonificacion,f.endoso_fenalco,f.endoso_fiducia,f.causacion_int_ms,f.fecha_causacion_int_ms,f.procesado 
			FROM  con.factura f 
			WHERE  f.negasoc=_proyeccion_facturas.cod_neg AND 
				   f.documento=_proyeccion_facturas.documento AND 
				   f.reg_status='' AND 
				   f.tipo_documento='FAC' AND 
				   f.valor_saldo > 0;
		
		 --2.) Detalle de la factura.
		 INSERT INTO con.factura_detalle(
							            reg_status, dstrct, tipo_documento, documento, item, nit, concepto, 
							            numero_remesa, descripcion, codigo_cuenta_contable, cantidad, 
							            valor_unitario, valor_unitariome, valor_item, valor_itemme, valor_tasa, 
							            moneda, last_update, user_update, creation_date, creation_user, 
							            base, auxiliar, valor_ingreso, tipo_documento_rel, transaccion, 
							            documento_relacionado, tipo_referencia_1, referencia_1, tipo_referencia_2, 
							            referencia_2, tipo_referencia_3, referencia_3)							            
		   SELECT fdet.reg_status,fdet.dstrct,fdet.tipo_documento,_proyeccion_facturas.new_documento,fdet.item,fdet.nit,fdet.concepto,
				   fdet.numero_remesa,fdet.descripcion,_proyeccion_facturas.cuenta AS codigo_cuenta_contable,fdet.cantidad,
				   fdet.valor_unitario,fdet.valor_unitariome,fdet.valor_item,fdet.valor_itemme,
				   fdet.valor_tasa,fdet.moneda,'0099-01-01 00:00:00'::timestamp AS last_update,fdet.user_update,current_timestamp AS creation_date,
				    _usuario AS creation_user,fdet.base,fdet.auxiliar,fdet.valor_ingreso,fdet.tipo_documento_rel,
				   0::integer AS transaccion,fdet.documento_relacionado,fdet.tipo_referencia_1,fdet.referencia_1,
				   fdet.tipo_referencia_2,fdet.referencia_2,fdet.tipo_referencia_3,fdet.referencia_3 
			FROM con.factura_detalle fdet 
		    INNER JOIN con.factura f ON f.documento=fdet.documento AND f.nit=fdet.nit AND f.tipo_documento=fdet.tipo_documento
			WHERE 
			     f.negasoc=_proyeccion_facturas.cod_neg AND 
				 f.documento=_proyeccion_facturas.documento AND 
				 f.reg_status='' AND 
				 f.tipo_documento='FAC' AND 
				 fdet.reg_status='' AND 
				 f.valor_saldo >0;
		
			 --saldos reales de la factura
		 	 SELECT INTO _saldosCartera documento, 
				    negocio, 
				    cuota,
				    total_factura,
				    saldo_capital,
				    saldo_interes,
				    saldo_cat,
				    saldo_cuota_manejo,
				    saldo_seguro,
				    total_abonos,
				    saldo_factura
			FROM  eg_detalle_saldo_facturas_mc(_proyeccion_facturas.cod_neg::VARCHAR,_proyeccion_facturas.item::INTEGER);
			
			IF _saldosCartera IS NOT NULL THEN 
			
				UPDATE con.factura_detalle SET valor_unitario=_saldosCartera.saldo_cat ,valor_unitariome=_saldosCartera.saldo_cat, 
											   valor_item=_saldosCartera.saldo_cat, valor_itemme=_saldosCartera.saldo_cat
					WHERE documento=_proyeccion_facturas.new_documento AND tipo_documento='FAC' AND descripcion='CAT';
					
				UPDATE con.factura_detalle SET valor_unitario=_saldosCartera.saldo_cuota_manejo ,valor_unitariome=_saldosCartera.saldo_cuota_manejo, 
											   valor_item=_saldosCartera.saldo_cuota_manejo, valor_itemme=_saldosCartera.saldo_cuota_manejo
					WHERE documento=_proyeccion_facturas.new_documento  AND tipo_documento='FAC' AND descripcion='CUOTA-ADMINISTRCION';
					
				UPDATE con.factura_detalle SET valor_unitario=_saldosCartera.saldo_interes ,valor_unitariome=_saldosCartera.saldo_interes, 
											   valor_item=_saldosCartera.saldo_interes, valor_itemme=_saldosCartera.saldo_interes 
					WHERE documento=_proyeccion_facturas.new_documento AND tipo_documento='FAC' AND descripcion='INTERES';
			
				UPDATE con.factura_detalle SET valor_unitario=_saldosCartera.saldo_seguro ,valor_unitariome=_saldosCartera.saldo_seguro, 
											   valor_item=_saldosCartera.saldo_seguro, valor_itemme=_saldosCartera.saldo_seguro 
				   WHERE documento=_proyeccion_facturas.new_documento AND tipo_documento='FAC' AND descripcion='SEGURO';
				
				UPDATE con.factura_detalle SET valor_unitario=_saldosCartera.saldo_capital ,valor_unitariome=_saldosCartera.saldo_capital ,
											   valor_item=_saldosCartera.saldo_capital, valor_itemme=_saldosCartera.saldo_capital 
				   WHERE documento=_proyeccion_facturas.new_documento  AND tipo_documento='FAC' AND descripcion='CAPITAL';
				   
				--cabecera de la factura.
				UPDATE con.factura 	SET valor_factura=valor_saldo, valor_facturame=valor_saldo, valor_abono=0.00, valor_abonome=0.00	
					WHERE  documento=_proyeccion_facturas.new_documento  AND tipo_documento='FAC';
				   
			END IF; 
		
		 --3.)Cancelamos la factura con nota de ajuste
		  FOR _facturas_cartera IN 	 							 
							 SELECT
							    fr.codcli,
								fr.nit AS nit_cliente,
								fr.documento,
								fr.valor_factura,
								fr.valor_abono,
								fr.valor_saldo,
								cmc.cuenta AS cuenta_cartera,							 	
								fr.negasoc AS negocio	 		
							 FROM con.factura fr
							 INNER JOIN con.cmc_doc cmc ON cmc.cmc=fr.cmc AND cmc.tipodoc=fr.tipo_documento
							 INNER JOIN negocios n ON n.cod_neg=fr.negasoc	 
							 WHERE  fr.reg_status=''
							 AND fr.tipo_documento='FAC'
							 AND fr.negasoc=_proyeccion_facturas.cod_neg
							 AND fr.documento=_proyeccion_facturas.documento
							 AND fr.valor_saldo > 0		
							 ORDER BY fecha_vencimiento asc 
		  LOOP
				 
			IF _items_fg=0 THEN 
					
				numero_ingreso_fg := get_lcod('ICAC');	 				
				SELECT INTO _banco_record branch_code,bank_account_no,codigo_cuenta FROM public.banco WHERE branch_code='CAJA REFINANCIA' AND bank_account_no='CAJA REFINANCIACION'; 				
			    _banco_record.branch_code='';
			    _banco_record.bank_account_no:='';
			    _banco_record.codigo_cuenta:=_facturas_cartera.cuenta_cartera;
			    
				--INSERTA LA CABECERA DEL INGRESO PARA CANCELAR LA CARTERA.
				INSERT INTO con.ingreso (dstrct,tipo_documento,num_ingreso,codcli,
						     nitcli,concepto,tipo_ingreso,fecha_consignacion,fecha_ingreso,
						     branch_code,bank_account_no,codmoneda,agencia_ingreso,descripcion_ingreso,
						     vlr_ingreso,vlr_ingreso_me,vlr_tasa,fecha_tasa,cant_item,creation_user, creation_date,
						     base,cuenta, tipo_referencia_1,referencia_1,referencia_2)
				 VALUES ('FINV', 'ICA', numero_ingreso_fg, _facturas_cartera.codcli,
				    _facturas_cartera.nit_cliente,'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
				    _banco_record.branch_code, _banco_record.bank_account_no,'PES','OP','REVERSION DE CAPITAL - PLAN AL DIA',
				    0.00, 0.00, '1.000000', CURRENT_DATE, _items_fg, _usuario, CURRENT_TIMESTAMP,
				    'COL', _banco_record.codigo_cuenta, 'NEG', _facturas_cartera.negocio, '' );	 			
			END IF;
				 			
	 		_items_fg:=_items_fg+1;
	 			
	 		 --INSERTA EL DETALLE DEL INGRESO
		     INSERT INTO con.ingreso_detalle (dstrct,tipo_documento,num_ingreso,item,
		                                             nitcli,valor_ingreso,valor_ingreso_me,factura,
		                                             fecha_factura,tipo_doc,documento,creation_user,
		                                             creation_date,base,cuenta,descripcion,
		                                             valor_tasa,saldo_factura,tipo_referencia_2, referencia_2 )
		            VALUES ('FINV', 'ICA', numero_ingreso_fg,_items_fg,
		                    _facturas_cartera.nit_cliente, _facturas_cartera.valor_saldo, _facturas_cartera.valor_saldo, _facturas_cartera.documento,
		                    CURRENT_DATE, 'FAC', _facturas_cartera.documento,  _usuario, 
		                    CURRENT_TIMESTAMP, 'COL',_facturas_cartera.cuenta_cartera,'REVERSION DE CAPITAL - PLAN AL DIA',
		                    '1.0000000000', 0.00,'NEG',_facturas_cartera.negocio);
		                    
		                    
		        -- RESTA EL SALDO DE LA CUOTA DE CAPITAL A LA CARTERA.
		        UPDATE con.factura
			        SET valor_abono   = (valor_abono + _facturas_cartera.valor_saldo),
			            valor_saldo   = (valor_saldo - _facturas_cartera.valor_saldo),
			            valor_abonome = (valor_abonome + _facturas_cartera.valor_saldo),
			            valor_saldome = (valor_saldome - _facturas_cartera.valor_saldo),
			            user_update   = _usuario,
			            fecha_ultimo_pago= CURRENT_DATE,
			            last_update   = CURRENT_TIMESTAMP
		        WHERE documento =  _facturas_cartera.documento and tipo_documento='FAC' AND reg_status='';
				        
				 
		   END LOOP; --fin loop interno
				 
		   IF numero_ingreso_fg !='' THEN  
				  --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE LAS CUOTAS DE CAPITAL.
			     UPDATE con.ingreso 
				    	SET vlr_ingreso=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_fg),
				    	    vlr_ingreso_me=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_fg),
				    	    cant_item =_items_fg
				 WHERE num_ingreso=numero_ingreso_fg AND tipo_documento='ICA';
				 
				 UPDATE administrativo.control_refinanciacion_negocios SET estado='S', nota_ajuste=numero_ingreso_fg  WHERE key_ref=_key_ref AND cod_neg=_negocio;
				 UPDATE negocios SET refinanciado='S' WHERE cod_neg=_negocio;
				 _flag:=FALSE;
		   END IF;
      END LOOP;
      
      
      --si se pagaron todas las facturas con el pago
      IF _flag THEN 
      	 UPDATE administrativo.control_refinanciacion_negocios SET estado='S', nota_ajuste=''  WHERE key_ref=_key_ref AND cod_neg=_negocio;
		 UPDATE negocios SET refinanciado='S' WHERE cod_neg=_negocio;
      END IF;
     
     RETURN 'OK';

  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION administrativo.aplicar_plan_aldia(_usuario character VARYING, _negocio character VARYING, _key_ref character VARYING)
  OWNER TO postgres;

-- SELECT administrativo.aplicar_plan_aldia('EGONZALEZ'::character VARYING, 'MC14654'::character VARYING, ''::character VARYING)

--  SELECT * FROM administrativo.control_refinanciacion_negocios WHERE cod_neg='MC15047';
--  SELECT * FROM negocios WHERE cod_neg='MC15047';
 