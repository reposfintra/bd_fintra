
-- Table: administrativo.saldos_refinanciacion_negocios

-- DROP TABLE administrativo.saldos_refinanciacion_negocios;

CREATE TABLE administrativo.saldos_refinanciacion_negocios
(
  id serial,
  reg_status character varying(1) NOT NULL DEFAULT ''::character varying,
  dstrct character varying(4) NOT NULL DEFAULT 'FINV'::character varying,
  documento character varying,
  negocio character varying,
  cuota character varying,
  fecha_vencimiento date,
  dias_mora numeric,
  esquema character varying,
  estado character varying,
  valor_saldo_capital numeric,
  valor_saldo_mi numeric,
  valor_saldo_ca numeric,
  valor_cm numeric,
  valor_seguro numeric,
  valor_saldo_cuota numeric,
  ixm numeric,
  gac numeric,
  suma_saldos NUMERIC,
  creation_date timestamp without time zone DEFAULT now(),
  creation_user character varying(10) DEFAULT ''::character varying,
  last_update timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
  user_update character varying(10) NOT NULL DEFAULT ''::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE administrativo.saldos_refinanciacion_negocios
  OWNER TO postgres;

 ALTER TABLE administrativo.saldos_refinanciacion_negocios ADD COLUMN key_ref character VARYING  NOT NULL DEFAULT ''::character VARYING;