
-- Table: administrativo.proyeccion_refinanciacion_negocios

-- DROP TABLE administrativo.proyeccion_refinanciacion_negocios;

CREATE TABLE administrativo.proyeccion_refinanciacion_negocios
(
  id serial,
  reg_status character varying(1) NOT NULL DEFAULT ''::character varying,
  dstrct character varying(4) NOT NULL DEFAULT 'FINV'::character varying,
  documento character varying(10),
  cod_neg character varying(15),
  item integer,
  fecha date,
  capital moneda,
  interes moneda,
  cat moneda,
  cuota_manejo moneda,
  valor_cuota_sin_aplicacion moneda,
  valor_cuota moneda,
  valor_extracto numeric,
  dias_vencidos integer,
  estado text,
  color character varying(1),
  aplicado character varying(1),
  creation_date timestamp without time zone DEFAULT now(),
  creation_user character varying(10) DEFAULT ''::character varying,
  last_update timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
  user_update character varying(10) NOT NULL DEFAULT ''::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE administrativo.proyeccion_refinanciacion_negocios
  OWNER TO postgres;
 
 ALTER TABLE administrativo.proyeccion_refinanciacion_negocios ADD COLUMN key_ref character VARYING  NOT NULL DEFAULT ''::character VARYING;