-- Table: administrativo.control_refinanciacion_negocios

-- DROP TABLE administrativo.control_refinanciacion_negocios;

CREATE TABLE administrativo.control_refinanciacion_negocios
(
  id serial NOT NULL,
  reg_status character varying(1) NOT NULL DEFAULT ''::character varying,
  dstrct character varying(4) NOT NULL DEFAULT ''::character varying,
  cod_neg character varying(15) NOT NULL DEFAULT ''::character varying,
  estado character varying(1) NOT NULL DEFAULT ''::character varying,
  creation_date timestamp without time zone NOT NULL DEFAULT now(),
  creation_user character varying(10) NOT NULL DEFAULT ''::character varying,
  last_update timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
  user_update character varying(10) NOT NULL DEFAULT ''::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE administrativo.control_refinanciacion_negocios
  OWNER TO postgres;

 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN valor_a_pagar NUMERIC  NOT NULL DEFAULT 0.00::NUMERIC;
 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN porc_negociacion NUMERIC  NOT NULL DEFAULT 0.00::NUMERIC;
 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN id_rop INT4  NOT NULL DEFAULT 0.00::INT4;
 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN key_ref character VARYING  NOT NULL DEFAULT ''::character VARYING;
 ALTER TABLE administrativo.control_refinanciacion_negocios ADD COLUMN nota_ajuste character VARYING  NOT NULL DEFAULT ''::character VARYING;
