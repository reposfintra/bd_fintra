-- Function: public.eg_ia_reversion_negocio(_cog_negocio varchar, _usuario CHARACTER VARYING);

-- DROP FUNCTION public.eg_ia_reversion_negocio(_cog_negocio varchar, _usuario CHARACTER VARYING);

CREATE OR REPLACE FUNCTION public.eg_ia_reversion_negocio(_cog_negocio varchar, _usuario CHARACTER VARYING)
    RETURNS CHARACTER VARYING AS $$
DECLARE

     facturas_negocio_record RECORD;
    _facturas_cartera RECORD;
    _banco_record RECORD;
    _negocio_nuevo RECORD;
    numero_ingreso_if  CHARACTER VARYING := '';
    numero_ingreso_cm  CHARACTER VARYING := '';
    numero_ingreso_ca  CHARACTER VARYING := '';
    numero_ingreso_fg  CHARACTER VARYING := '';

    _negocio_a_renovar  CHARACTER VARYING := '';
    _items_if  INTEGER := 0;
    _items_cm  INTEGER := 0;
    _items_ca   INTEGER := 0;
    _items_fg  INTEGER := 0;
    saldo_factura_ingreso NUMERIC:= 0;
     diferidos_array VARCHAR(20) [];
    _cuentas_fiducia VARCHAR(10) []:='{16252135,16252136,11xxxx}';    

    
BEGIN
	
	
  
	-- 1. REVERSION DE INTERESES,CUOTAS DE ADMINISTRACION,CAT ,SEGURO .
    FOR facturas_negocio_record IN 
    								SELECT f.documento,
    								       f.codcli AS cod_cli,
    								       f.nit AS nit_cliente,    								       
    								       f.valor_abono,
    								       f.valor_saldo, 
    								       f.num_doc_fen AS cuota,
    								       cod_neg AS negocio,
    								       interes AS interes_a_cobrar,
    								       cuota_manejo AS cuota_admon_a_cobrar,
    								       cat AS cat_a_cobrar,
    								       seguro,    								    
    								       CASE WHEN ing.endosado ='S' THEN  _cuentas_fiducia [1] ELSE cmc.cuenta  END AS cuenta_interes,
                                           CASE WHEN ing.endosado ='S' THEN  _cuentas_fiducia [2] ELSE cmc2.cuenta END AS cuenta_cuota_admin,
                                           cmc3.cuenta AS cuenta_cat,
                                           cmc4.cuenta AS cuenta_cartera,
                                           ing.cod AS diferido_if,
                                           ing2.cod AS diferido_cm,
                                           ing3.cod AS diferido_ca                                           
    								FROM TEM.backup_refinaciacion br
    								INNER JOIN con.factura f ON f.negasoc = br.cod_neg AND f.num_doc_fen=br.item
    								INNER JOIN ing_fenalco ing ON (ing.codneg = br.cod_neg AND ing.tipodoc IN  ('IF','MI') AND ing.cuota = f.num_doc_fen)
                                    INNER JOIN ing_fenalco ing2 ON (ing2.codneg = br.cod_neg AND ing2.tipodoc = 'CM' AND ing2.cuota = f.num_doc_fen)
                                    INNER JOIN ing_fenalco ing3 ON (ing3.codneg = br.cod_neg AND ing3.tipodoc = 'CA' AND ing3.cuota = f.num_doc_fen)
                                    INNER JOIN con.cmc_doc cmc ON (cmc.cmc = ing.cmc AND ing.tipodoc = cmc.tipodoc)
                                    INNER JOIN con.cmc_doc cmc2 ON (cmc2.cmc = ing2.cmc AND ing2.tipodoc = cmc2.tipodoc)
                                    INNER JOIN con.cmc_doc cmc3 ON (cmc3.cmc = ing3.cmc AND ing3.tipodoc = cmc3.tipodoc)
                                    INNER JOIN con.cmc_doc cmc4 ON (cmc4.cmc = f.cmc AND cmc4.tipodoc=f.tipo_documento)
    								WHERE br.cod_neg=_cog_negocio AND f.valor_saldo>0 AND f.reg_status=''
    
   	LOOP
           
   	    saldo_factura_ingreso := facturas_negocio_record.valor_saldo;
	     --ANULACION DE LOS INTERESES FUTUROS
        IF facturas_negocio_record.interes_a_cobrar > 0 THEN

        	--inserta las cabeceras de los ingresos. 
        	IF _items_if=0 THEN   
        	
 				--OBTENIE EL NUMERO DE INGRESO PARA IA DE INTERESES
	            numero_ingreso_if := get_lcod('ICAC'); 	           
	            
	            --INSERTA LA CABECERA DEL INGRESO INTERESES
	            INSERT INTO con.ingreso(dstrct,tipo_documento,num_ingreso,codcli,
	                                     nitcli,concepto,tipo_ingreso,fecha_consignacion,fecha_ingreso,
	                                     branch_code,bank_account_no,codmoneda,agencia_ingreso,descripcion_ingreso,
	                                     vlr_ingreso,vlr_ingreso_me,vlr_tasa,fecha_tasa,cant_item,creation_user, creation_date,
	                                     base,cuenta, tipo_referencia_1,referencia_1,referencia_2)
	           	 VALUES ('FINV', 'ICA', numero_ingreso_if, facturas_negocio_record.cod_cli,
	                    facturas_negocio_record.nit_cliente,'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
	                    '', '','PES','OP','REVERSION INTERESES DE FINANCIACION',
	                    0.00, 0.00, '1.000000', CURRENT_DATE, _items_if, _usuario, CURRENT_TIMESTAMP,
	                    'COL', facturas_negocio_record.cuenta_interes, 'NEG', facturas_negocio_record.negocio, '' );
		                   
             END IF;

             _items_if:=_items_if+1;	
             saldo_factura_ingreso:=saldo_factura_ingreso-facturas_negocio_record.interes_a_cobrar;
          
	         --INSERTA EL DETALLE DEL INGRESO INTERESES
	         INSERT INTO con.ingreso_detalle (dstrct,tipo_documento,num_ingreso,item,
	                                             nitcli,valor_ingreso,valor_ingreso_me,factura,
	                                             fecha_factura,tipo_doc,documento,creation_user,
	                                             creation_date,base,cuenta,descripcion,
	                                             valor_tasa,saldo_factura,tipo_referencia_2, referencia_2 )
	            VALUES ('FINV', 'ICA', numero_ingreso_if,_items_if,
	                    facturas_negocio_record.nit_cliente, facturas_negocio_record.interes_a_cobrar, facturas_negocio_record.interes_a_cobrar, facturas_negocio_record.documento,
	                    CURRENT_DATE, 'FAC', facturas_negocio_record.documento,  _usuario, 
	                    CURRENT_TIMESTAMP, 'COL',facturas_negocio_record.cuenta_cartera,'REVERSION INTERESES DE FINANCIACION',
	                    '1.0000000000', saldo_factura_ingreso,'IF',facturas_negocio_record.diferido_if);
	                    
            -- RESTA EL SALDO DEL INTERES A LA CARTERA.
	        UPDATE con.factura
		        SET valor_abono   = (valor_abono + facturas_negocio_record.interes_a_cobrar),
		            valor_saldo   = (valor_saldo - facturas_negocio_record.interes_a_cobrar),
		            valor_abonome = (valor_abonome + facturas_negocio_record.interes_a_cobrar),
		            valor_saldome = (valor_saldome - facturas_negocio_record.interes_a_cobrar),
		            user_update   = _usuario,
		            fecha_ultimo_pago= CURRENT_DATE,
		            last_update   = CURRENT_TIMESTAMP
	        WHERE documento = facturas_negocio_record.documento and tipo_documento='FAC';
	                    
			--AGREGAMOS LOS DIFERIDOS DE ITERESES PARA ANULARLOS
            diferidos_array := array_append(diferidos_array, facturas_negocio_record.diferido_if);
            
        END IF;
        
         --ANULACION DE LAS CUAOTAS DE ADMINISTRACION FUTURAS
        IF facturas_negocio_record.cuota_admon_a_cobrar > 0  then
        	
        	IF _items_cm=0 THEN 
        	
        			--OBTENIE EL NUMERO DE INGRESO PARA IA DE CUOTAS DE ADMINISTRACION
        		    numero_ingreso_cm := get_lcod('ICAC'); 
        		    
		            --INSERTA LA CABECERA DEL INGRESO INTERESES
		            INSERT INTO con.ingreso (dstrct,tipo_documento,num_ingreso,codcli,
		                                     nitcli,concepto,tipo_ingreso,fecha_consignacion,fecha_ingreso,
		                                     branch_code,bank_account_no,codmoneda,agencia_ingreso,descripcion_ingreso,
		                                     vlr_ingreso,vlr_ingreso_me,vlr_tasa,fecha_tasa,cant_item,creation_user, creation_date,
		                                     base,cuenta, tipo_referencia_1,referencia_1,referencia_2)
		           	 VALUES ('FINV', 'ICA', numero_ingreso_cm, facturas_negocio_record.cod_cli,
		                    facturas_negocio_record.nit_cliente,'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
		                    '', '','PES','OP','REVERSION CUOTAS ADMINISTRACION',
		                    0.00, 0.00, '1.000000', CURRENT_DATE, _items_cm, _usuario, CURRENT_TIMESTAMP,
		                    'COL', facturas_negocio_record.cuenta_cuota_admin, 'NEG', facturas_negocio_record.negocio, '' );
        	
        	END IF;
        	
        	_items_cm:=_items_cm+1;
        	saldo_factura_ingreso:=saldo_factura_ingreso-facturas_negocio_record.cuota_admon_a_cobrar;
        	
           --INSERTA EL DETALLE DEL INGRESO PARA LAS CUOTAS DE ADMINISTRACION.
	        INSERT INTO con.ingreso_detalle (dstrct,tipo_documento,num_ingreso,item,
	                                         nitcli,valor_ingreso,valor_ingreso_me,factura,
	                                         fecha_factura,tipo_doc,documento,creation_user,
	                                         creation_date,base,cuenta,descripcion,
	                                         valor_tasa,saldo_factura,tipo_referencia_2, referencia_2 )
	            VALUES ('FINV', 'ICA', numero_ingreso_cm,_items_cm,
	                    facturas_negocio_record.nit_cliente, facturas_negocio_record.cuota_admon_a_cobrar, facturas_negocio_record.cuota_admon_a_cobrar, facturas_negocio_record.documento,
	                    CURRENT_DATE, 'FAC', facturas_negocio_record.documento,  _usuario, 
	                    CURRENT_TIMESTAMP, 'COL',facturas_negocio_record.cuenta_cartera,'REVERSION CUOTAS ADMINISTRACION',
	                    '1.0000000000', saldo_factura_ingreso,'CM',facturas_negocio_record.diferido_cm);
	                    
	        -- RESTA EL SALDO DE LA CUOTA DE ADMINISTRACION A LA CARTERA.
	        UPDATE con.factura
		        SET valor_abono   = (valor_abono + facturas_negocio_record.cuota_admon_a_cobrar),
		            valor_saldo   = (valor_saldo - facturas_negocio_record.cuota_admon_a_cobrar),
		            valor_abonome = (valor_abonome + facturas_negocio_record.cuota_admon_a_cobrar),
		            valor_saldome = (valor_saldome - facturas_negocio_record.cuota_admon_a_cobrar),
		            user_update   = _usuario,
		            fecha_ultimo_pago= CURRENT_DATE,
		            last_update   = CURRENT_TIMESTAMP
	        WHERE documento = facturas_negocio_record.documento and tipo_documento='FAC';
	                    
	         --AGREGAMOS LOS DIFERIDOS CUOTA DE ADMINISTRACION PARA ANULARLOS
	         diferidos_array := array_append(diferidos_array, facturas_negocio_record.diferido_cm);       
        
        
        END IF;
        
        
        --ANULACION DEl CAT DEL NEGOCIO 
        IF facturas_negocio_record.cat_a_cobrar > 0  then
        	
        	IF _items_ca=0 THEN 
        	
        			--OBTENIE EL NUMERO DE INGRESO PARA IA DE CUOTAS DE CAT
        		    numero_ingreso_ca := get_lcod('ICAC'); 
        		    
		            --INSERTA LA CABECERA DEL INGRESO CAT
		            INSERT INTO con.ingreso (dstrct,tipo_documento,num_ingreso,codcli,
		                                     nitcli,concepto,tipo_ingreso,fecha_consignacion,fecha_ingreso,
		                                     branch_code,bank_account_no,codmoneda,agencia_ingreso,descripcion_ingreso,
		                                     vlr_ingreso,vlr_ingreso_me,vlr_tasa,fecha_tasa,cant_item,creation_user, creation_date,
		                                     base,cuenta, tipo_referencia_1,referencia_1,referencia_2)
		           	 VALUES ('FINV', 'ICA', numero_ingreso_ca, facturas_negocio_record.cod_cli,
		                    facturas_negocio_record.nit_cliente,'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
		                    '', '','PES','OP','REVERSION CUOTAS DEL CAT',
		                    0.00, 0.00, '1.000000', CURRENT_DATE, _items_ca, _usuario, CURRENT_TIMESTAMP,
		                    'COL', facturas_negocio_record.cuenta_cat, 'NEG', facturas_negocio_record.negocio, '' );
        	
        	END IF;
        	
        	_items_ca:=_items_ca+1;
        	saldo_factura_ingreso:=saldo_factura_ingreso-facturas_negocio_record.cat_a_cobrar;
        	
           --INSERTA EL DETALLE DEL INGRESO PARA LAS CUOTAS DE ADMINISTRACION.
	        INSERT INTO con.ingreso_detalle (dstrct,tipo_documento,num_ingreso,item,
	                                         nitcli,valor_ingreso,valor_ingreso_me,factura,
	                                         fecha_factura,tipo_doc,documento,creation_user,
	                                         creation_date,base,cuenta,descripcion,
	                                         valor_tasa,saldo_factura,tipo_referencia_2, referencia_2 )
	            VALUES ('FINV', 'ICA', numero_ingreso_ca,_items_ca,
	                    facturas_negocio_record.nit_cliente, facturas_negocio_record.cat_a_cobrar, facturas_negocio_record.cat_a_cobrar, facturas_negocio_record.documento,
	                    CURRENT_DATE, 'FAC', facturas_negocio_record.documento,  _usuario, 
	                    CURRENT_TIMESTAMP, 'COL',facturas_negocio_record.cuenta_cartera,'REVERSION CUOTAS DEL CAT',
	                    '1.0000000000', saldo_factura_ingreso,'CA',facturas_negocio_record.diferido_ca);
	                    
	        -- RESTA EL SALDO DE LA CUOTA DE CAT A LA CARTERA.
	        UPDATE con.factura
		        SET valor_abono   = (valor_abono + facturas_negocio_record.cat_a_cobrar),
		            valor_saldo   = (valor_saldo - facturas_negocio_record.cat_a_cobrar),
		            valor_abonome = (valor_abonome + facturas_negocio_record.cat_a_cobrar),
		            valor_saldome = (valor_saldome - facturas_negocio_record.cat_a_cobrar),
		            user_update   = _usuario,
		            fecha_ultimo_pago= CURRENT_DATE,
		            last_update   = CURRENT_TIMESTAMP
	        WHERE documento = facturas_negocio_record.documento and tipo_documento='FAC';
	                    
	         --AGREGAMOS LOS DIFERIDOS CUOTA DE ADMINISTRACION PARA ANULARLOS
	         diferidos_array := array_append(diferidos_array, facturas_negocio_record.diferido_ca);               
        
        END IF;
        
        
        
        --ANULAR LOS DIFERIDOS INTERESES Y CUOTAS DE ADMINISTRACION.
        UPDATE ing_fenalco
        SET reg_status  = 'A',
            user_update = _usuario,
            last_update = CURRENT_TIMESTAMP
        WHERE codneg = facturas_negocio_record.negocio 
        AND cod = ANY (diferidos_array) AND  (periodo = '' OR periodo isnull)
        AND procesado_dif != 'S' AND cuota = facturas_negocio_record.cuota;
        
        
        diferidos_array := NULL;
        
    END LOOP;
    
    IF numero_ingreso_if != ''  THEN     
     --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE INTERESES.
		     UPDATE con.ingreso 
			    	SET vlr_ingreso=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_if),
			    	    vlr_ingreso_me=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_if),
			    	    cant_item =_items_if
			 WHERE num_ingreso=numero_ingreso_if AND tipo_documento='ICA';    
    END IF; 
    
	 IF numero_ingreso_cm !='' THEN 
	 --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE LAS CUOTAS DE AMINISTRACION.
	     UPDATE con.ingreso 
		    	SET vlr_ingreso=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_cm),
		    	    vlr_ingreso_me=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_cm),
		    	    cant_item =_items_cm
		 WHERE num_ingreso=numero_ingreso_cm AND tipo_documento='ICA';
	 END IF; 
	 
	 
	  IF numero_ingreso_ca !='' THEN 
	 --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE LAS CUOTAS DEl CAT .
	     UPDATE con.ingreso 
		    	SET vlr_ingreso=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_ca),
		    	    vlr_ingreso_me=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_ca),
		    	    cant_item =_items_ca
		 WHERE num_ingreso=numero_ingreso_ca AND tipo_documento='ICA';
	 END IF; 
	
	 
	 
	 -- 2.) REVERSION DE LA CARTERA DEL CLIENTE.
	 FOR _facturas_cartera IN 	 							 
							 SELECT
							 		f.nit AS nit_cliente,
							 		f.documento,
							 		f.valor_factura,
							 		f.valor_abono,
							 		f.valor_saldo,
							 		cmc.cuenta AS cuenta_cartera,							 	
							 		f.negasoc AS negocio	 		
							 FROM con.factura f
							 INNER JOIN tem.backup_refinaciacion br ON f.negasoc=br.cod_neg AND f.num_doc_fen=br.item
							 INNER JOIN con.cmc_doc cmc ON cmc.cmc=f.cmc AND cmc.tipodoc=f.tipo_documento
							 INNER JOIN negocios n ON n.cod_neg=f.negasoc	 
							 WHERE  f.reg_status=''
							 	AND f.tipo_documento='FAC'
							 	AND f.negasoc=_cog_negocio
							 	AND f.valor_saldo > 0							 	
	 LOOP
	 
 			IF _items_fg=0 THEN 
 		
 				numero_ingreso_fg := get_lcod('ICAC');	 				
 				SELECT INTO _banco_record branch_code,bank_account_no,codigo_cuenta FROM public.banco WHERE branch_code='CAJA REFINANCIA' AND bank_account_no='CAJA REFINANCIACION'; 				
 			
 				
 				--INSERTA LA CABECERA DEL INGRESO PARA CANCELAR LA CARTERA.
	            INSERT INTO con.ingreso (dstrct,tipo_documento,num_ingreso,codcli,
               		                     nitcli,concepto,tipo_ingreso,fecha_consignacion,fecha_ingreso,
	                                     branch_code,bank_account_no,codmoneda,agencia_ingreso,descripcion_ingreso,
	                                     vlr_ingreso,vlr_ingreso_me,vlr_tasa,fecha_tasa,cant_item,creation_user, creation_date,
	                                     base,cuenta, tipo_referencia_1,referencia_1,referencia_2)
	           	 VALUES ('FINV', 'ICA', numero_ingreso_fg, facturas_negocio_record.cod_cli,
	                    _facturas_cartera.nit_cliente,'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
	                    _banco_record.branch_code, _banco_record.bank_account_no,'PES','OP','REVERSION DE CAPITAL.',
	                    0.00, 0.00, '1.000000', CURRENT_DATE, _items_fg, _usuario, CURRENT_TIMESTAMP,
	                    'COL', _banco_record.codigo_cuenta, 'NEG', facturas_negocio_record.negocio, '' );	 			
 			END IF;
	 			
 			_items_fg:=_items_fg+1;
 			
 			 --INSERTA EL DETALLE DEL INGRESO INTERESES
	         INSERT INTO con.ingreso_detalle (dstrct,tipo_documento,num_ingreso,item,
	                                             nitcli,valor_ingreso,valor_ingreso_me,factura,
	                                             fecha_factura,tipo_doc,documento,creation_user,
	                                             creation_date,base,cuenta,descripcion,
	                                             valor_tasa,saldo_factura,tipo_referencia_2, referencia_2 )
	            VALUES ('FINV', 'ICA', numero_ingreso_fg,_items_fg,
	                    _facturas_cartera.nit_cliente, _facturas_cartera.valor_saldo, _facturas_cartera.valor_saldo, _facturas_cartera.documento,
	                    CURRENT_DATE, 'FAC', _facturas_cartera.documento,  _usuario, 
	                    CURRENT_TIMESTAMP, 'COL',_facturas_cartera.cuenta_cartera,'REVERSION DE CAPITAL - RENOVACION DE CREDITOS EDUC.',
	                    '1.0000000000', 0.00,'','');
	                    
	                    
	        -- RESTA EL SALDO DE LA CUOTA DE CAPITAL A LA CARTERA.
	        UPDATE con.factura
		        SET valor_abono   = (valor_abono + _facturas_cartera.valor_saldo),
		            valor_saldo   = (valor_saldo - _facturas_cartera.valor_saldo),
		            valor_abonome = (valor_abonome + _facturas_cartera.valor_saldo),
		            valor_saldome = (valor_saldome - _facturas_cartera.valor_saldo),
		            user_update   = _usuario,
		            fecha_ultimo_pago= CURRENT_DATE,
		            last_update   = CURRENT_TIMESTAMP
	        WHERE documento =  _facturas_cartera.documento and tipo_documento='FAC';
	        
	 
	 END LOOP;
	 
	  IF numero_ingreso_fg !='' THEN  
		  --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE LAS CUOTAS DE CAPITAL.
	     UPDATE con.ingreso 
		    	SET vlr_ingreso=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_fg),
		    	    vlr_ingreso_me=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_fg),
		    	    cant_item =_items_fg
		 WHERE num_ingreso=numero_ingreso_fg AND tipo_documento='ICA';
		 
      END IF;
      
      RAISE NOTICE 'numero_ingreso_if : %',numero_ingreso_if;
      RAISE NOTICE 'numero_ingreso_cm : %',numero_ingreso_cm;
      RAISE NOTICE 'numero_ingreso_ca : %',numero_ingreso_ca;
      RAISE NOTICE 'numero_ingreso_fg : %',numero_ingreso_fg;
    RETURN 'OK';
END;
$$
LANGUAGE plpgsql
VOLATILE;
ALTER FUNCTION public.eg_ia_reversion_negocio(_cog_negocio varchar, _usuario CHARACTER VARYING)
    OWNER TO postgres;

-- SELECT public.eg_ia_reversion_negocio('MC15923'::varchar, 'EDGARGM87'::CHARACTER VARYING);
   
  