CREATE OR REPLACE FUNCTION fn_consultar_documentos_negocios(_cod_neg character varying)
  RETURNS SETOF record AS
$BODY$

declare

	_documentos_negocios record;

BEGIN

	for _documentos_negocios in SELECT tipo_documento::varchar, documento::varchar, proveedor::varchar, descripcion::varchar, documento_relacionado::varchar, vlr_neto::numeric, fecha_documento::date, periodo::varchar
								FROM fin.cxp_doc
								WHERE documento_relacionado = _cod_neg
								UNION ALL 
								SELECT tipo_documento::varchar, documento::varchar, nit::varchar, descripcion::varchar, negasoc::varchar, valor_factura::numeric, fecha_factura::date, periodo::varchar
								FROM con.factura fac
								WHERE negasoc = _cod_neg
								UNION ALL 
								SELECT e.tipo_documento::varchar, document_no::varchar, nit::varchar, payment_name::varchar, cxp.documento::varchar, vlr::numeric, fecha_cheque::date, e.periodo::varchar
								FROM egreso e
								INNER JOIN fin.cxp_doc cxp ON
								cxp.cheque = e.document_no
								WHERE cxp.documento_relacionado = _cod_neg
								AND cxp.cheque ilike 'EG%'
								UNION ALL 
								SELECT ing.tipodoc::varchar, ing.cod::varchar, ing.nit::varchar, 'Diferido ' || ing.cod::varchar AS descripcion, ing.codneg::varchar, ing.valor::numeric, ing.fecha_doc::date, ing.periodo::varchar
								FROM ing_fenalco ing
								WHERE codneg = _cod_neg
								ORDER BY tipo_documento, documento
	
	loop
	
	
	RETURN next _documentos_negocios;
	
	end loop;
	
END;
$BODY$
LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION fn_consultar_documentos_negocios(character varying)
  OWNER TO postgres;