
-- Function: eg_proyeccion_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING, _valor_pago NUMERIC );

-- DROP FUNCTION eg_proyeccion_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING, _valor_pago NUMERIC );

CREATE OR REPLACE FUNCTION eg_proyeccion_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING, _valor_pago NUMERIC )
  RETURNS SETOF tem.proyeccion_negocios_refinanciacion AS
$BODY$
DECLARE
  _maxFechaPago date ;
  _maxCuota integer:=0;
  _facturasFuturas record;
  _contadorCuotas  integer:=1;
  _saldo_aplicacion NUMERIC:=_valor_pago;
  _flag boolean:=TRUE;
  _rs tem.proyeccion_negocios_refinanciacion;

BEGIN
    --## Eliminamos tabla temporal 
    TRUNCATE TABLE tem.proyeccion_negocios_refinanciacion;
    
	--## Buscamos la fecha de pago de la ultima cuota.
	_maxFechaPago:=(SELECT MAX(fecha)::date AS max_fecha_vencimiento FROM documentos_neg_aceptado WHERE cod_neg=_cod_neg AND reg_status='');
	_maxCuota :=(SELECT count(0) AS max_cuota FROM documentos_neg_aceptado WHERE cod_neg=_cod_neg AND reg_status='');
	--##Recorremos las cuotas futuras 
	 FOR _facturasFuturas IN (
	 						   SELECT 
								       f.documento,
									   dna.cod_neg,
								       dna.item::integer,
								       dna.fecha::date,
								       dna.capital,
								       dna.interes,
								       dna.cat,
								       dna.cuota_manejo,
								       f.valor_saldo AS valor_cuota_sin_aplicacion,
								       f.valor_saldo AS valor_cuota,								      
								       (current_date-f.fecha_vencimiento::date) AS dias_vencidos,
								       CASE WHEN to_char(f.fecha_vencimiento::date,'YYYYMM')::integer =to_char(current_date,'YYYYMM')::integer THEN 'CORRIENTE'
								             WHEN (current_date-f.fecha_vencimiento::date) > 0 THEN 'VENCIDO'
								            WHEN (current_date-f.fecha_vencimiento::date) < 0 THEN 'FUTURO'
								       END AS estado,
								       'N'::varchar AS color,
								       'N'::varchar AS aplicado
								FROM documentos_neg_aceptado dna
								INNER JOIN con.factura f ON f.negasoc=dna.cod_neg AND dna.item=f.num_doc_fen
								WHERE dna.cod_neg=_cod_neg 
								AND DNA.reg_status=''
								AND f.valor_saldo >0
								AND f.reg_status=''
								AND f.tipo_documento='FAC'
								AND to_char(f.fecha_vencimiento,'YYYYMM')::INTEGER >TO_CHAR(CURRENT_DATE,'YYYYMM')::INTEGER
								ORDER BY f.num_doc_fen::integer			
								)
								UNION ALL
							   (
								SELECT 
								       f.documento,
									   dna.cod_neg,
								       dna.item::integer,
								       dna.fecha::date ,
								       dna.capital,
								       dna.interes,
								       dna.cat,
								       dna.cuota_manejo,
								       f.valor_saldo AS valor_cuota_sin_aplicacion,
								       f.valor_saldo AS valor_cuota,
								       (current_date-f.fecha_vencimiento::date) AS dias_vencidos,
								        CASE WHEN to_char(f.fecha_vencimiento::date,'YYYYMM')::integer =to_char(current_date,'YYYYMM')::integer THEN 'CORRIENTE'
								             WHEN (current_date-f.fecha_vencimiento::date) > 0 THEN 'VENCIDO'
								            WHEN (current_date-f.fecha_vencimiento::date) < 0 THEN 'FUTURO'
								       END AS estado,
								       'S'::varchar AS color,
								       'N'::varchar AS aplicado
								FROM documentos_neg_aceptado dna
								INNER JOIN con.factura f ON f.negasoc=dna.cod_neg AND dna.item=f.num_doc_fen
								WHERE dna.cod_neg=_cod_neg
								AND DNA.reg_status=''
								AND f.valor_saldo >0
								AND f.reg_status=''
								AND f.tipo_documento='FAC'
								AND to_char(f.fecha_vencimiento,'YYYYMM')::INTEGER <=TO_CHAR(CURRENT_DATE,'YYYYMM')::INTEGER
								ORDER BY f.num_doc_fen::integer desc
								)
	
     LOOP 
     				IF _facturasFuturas.estado IN  ('CORRIENTE','VENCIDO')THEN   
     				
     					IF _contadorCuotas =1 THEN 
     						_contadorCuotas :=_maxCuota;
     					END IF;
     				   _contadorCuotas:=_contadorCuotas+1;
     				   _facturasFuturas.item := _contadorCuotas;
     				   _maxFechaPago := (_maxFechaPago + '1 month'::INTERVAL)::date;
     				   _facturasFuturas.fecha := _maxFechaPago;
     				   _facturasFuturas.dias_vencidos:=(current_date-_maxFechaPago::date) ;
     				   _facturasFuturas.estado ='FUTURO';
     				   
     				ELSE 
     			
     				   _contadorCuotas:=_facturasFuturas.item; 
     				END IF; 
     				INSERT INTO tem.proyeccion_negocios_refinanciacion(documento,cod_neg,item,fecha,capital,
  		 											interes,cat,cuota_manejo,valor_cuota,dias_vencidos,estado,color,aplicado,valor_cuota_sin_aplicacion)
  		 			VALUES (_facturasFuturas.documento,_facturasFuturas.cod_neg,_facturasFuturas.item,_facturasFuturas.fecha,_facturasFuturas.capital,
  		 					_facturasFuturas.interes,_facturasFuturas.cat,_facturasFuturas.cuota_manejo,_facturasFuturas.valor_cuota,_facturasFuturas.dias_vencidos,
  		 					_facturasFuturas.estado,_facturasFuturas.color,_facturasFuturas.aplicado, _facturasFuturas.valor_cuota_sin_aplicacion);
     			
     END LOOP; 
				
    --Simulacion del pago sobre saldo 
    
     FOR _facturasFuturas IN SELECT documento,
     								cod_neg,
     								item,
     								fecha,
     								capital,
  		 							interes,
  		 							cat,
  		 							cuota_manejo,
  		 							valor_cuota_sin_aplicacion,
  		 							valor_cuota,
  		 							dias_vencidos,
  		 							estado,
  		 							color,
  		 							aplicado
  		 					  FROM  tem.proyeccion_negocios_refinanciacion WHERE cod_neg =_cod_neg ORDER BY item DESC
    LOOP
                  IF _flag THEN 
                        IF _saldo_aplicacion !=0 THEN 
                  		   _saldo_aplicacion:=_facturasFuturas.valor_cuota - _saldo_aplicacion;          
		    			   IF _saldo_aplicacion > 0  THEN 
		                   		_facturasFuturas.valor_cuota=_saldo_aplicacion;  
		                   		_facturasFuturas.aplicado='P';
		                   		_flag:=FALSE;
		                   ELSIF _saldo_aplicacion < 0 THEN 
		                   	    _facturasFuturas.valor_cuota=0.00;
		                   	    _facturasFuturas.aplicado='T';
		                   		_saldo_aplicacion:=_saldo_aplicacion*-1;
		                   ELSE 
		                        _facturasFuturas.valor_cuota=0.00;
		                        _facturasFuturas.aplicado='T';
		                        _saldo_aplicacion:=0.00;
		                        _flag:=FALSE;
		                   END IF;
		                END IF; 
                   END IF;
                   
                   _rs:=_facturasFuturas;
                   RETURN NEXT _rs;
    				
    END LOOP;



END;$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION eg_proyeccion_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING, _valor_pago NUMERIC )
  OWNER TO postgres;