-- Function: sp_seguimientocarterageotech2(numeric, character varying, character varying)
-- DROP FUNCTION sp_seguimientocarterageotech2(numeric, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_seguimientocarterageotech(_periodoasignacion numeric, _unidadnegocio character varying, _agenteext character varying)
  RETURNS SETOF record AS
$BODY$

DECLARE

	RsCarteraTotales record;

BEGIN

	
	IF ( _unidadnegocio != 29 ) THEN
		
		FOR RsCarteraTotales IN	
			
			select * from sp_seguimientocartera2(_periodoasignacion, _unidadnegocio, _agenteext) as coco(cedula varchar, nombre_cliente varchar, direccion varchar, ciudad varchar, telefono varchar, telcontacto varchar, negocio varchar, id_convenio varchar, pagaduria varchar, cuota varchar, valor_asignado numeric, fecha_vencimiento date, periodo_vcto numeric, vencimiento_mayor varchar, dias_vencidos numeric, dia_pago numeric, status varchar, status_vencimiento varchar, debido_cobrar numeric, recaudosxcuota_fiducia numeric, recaudosxcuota_fenalco numeric, recaudosxcuota numeric, agente varchar, agente_campo varchar)
			
		LOOP
			
			RETURN NEXT RsCarteraTotales;		
			
		END LOOP;
		
	ELSE
		
		FOR RsCarteraTotales IN	
			
			select * from sp_seguimiento_cartera_geotech_migrar(_periodoasignacion, _unidadnegocio, _agenteext) as coco(cedula varchar, nombre_cliente varchar, direccion varchar, ciudad varchar, telefono varchar, telcontacto varchar, negocio varchar, id_convenio varchar, pagaduria varchar, cuota varchar, valor_asignado numeric, fecha_vencimiento date, periodo_vcto numeric, vencimiento_mayor varchar, dias_vencidos numeric, dia_pago numeric, status varchar, status_vencimiento varchar, debido_cobrar numeric, recaudosxcuota_fiducia numeric, recaudosxcuota_fenalco numeric, recaudosxcuota numeric, agente varchar, agente_campo varchar)
			
		LOOP
			
			RETURN NEXT RsCarteraTotales;		
			
		END LOOP;	
		
		
		
	END IF;
	

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION sp_seguimientocarterageotech2(numeric, character varying, character varying)
  OWNER TO postgres;


