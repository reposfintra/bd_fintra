-- Function: fn_cxc_caja_recaudo(items_cxp text[], usuario character varying,bank character varying)

-- DROP FUNCTION fn_cxc_caja_recaudo(items_cxp text[], usuario character varying,bank character varying);

CREATE OR REPLACE FUNCTION fn_cxc_caja_recaudo(items_cxp text[], usuario character varying,bank character varying,fecha character varying)
  RETURNS text AS
$BODY$
DECLARE

rs text :='OK';
total_factura numeric:=0;
DetalleCXC record;
_caja record;
items integer:=0;
_num_documento varchar:='' ;
_cod_cli varchar:='' ;


BEGIN
			SELECT  into _num_documento PREFIX||LPAD(LAST_NUMBER, 7, '0') FROM SERIES WHERE DOCUMENT_TYPE = 'FAC' AND ID=1391 AND REG_STATUS='';	
	
			SELECT INTO _caja table_code,descripcion,dato,entidad from tablagen where table_code=bank and table_type='CAJA';
			
			select into _cod_cli codcli from cliente where nit=_caja.dato;
				
         	raise notice '_num_documento: %',_num_documento;
         
			INSERT INTO con.factura (
            dstrct,documento,tipo_documento,nit,codcli,concepto,fecha_factura,fecha_vencimiento,descripcion,observacion,
            valor_factura,valor_saldo, valor_facturame,valor_saldome,valor_tasa,moneda,cantidad_items,forma_pago,
            agencia_facturacion,agencia_cobro,base,last_update,user_update,creation_date,creation_user,cmc,agencia_impresion) 
            VALUES 
            ('FINV',_num_documento,'FAC',_caja.dato,_cod_cli,'TR',now()::date,now()::date+1,'RECAUDOS '||_caja.descripcion||' '||fecha,'RECAUDOS '||_caja.descripcion||' '||fecha,
            0,0,0,0,1,'PES',1,'CREDITO',
            'OP','BQ','COL',NOW(),usuario,now(),usuario,'SE','OP');


			raise notice 'items_cxp::varchar[]= %', items_cxp::varchar[];
			
                                   
					FOR DetalleCXC in (select tipo_documento,num_ingreso,nitcli,fecha_consignacion,fecha_ingreso,bank_account_no,
										            descripcion_ingreso,periodo,vlr_ingreso from con.ingreso 
										            where num_ingreso=ANY (items_cxp::varchar[]) and reg_status='' and referencia_3 != 'P')
					loop
							items := items+1;
						   raise notice 'DetalleCXC.num_ingreso: %',DetalleCXC.num_ingreso;
						  
							INSERT INTO con.factura_detalle (dstrct,tipo_documento, documento, item, nit, concepto, descripcion,
							codigo_cuenta_contable,  cantidad, valor_unitario,valor_unitariome,valor_item,valor_itemme,valor_tasa,
							moneda,base,last_update,user_update,creation_date,creation_user,tipo_documento_rel,documento_relacionado) 
							VALUES ('FINV','FAC',_num_documento,items,_cod_cli,'TR','RECAUDOS '||_caja.descripcion||' '||DetalleCXC.num_ingreso,
							_caja.entidad,1,DetalleCXC.vlr_ingreso,DetalleCXC.vlr_ingreso,DetalleCXC.vlr_ingreso,DetalleCXC.vlr_ingreso,1,
							'PES','COL',now(),usuario,now(),usuario,DetalleCXC.tipo_documento,DetalleCXC.num_ingreso);
	
							total_factura :=  total_factura + DetalleCXC.vlr_ingreso;
						
							UPDATE con.ingreso
							   SET
							   last_update=NOW(),
							   referencia_3 = 'P'
							 WHERE
							 num_ingreso = DetalleCXC.num_ingreso;
					 
					END LOOP;
					
			    UPDATE SERIES SET LAST_NUMBER = LAST_NUMBER+1 WHERE  DOCUMENT_TYPE = 'FAC' AND ID=1391 AND REG_STATUS=''; 

					UPDATE con.factura
					   SET
					    valor_factura=valor_factura+total_factura, --total_factura,
					    valor_facturame=valor_facturame+total_factura,--total_factura,
					    valor_saldo=valor_saldo+total_factura,--total_factura,
					    valor_saldome=valor_saldome+total_factura,--total_factura
					    cantidad_items=items
					 WHERE
					 documento = _num_documento;
			
			
raise notice 'rs %',rs;
raise notice '_codigo_cxp_aseguradora %',_num_documento;

	RETURN 	 rs||';'||_num_documento;

END $BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION fn_cxc_caja_recaudo(text[], character varying, character varying)
  OWNER TO postgres;
