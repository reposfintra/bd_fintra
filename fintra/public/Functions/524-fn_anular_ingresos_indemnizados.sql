-- DROP FUNCTION  fn_anular_ingresos_indemnizados(INTEGER, CHARACTER VARYING, CHARACTER VARYING);

-- SELECT fn_anular_ingresos_indemnizados(_unidad_negocio, _periodo, _usuario);

CREATE OR REPLACE FUNCTION fn_anular_ingresos_indemnizados(_unidad_negocio INTEGER, _periodo CHARACTER VARYING, _usuario CHARACTER VARYING)
    RETURNS CHARACTER VARYING AS $$
DECLARE

    /**************************************************************
    * Autor: Juan David Bermudez                                  *
    * Fecha: 2018-10-11                                           *
    * Descripcion: Crea las notas de ajuster, restar las carteras *
    *           y Anula los diferidos futuros cuando se indemniza *
    **************************************************************/

    negocios_indemnizados_record RECORD;
    negocios_record              RECORD;
--     respuesta                    CHARACTER VARYING = ' INGRESOS GENERADOS';
    numero_ingreso               CHARACTER VARYING;
    contador_ingresos            INTEGER = 0;
    contador_negocios            INTEGER = 0;
    total_diferidos              INTEGER = 0;
    total_facturas               INTEGER = 0;
    rows_updated                 INTEGER = 0;
    saldo_a_restar               NUMERIC(11, 2); -- saldo a restar a la fac
    saldo_factura_ingreso        NUMERIC(11, 2);
    diferidos_array              VARCHAR(20)[];

BEGIN

    FOR negocios_record IN SELECT negocio FROM administrativo.control_indemnizacion_fianza WHERE periodo_foto = _periodo GROUP BY negocio ORDER BY negocio LOOP


        --SE CREAN LOS INGRESOS POR CADA NEGOCIO
        FOR negocios_indemnizados_record IN SELECT cf.periodo_foto,
                                                   cf.nit_empresa_fianza,
                                                   cf.codcli,
                                                   cf.nit_cliente,
                                                   cf.negocio,
                                                   cf.documento,
                                                   cf.cuota,
                                                   cf.fecha_vencimiento,
                                                   cf.estado_factura,
                                                   cf.altura_mora,
                                                   cf.dias_vencidos,
                                                   f.valor_saldo,
                                                   CASE WHEN cf.valor_saldo_mi > 0 THEN 0 ELSE dna.interes END AS interes_a_cobrar,
                                                   dna.cuota_manejo                                            AS cuota_manejo_a_cobrar,
                                                   cmc.cuenta                                                  AS cuenta_cartera,
                                                   c.cuenta_seguro,
                                                   cfj.cuenta                                                  AS cuenta_interes,
                                                   cfj1.cuenta                                                 AS cuenta_cuota_admin,
                                                   dna.seguro                                                  AS seguro_a_cobrar,
                                                   ing.cod                                                     AS diferido_mi,
                                                   ing2.cod                                                    AS diferido_cm
                                            FROM administrativo.control_indemnizacion_fianza cf
                                                     INNER JOIN con.factura f ON (f.negasoc = cf.negocio AND f.documento = cf.documento AND f.fecha_vencimiento = cf.fecha_vencimiento)
                                                     INNER JOIN negocios n ON n.cod_neg = f.negasoc AND n.cod_cli = f.nit
                                                     INNER JOIN convenios c ON c.id_convenio = n.id_convenio
                                                     INNER JOIN rel_unidadnegocio_convenios ruc ON ruc.id_convenio = c.id_convenio
                                                     LEFT JOIN convenios_cargos_fijos cfj ON cfj.id_convenio = c.id_convenio AND cfj.reg_status = '' AND cfj.tipodoc = 'MI'
                                                     LEFT JOIN convenios_cargos_fijos cfj1 ON cfj1.id_convenio = c.id_convenio AND cfj1.reg_status = '' AND cfj1.tipodoc = 'CM'
                                                     INNER JOIN con.cmc_doc cmc ON (cmc.tipodoc = f.tipo_documento AND cmc.cmc = f.cmc)
                                                     INNER JOIN documentos_neg_aceptado dna ON (dna.cod_neg = f.negasoc AND f.fecha_vencimiento = dna.fecha)
                                                     INNER JOIN ing_fenalco ing ON (ing.codneg = dna.cod_neg AND ing.tipodoc = 'MI' AND ing.cuota = dna.item)
                                                     INNER JOIN ing_fenalco ing2 ON (ing2.codneg = dna.cod_neg AND ing2.tipodoc = 'CM' AND ing2.cuota = dna.item)
                                            WHERE estado_factura != 'VENCIDO' -- SOLO SE CREA EL INGRESO PARA CUANDO EL ESTADO ES CORRIENTE O FUTURA
                                              AND estado_factura != ''
                                              AND f.reg_status = ''
                                              AND f.tipo_documento = 'FAC'
                                              AND cf.periodo_foto = _periodo
                                              AND ruc.id_unid_negocio = _unidad_negocio
                                              AND cf.negocio = negocios_record.negocio
                                            ORDER BY cf.cuota LOOP

--             RAISE NOTICE 'NEGOCIO: % - DOCUMENTO: % - CUOTA: % - ESTADO: %', negocios_indemnizados_record.negocio,
--             negocios_indemnizados_record.documento, negocios_indemnizados_record.cuota, negocios_indemnizados_record.estado_factura;


            IF negocios_indemnizados_record.estado_factura = 'FUTURA'
            THEN

                saldo_factura_ingreso := negocios_indemnizados_record.valor_saldo;

                IF negocios_indemnizados_record.interes_a_cobrar > 0
                THEN

                    SELECT INTO numero_ingreso get_lcod('ICAC'); --OBTENIE EL NUMERO DE INGRESO
--                     RAISE NOTICE 'NUMERO DE INGRESO: % - PARA INTERESES:  % - DIFERIDO: %', numero_ingreso, negocios_indemnizados_record.interes_a_cobrar, negocios_indemnizados_record.diferido_mi;

                    --INSERTA LA CABECERA DEL INGRESO INTERESES
                    INSERT INTO con.ingreso (dstrct,
                                             tipo_documento,
                                             num_ingreso,
                                             codcli,
                                             nitcli,
                                             concepto,
                                             tipo_ingreso,
                                             fecha_consignacion,
                                             fecha_ingreso,
                                             branch_code,
                                             bank_account_no,
                                             codmoneda,
                                             agencia_ingreso,
                                             descripcion_ingreso,
                                             vlr_ingreso,
                                             vlr_ingreso_me,
                                             vlr_tasa,
                                             fecha_tasa,
                                             cant_item,
                                             creation_user,
                                             creation_date,
                                             base,
                                             cuenta,
                                             tipo_referencia_1,
                                             referencia_1,
                                             referencia_2
                                            )
                    VALUES ('FINV',
                            'ICA',
                            numero_ingreso,
                            negocios_indemnizados_record.codcli,
                            negocios_indemnizados_record.nit_cliente,
                            'FE',
                            'C',
                            CURRENT_DATE,
                            CURRENT_TIMESTAMP,
                            'CAJA TESORERIA',
                            'INDEMNIZACION MICRO',
                            'PES',
                            'OP',
                            'AJUSTE FACTURAS INTERESES POR INDEMNIZACION',
                            negocios_indemnizados_record.interes_a_cobrar,
                            negocios_indemnizados_record.interes_a_cobrar,
                            '1.000000',
                            CURRENT_DATE,
                            1,
                            _usuario,
                            CURRENT_TIMESTAMP,
                            'COL',
                            negocios_indemnizados_record.cuenta_cartera,
                            'NEG',
                            negocios_indemnizados_record.negocio,
                            negocios_indemnizados_record.diferido_mi
                           );

                    saldo_factura_ingreso := saldo_factura_ingreso - negocios_indemnizados_record.interes_a_cobrar;

                    --INSERTA EL DETALLE DEL INGRESO INTERESES
                    INSERT INTO con.ingreso_detalle (dstrct,
                                                     tipo_documento,
                                                     num_ingreso,
                                                     item,
                                                     nitcli,
                                                     valor_ingreso,
                                                     valor_ingreso_me,
                                                     factura,
                                                     fecha_factura,
                                                     tipo_doc,
                                                     documento,
                                                     creation_user,
                                                     creation_date,
                                                     base,
                                                     cuenta,
                                                     descripcion,
                                                     valor_tasa,
                                                     saldo_factura,
                                                     referencia_2
                                                    )
                    VALUES ('FINV',
                            'ICA',
                            numero_ingreso,
                            negocios_indemnizados_record.cuota :: BIGINT,
                            negocios_indemnizados_record.nit_cliente,
                            negocios_indemnizados_record.interes_a_cobrar,
                            negocios_indemnizados_record.interes_a_cobrar,
                            negocios_indemnizados_record.documento,
                            CURRENT_DATE,
                            'FAC',
                            negocios_indemnizados_record.documento,
                            _usuario,
                            CURRENT_TIMESTAMP,
                            'COL',
                            negocios_indemnizados_record.cuenta_interes,
                            'AJUSTE AL INTERES',
                            '1.0000000000',
                            saldo_factura_ingreso,
                            negocios_indemnizados_record.diferido_mi
                           );

                    diferidos_array := array_append(diferidos_array, negocios_indemnizados_record.diferido_mi);
                END IF;

                SELECT INTO numero_ingreso get_lcod('ICAC'); --OBTENIE EL NUMERO DE INGRESO
--                 RAISE NOTICE 'NUMERO DE INGRESO: % - PARA CUOTA ADMON: % - DIFERIDO: %', numero_ingreso, negocios_indemnizados_record.cuota_manejo_a_cobrar, negocios_indemnizados_record.diferido_cm;

                --INSERTA LA CABECERA DEL INGRESO CUOTA ADMINISTRACION
                INSERT INTO con.ingreso (dstrct,
                                         tipo_documento,
                                         num_ingreso,
                                         codcli,
                                         nitcli,
                                         concepto,
                                         tipo_ingreso,
                                         fecha_consignacion,
                                         fecha_ingreso,
                                         branch_code,
                                         bank_account_no,
                                         codmoneda,
                                         agencia_ingreso,
                                         descripcion_ingreso,
                                         vlr_ingreso,
                                         vlr_ingreso_me,
                                         vlr_tasa,
                                         fecha_tasa,
                                         cant_item,
                                         creation_user,
                                         creation_date,
                                         base,
                                         cuenta,
                                         tipo_referencia_1,
                                         referencia_1,
                                         referencia_2
                                        )
                VALUES ('FINV',
                        'ICA',
                        numero_ingreso,
                        negocios_indemnizados_record.codcli,
                        negocios_indemnizados_record.nit_cliente,
                        'FE',
                        'C',
                        CURRENT_DATE,
                        CURRENT_DATE,
                        'CAJA TESORERIA',
                        'INDEMNIZACION MICRO',
                        'PES',
                        'OP',
                        'AJUSTE FACTURAS DE CUOTA ADMON POR INDEMNIZACION',
                        negocios_indemnizados_record.cuota_manejo_a_cobrar,
                        negocios_indemnizados_record.cuota_manejo_a_cobrar,
                        '1.000000',
                        CURRENT_DATE,
                        1,
                        _usuario,
                        CURRENT_TIMESTAMP,
                        'COL',
                        negocios_indemnizados_record.cuenta_cartera,
                        'NEG',
                        negocios_indemnizados_record.negocio,
                        negocios_indemnizados_record.diferido_cm
                       );

                saldo_factura_ingreso := saldo_factura_ingreso - negocios_indemnizados_record.cuota_manejo_a_cobrar;

                --INSERTA EL DETALLE DEL INGRESO CUOTA ADMINISTRACION
                INSERT INTO con.ingreso_detalle (dstrct,
                                                 tipo_documento,
                                                 num_ingreso,
                                                 item,
                                                 nitcli,
                                                 valor_ingreso,
                                                 valor_ingreso_me,
                                                 factura,
                                                 fecha_factura,
                                                 tipo_doc,
                                                 documento,
                                                 creation_user,
                                                 creation_date,
                                                 base,
                                                 cuenta,
                                                 descripcion,
                                                 valor_tasa,
                                                 saldo_factura,
                                                 referencia_2
                                                )
                VALUES ('FINV',
                        'ICA',
                        numero_ingreso,
                        negocios_indemnizados_record.cuota :: BIGINT,
                        negocios_indemnizados_record.nit_cliente,
                        negocios_indemnizados_record.cuota_manejo_a_cobrar,
                        negocios_indemnizados_record.cuota_manejo_a_cobrar,
                        negocios_indemnizados_record.documento,
                        CURRENT_DATE,
                        'FAC',
                        negocios_indemnizados_record.documento,
                        _usuario,
                        CURRENT_TIMESTAMP,
                        'COL',
                        negocios_indemnizados_record.cuenta_cuota_admin,
                        'AJUSTE A LA CUOTA ADMINISTRACION',
                        '1.0000000000',
                        saldo_factura_ingreso,
                        negocios_indemnizados_record.diferido_cm
                       );

                diferidos_array := array_append(diferidos_array, negocios_indemnizados_record.diferido_cm);


                SELECT INTO numero_ingreso get_lcod('ICAC'); --OBTENIE EL NUMERO DE INGRESO
--                 RAISE NOTICE 'NUMERO DE INGRESO: % - PARA SEGURO: %', numero_ingreso, negocios_indemnizados_record.seguro_a_cobrar;

                --INSERTA LA CABECERA DEL INGRESO SEGURO
                INSERT INTO con.ingreso (dstrct,
                                         tipo_documento,
                                         num_ingreso,
                                         codcli,
                                         nitcli,
                                         concepto,
                                         tipo_ingreso,
                                         fecha_consignacion,
                                         fecha_ingreso,
                                         branch_code,
                                         bank_account_no,
                                         codmoneda,
                                         agencia_ingreso,
                                         descripcion_ingreso,
                                         vlr_ingreso,
                                         vlr_ingreso_me,
                                         vlr_tasa,
                                         fecha_tasa,
                                         cant_item,
                                         creation_user,
                                         creation_date,
                                         base,
                                         cuenta,
                                         tipo_referencia_1,
                                         referencia_1
                                        )
                VALUES ('FINV',
                        'ICA',
                        numero_ingreso,
                        negocios_indemnizados_record.codcli,
                        negocios_indemnizados_record.nit_cliente,
                        'FE',
                        'C',
                        CURRENT_DATE,
                        CURRENT_DATE,
                        'CAJA TESORERIA',
                        'INDEMNIZACION MICRO',
                        'PES',
                        'OP',
                        'AJUSTE FACTURAS DE SEGURO POR INDEMNIZACION',
                        negocios_indemnizados_record.seguro_a_cobrar,
                        negocios_indemnizados_record.seguro_a_cobrar,
                        '1.000000',
                        CURRENT_DATE,
                        1,
                        _usuario,
                        CURRENT_TIMESTAMP,
                        'COL',
                        negocios_indemnizados_record.cuenta_cartera,
                        'NEG',
                        negocios_indemnizados_record.negocio
                       );

                saldo_factura_ingreso := saldo_factura_ingreso - negocios_indemnizados_record.seguro_a_cobrar;

                --INSERTA EL DETALLE DEL INGRESO SEGURO
                INSERT INTO con.ingreso_detalle (dstrct,
                                                 tipo_documento,
                                                 num_ingreso,
                                                 item,
                                                 nitcli,
                                                 valor_ingreso,
                                                 valor_ingreso_me,
                                                 factura,
                                                 fecha_factura,
                                                 tipo_doc,
                                                 documento,
                                                 creation_user,
                                                 creation_date,
                                                 base,
                                                 cuenta,
                                                 descripcion,
                                                 valor_tasa,
                                                 referencia_2,
                                                 saldo_factura
                                                )
                VALUES ('FINV',
                        'ICA',
                        numero_ingreso,
                        negocios_indemnizados_record.cuota :: BIGINT,
                        negocios_indemnizados_record.nit_cliente,
                        negocios_indemnizados_record.seguro_a_cobrar,
                        negocios_indemnizados_record.seguro_a_cobrar,
                        negocios_indemnizados_record.documento,
                        CURRENT_DATE,
                        'FAC',
                        negocios_indemnizados_record.documento,
                        _usuario,
                        CURRENT_TIMESTAMP,
                        'COL',
                        negocios_indemnizados_record.cuenta_seguro,
                        'AJUSTE AL SEGURO',
                        '1.0000000000',
                        negocios_indemnizados_record.negocio,
                        saldo_factura_ingreso
                       );

                contador_ingresos := contador_ingresos + 3;

                saldo_a_restar := negocios_indemnizados_record.interes_a_cobrar + negocios_indemnizados_record.seguro_a_cobrar + negocios_indemnizados_record.cuota_manejo_a_cobrar;

--                 RAISE NOTICE 'Factura: % - Saldo a restar %', negocios_indemnizados_record.documento, saldo_a_restar;

                -- RESTA EL SALDO DE LA FACTURA
                UPDATE con.factura
                SET valor_abono   = (valor_abono + saldo_a_restar),
                    valor_saldo   = (valor_saldo - saldo_a_restar),
                    valor_abonome = valor_abono,
                    valor_saldome = valor_saldo,
                    user_update   = _usuario,
                    last_update   = CURRENT_TIMESTAMP
                WHERE documento = negocios_indemnizados_record.documento;

            ELSIF negocios_indemnizados_record.estado_factura = 'CORRIENTE'
                THEN

                    saldo_factura_ingreso := negocios_indemnizados_record.valor_saldo;

                    SELECT INTO numero_ingreso get_lcod('ICAC'); --OBTENIE EL NUMERO DE INGRESO
--                     RAISE NOTICE 'NUMERO DE INGRESO: % - PARA CUOTA ADMON: % - DIFERIDO: %', numero_ingreso, negocios_indemnizados_record.cuota_manejo_a_cobrar, negocios_indemnizados_record.diferido_cm;

                    --INSERTA LA CABECERA DEL INGRESO CUOTA ADMINISTRACION
                    INSERT INTO con.ingreso (dstrct,
                                             tipo_documento,
                                             num_ingreso,
                                             codcli,
                                             nitcli,
                                             concepto,
                                             tipo_ingreso,
                                             fecha_consignacion,
                                             fecha_ingreso,
                                             branch_code,
                                             bank_account_no,
                                             codmoneda,
                                             agencia_ingreso,
                                             descripcion_ingreso,
                                             vlr_ingreso,
                                             vlr_ingreso_me,
                                             vlr_tasa,
                                             fecha_tasa,
                                             cant_item,
                                             creation_user,
                                             creation_date,
                                             base,
                                             cuenta,
                                             tipo_referencia_1,
                                             referencia_1,
                                             referencia_2
                                            )
                    VALUES ('FINV',
                            'ICA',
                            numero_ingreso,
                            negocios_indemnizados_record.codcli,
                            negocios_indemnizados_record.nit_cliente,
                            'FE',
                            'C',
                            CURRENT_DATE,
                            CURRENT_DATE,
                            'CAJA TESORERIA',
                            'INDEMNIZACION MICRO',
                            'PES',
                            'OP',
                            'AJUSTE FACTURAS DE CUOTA ADMON POR INDEMNIZACION',
                            negocios_indemnizados_record.cuota_manejo_a_cobrar,
                            negocios_indemnizados_record.cuota_manejo_a_cobrar,
                            '1.000000',
                            CURRENT_DATE,
                            1,
                            _usuario,
                            CURRENT_TIMESTAMP,
                            'COL',
                            negocios_indemnizados_record.cuenta_cartera,
                            'NEG',
                            negocios_indemnizados_record.negocio,
                            negocios_indemnizados_record.diferido_cm
                           );

                    saldo_factura_ingreso := saldo_factura_ingreso - negocios_indemnizados_record.cuota_manejo_a_cobrar;

                    --INSERTA EL DETALLE DEL INGRESO CUOTA ADMINISTRACION
                    INSERT INTO con.ingreso_detalle (dstrct,
                                                     tipo_documento,
                                                     num_ingreso,
                                                     item,
                                                     nitcli,
                                                     valor_ingreso,
                                                     valor_ingreso_me,
                                                     factura,
                                                     fecha_factura,
                                                     tipo_doc,
                                                     documento,
                                                     creation_user,
                                                     creation_date,
                                                     base,
                                                     cuenta,
                                                     descripcion,
                                                     valor_tasa,
                                                     saldo_factura,
                                                     referencia_2
                                                    )
                    VALUES ('FINV',
                            'ICA',
                            numero_ingreso,
                            negocios_indemnizados_record.cuota :: BIGINT,
                            negocios_indemnizados_record.nit_cliente,
                            negocios_indemnizados_record.cuota_manejo_a_cobrar,
                            negocios_indemnizados_record.cuota_manejo_a_cobrar,
                            negocios_indemnizados_record.documento,
                            CURRENT_DATE,
                            'FAC',
                            negocios_indemnizados_record.documento,
                            _usuario,
                            CURRENT_TIMESTAMP,
                            'COL',
                            negocios_indemnizados_record.cuenta_cuota_admin,
                            'AJUSTE A LA CUOTA ADMINISTRACION',
                            '1.0000000000',
                            saldo_factura_ingreso,
                            negocios_indemnizados_record.diferido_cm
                           );

                    diferidos_array := array_append(diferidos_array, negocios_indemnizados_record.diferido_cm);

                    SELECT INTO numero_ingreso get_lcod('ICAC'); --OBTENIE EL NUMERO DE INGRESO
--                     RAISE NOTICE 'NUMERO DE INGRESO: % - PARA SEGURO: %', numero_ingreso, negocios_indemnizados_record.seguro_a_cobrar;

                    --INSERTA LA CABECERA DEL INGRESO SEGURO
                    INSERT INTO con.ingreso (dstrct,
                                             tipo_documento,
                                             num_ingreso,
                                             codcli,
                                             nitcli,
                                             concepto,
                                             tipo_ingreso,
                                             fecha_consignacion,
                                             fecha_ingreso,
                                             branch_code,
                                             bank_account_no,
                                             codmoneda,
                                             agencia_ingreso,
                                             descripcion_ingreso,
                                             vlr_ingreso,
                                             vlr_ingreso_me,
                                             vlr_tasa,
                                             fecha_tasa,
                                             cant_item,
                                             creation_user,
                                             creation_date,
                                             base,
                                             cuenta,
                                             tipo_referencia_1,
                                             referencia_1
                                            )
                    VALUES ('FINV',
                            'ICA',
                            numero_ingreso,
                            negocios_indemnizados_record.codcli,
                            negocios_indemnizados_record.nit_cliente,
                            'FE',
                            'C',
                            CURRENT_DATE,
                            CURRENT_DATE,
                            'CAJA TESORERIA',
                            'INDEMNIZACION MICRO',
                            'PES',
                            'OP',
                            'AJUSTE FACTURAS DE SEGURO POR INDEMNIZACION',
                            negocios_indemnizados_record.seguro_a_cobrar,
                            negocios_indemnizados_record.seguro_a_cobrar,
                            '1.000000',
                            CURRENT_DATE,
                            1,
                            _usuario,
                            CURRENT_TIMESTAMP,
                            'COL',
                            negocios_indemnizados_record.cuenta_cartera,
                            'NEG',
                            negocios_indemnizados_record.negocio
                           );

                    saldo_factura_ingreso := saldo_factura_ingreso - negocios_indemnizados_record.seguro_a_cobrar;

                    --INSERTA EL DETALLE DEL INGRESO SEGURO
                    INSERT INTO con.ingreso_detalle (dstrct,
                                                     tipo_documento,
                                                     num_ingreso,
                                                     item,
                                                     nitcli,
                                                     valor_ingreso,
                                                     valor_ingreso_me,
                                                     factura,
                                                     fecha_factura,
                                                     tipo_doc,
                                                     documento,
                                                     creation_user,
                                                     creation_date,
                                                     base,
                                                     cuenta,
                                                     descripcion,
                                                     valor_tasa,
                                                     referencia_2,
                                                     saldo_factura
                                                    )
                    VALUES ('FINV',
                            'ICA',
                            numero_ingreso,
                            negocios_indemnizados_record.cuota :: BIGINT,
                            negocios_indemnizados_record.nit_cliente,
                            negocios_indemnizados_record.seguro_a_cobrar,
                            negocios_indemnizados_record.seguro_a_cobrar,
                            negocios_indemnizados_record.documento,
                            CURRENT_DATE,
                            'FAC',
                            negocios_indemnizados_record.documento,
                            _usuario,
                            CURRENT_TIMESTAMP,
                            'COL',
                            negocios_indemnizados_record.cuenta_seguro,
                            'AJUSTE AL SEGURO',
                            '1.0000000000',
                            negocios_indemnizados_record.negocio,
                            saldo_factura_ingreso
                           );

                    contador_ingresos := contador_ingresos + 2;

--                     RAISE NOTICE 'Factura: % - Saldo a restar %', negocios_indemnizados_record.documento, saldo_a_restar;

                    -- RESTA EL SALDO DE LA FACTURA
                    UPDATE con.factura
                    SET valor_abono   = (valor_abono + saldo_a_restar),
                        valor_saldo   = (valor_saldo - saldo_a_restar),
                        valor_abonome = valor_abono,
                        valor_saldome = valor_saldo,
                        user_update   = _usuario,
                        last_update   = CURRENT_TIMESTAMP
                    WHERE documento = negocios_indemnizados_record.documento;

            END IF;

            --ANULAR LOS DIFERIDOS
            UPDATE ing_fenalco
            SET reg_status  = 'A',
                user_update = _usuario,
                last_update = CURRENT_TIMESTAMP
            WHERE codneg = negocios_indemnizados_record.negocio AND cod = ANY (diferidos_array) AND periodo = ''
               OR periodo ISNULL AND procesado_dif != 'S' AND cuota = negocios_indemnizados_record.cuota;

--             IF FOUND
--             THEN
--                 GET DIAGNOSTICS rows_updated := ROW_COUNT;
--                 total_diferidos := total_diferidos + rows_updated;
--             END IF;
--             total_facturas := total_facturas + 1;
            diferidos_array := NULL;
        END LOOP;
        contador_negocios := contador_negocios + 1;
    END LOOP;

--     RAISE NOTICE 'Cantidad ingresos creados: %', contador_ingresos;
--     RAISE NOTICE 'Cantidad diferidos anulados: %', total_diferidos;
--     RAISE NOTICE 'Cantidad facturas: %', total_facturas;
--     RAISE NOTICE 'Cantidad negocios indemnizados: %', contador_negocios;

    RETURN 'OK';
--     RETURN contador_ingresos || respuesta;
END;
$$
LANGUAGE plpgsql
VOLATILE;
ALTER FUNCTION fn_anular_ingresos_indemnizados(INTEGER, CHARACTER VARYING, CHARACTER VARYING)
    OWNER TO postgres;
