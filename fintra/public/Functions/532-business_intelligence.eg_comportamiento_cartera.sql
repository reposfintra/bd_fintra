﻿

CREATE OR REPLACE FUNCTION business_intelligence.eg_comportamiento_cartera()
  RETURNS varchar AS
$BODY$

DECLARE
  carteraRecord record;
  periodRecord record;
  next_cartera record;
  ingreso_pagos record;
 
  _periodo varchar ;
  _years integer;
  _mes integer;
  _id integer;
 
BEGIN
 
    truncate TABLE business_intelligence.comportamiento_cartera;
 FOR periodRecord IN
     SELECT substring(valor,1,4)::integer AS YEAR ,
         substring(valor,5,2)::integer AS MONTH,
         valor AS PERIOD,
         CASE WHEN substring(valor,5,2)::integer =12 THEN ((substring(valor,1,4)::integer+1)||'01')::integer
         ELSE valor::integer+1 END AS next_period    
        FROM con.periodos_foto
        WHERE valor::integer>=201812 ORDER BY valor::integer
    LOOP
 
     _periodo:=periodRecord.PERIOD;
  _years := periodRecord.YEAR;
  _mes :=periodRecord.MONTH;
     _id :=(SELECT min(id) FROM con.foto_cartera WHERE periodo_lote=_periodo AND ano=_years AND mes=_mes);
 
  FOR carteraRecord IN
    SELECT
           f.periodo_lote::integer AS periodo_foto,
           u.descripcion AS unidad_negocio,
           f.nit,
           get_nombc(f.nit) AS nombre,
           f.negasoc AS negocio,
           f.documento,
           f.num_doc_fen AS cuota,
           n.vr_negocio,
           to_char(f_desem,'YYYYMM')AS periodo_desembolso,
           ''::varchar AS altura_mora_foto,
           CASE WHEN f.fecha_vencimiento::date < current_date THEN 'VENCIDO'
             WHEN to_char(f.fecha_vencimiento,'YYYYMM')::integer = to_char(current_date,'YYYYMM')::integer  THEN 'A VENCER'
            ELSE  'FUTURO' END AS estado_cuota,      
           CASE WHEN u.descripcion IN ('MICROCREDITO','EDUCATIVO FA','EDUCATIVO FB',
                                         'CONSUMO FA','LIBRANZA','CONSUMO FB','RIESGO PROPIO',
                                         'AUTOMOTOR FA','EDUCATIVO FB') THEN c.agencia
                WHEN pr.departamento IS NOT NULL  THEN  pr.departamento
            ELSE '-'  END AS agencia,
            f.fecha_vencimiento,
            f.fecha_ultimo_pago,
            '0101-01-01'::DATE AS fecha_cosignacion,
            0::integer AS diff_pago,
            ''::varchar AS  clasificacion,
            sum(f.valor_factura) AS valor_factura,
            sum(f.valor_abono) AS valor_abono,
            sum(f.valor_saldo) AS valor_saldo,
            0.00::NUMERIC AS valor_abono_np,
            0.00::NUMERIC AS valor_saldo_np,
            0.00::NUMERIC AS valor_ingreso,
            0.00::NUMERIC AS current_saldo
    FROM con.foto_cartera f
    INNER JOIN negocios n ON  (n.cod_neg=f.negasoc AND n.cod_cli=f.nit)
    INNER JOIN solicitud_aval sa ON sa.cod_neg=n.cod_neg
    LEFT JOIN apicredit.pre_solicitudes_creditos pr ON pr.numero_solicitud=sa.numero_solicitud
    INNER JOIN convenios c ON c.id_convenio=n.id_convenio
    INNER JOIN rel_unidadnegocio_convenios ur ON ur.id_convenio=c.id_convenio
    INNER JOIN unidad_negocio u ON u.id=ur.id_unid_negocio AND u.ref_4 !=''
    WHERE
        f.periodo_lote IN (_periodo) AND f.valor_saldo > 0
        AND f.tipo_documento='FAC'
        AND f.reg_status=''
        AND f.ano=_years
        AND f.mes=_mes
        AND f.id > _id
        AND n.cod_neg NOT IN (SELECT negocio_reestructuracion FROM rel_negocios_reestructuracion)
        AND n.negocio_rel =''
        AND n.negocio_rel_seguro =''
        AND n.negocio_rel_gps =''
        AND u.id=1
        AND n.estado_neg NOT IN ('R', 'D')
        AND substring(f.documento,1,2) not in ('CP','FF','DF')
    GROUP BY
    u.descripcion,
    f.periodo_lote,
    n.vr_negocio,
    nit,
    f.negasoc,
    to_char(f_desem,'YYYYMM'),
    c.agencia,
    pr.departamento,
    f.fecha_vencimiento,
    f.documento,
    f.num_doc_fen,
    f.fecha_ultimo_pago
  LOOP
 
         --Altura mora
   carteraRecord.altura_mora_foto := eg_altura_mora_periodo(carteraRecord.negocio,carteraRecord.periodo_foto::integer,1,0) ;
   
   --Fecha de consignacion
   SELECT INTO ingreso_pagos
       max(ing.fecha_consignacion) AS fecha_consignacion,
       sum(dtein.valor_ingreso) AS valor_ingreso
      FROM con.factura fac
      INNER JOIN con.ingreso_detalle dtein ON dtein.documento = fac.documento AND fac.nit=dtein.nitcli AND fac.tipo_documento=dtein.tipo_doc AND fac.dstrct=dtein.dstrct
      INNER JOIN con.ingreso ing ON dtein.num_ingreso::text = ing.num_ingreso AND dtein.nitcli=ing.nitcli AND ing.tipo_documento=dtein.tipo_documento AND ing.dstrct=ing.dstrct
     WHERE fac.reg_status::text = ''
     AND fac.negasoc::text <> ''
     AND fac.dstrct::text = 'FINV'
     AND fac.tipo_documento::text = 'FAC'
     AND fac.descripcion <> 'CXC AVAL'
     AND SUBSTRING(fac.documento, 1, 2) NOT IN  ('CP','FF','DF')
     AND ing.reg_status::text = ''
     AND ing.tipo_documento::text IN ('ICA', 'ING')
     AND ing.dstrct::text = 'FINV'
     AND fac.negasoc=carteraRecord.negocio
     AND fac.documento=carteraRecord.documento
     --AND fac.valor_saldo >0
     GROUP BY fac.negasoc;
             
   carteraRecord.fecha_cosignacion :=ingreso_pagos.fecha_consignacion;
   carteraRecord.valor_ingreso := ingreso_pagos.valor_ingreso;
   
   --Valor saldo hoy
   carteraRecord.current_saldo :=(SELECT valor_saldo FROM con.factura WHERE negasoc=carteraRecord.negocio AND documento=carteraRecord.documento AND tipo_documento='FAC' AND reg_status='');          
   
 
   IF to_char(carteraRecord.fecha_cosignacion,'YYYYMM')::INTEGER > carteraRecord.periodo_foto THEN
    carteraRecord.fecha_cosignacion:=NULL;
   END IF;
   
   --diff de pago
     -- AND ROUND(((carteraRecord.valor_abono/carteraRecord.valor_factura)*100),2) >=95
      IF carteraRecord.fecha_cosignacion IS NOT NULL  THEN
        carteraRecord.diff_pago:=carteraRecord.fecha_cosignacion-carteraRecord.fecha_vencimiento;
      ELSE
        carteraRecord.diff_pago:=current_date-carteraRecord.fecha_vencimiento;
      END IF;
     
--       RAISE NOTICE 'periodRecord.next_period : %',periodRecord.next_period;
--       RAISE NOTICE 'periodRecord.next_period : %',substring(periodRecord.next_period,1,4);
--       RAISE NOTICE 'periodRecord.next_period : %',substring(periodRecord.next_period,5);
     
      --SALDO FOTO NEXT
      SELECT INTO next_cartera SUM(valor_abono) AS valor_abono, SUM(valor_saldo) AS valor_saldo
      FROM con.foto_cartera
      WHERE negasoc=carteraRecord.negocio
      AND documento=carteraRecord.documento
      AND ano=substring(periodRecord.next_period,1,4)::INTEGER
      AND mes=substring(periodRecord.next_period,5)::INTEGER
      AND periodo_lote=periodRecord.next_period;
     
      carteraRecord.valor_abono_np :=next_cartera.valor_abono;
      carteraRecord.valor_saldo_np :=next_cartera.valor_saldo;
     
      --clasificacion pago anticipado, pago al dia , pago extem
      IF carteraRecord.valor_saldo_np IS NOT NULL AND carteraRecord.valor_saldo_np =0 THEN
       carteraRecord.clasificacion :='1-CUOTA PAGADA';  
      ELSIF carteraRecord.valor_abono_np IS NOT NULL AND carteraRecord.valor_abono_np > 0 AND  ROUND(((carteraRecord.valor_abono_np/carteraRecord.valor_factura)*100),2) <=95 THEN        
         carteraRecord.clasificacion :='2-ABONO A CUOTA MENOR 95 %';
      ELSIF carteraRecord.valor_abono_np IS NOT NULL AND carteraRecord.valor_abono_np > 0 AND  ROUND(((carteraRecord.valor_abono_np/carteraRecord.valor_factura)*100),2) >=95 THEN
         carteraRecord.clasificacion :='3-ABONO A CUOTA MAYOR 95 %';
      ELSIF carteraRecord.valor_abono_np IS NOT NULL AND  carteraRecord.valor_abono_np = 0.00 THEN
         carteraRecord.clasificacion :='4-SIN ABONOS';
      ELSIF carteraRecord.valor_saldo_np IS NULL AND carteraRecord.valor_abono_np IS NULL THEN
         carteraRecord.clasificacion :='5-SIN CLASIFICAR';
      END IF;
     
      INSERT INTO business_intelligence.comportamiento_cartera(periodo_foto,unidad_negocio,nit,nombre,negocio,documento,
                    cuota,vr_negocio,periodo_desembolso,altura_mora_foto,
                    estado_cuota,agencia,fecha_vencimiento,fecha_ultimo_pago,fecha_cosignacion,
                    diff_pago,valor_factura,valor_abono,valor_saldo,current_saldo,valor_abono_np,
                    valor_saldo_np,valor_ingreso,clasificacion)
                   
      VALUES (carteraRecord.periodo_foto, carteraRecord.unidad_negocio,carteraRecord.nit,carteraRecord.nombre,
   carteraRecord.negocio,carteraRecord.documento,carteraRecord.cuota,carteraRecord.vr_negocio,
   carteraRecord.periodo_desembolso,carteraRecord.altura_mora_foto,carteraRecord.estado_cuota,
   carteraRecord.agencia,carteraRecord.fecha_vencimiento,carteraRecord.fecha_ultimo_pago,carteraRecord.fecha_cosignacion,
   carteraRecord.diff_pago,carteraRecord.valor_factura,carteraRecord.valor_abono,carteraRecord.valor_saldo,carteraRecord.current_saldo,
   carteraRecord.valor_abono_np,carteraRecord.valor_saldo_np,carteraRecord.valor_ingreso,carteraRecord.clasificacion
   );
   
   --RAISE NOTICE '##############################################################################################################';
   
       
  END LOOP;
 
END LOOP;

    RETURN  'OK';

END;$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION business_intelligence.eg_comportamiento_cartera()
  OWNER TO postgres;