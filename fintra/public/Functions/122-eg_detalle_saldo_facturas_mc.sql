-- Function: eg_detalle_saldo_facturas_mc(text, integer)

-- DROP FUNCTION eg_detalle_saldo_facturas_mc(text, integer);

CREATE OR REPLACE FUNCTION eg_detalle_saldo_facturas_mc(codneg TEXT, cuota INTEGER)
    RETURNS SETOF RS_DETALLE_SALDO_FACTURAS AS
$BODY$
DECLARE

    _saldo_aplicar  NUMERIC := 0.00;
    _recordFacturas RECORD;
    rs              RS_DETALLE_SALDO_FACTURAS;

BEGIN
    _saldo_aplicar := (SELECT sum(valor_abono) FROM con.factura fac WHERE fac.negasoc = codneg AND fac.num_doc_fen = cuota AND fac.reg_status = '' AND fac.reg_status = '' AND fac.tipo_documento = 'FAC' AND documento LIKE 'MC%');
    rs.total_abonos := _saldo_aplicar;

    FOR _recordFacturas IN (
        SELECT fdet.descripcion
             , fdet.valor_unitario
             , fac.documento
             , fac.fecha_vencimiento
             , fac.valor_factura
             , fac.valor_abono
             , fac.valor_saldo
             , coalesce(tem.esquema, 'N') AS esquema
        FROM con.factura fac
                 INNER JOIN con.factura_detalle fdet ON (fac.documento = fdet.documento)
                 INNER JOIN conceptos_facturacion cf ON (fdet.descripcion = cf.descripcion)
                 LEFT JOIN tem.negocios_facturacion_old tem ON (tem.cod_neg = fac.negasoc)
        WHERE fac.negasoc = codneg
          AND fac.reg_status = ''
          AND fac.dstrct = 'FINV'
          AND fac.tipo_documento = 'FAC'
          AND fac.num_doc_fen = cuota
        ORDER BY cf.prioridad_pago
    )
        LOOP
            rs.documento = _recordFacturas.documento;
            rs.total_factura := _recordFacturas.valor_factura;

            IF (_recordFacturas.descripcion = 'CUOTA-ADMINISTRCION') THEN
                IF (_saldo_aplicar >= _recordFacturas.valor_unitario) THEN
                    rs.saldo_cuota_manejo := 0.00;
                ELSIF (_saldo_aplicar > 0) THEN
                    rs.saldo_cuota_manejo := _recordFacturas.valor_unitario - _saldo_aplicar ;
                ELSE
                    rs.saldo_cuota_manejo := _recordFacturas.valor_unitario;
                END IF;

            ELSIF (_recordFacturas.esquema = 'S') THEN
                rs.saldo_cuota_manejo := 0.00;
            END IF;

            IF (_recordFacturas.descripcion = 'CAT') THEN
                IF (_saldo_aplicar >= _recordFacturas.valor_unitario) THEN
                    rs.saldo_cat := 0.00;
                ELSIF (_saldo_aplicar > 0) THEN
                    rs.saldo_cat := _recordFacturas.valor_unitario - _saldo_aplicar ;
                ELSE
                    rs.saldo_cat := _recordFacturas.valor_unitario;
                END IF;

            ELSIF (_recordFacturas.esquema = 'S') THEN
                rs.saldo_cat := 0.00;
            END IF;

            IF (_recordFacturas.descripcion = 'INTERES') THEN
                IF (_saldo_aplicar >= _recordFacturas.valor_unitario) THEN
                    rs.saldo_interes := 0.00;
                ELSIF (_saldo_aplicar > 0) THEN
                    rs.saldo_interes := _recordFacturas.valor_unitario - _saldo_aplicar ;
                ELSE
                    rs.saldo_interes := _recordFacturas.valor_unitario;
                END IF;

            ELSIF (_recordFacturas.esquema = 'S') THEN
                rs.saldo_interes := 0.00;
            END IF;

            IF (_recordFacturas.descripcion = 'SEGURO') THEN
                IF (_saldo_aplicar >= _recordFacturas.valor_unitario) THEN
                    rs.saldo_seguro := 0.00;
                ELSIF (_saldo_aplicar > 0) THEN
                    rs.saldo_seguro := _recordFacturas.valor_unitario - _saldo_aplicar ;
                ELSE
                    rs.saldo_seguro := _recordFacturas.valor_unitario;
                END IF;
            END IF;

            IF (_recordFacturas.descripcion = 'CAPITAL') THEN
                IF (_saldo_aplicar >= _recordFacturas.valor_unitario) THEN
                    rs.saldo_capital := 0.00;
                ELSIF (_saldo_aplicar > 0) THEN
                    rs.saldo_capital := _recordFacturas.valor_unitario - _saldo_aplicar ;
                ELSE
                    rs.saldo_capital := _recordFacturas.valor_unitario;
                END IF;

            END IF;

            IF (_recordFacturas.descripcion = 'CAPITAL-AVAL') THEN
                IF (_saldo_aplicar >= _recordFacturas.valor_unitario) THEN
                    rs.saldo_capital_aval := 0.00;
                ELSIF (_saldo_aplicar > 0) THEN
                    rs.saldo_capital_aval := _recordFacturas.valor_unitario - _saldo_aplicar ;
                ELSE
                    rs.saldo_capital_aval := _recordFacturas.valor_unitario;
                END IF;
            END IF;

            IF (_recordFacturas.descripcion = 'INTERES-AVAL') THEN
                IF (_saldo_aplicar >= _recordFacturas.valor_unitario) THEN
                    rs.saldo_interes_aval := 0.00;
                ELSIF (_saldo_aplicar > 0) THEN
                    rs.saldo_interes_aval := _recordFacturas.valor_unitario - _saldo_aplicar ;
                ELSE
                    rs.saldo_interes_aval := _recordFacturas.valor_unitario;
                END IF;
            END IF;

            --Validamos el abono
            _saldo_aplicar := _saldo_aplicar - _recordFacturas.valor_unitario;

        END LOOP;

    rs.cuota := cuota;
    rs.negocio := codneg;
    rs.saldo_factura := COALESCE(rs.saldo_cuota_manejo, 0.00) + COALESCE(rs.saldo_cat, 0.00) + COALESCE(rs.saldo_interes, 0.00) + COALESCE(rs.saldo_seguro, 0.00) + COALESCE(rs.saldo_capital, 0.00) + COALESCE(rs.saldo_capital_aval, 0.00) +
                        COALESCE(rs.saldo_interes_aval, 0.00);

    --validamos campos en null
    IF rs.documento IS NULL THEN
        rs.documento := '';
    END IF;
    IF rs.total_factura IS NULL THEN
        rs.total_factura := 0.00;
    END IF;
    IF rs.total_abonos IS NULL THEN
        rs.total_abonos := 0.00;
    END IF;
    IF rs.saldo_cuota_manejo IS NULL THEN
        rs.saldo_cuota_manejo := 0.00;
    END IF;
    IF rs.saldo_cat IS NULL THEN
        rs.saldo_cat := 0.00;
    END IF;
    IF rs.saldo_interes IS NULL THEN
        rs.saldo_interes := 0.00;
    END IF;
    IF rs.saldo_seguro IS NULL THEN
        rs.saldo_seguro := 0.00;
    END IF;
    IF rs.saldo_capital IS NULL THEN
        rs.saldo_capital := 0.00;
    END IF;
    IF rs.saldo_capital_aval IS NULL THEN
        rs.saldo_capital_aval := 0.00;
    END IF;
    IF rs.saldo_interes_aval IS NULL THEN
        rs.saldo_interes_aval := 0.00;
    END IF;

    RETURN NEXT rs;
END;
$BODY$
    LANGUAGE plpgsql
    VOLATILE;
ALTER FUNCTION eg_detalle_saldo_facturas_mc(TEXT, INTEGER)
    OWNER TO postgres;
