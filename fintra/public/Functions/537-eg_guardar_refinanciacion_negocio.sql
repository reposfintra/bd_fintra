
-- Function: eg_guardar_refinanciacion_negocio(character varying, character varying, numeric, character varying, numeric)

-- DROP FUNCTION eg_guardar_refinanciacion_negocio(character varying, character varying, numeric, character varying, numeric);

CREATE OR REPLACE FUNCTION eg_guardar_refinanciacion_negocio(
    _cod_neg character varying,
    _tipo_refinanciacion character varying,
    _valor_pago numeric,
    _usuario character varying,
    _porc_negociacion numeric)
  RETURNS text AS
$BODY$
DECLARE

  retorno varchar:='OK'; 
  _estado varchar:='';
  _contador integer:=0;
  _saldo_a_refinanciar NUMERIC:=0;
  _identificador varchar;
  _key int4;

BEGIN
	
	_estado:=COALESCE((SELECT estado FROM administrativo.control_refinanciacion_negocios WHERE cod_neg=_cod_neg AND estado !='S'),'');
	_contador:=COALESCE((SELECT count(0) FROM administrativo.control_refinanciacion_negocios WHERE cod_neg=_cod_neg AND estado ='S'),0);
    PERFORM * FROM negocios WHERE cod_neg =_cod_neg AND estado_neg='T';
	IF FOUND THEN 
			IF  _contador <=2 THEN 
					IF _tipo_refinanciacion='A' THEN 
					
								IF _estado !='P' THEN 
								
									_key:=nextval('key_refinanciacion_id_seq');
									_identificador:=OVERLAY('PAA0000000' PLACING _key FROM 11 - length(_key) FOR length(_key));
								
									--GUARDAMOS LOS SALDOS DE LA REFINACIACION 
									INSERT INTO administrativo.saldos_refinanciacion_negocios(documento, negocio, cuota, fecha_vencimiento, 
																					            dias_mora, esquema, estado, valor_saldo_capital, valor_saldo_mi, 
																					            valor_saldo_ca, valor_cm, valor_seguro, valor_saldo_cuota, ixm, 
																					            gac, suma_saldos,creation_user,key_ref)
									      SELECT documento,
									             negocio, 
									             cuota,
									             fecha_vencimiento,
									             dias_mora,
									             esquema,
									             estado,
									             valor_saldo_capital,
									             valor_saldo_mi,
									             valor_saldo_ca,
									             valor_cm,
									             valor_seguro,
									             valor_saldo_cuota,
									             IxM,
									             GaC,
									             suma_saldos,
									             _usuario,
									             _identificador
									FROM eg_saldos_refinanciacion_negocios(_cod_neg,_tipo_refinanciacion) as factura (
									documento varchar, 
									negocio varchar, 
									cuota varchar, 
									fecha_vencimiento date,
									dias_mora numeric, 
									esquema varchar,
									estado varchar, 
									valor_saldo_capital numeric,
									valor_saldo_mi numeric, 
									valor_saldo_ca numeric, 
									valor_cm numeric,
									valor_seguro numeric, 
									valor_saldo_cuota numeric,
									IxM numeric,
									GaC numeric, 
									suma_saldos numeric); 
									
									--GARDAMOR LA PROYECCION DEL LA REFINACIACION
									INSERT INTO administrativo.proyeccion_refinanciacion_negocios(
														            documento, cod_neg, item, fecha, capital, 
														            interes, cat, cuota_manejo, valor_cuota_sin_aplicacion, valor_cuota, 
														            valor_extracto, dias_vencidos, estado, color, aplicado, creation_user,key_ref)
									SELECT  
										    documento,
											cod_neg,
											item,
											fecha,
											capital,
											interes,
											cat,
											cuota_manejo,
											valor_cuota_sin_aplicacion,
											valor_cuota,
											(valor_cuota_sin_aplicacion-valor_cuota) AS valor_extracto,
											dias_vencidos,
											estado,
											color,
											aplicado,
											_usuario,
											_identificador
									FROM eg_proyeccion_refinanciacion_negocios(_cod_neg, _tipo_refinanciacion, _valor_pago) ORDER BY item::integer;
									
								   UPDATE negocios SET refinanciado='P' WHERE cod_neg=_cod_neg;
								   
								   INSERT INTO administrativo.control_refinanciacion_negocios(cod_neg,estado,creation_user,valor_a_pagar,porc_negociacion,key_ref)
								   VALUES(_cod_neg,'P',_usuario,_valor_pago,_porc_negociacion,_identificador);
								   
							  ELSE 
							  	  retorno:='NEGOCIO EN PROCESO DE REFINANCIACION TIPO A.';
							  END IF;
					       
				   END IF; 
			ELSE 
					retorno:='MAXIMO INTENTOS PERMITIDOS SUPERADO.';
			END IF;
	  ELSE 
	  			retorno:='NEGOCIO NO EXISTE EN LA BD.';
	  END IF;
	
  RETURN retorno;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION eg_guardar_refinanciacion_negocio(character varying, character varying, numeric, character varying, numeric)
  OWNER TO postgres;