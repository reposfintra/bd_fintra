-- Function: fn_cxp_definitiva_aseguradoras(items_cxp text[], usuario character varying);

-- DROP FUNCTION fn_cxp_definitiva_aseguradoras(items_cxp text[], usuario character varying,nit_aseguradora character varying);

/*******************Descripcion*******************
*Funcion para generar la cxp consolidada para el pago de polizas a las aseguradoras *
*Crea cxp a clientes en caso de un pago anticipado
*Autor: Manuel Camargo*
******************Descripcion********************/

CREATE OR REPLACE FUNCTION fn_cxp_consolidada_aseguradoras(
    items_cxp text[],
    usuario character varying,
    nit_aseguradora character varying,
    tipo_poliza character varying)
  RETURNS text AS
$BODY$
DECLARE

recordCabecera record;
recordCabeceraCxpcliente record;
rs text :='OK';
total_factura numeric:=0;
total_factura_cliente numeric:=0;
recordDetalleCXP record;
recordProveedorCXP record;
recordClienteCXP record;
recordDetalleCXPCliente record;
items integer:=1;
itemsDetalle integer:=1;
_codigo_cxp_aseguradora varchar:='' ;
_codigo_cxp_cliente varchar:='' ;
_detalleCxp record;
_negocio record;
_CxpXvencer record;
_convenio record;
_cuota int:=0;
_t_cuotas int:=0;
_saldo_cartera int;
_cxps_clientes varchar:='cxpc: ';



begin
	
	
	  
                for  _detalleCxp in 
                select
				proveedor,
				cxp.documento,
				cxp.fecha_vencimiento,
				cxp.documento_relacionado,
				cxp.vlr_neto,
				dpn.item as cuota,
				dpn.id_poliza,
				np.descripcion as poliza,
				sum (fac.valor_saldo)::int as saldo_cartera 
				from fin.cxp_doc cxp 
				inner join detalle_poliza_negocio dpn on dpn.cxp_generada=cxp.documento
				inner join administrativo.nuevas_polizas np on np.id=dpn.id_poliza
				inner join con.foto_cartera fac on fac.negasoc=cxp.documento_relacionado
				where cxp.reg_status='' 
				and cxp.documento = ANY (items_cxp::varchar[])
				and cxp.tipo_documento='FAP' 
				and cxp.handle_code='PZ' 
				and cxp.referencia_3 !='P'
				and cxp.vlr_saldo >0
				and fac.periodo_lote=replace(substring(cxp.fecha_vencimiento,1,7),'-','')
				and fac.reg_status=''
				and fac.tipo_documento='FAC'
				and dpn.pago_total='N'
				group by 
				cxp.proveedor,
				cxp.documento,
				cxp.fecha_vencimiento,
				cxp.documento_relacionado,
				cxp.vlr_neto,
				dpn.item,
				dpn.id_poliza,
				np.descripcion
				
				loop
					
					select into _negocio nro_docs,cod_cli,id_convenio from negocios where cod_neg=_detalleCxp.documento_relacionado;
					
					_cuota=_detalleCxp.cuota;
					_t_cuotas=_negocio.nro_docs;
					_saldo_cartera=_detalleCxp.saldo_cartera;
				
						raise notice 'DOCUMENTO: %',_detalleCxp.documento;		
						if (_cuota < _t_cuotas) and _saldo_cartera=0  then
						
							
							for _CxpXvencer in 
							select * from  fin.cxp_doc 
							where fecha_vencimiento::date > _detalleCxp.fecha_vencimiento::date
							and tipo_documento='FAP'
							and handle_code='PZ'
							and referencia_1=_detalleCxp.id_poliza
							and documento_relacionado=_detalleCxp.documento_relacionado
							
							loop
							
								INSERT INTO FIN.CXP_DOC
							    (
							    PROVEEDOR,TIPO_DOCUMENTO,DOCUMENTO,DESCRIPCION,AGENCIA,HANDLE_CODE,BANCO,
							    SUCURSAL, VLR_NETO, VLR_SALDO, VLR_NETO_ME,VLR_SALDO_ME,TASA,
							    CREATION_DATE,CREATION_USER,BASE,MONEDA_BANCO,FECHA_DOCUMENTO,FECHA_VENCIMIENTO,
							    CLASE_DOCUMENTO_REL,MONEDA,TIPO_DOCUMENTO_REL,DOCUMENTO_RELACIONADO,DSTRCT,
							    CLASE_DOCUMENTO,TIPO_REFERENCIA_1
							    )
							    select 
							    proveedor,'NC',_CxpXvencer.documento||'-1','NC - CANCELA CXP ASEGURADORA '||_CxpXvencer.documento,
							    AGENCIA,HANDLE_CODE,BANCO,SUCURSAL,VLR_NETO,VLR_NETO,VLR_NETO_ME,VLR_NETO_ME,TASA,NOW(),usuario,
							    BASE,MONEDA_BANCO,NOW()::DATE,NOW()::DATE,4,MONEDA,'FAP',DOCUMENTO,DSTRCT,4,'FACT'
							    from fin.cxp_doc 
							    where documento = _CxpXvencer.documento 
							    and tipo_documento='FAP' 
							    and dstrct='FINV' 
							    AND reg_status='';
								
						
						
								INSERT INTO FIN.CXP_ITEMS_DOC
							    (
							    PROVEEDOR,TIPO_DOCUMENTO,DOCUMENTO,ITEM,DESCRIPCION,
							    VLR, VLR_ME,CODIGO_CUENTA,CREATION_DATE, CREATION_USER,BASE,DSTRCT
							    )	    
							    select 
							    proveedor,'NC',_CxpXvencer.documento||'-1',1,'NC - CANCELA CXP ASEGURADORA '||_CxpXvencer.documento,
							    VLR_NETO,VLR_NETO_ME,11050534,NOW(),usuario,'COL','FINV'
							    from fin.cxp_doc 
							    where documento=_CxpXvencer.documento 
							    and tipo_documento='FAP' 
							    and dstrct='FINV' 
							    AND reg_status='';
							    
								
								
				        		--ACTUALIZAMOS CXP COLOCANDO SALDO EN CERO
							    UPDATE fin.cxp_doc SET vlr_total_abonos = vlr_neto, vlr_saldo = 0, vlr_total_abonos_me = vlr_neto_me, vlr_saldo_me = 0
								WHERE documento =_CxpXvencer.documento and reg_status='' and dstrct='FINV' and tipo_documento='FAP';
				
							end loop;
							
							--Actualizamos detalle cxp marcandola como pago total
							update detalle_poliza_negocio 
							set pago_total='S'					
							where cod_neg=_detalleCxp.documento_relacionado
							and id_poliza=_detalleCxp.id_poliza
							and fecha_vencimiento::date > _detalleCxp.fecha_vencimiento::date;
							
							--Buscamos informacion bancaria del cliente						
							SELECT INTO recordClienteCXP payment_name, branch_code, bank_account_no  
							FROM proveedor
							WHERE nit = _negocio.cod_cli;
							--
							/*select into _convenio  hc_cxp,cuenta_cxp,prefijo_cxp  
							from convenios where id_convenio=_negocio.id_convenio;*/
								
								SELECT INTO recordCabeceraCxpcliente
						        'FINV'::VARCHAR as distrito,
								_negocio.cod_cli::VARCHAR  as proveedor,
								'FAP'::VARCHAR as tipo_doc,
								'CXP DEVOLUCION POLIZA '||_detalleCxp.poliza||' COBRADA A: '||recordClienteCXP.payment_name as descripcion,
								'OP'::VARCHAR as agencia,
								'RG'::VARCHAR as handle_code,
								(select table_code from tablagen where table_type= 'AUTCXP' AND dato='PREDETERMINADO')as aprobador,
								recordClienteCXP.branch_code::VARCHAR as banco,
								recordClienteCXP.bank_account_no::VARCHAR as sucursal,
								'PES'::VARCHAR as moneda,
								0::numeric as vlr_neto,
								1.0000000000::numeric as tasa;
								
								
								--Buscamos serial cxp devolucion
		            			SELECT INTO _codigo_cxp_cliente get_lcod('CXP_DEV_POLIZA');
		            			 
		            			-- Insertamos cabecera cxp devolucion al cliente
								INSERT INTO fin.cxp_doc(
							    reg_status, dstrct, proveedor, tipo_documento, documento, descripcion,
							    agencia, handle_code, id_mims, tipo_documento_rel, documento_relacionado,
							    fecha_aprobacion, aprobador, usuario_aprobacion, banco, sucursal,
							    moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me, vlr_total_abonos_me,
							    vlr_saldo_me, tasa, usuario_contabilizo, fecha_contabilizacion,
							    usuario_anulo, fecha_anulacion, fecha_contabilizacion_anulacion,
							    observacion, num_obs_autorizador, num_obs_pagador, num_obs_registra,
							    last_update, user_update, creation_date, creation_user, base,
							    corrida, cheque, periodo, fecha_procesado, fecha_contabilizacion_ajc,
							    fecha_contabilizacion_ajv, periodo_ajc, periodo_ajv, usuario_contabilizo_ajc,
							    usuario_contabilizo_ajv, transaccion_ajc, transaccion_ajv, clase_documento,
							    transaccion, moneda_banco, fecha_documento, fecha_vencimiento,
							    ultima_fecha_pago, flujo, transaccion_anulacion, ret_pago, clase_documento_rel,
							    tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,
							    tipo_referencia_3, referencia_3, indicador_traslado_fintra, factoring_formula_aplicada,
							    factura_tipo_nomina)
						        VALUES ('', 'FINV', recordCabeceraCxpcliente.proveedor, recordCabeceraCxpcliente.tipo_doc, _codigo_cxp_cliente, recordCabeceraCxpcliente.descripcion,
							    recordCabeceraCxpcliente.agencia, recordCabeceraCxpcliente.handle_code, '', 'NEG', _detalleCxp.documento_relacionado,
							    '0099-01-01 00:00:00'::timestamp, recordCabeceraCxpcliente.aprobador, '', recordCabeceraCxpcliente.banco, recordCabeceraCxpcliente.sucursal,
							    'PES', recordCabeceraCxpcliente.vlr_neto, 0,  recordCabeceraCxpcliente.vlr_neto, recordCabeceraCxpcliente.vlr_neto, 0,
							     recordCabeceraCxpcliente.vlr_neto,  1, '', '0099-01-01 00:00:00'::timestamp,
							    '',  '0099-01-01 00:00:00'::timestamp,  '0099-01-01 00:00:00'::timestamp,
							    '', 0, 0, 0,
							    '0099-01-01 00:00:00'::timestamp, '', NOW(), usuario, 'COL',
							    '', '', '', '0099-01-01 00:00:00'::timestamp, '0099-01-01 00:00:00'::timestamp,
							    '0099-01-01 00:00:00'::timestamp, '', '', '',
							    '', 0, 0, '4',
							    0, recordCabeceraCxpcliente.moneda, NOW()::date,(NOW() + '8 day')::date,
							     '0099-01-01 00:00:00'::timestamp, 'S', 0, 'N', '4',
							    '', '','','',
							    '','', 'N', 'N',
							    'N');
							    
							  
							    itemsDetalle :=1;
							    
							FOR recordDetalleCXPCliente in (
				    			SELECT
								_detalleCxp.documento_relacionado::VARCHAR AS planilla,
								'FINV'::VARCHAR as distrito,
								_negocio.cod_cli as proveedor,
								'FAP'::VARCHAR as tipo_doc,
								'' as descripcion,
								'OP'::VARCHAR as agencia,
								(select table_code from tablagen where table_type= 'AUTCXP' AND dato='PREDETERMINADO')as aprobador,
								recordClienteCXP.branch_code::VARCHAR as banco,
								recordClienteCXP.bank_account_no::VARCHAR as sucursal,
								'PES'::VARCHAR as moneda,
								cxp.vlr_neto::numeric as vlr_neto,
								1.0000000000::numeric as tasa,
								'FAP'::VARCHAR as tipo_referencia_1,
								cxp.documento::VARCHAR as referencia_1,
								'NEG'::VARCHAR as tipo_referencia_2,
								cxp.documento_relacionado::VARCHAR as referencia_2,
								''::VARCHAR as tipo_referencia_3,
								''::VARCHAR as referencia_3,
								cxp.documento::varchar as num_cxp
								FROM fin.cxp_doc cxp
								inner join detalle_poliza_negocio dpn on dpn.cxp_generada=cxp.documento
								WHERE documento_relacionado=_detalleCxp.documento_relacionado
								and tipo_documento='FAP'
								and cxp.dstrct='FINV' 
								and cxp.reg_status='' 
								and dpn.pago_total='S'
								and referencia_1=_detalleCxp.id_poliza)								
		
							LOOP
							    raise notice 'recordDetalleCXP.referencia_1: %',recordDetalleCXPCliente.referencia_1;
							       -- INSERTAMOS EN DETALLE DE CXP cliente
								INSERT INTO fin.cxp_items_doc(
								    reg_status, dstrct, proveedor, tipo_documento, documento, item,
								    descripcion, vlr, vlr_me, codigo_cuenta, codigo_abc, planilla,
								    last_update, user_update, creation_date, creation_user, base,
								    codcliarea, tipcliarea, concepto, auxiliar, tipo_referencia_1,
								    referencia_1, tipo_referencia_2, referencia_2, tipo_referencia_3,
								    referencia_3)
								VALUES('', 'FINV', recordDetalleCXPCliente.proveedor, recordDetalleCXPCliente.tipo_doc, _codigo_cxp_cliente,lpad(itemsDetalle, 3, '0'),
								    'CXP DEVOLUCION POLIZA '||_detalleCxp.poliza||': '|| recordDetalleCXPCliente.num_cxp,  recordDetalleCXPCliente.vlr_neto,  recordDetalleCXPCliente.vlr_neto,'23802004' , '',recordDetalleCXPCliente.planilla,
								    '0099-01-01 00:00:00'::timestamp, '', NOW(), usuario, 'COL',
								    '','','', '', recordDetalleCXPCliente.tipo_referencia_1,
								    recordDetalleCXPCliente.referencia_1, recordDetalleCXPCliente.tipo_referencia_2,recordDetalleCXPCliente.referencia_2,'', '');
		
								total_factura_cliente :=  total_factura + recordDetalleCXPCliente.vlr_neto;
								itemsDetalle := itemsDetalle+1;
								
							END LOOP;
					        -- ACTUALIZAMOS TOTAL CABECERA CXP Del cliente
		
								UPDATE fin.cxp_doc
								   SET
								    vlr_neto=(SELECT sum(vlr) FROM fin.cxp_items_doc  where documento=_codigo_cxp_cliente and tipo_documento='FAP'), --total_factura_cliente,
								    vlr_saldo=(SELECT sum(vlr) FROM fin.cxp_items_doc  where documento=_codigo_cxp_cliente and tipo_documento='FAP'),--total_factura_cliente,
								    vlr_neto_me=(SELECT sum(vlr) FROM fin.cxp_items_doc  where documento=_codigo_cxp_cliente and tipo_documento='FAP'),--total_factura_cliente,
								    vlr_saldo_me=(SELECT sum(vlr) FROM fin.cxp_items_doc  where documento=_codigo_cxp_cliente and tipo_documento='FAP')--total_factura_cliente
								 WHERE
								 documento = _codigo_cxp_cliente;
				
						
								_cxps_clientes=_cxps_clientes ||' '||_codigo_cxp_cliente;			
						end if; 
				
				end loop;
               --  CABECERA DE LA NOTA CREDITO 
           			INSERT INTO FIN.CXP_DOC
				    (
				    PROVEEDOR,TIPO_DOCUMENTO,DOCUMENTO,DESCRIPCION,AGENCIA,HANDLE_CODE,BANCO,
				    SUCURSAL, VLR_NETO, VLR_SALDO, VLR_NETO_ME,VLR_SALDO_ME,TASA,
				    CREATION_DATE,CREATION_USER,BASE,MONEDA_BANCO,FECHA_DOCUMENTO,FECHA_VENCIMIENTO,
				    CLASE_DOCUMENTO_REL,MONEDA,TIPO_DOCUMENTO_REL,DOCUMENTO_RELACIONADO,DSTRCT,
				    CLASE_DOCUMENTO,TIPO_REFERENCIA_1
				    ) 
				    select proveedor,'NC',DOCUMENTO||'-1','NC - CANCELA CXP ASEGURADORA '||documento,
				    AGENCIA,HANDLE_CODE,BANCO,SUCURSAL,VLR_NETO,VLR_NETO,VLR_NETO_ME,VLR_NETO_ME,TASA,NOW(),usuario,
				    BASE,MONEDA_BANCO,NOW()::DATE,NOW()::DATE,4,MONEDA,'FAP',DOCUMENTO,DSTRCT,4,'FACT'
				    from fin.cxp_doc where documento= ANY (items_cxp::varchar[]) and tipo_documento='FAP' and dstrct='FINV' AND reg_status='';
						
			
					INSERT INTO FIN.CXP_ITEMS_DOC
				    (
				    PROVEEDOR,TIPO_DOCUMENTO,DOCUMENTO,ITEM,DESCRIPCION,
				    VLR, VLR_ME,CODIGO_CUENTA,CREATION_DATE, CREATION_USER,BASE,DSTRCT
				    )	    
				    select proveedor,'NC',DOCUMENTO||'-1',1,'NC - CANCELA CXP ASEGURADORA '||documento,
				    VLR_NETO,VLR_NETO_ME,11050534,NOW(),usuario,'COL','FINV'
				    from fin.cxp_doc where documento= ANY (items_cxp::varchar[]) and tipo_documento='FAP' and dstrct='FINV' AND reg_status='';
				    
			    raise notice 'DOCUMENTO: %',_detalleCxp.documento;
	        -- ACTUALIZAMOS CXP RELACIONADAS COLOCANDO SALDO EN CERO
			    UPDATE fin.cxp_doc SET vlr_total_abonos = vlr_neto, vlr_saldo = 0, vlr_total_abonos_me = vlr_neto_me, vlr_saldo_me = 0
				WHERE documento =ANY (items_cxp::varchar[]) and reg_status='' and dstrct='FINV' and tipo_documento='FAP';
				
			--Buscamos informacion bancaria de la aseguradora
				SELECT INTO recordProveedorCXP payment_name, branch_code, bank_account_no  FROM proveedor
				WHERE nit = nit_aseguradora;
				
               --CABECERA CXP DEFINITIVA
				SELECT INTO recordCabecera
			        'FINV'::VARCHAR as distrito,
					nit_aseguradora::VARCHAR  as proveedor,
					'FAP'::VARCHAR as tipo_doc,
					'CXP A '||recordProveedorCXP.payment_name ||' '||tipo_poliza as descripcion,
					'OP'::VARCHAR as agencia,
					'PZ'::VARCHAR as handle_code,
					(select table_code from tablagen where table_type= 'AUTCXP' AND dato='PREDETERMINADO')as aprobador,
					recordProveedorCXP.branch_code::VARCHAR as banco,
					recordProveedorCXP.bank_account_no::VARCHAR as sucursal,
					'PES'::VARCHAR as moneda,
					0::numeric as vlr_neto,
					1.0000000000::numeric as tasa;
				
			IF(FOUND)THEN

            		SELECT INTO _codigo_cxp_aseguradora get_lcod_aseguradora('CXP_ASEGURADORA');


				
						INSERT INTO fin.cxp_doc(
					    reg_status, dstrct, proveedor, tipo_documento, documento, descripcion,
					    agencia, handle_code, id_mims, tipo_documento_rel, documento_relacionado,
					    fecha_aprobacion, aprobador, usuario_aprobacion, banco, sucursal,
					    moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me, vlr_total_abonos_me,
					    vlr_saldo_me, tasa, usuario_contabilizo, fecha_contabilizacion,
					    usuario_anulo, fecha_anulacion, fecha_contabilizacion_anulacion,
					    observacion, num_obs_autorizador, num_obs_pagador, num_obs_registra,
					    last_update, user_update, creation_date, creation_user, base,
					    corrida, cheque, periodo, fecha_procesado, fecha_contabilizacion_ajc,
					    fecha_contabilizacion_ajv, periodo_ajc, periodo_ajv, usuario_contabilizo_ajc,
					    usuario_contabilizo_ajv, transaccion_ajc, transaccion_ajv, clase_documento,
					    transaccion, moneda_banco, fecha_documento, fecha_vencimiento,
					    ultima_fecha_pago, flujo, transaccion_anulacion, ret_pago, clase_documento_rel,
					    tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,
					    tipo_referencia_3, referencia_3, indicador_traslado_fintra, factoring_formula_aplicada,
					    factura_tipo_nomina)
				        VALUES ('', 'FINV', recordCabecera.proveedor, recordCabecera.tipo_doc, _codigo_cxp_aseguradora, recordCabecera.descripcion,
					    recordCabecera.agencia, recordCabecera.handle_code, '', '', '',
					    '0099-01-01 00:00:00'::timestamp, recordCabecera.aprobador, '', recordCabecera.banco, recordCabecera.sucursal,
					    'PES', recordCabecera.vlr_neto, 0,  recordCabecera.vlr_neto, recordCabecera.vlr_neto, 0,
					     recordCabecera.vlr_neto,  1, '', '0099-01-01 00:00:00'::timestamp,
					    '',  '0099-01-01 00:00:00'::timestamp,  '0099-01-01 00:00:00'::timestamp,
					    '', 0, 0, 0,
					    '0099-01-01 00:00:00'::timestamp, '', NOW(), usuario, 'COL',
					    '', '', '', '0099-01-01 00:00:00'::timestamp, '0099-01-01 00:00:00'::timestamp,
					    '0099-01-01 00:00:00'::timestamp, '', '', '',
					    '', 0, 0, '4',
					    0, recordCabecera.moneda, NOW()::date,(NOW() + '8 day')::date,
					     '0099-01-01 00:00:00'::timestamp, 'S', 0, 'N', '4',
					    '', '','','',
					    '','', 'N', 'N',
					    'N');
				
				
				        -- DETALLE CXP DEFINITIVA
                                        items :=1;
					FOR recordDetalleCXP in (SELECT
									''::VARCHAR AS planilla,
									'FINV'::VARCHAR as distrito,
									nit_aseguradora as proveedor,
									'FAP'::VARCHAR as tipo_doc,
									'' as descripcion,
									'OP'::VARCHAR as agencia,
									(select table_code from tablagen where table_type= 'AUTCXP' AND dato='PREDETERMINADO')as aprobador,
									recordProveedorCXP.branch_code::VARCHAR as banco,
									recordProveedorCXP.bank_account_no::VARCHAR as sucursal,
									'PES'::VARCHAR as moneda,
									vlr_neto::numeric as vlr_neto,
									1.0000000000::numeric as tasa,
									'FAP'::VARCHAR as tipo_referencia_1,
									documento::VARCHAR as referencia_1,
									'NEG'::VARCHAR as tipo_referencia_2,
									documento_relacionado::VARCHAR as referencia_2,
									''::VARCHAR as tipo_referencia_3,
									''::VARCHAR as referencia_3
								FROM fin.cxp_doc
								WHERE documento= ANY (items_cxp::varchar[]) and tipo_documento='FAP' and dstrct='FINV' AND reg_status='')
								

					LOOP
					  
					       -- INSERTAMOS EN DETALLE DE CXP DEFINITIVA
						INSERT INTO fin.cxp_items_doc(
						    reg_status, dstrct, proveedor, tipo_documento, documento, item,
						    descripcion, vlr, vlr_me, codigo_cuenta, codigo_abc, planilla,
						    last_update, user_update, creation_date, creation_user, base,
						    codcliarea, tipcliarea, concepto, auxiliar, tipo_referencia_1,
						    referencia_1, tipo_referencia_2, referencia_2, tipo_referencia_3,
						    referencia_3)
						VALUES('', 'FINV', recordDetalleCXP.proveedor, recordDetalleCXP.tipo_doc, _codigo_cxp_aseguradora,lpad(items, 3, '0'),
						    'CXP A '||recordProveedorCXP.payment_name||' '||tipo_poliza ,  recordDetalleCXP.vlr_neto,  recordDetalleCXP.vlr_neto, 11050534, '','',
						    '0099-01-01 00:00:00'::timestamp, '', NOW(), usuario, 'COL',
						    '','','', '', recordDetalleCXP.tipo_referencia_1,
						    recordDetalleCXP.referencia_1, recordDetalleCXP.tipo_referencia_2,recordDetalleCXP.referencia_2,'', '');

							total_factura :=  total_factura + recordDetalleCXP.vlr_neto;
							items := items+1;
						
							--Actualizacion la cxp pequeña de poliza como procesada
							   UPDATE fin.cxp_doc
							   SET
							   last_update=NOW(),
							   user_update=usuario,
							   referencia_3 = 'P'
							   WHERE
							   documento = recordDetalleCXP.referencia_1;
							   
					END LOOP;
					
			        -- ACTUALIZAMOS TOTAL CABECERA CXP DEFINITIVA

						UPDATE fin.cxp_doc
						   SET
						    vlr_neto=(SELECT sum(vlr) FROM fin.cxp_items_doc  where documento=_codigo_cxp_aseguradora and tipo_documento='FAP'), --total_factura,
						    vlr_saldo=(SELECT sum(vlr) FROM fin.cxp_items_doc  where documento=_codigo_cxp_aseguradora and tipo_documento='FAP'),--total_factura,
						    vlr_neto_me=(SELECT sum(vlr) FROM fin.cxp_items_doc  where documento=_codigo_cxp_aseguradora and tipo_documento='FAP'),--total_factura,
						    vlr_saldo_me=(SELECT sum(vlr) FROM fin.cxp_items_doc  where documento=_codigo_cxp_aseguradora and tipo_documento='FAP')--total_factura
						 WHERE
						 documento = _codigo_cxp_aseguradora;
			
			ELSE
			  rs:='ERROR';
			END IF;


	RETURN 	 rs||';'||_codigo_cxp_aseguradora::varchar||';'||_cxps_clientes::varchar;

END $BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION fn_cxp_consolidada_aseguradoras(text[], character varying, character varying, character varying)
  OWNER TO postgres;
