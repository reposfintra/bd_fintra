-- Function: sp_seguimiento_cartera_geotech_migrar(numeric, character varying, character varying)

-- DROP FUNCTION sp_seguimiento_cartera_geotech_migrar(numeric, character varying, character varying);

CREATE OR REPLACE FUNCTION sp_seguimiento_cartera_geotech_migrar(periodoasignacion numeric, unidadnegocio character varying, agenteext character varying)
  RETURNS SETOF record AS
$BODY$

DECLARE

	CarteraTotales record;
	CarteraGeneral record;
	CarteraWtramoAnterior record;
	ClienteRec record;
	BankPay record;
	FchLastPay record;
	
	NegocioAvales record;
	NegocioSeguros record;
	NegocioGps record;
	
	NegocioSegurosGps record;
	NegocioVencimientoSeguro record;
	NegocioVencimientoGps record;
	
	SumaDeAval record;
	Rs_ResultPay record;

	PercValorAsignado numeric;
	PercCantAsignado numeric;
	_TramoAnterior numeric;
	PeriodoTramo numeric;
	PeriodoTramoAnterior numeric;
	_SumDebidoCobrar numeric;
	Ingresoxcuota_fiducia numeric;
	Ingresoxcuota_fenalco numeric;
	IngresoxCuota numeric;

	MaxVel integer := 0;

	CadAgentes varchar;
	periodo_corte varchar;
	FechaCortePeriodo varchar;
	FechaCortePeriodoAnt varchar;
	StatusVcto varchar;
	UltimoPago varchar;
	NegocioArray record;
	FirstTime varchar;


	ReturnTabla varchar := '';

	miHoy date;

BEGIN

	if ( substring(PeriodoAsignacion,5) = '01' ) then
		PeriodoTramo = substring(PeriodoAsignacion,1,4)::numeric-1||'12';
		_TramoAnterior = substring(PeriodoAsignacion,1,4)::numeric-1||'12';
	else
		PeriodoTramo = PeriodoAsignacion::numeric - 1;
		_TramoAnterior = PeriodoAsignacion::numeric - 1;
	end if;
	
	--PeriodoTramo = PeriodoAsignacion::numeric - 1; 
	PeriodoTramoAnterior = PeriodoTramo::numeric - 1;
	
	--_TramoAnterior = PeriodoAsignacion::numeric - 1;
	
	select into FechaCortePeriodo to_char(to_timestamp(substring(PeriodoTramo,1,4)::numeric || '-' || to_char(substring(PeriodoTramo,5,2)::numeric,'FM00') || '-01', 'YYYY-MM-DD') + INTERVAL '1 month' - INTERVAL '1 days', 'YYYY-MM-DD');
	select into FechaCortePeriodoAnt to_char(to_timestamp(substring(PeriodoTramoAnterior,1,4)::numeric || '-' || to_char(substring(PeriodoTramoAnterior,5,2)::numeric,'FM00') || '-01', 'YYYY-MM-DD') + INTERVAL '1 month' - INTERVAL '1 days', 'YYYY-MM-DD');
	
	raise notice 'PeriodoTramo: %,FechaCortePeriodo: %',PeriodoTramo,FechaCortePeriodo;
	
	miHoy = now()::date;

	FOR CarteraGeneral IN 
		
		select 
			nit::varchar as cedula,
			''::varchar as nombre_cliente, 
			''::varchar as direccion, 
			''::varchar as ciudad, 
			''::varchar as telefono, 
			''::varchar as telcontacto,
			negasoc::varchar as negocio,		
			id_convenio::varchar,
			''::varchar as pagaduria, 
			num_doc_fen::varchar as cuota,
			fcg.valor_factura::numeric as valor_asignado,
			fecha_vencimiento::date,
			replace(substring(fecha_vencimiento,1,7),'-','')::numeric as periodo_vcto,
			(
			SELECT 
				CASE WHEN maxdia >= 365 THEN '8- MAYOR A 1 ANIO'
				     WHEN maxdia >= 181 THEN '7- ENTRE 180 Y 360'	
				     WHEN maxdia >= 121 THEN '6- ENTRE 121 Y 180'
				     WHEN maxdia >= 91 THEN '5- ENTRE 91 Y 120'
				     WHEN maxdia >= 61 THEN '4- ENTRE 61 Y 90'
				     WHEN maxdia >= 31 THEN '3- ENTRE 31 Y 60'
				     WHEN maxdia >= 1 THEN '2- 1 A 30'	     
				     WHEN maxdia <= 0 THEN '1- CORRIENTE'
					ELSE '0' END AS rango
			FROM (	
				 SELECT max(FechaCortePeriodo::date-(fecha_vencimiento)) as maxdia
				 FROM con.foto_cartera_geotech fra            
				 WHERE fra.dstrct = 'GEOT'       
					  AND fra.valor_saldo > 0            
					  AND fra.reg_status = '' 
					  AND fra.negasoc = fcg.negasoc
					  AND fra.tipo_documento in ('FAC','NDC','FACN')
					  AND substring(fra.documento,1,2) not in ('CP','FF','DF') 
					  AND fra.periodo_lote = PeriodoAsignacion
				 GROUP BY negasoc
					
			) tabla2
			)::varchar as vencimiento_mayor,
			(FechaCortePeriodo::date-fecha_vencimiento)::numeric AS dias_vencidos,
			--substring(fecha_vencimiento,9)::numeric as dia_pago,
			'5'::numeric as dia_pago,
			
			''::varchar as status,
			''::varchar as status_vencimiento,
			fcg.valor_factura::numeric as debido_cobrar,
			0::numeric as recaudosxcuota_fiducia,
			0::numeric as recaudosxcuota_fenalco,
			coalesce(fcg.valor_abono::numeric,0) as recaudosxcuota,
			agente::varchar,
			agente_campo::varchar
		--select *	
		from con.foto_cartera_geotech fcg --, con.foto_cartera_apoteosys fca
		where periodo_lote = PeriodoAsignacion
			--and valor_saldo > 0 
			and fcg.reg_status = '' 
			and fcg.dstrct = 'GEOT'
			and fcg.tipo_documento = 'FACN' --and fcg.nit = '890100531-8'
			and fcg.id_convenio = (select id_convenio from rel_unidadnegocio_convenios where id_unid_negocio in (select id from unidad_negocio where id = UnidadNegocio))
		order by negasoc
	LOOP
		
		SELECT INTO ClienteRec nomcli,direccion,ciudad,case when telefono is null then '0' else telefono end as telefono,telcontacto FROM cliente WHERE nit = CarteraGeneral.cedula;
		CarteraGeneral.nombre_cliente = ClienteRec.nomcli;
		CarteraGeneral.direccion = ClienteRec.direccion;
		CarteraGeneral.ciudad = ClienteRec.ciudad;
		CarteraGeneral.telefono = ClienteRec.telefono;
		CarteraGeneral.telcontacto = ClienteRec.telcontacto;
		
		
		RETURN NEXT CarteraGeneral;
		
	END LOOP;
		
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION sp_seguimiento_cartera_geotech_migrar(numeric, character varying, character varying)
  OWNER TO postgres;
