
-- Function: eg_saldos_refinanciacion_negocios(character varying)

-- DROP FUNCTION eg_saldos_refinanciacion_negocios(character varying,character VARYING);

CREATE OR REPLACE FUNCTION eg_saldos_refinanciacion_negocios(_cod_neg character VARYING, _tipo_refinanciacion character VARYING)
  RETURNS SETOF record AS
$BODY$
DECLARE
  listaFacturas record;
  filaLiquidador record;
  saldo_factura record;
  fechaAnterior date;
  sumaConceptos NUMERIC;
  tasaIm  NUMERIC;
  tasaIg  NUMERIC;
  query varchar;

BEGIN
	
  IF _tipo_refinanciacion='A' THEN 
  
     query:='SELECT   
				 fac.documento::varchar as documento,
				 fac.negasoc::varchar as negocio,
				 fac.num_doc_fen::varchar as cuota,
				 fac.fecha_vencimiento::date as fecha_vencimiento,	
				 (now()::date-(fac.fecha_vencimiento))::numeric as dias_mora,
				 COALESCE(e.esquema,''N'') AS esquema, 
				 ''''::varchar as estado,
				 0::numeric as valor_saldo_capital,
				 0::numeric as valor_saldo_mi,
				 0::numeric as valor_saldo_ca,
				 0::numeric as valor_cm,
                 0::numeric as valor_seguro,
                 0::numeric as valor_saldo_cuota,
				 0::numeric as IxM,
				 0::numeric as GaC,
				 0::numeric as suma_saldos	
			FROM con.factura as fac 
			INNER JOIN con.factura_detalle as facdet on (fac.documento=facdet.documento AND fac.tipo_documento=facdet.tipo_documento AND fac.nit=facdet.nit)
		    LEFT  JOIN tem.negocios_facturacion_old e ON e.cod_neg=fac.negasoc
			WHERE fac.negasoc='''|| _cod_neg||'''
			AND fac.documento LIKE ''MC%'' 
			AND valor_saldo > 0 
			AND fac.reg_status !=''A''
			AND facdet.descripcion=''CAPITAL''	
            AND to_char(fac.fecha_vencimiento,''YYYYMM'')::INTEGER <=TO_CHAR(CURRENT_DATE,''YYYYMM'')::INTEGER
			order by  fac.num_doc_fen::numeric ';
  
  ELSIF   _tipo_refinanciacion='B' THEN 
  
  	 query:='SELECT   
				 fac.documento::varchar as documento,
				 fac.negasoc::varchar as negocio,
				 fac.num_doc_fen::varchar as cuota,
				 fac.fecha_vencimiento::date as fecha_vencimiento,	
				 (now()::date-(fac.fecha_vencimiento))::numeric as dias_mora,
				 COALESCE(e.esquema,''N'') AS esquema, 
				 ''::varchar as estado,
				 0::numeric as valor_saldo_capital,
				 0::numeric as valor_saldo_mi,
				 0::numeric as valor_saldo_ca,
				 0::numeric as valor_cm,
                 0::numeric as valor_seguro,
                 0::numeric as valor_saldo_cuota,
				 0::numeric as IxM,
				 0::numeric as GaC,
				 0::numeric as suma_saldos	
			FROM con.factura as fac 
			INNER JOIN con.factura_detalle as facdet on (fac.documento=facdet.documento AND fac.tipo_documento=facdet.tipo_documento AND fac.nit=facdet.nit)
		    LEFT  JOIN tem.negocios_facturacion_old e ON e.cod_neg=fac.negasoc
			WHERE fac.negasoc= '''|| _cod_neg||'''
			AND fac.documento LIKE ''MC%'' 
			AND valor_saldo > 0 
			AND fac.reg_status !=''A''
			AND facdet.descripcion=''CAPITAL''	
			order by  fac.num_doc_fen::numeric ';
  
  ELSE 
    -- RETURN NEXT NULL;
  END IF;
  
     RAISE NOTICE 'sql : % ', query;
 
   FOR listaFacturas IN EXECUTE query
	LOOP
	    
		sumaConceptos=0;
        tasaIm=0;
		tasaIg=0;
		
        SELECT INTO saldo_factura * FROM eg_detalle_saldo_facturas_mc(listaFacturas.negocio, listaFacturas.cuota::integer);

		IF (listaFacturas.dias_mora > 0) THEN 
			/* ***********************
			* Estado de la factura   *
			**************************/
			listaFacturas.estado='VENCIDO';
			listaFacturas.valor_saldo_capital:=saldo_factura.saldo_capital;
			listaFacturas.valor_saldo_mi :=saldo_factura.saldo_interes;
			listaFacturas.valor_saldo_ca :=saldo_factura.saldo_cat;
			listaFacturas.valor_cm :=saldo_factura.saldo_cuota_manejo;
			listaFacturas.valor_seguro:=saldo_factura.saldo_seguro;
			listaFacturas.valor_saldo_cuota := saldo_factura.saldo_factura;
			sumaConceptos=saldo_factura.saldo_factura;

			/* *******************************************
			* Buscamos tasa del negocio. por el convenio *
			**********************************************/
			
			SELECT INTO tasaIm c.tasa_usura FROM negocios as n
			INNER JOIN convenios as c on (c.id_convenio=n.id_convenio)
			WHERE n.cod_neg =listaFacturas.negocio;
           
			 IF (FOUND) THEN
				listaFacturas.IxM:= round((((sumaConceptos * tasaIm)/100 ) / 30) * listaFacturas.dias_mora );
			END IF;
			
			/* *******************************
			* Gastos de Cobranza por factura *
			**********************************/	
			SELECT INTO tasaIg max(porcentaje) FROM sanciones_condonaciones 
			WHERE id_unidad_negocio = 1 
			AND id_conceptos_recaudo in (2,4,6) 
			AND id_tipo_acto = 1 
			AND categoria='GAC'
			AND periodo = replace(substring(now(),1,7),'-','')
			AND listaFacturas.dias_mora between dias_rango_ini and dias_rango_fin
            GROUP BY id_conceptos_recaudo
			ORDER by id_conceptos_recaudo ;
			
			 IF (FOUND) THEN
				listaFacturas.GaC =round((sumaConceptos * tasaIg) ::numeric /100) ; 
			END IF;

            /* *******************
			 ** Suma saldos *****
			 *********************/
            listaFacturas.suma_saldos=sumaConceptos + listaFacturas.IxM +listaFacturas.GaC ;                     
			fechaAnterior =listaFacturas.fecha_vencimiento;
			
		ELSIF (to_char(listaFacturas.fecha_vencimiento,'YYYYMM')::INTEGER =to_char(current_date,'YYYYMM')) THEN 	
		    listaFacturas.estado='CORRIENTE';	
		    listaFacturas.valor_saldo_capital:=saldo_factura.saldo_capital;
			listaFacturas.valor_saldo_mi :=saldo_factura.saldo_interes;
			listaFacturas.valor_saldo_ca :=saldo_factura.saldo_cat;
			listaFacturas.valor_cm :=saldo_factura.saldo_cuota_manejo;
			listaFacturas.valor_seguro:=saldo_factura.saldo_seguro;
			listaFacturas.valor_saldo_cuota := saldo_factura.saldo_factura;
		    listaFacturas.suma_saldos :=saldo_factura.saldo_factura;
		ELSE 

		    /* ***********************
		     * Estado de la factura   *
		     **************************/
		     listaFacturas.estado='FUTURO';	
             SELECT INTO filaLiquidador * FROM documentos_neg_aceptado d   where d.cod_neg=listaFacturas.negocio and d.item =listaFacturas.cuota ;
		
		     listaFacturas.valor_saldo_ca=filaLiquidador.cat;
    	     listaFacturas.suma_saldos= listaFacturas.valor_saldo_capital + filaLiquidador.cat;
             
		END IF;


	RETURN NEXT listaFacturas;

   END LOOP; 

END;$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION eg_saldos_refinanciacion_negocios(character VARYING,character VARYING)
  OWNER TO postgres;