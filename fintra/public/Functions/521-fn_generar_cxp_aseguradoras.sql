-- Function: fn_generar_cxp_aseguradoras(character varying, character varying)

-- DROP FUNCTION fn_generar_cxp_aseguradoras(character varying, character varying);


CREATE OR REPLACE FUNCTION fn_generar_cxp_aseguradoras(
    _codigo character varying,
    _usuario character varying)
  RETURNS text AS
$BODY$

DECLARE

	_config_poliza RECORD;
	_detalle_cxp RECORD;
	_resp varchar;
	_compra_cartera varchar;
	_serial varchar;
	_valor_cxp numeric:=0;
	_impuesto RECORD;
	_info_negocio RECORD;

BEGIN
				
				select into _info_negocio  n.cod_neg,n.id_convenio from solicitud_aval sa 
				inner join negocios n on n.cod_neg=sa.cod_neg 
				where numero_solicitud=_codigo;
				
				
				for _detalle_cxp in

				select dpn.cod_neg,dpn.item,dpn.nit_aseguradora,dpn.valor,dpn.fecha_vencimiento,dpn.id_poliza,dpn.valor_iva
				from detalle_poliza_negocio dpn
				where dpn.cod_neg=_codigo order by dpn.nit_aseguradora,dpn.item 
				
				loop
			    
				_valor_cxp=_detalle_cxp.valor+_detalle_cxp.valor_iva;
				
				select into _serial get_lcod('CXP_POLIZA',_detalle_cxp.item);
				
				
				INSERT INTO FIN.CXP_ITEMS_DOC
			    (
			    PROVEEDOR,TIPO_DOCUMENTO,DOCUMENTO,ITEM,DESCRIPCION,
			    VLR,
			    VLR_ME,
			    CODIGO_CUENTA,
			    PLANILLA,
			    CREATION_DATE,
			    CREATION_USER,
			    BASE,
			    AUXILIAR,
			    DSTRCT)
			    VALUES
			    (_detalle_cxp.nit_aseguradora,'FAP',_serial,1,'CXP - A: '||GET_NOMBP(_detalle_cxp.nit_aseguradora),
			    ROUND(_detalle_cxp.valor),ROUND(_detalle_cxp.valor),'11050534',_info_negocio.cod_neg,NOW(),_USUARIO,'COL','AR-'||_detalle_cxp.nit_aseguradora,'FINV');
			    
			    if _detalle_cxp.valor_iva>0 then
			    
			     INSERT INTO FIN.CXP_ITEMS_DOC
			    (
			    PROVEEDOR,TIPO_DOCUMENTO,DOCUMENTO,ITEM,DESCRIPCION,
			    VLR,
			    VLR_ME,
			    CODIGO_CUENTA,
			    PLANILLA,
			    CREATION_DATE,
			    CREATION_USER,
			    BASE,
			    AUXILIAR,
			    DSTRCT)
			    VALUES
			    (_detalle_cxp.nit_aseguradora,'FAP',_serial,2,'IVA',
			    ROUND(_detalle_cxp.valor_iva),ROUND(_detalle_cxp.valor_iva),'11050534',_info_negocio.cod_neg,NOW(),_USUARIO,'COL','AR-'||_detalle_cxp.nit_aseguradora,'FINV');
				
				end if;
				
			    INSERT INTO
			    FIN.CXP_DOC
			    (
			    PROVEEDOR,TIPO_DOCUMENTO,DOCUMENTO,DESCRIPCION,AGENCIA,BANCO,
			    SUCURSAL,VLR_NETO,VLR_SALDO,VLR_NETO_ME,VLR_SALDO_ME,
			    TASA,CREATION_DATE,CREATION_USER, BASE,MONEDA_BANCO,FECHA_DOCUMENTO,FECHA_VENCIMIENTO,
			    CLASE_DOCUMENTO_REL,
			    MONEDA,
			    TIPO_DOCUMENTO_REL,
			    DOCUMENTO_RELACIONADO,
			    HANDLE_CODE,
			    DSTRCT,
			    tipo_referencia_1,
			    referencia_1
			    )
			    VALUES

			    (_detalle_cxp.nit_aseguradora,'FAP',_serial,'CXP - A: '||GET_NOMBP(_detalle_cxp.nit_aseguradora),'OP',GET_BANCOCXPM(_detalle_cxp.nit_aseguradora),
			    GET_SUCURSALBANK(_detalle_cxp.nit_aseguradora),ROUND(_valor_cxp),ROUND(_valor_cxp),ROUND(_valor_cxp),ROUND(_valor_cxp),
			    1,NOW(),_usuario,'COL','PES',NOW(),_detalle_cxp.fecha_vencimiento::date,'NEG','PES','NEG',_info_negocio.cod_neg,'PZ','FINV','PLZ',_detalle_cxp.id_poliza);
			    
			    
			    update detalle_poliza_negocio set cod_neg=_info_negocio.cod_neg,cxp_generada=_serial ,user_update=_usuario,last_update=now()
                where item=_detalle_cxp.item
                and id_poliza=_detalle_cxp.id_poliza
                and cod_neg=_codigo
                and fecha_vencimiento::date=_detalle_cxp.fecha_vencimiento::date;
			
				UPDATE series 
                set last_number=last_number+1 
                where document_type = 'CXP_POLIZA'
                and reg_status='';
                
                
                if _detalle_cxp.valor_iva>0 then
                select into _impuesto  codigo_impuesto,porcentaje1
				from tipo_de_impuesto ti
				inner join convenios c on c.impuesto=ti.codigo_impuesto
				where c.id_convenio=_info_negocio.id_convenio
				and fecha_vigencia=to_date(to_char(current_date, 'YYYY')||'1231','YYYYMMDD');
                
                				
				INSERT INTO fin.cxp_imp_doc
				(reg_status, dstrct, proveedor, tipo_documento, documento,
			     cod_impuesto, porcent_impuesto, vlr_total_impuesto, vlr_total_impuesto_me, user_update, creation_user, base)
				VALUES('', 'FINV', _detalle_cxp.nit_aseguradora, 'FAP', _serial,
				_impuesto.codigo_impuesto, _impuesto.porcentaje1, _detalle_cxp.valor_iva, _detalle_cxp.valor_iva, _usuario, _usuario, 'COL');
				end if;
			
                        
				end loop;
			
				
	RETURN 'OK';

END;

$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION fn_generar_cxp_aseguradoras(character varying, character varying)
  OWNER TO postgres;
