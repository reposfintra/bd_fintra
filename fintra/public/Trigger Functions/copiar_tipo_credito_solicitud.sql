CREATE OR REPLACE FUNCTION copiar_tipo_credito_solicitud()
    RETURNS "trigger" AS $$
    /************************************************************************
    *   AUTHOR JDBERMUDEZ                                                   *
    *   DATE: 16-08-2018                                                    *
    *   DESCRIPTION: copia el tipo de crédito de la presolicitud de micro   *
    *                crédito en la tabla solicitud aval.                    *
    *************************************************************************/
BEGIN
    new.tipo_credito := coalesce((SELECT tipo_credito FROM apicredit.pre_solicitudes_creditos WHERE numero_solicitud = new.numero_solicitud), 'NUE');
    RETURN new;
END;
$$
LANGUAGE plpgsql
VOLATILE;