-- Type: rs_detalle_saldo_facturas

-- DROP TYPE rs_detalle_saldo_facturas;

CREATE TYPE rs_detalle_saldo_facturas AS
    (documento CHARACTER VARYING,
    negocio CHARACTER VARYING,
    cuota INTEGER,
    total_factura NUMERIC,
    saldo_capital NUMERIC,
    saldo_interes NUMERIC,
    saldo_cat NUMERIC,
    saldo_cuota_manejo NUMERIC,
    saldo_seguro NUMERIC,
    saldo_capital_aval NUMERIC,
    saldo_interes_aval NUMERIC,
    total_abonos NUMERIC,
    saldo_factura NUMERIC);
ALTER TYPE rs_detalle_saldo_facturas
    OWNER TO postgres;
