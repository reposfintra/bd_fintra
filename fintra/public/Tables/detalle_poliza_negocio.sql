CREATE TABLE detalle_poliza_negocio (
		id                      SERIAL,
		cod_neg                 VARCHAR(15) NOT NULL,
		id_configuracion_poliza INTEGER     NOT NULL,
		fecha_vencimiento       TIMESTAMP WITHOUT TIME ZONE,
		item                    VARCHAR(15) NOT NULL,
		valor                   NUMERIC(11, 2) NOT NULL,
		CONSTRAINT detalle_poliza_negocio_pk PRIMARY KEY (cod_neg, id_configuracion_poliza, item),
		CONSTRAINT id_configuracion_poliza_detalle_poliza_negocio_fk FOREIGN KEY (id_configuracion_poliza) REFERENCES administrativo.nueva_configuracion_poliza (id)
);

ALTER TABLE detalle_poliza_negocio ALTER COLUMN item TYPE integer USING item::integer;

-- Se elimina constraint id_configuracion_poliza_detalle_poliza_negocio_fk
ALTER TABLE detalle_poliza_negocio DROP CONSTRAINT id_configuracion_poliza_detalle_poliza_negocio_fk;

-- Se elimina columna  id_configuracion_poliza

ALTER TABLE detalle_poliza_negocio DROP COLUMN id_configuracion_poliza;

--Se agregan columnas de la configuracion poliza 
ALTER TABLE detalle_poliza_negocio ADD COLUMN nit_aseguradora bigint NOT null DEFAULT 0;
ALTER TABLE detalle_poliza_negocio ADD COLUMN id_poliza integer NOT null DEFAULT 0;
ALTER TABLE detalle_poliza_negocio ADD COLUMN tc_tipo character(1) NOT null DEFAULT '';
ALTER TABLE detalle_poliza_negocio ADD COLUMN tc_financiacion character(1) NOT null DEFAULT '';
ALTER TABLE detalle_poliza_negocio ADD COLUMN tvp_tipo character(1) NOT null DEFAULT '';
ALTER TABLE detalle_poliza_negocio ADD COLUMN tvp_calcular_sobre character varying(5) DEFAULT '';
ALTER TABLE detalle_poliza_negocio ADD COLUMN tvp_valor_absoluto numeric(11,2) DEFAULT 0;
ALTER TABLE detalle_poliza_negocio ADD COLUMN tvp_valor_porcentaje numeric(6,4) DEFAULT 0;
ALTER TABLE detalle_poliza_negocio ADD COLUMN id_sucursal integer NOT null DEFAULT 0;
ALTER TABLE detalle_poliza_negocio ADD COLUMN id_unidad_negocio integer NOT null DEFAULT 0;
ALTER TABLE detalle_poliza_negocio ADD COLUMN cxp_generada character varying(30) NOT NULL DEFAULT '';

--Se agregan columnas de auditoria
ALTER TABLE detalle_poliza_negocio ADD COLUMN reg_status VARCHAR(1) DEFAULT '' NOT NULL;
ALTER TABLE detalle_poliza_negocio ADD COLUMN dstrct VARCHAR(6) DEFAULT 'FINV' NOT NULL;
ALTER TABLE detalle_poliza_negocio ADD COLUMN creation_user VARCHAR(15) DEFAULT '' NOT NULL;
ALTER TABLE detalle_poliza_negocio ADD COLUMN creation_date TIMESTAMP WITHOUT TIME ZONE DEFAULT current_timestamp;
ALTER TABLE detalle_poliza_negocio ADD COLUMN user_update VARCHAR(15) DEFAULT '';
ALTER TABLE detalle_poliza_negocio ADD COLUMN last_update TIMESTAMP WITHOUT TIME ZONE DEFAULT '0099-01-01'::TIMESTAMP;

--Se agrega campo de validacion de compra de cartera
ALTER TABLE detalle_poliza_negocio ADD COLUMN pago_total VARCHAR(5) DEFAULT 'N' NOT NULL;

-- se agrega campo valor iva 
ALTER TABLE detalle_poliza_negocio ADD COLUMN valor_iva numeric(11,2) DEFAULT 0 NOT NULL;



