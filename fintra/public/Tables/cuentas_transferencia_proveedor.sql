-- Drop table

-- DROP TABLE public.cuentas_transferencia_proveedor

CREATE TABLE public.cuentas_transferencia_proveedor (
    id                 SERIAL      NOT NULL,
    reg_status         VARCHAR(1)  NOT NULL DEFAULT '' :: CHARACTER VARYING,
    dstrct             VARCHAR(4)  NOT NULL DEFAULT '' :: CHARACTER VARYING,
    nit_proveedor      VARCHAR(15) NOT NULL DEFAULT '' :: CHARACTER VARYING,
    cedula_titular     VARCHAR(15) NOT NULL DEFAULT '' :: CHARACTER VARYING,
    nombre_titular     VARCHAR(25) NOT NULL DEFAULT '' :: CHARACTER VARYING,
    banco_transfer     VARCHAR(20) NOT NULL DEFAULT '' :: CHARACTER VARYING,
    suc_transfer       VARCHAR(20) NOT NULL DEFAULT '' :: CHARACTER VARYING,
    tipo_cuenta        VARCHAR(15) NOT NULL,
    no_cuenta          VARCHAR(20) NOT NULL DEFAULT '' :: CHARACTER VARYING,
    cuenta_por_defecto VARCHAR(2)  NOT NULL DEFAULT '' :: CHARACTER VARYING,
    creation_date      TIMESTAMP   NOT NULL DEFAULT now(),
    creation_user      VARCHAR(15) NOT NULL DEFAULT '' :: CHARACTER VARYING,
    last_update        TIMESTAMP   NOT NULL DEFAULT '0099-01-01 00:00:00' :: TIMESTAMP WITHOUT TIME ZONE,
    user_update        VARCHAR(20) NOT NULL DEFAULT '' :: CHARACTER VARYING,
    cod_dpto           VARCHAR(5)  NULL,
    cod_ciudad         VARCHAR(5)  NULL
);

CREATE INDEX i_cuentas_proveedor
    ON cuentas_transferencia_proveedor (reg_status, nit_proveedor);

