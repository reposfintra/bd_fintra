-- Table: solicitud_estudiante

-- DROP TABLE solicitud_estudiante;

CREATE TABLE solicitud_estudiante (
    reg_status             CHARACTER VARYING(1)        NOT NULL DEFAULT ''::CHARACTER VARYING,
    dstrct                 CHARACTER VARYING(6)        NOT NULL DEFAULT ''::CHARACTER VARYING,
    numero_solicitud       INTEGER                     NOT NULL DEFAULT 0,
    parentesco_girador     CHARACTER VARYING(20)       NOT NULL DEFAULT ''::CHARACTER VARYING,
    universidad            CHARACTER VARYING(150)      NOT NULL DEFAULT ''::CHARACTER VARYING,
    programa               CHARACTER VARYING(100)      NOT NULL DEFAULT ''::CHARACTER VARYING,
    fecha_ingreso_programa TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT '0099-01-01 00:00:00'::TIMESTAMP WITHOUT TIME ZONE,
    codigo                 CHARACTER VARYING(15)       NOT NULL DEFAULT ''::CHARACTER VARYING,
    semestre               INTEGER                     NOT NULL DEFAULT 0,
    valor_semestre         NUMERIC(15, 2)              NOT NULL DEFAULT 0.0,
    tipo_carrera           CHARACTER VARYING(15)       NOT NULL DEFAULT ''::CHARACTER VARYING,
    trabaja                CHARACTER VARYING(1)        NOT NULL DEFAULT 'N'::CHARACTER VARYING,
    nombre_empresa         CHARACTER VARYING(100)      NOT NULL DEFAULT ''::CHARACTER VARYING,
    direccion_empresa      CHARACTER VARYING(100)      NOT NULL DEFAULT ''::CHARACTER VARYING,
    telefono_empresa       CHARACTER VARYING(15)       NOT NULL DEFAULT ''::CHARACTER VARYING,
    salario                NUMERIC(15, 2)              NOT NULL DEFAULT 0.0,
    creation_date          TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    creation_user          CHARACTER VARYING(50)       NOT NULL DEFAULT ''::CHARACTER VARYING,
    last_update            TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    user_update            CHARACTER VARYING(50)       NOT NULL DEFAULT ''::CHARACTER VARYING,
    colegio_bachillerato   CHARACTER VARYING(1)        NOT NULL DEFAULT ''::CHARACTER VARYING,
    nivel_educativo_padre  CHARACTER VARYING(30)       NOT NULL DEFAULT ''::CHARACTER VARYING,
    sisben                 CHARACTER VARYING(30)       NOT NULL DEFAULT ''::CHARACTER VARYING,
    CONSTRAINT fk_solicitud_aval FOREIGN KEY (numero_solicitud)
        REFERENCES solicitud_aval (numero_solicitud) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
)
    WITH (
        OIDS= FALSE
    );
ALTER TABLE solicitud_estudiante
    OWNER TO postgres;
GRANT ALL ON TABLE solicitud_estudiante TO postgres;
GRANT SELECT ON TABLE solicitud_estudiante TO msoto;



ALTER TABLE solicitud_estudiante
    ALTER COLUMN codigo TYPE VARCHAR(50);