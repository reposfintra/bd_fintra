-- Table: apicredit.cliente_aval_preferencial

-- DROP TABLE apicredit.cliente_aval_preferencial;

create table apicredit.cliente_aval_preferencial(
		id serial,
		reg_status        character(1) not null default '' ::character varying,
		dstrct            character varying(4) not null default '':: character varying,
		identificacion 	  character varying(15) not null default ''::character varying primary key ,
		nombre			  character varying (60) not null default ''::character varying,
		porcentaje 		  numeric(11,3) NOT NULL DEFAULT 0,
		iva		 		  numeric(11,3) NOT NULL DEFAULT 0,		
		creation_date     timestamp without time zone default now(),
		last_update       timestamp without time zone default now(),
		creation_user     character varying(10)  not null default ''
)WITH (
  OIDS=FALSE
);
ALTER TABLE apicredit.configuracion_codigos_dc
  OWNER TO postgres;
  
ALTER TABLE  apicredit.cliente_aval_preferencial ADD COLUMN periodo CHARACTER VARYING (6) NOT NULL DEFAULT '';

ALTER TABLE  apicredit.cliente_aval_preferencial ADD COLUMN valor_cartera numeric(11,2) NOT NULL DEFAULT 0;

ALTER TABLE apicredit.cliente_aval_preferencial ADD COLUMN valor_desembolso_base NUMERIC(11,2) NOT NULL DEFAULT 0;