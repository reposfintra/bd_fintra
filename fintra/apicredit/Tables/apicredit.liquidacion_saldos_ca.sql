-- DROP TABLE apicredit.liquidacion_saldos_ca;

CREATE TABLE apicredit.liquidacion_saldos_ca (
    reg_status        CHARACTER VARYING(1)        NOT NULL DEFAULT '' :: CHARACTER VARYING,
    dstrct            CHARACTER VARYING(4)        NOT NULL DEFAULT '' :: CHARACTER VARYING,
    negocio           VARCHAR(15)                 NOT NULL DEFAULT '',
    numero_solicitud  INTEGER                     NOT NULL DEFAULT 0,
    documento         VARCHAR(15)                 NOT NULL DEFAULT '',
    nit               VARCHAR(15)                 NOT NULL DEFAULT '',
    num_doc_fen       INTEGER                     NOT NULL DEFAULT 0,
    valor_factura     NUMERIC(11, 2)              NOT NULL DEFAULT 0,
    saldo_capital     NUMERIC(11, 2)              NOT NULL DEFAULT 0,
    saldo_interes     NUMERIC(11, 2)              NOT NULL DEFAULT 0,
    saldo_cuota_admin NUMERIC(11, 2)              NOT NULL DEFAULT 0,
    valor_abono_cuota NUMERIC(11, 2)              NOT NULL DEFAULT 0,
    valor_saldo_cuota NUMERIC(11, 2)              NOT NULL DEFAULT 0,
    fecha_vencimiento TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT '0099-01-01 00:00:00' :: TIMESTAMP WITHOUT TIME ZONE,
    dias_mora         INTEGER                     NOT NULL DEFAULT 0,
    estado_cartera    VARCHAR(50)                 NOT NULL DEFAULT '',
    last_update       TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT current_timestamp,
    user_update       CHARACTER VARYING(100)      NOT NULL DEFAULT '' :: CHARACTER VARYING,
    creation_date     TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT current_timestamp,
    creation_user     CHARACTER VARYING(100)      NOT NULL DEFAULT '' :: CHARACTER VARYING
);

CREATE INDEX CONCURRENTLY apicredit_liquidacion_saldos_ca_idx
    ON apicredit.liquidacion_saldos_ca (numero_solicitud);

ALTER TABLE apicredit.liquidacion_saldos_ca
    OWNER TO postgres;

ALTER TABLE apicredit.liquidacion_saldos_ca
    ADD COLUMN saldo_cat NUMERIC(11,2) NOT NULL DEFAULT 0;

ALTER TABLE apicredit.liquidacion_saldos_ca
    ADD COLUMN capital_aval NUMERIC(11,2) NOT NULL DEFAULT 0;

ALTER TABLE apicredit.liquidacion_saldos_ca
    ADD COLUMN interes_aval NUMERIC(11,2) NOT NULL DEFAULT 0;