-- DROP TABLE apicredit.categoria_padre_archivo

CREATE TABLE apicredit.categoria_padre_archivo (
    id             SERIAL PRIMARY KEY,
    nombre         VARCHAR(50),
    orden          INTEGER,
    unidad_negocio INTEGER
);

ALTER TABLE apicredit.categoria_padre_archivo
    OWNER TO postgres;
GRANT ALL ON TABLE apicredit.categoria_padre_archivo TO postgres;