-- SELECT apicredit.obtener_capital_departamento(_departamento CHARACTER VARYING);
-- DROP FUNCTION apicredit.obtener_capital_departamento(_departamento CHARACTER VARYING);

CREATE OR REPLACE FUNCTION apicredit.obtener_capital_departamento(_departamento CHARACTER VARYING)
    RETURNS CHARACTER VARYING AS $$
SELECT codciu FROM ciudad WHERE capital = 'S' AND coddpt = $1;
$$
LANGUAGE SQL;