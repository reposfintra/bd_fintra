-- Function: apicredit.calcular_fecha_pago_por_negocio(CHARACTER VARYING, CHARACTER VARYING)

-- DROP FUNCTION apicredit.buscar_fecha_pago_por_negocio(CHARACTER VARYING, CHARACTER VARYING);

CREATE OR REPLACE FUNCTION apicredit.calcular_fecha_pago_por_negocio(_identificacion CHARACTER VARYING, _negocio CHARACTER VARYING)
    RETURNS SETOF RECORD AS
$BODY$

DECLARE

    fecha_pago_record RECORD;
    recordRet         RECORD;
    _fecha_pago       VARCHAR;
    _cumple_tiempo    VARCHAR :='N';
BEGIN

    FOR fecha_pago_record IN SELECT fac.nit,
                                    max(fac.fecha_vencimiento) AS max_fecha_ven,
                                    sum(fac.valor_saldo) AS valor_saldo,
                                    (current_date - max(fac.fecha_vencimiento :: DATE)) :: INTEGER AS periodo_gracia,
                                    neg.nro_docs :: INTEGER AS cuotas
                             FROM con.factura fac
                                      INNER JOIN negocios neg ON (neg.cod_neg = fac.negasoc)
                             WHERE fac.reg_status = ''
                               AND neg.estado_neg = 'T'
                               AND (current_date - neg.fecha_negocio :: DATE) > 60
                               AND fac.nit = _identificacion
                               AND neg.cod_neg = _negocio
                             GROUP BY fac.nit, neg.nro_docs
                             ORDER BY max(fac.fecha_vencimiento) DESC
    LOOP

        RAISE NOTICE '_fechapago.valor_saldo : % ,_fechapago.cuotas : % , _fechapago.periodo_gracia : % , _fechapago.max_fecha_ven : % ', fecha_pago_record.valor_saldo, fecha_pago_record.cuotas, fecha_pago_record.periodo_gracia, fecha_pago_record.max_fecha_ven;

        --validad fecha de vencimientos.
        IF (fecha_pago_record.max_fecha_ven :: DATE < current_date AND fecha_pago_record.valor_saldo = 0)
        THEN
            _fecha_pago := CURRENT_DATE;
            _cumple_tiempo := 'S';
            EXIT;
        END IF;


        IF (fecha_pago_record.valor_saldo > 0 AND fecha_pago_record.cuotas <= 8 AND fecha_pago_record.periodo_gracia >= -60)
        THEN

            _fecha_pago := fecha_pago_record.max_fecha_ven;
            _cumple_tiempo := 'S';

        ELSIF (fecha_pago_record.valor_saldo > 0 AND (fecha_pago_record.cuotas BETWEEN 9 AND 14) AND fecha_pago_record.periodo_gracia >= -90)
            THEN

                _fecha_pago := fecha_pago_record.max_fecha_ven;
                _cumple_tiempo := 'S';

        ELSIF (fecha_pago_record.valor_saldo > 0 AND fecha_pago_record.cuotas > 14 AND fecha_pago_record.periodo_gracia >= -120)
            THEN

                _fecha_pago := fecha_pago_record.max_fecha_ven;
                _cumple_tiempo := 'S';

        ELSIF (fecha_pago_record.valor_saldo > 0)
            THEN

                _fecha_pago := fecha_pago_record.max_fecha_ven;
                _cumple_tiempo := 'N';

        END IF;

        IF _fecha_pago < current_date THEN
            _fecha_pago := current_date;
        END IF;
    END LOOP;
    RAISE NOTICE '_fecha_pago: % _cumple_tiempo: %', _fecha_pago, _cumple_tiempo;

    SELECT INTO recordRet _fecha_pago :: VARCHAR AS fecha_pago, _cumple_tiempo :: VARCHAR AS tiempo;

    RETURN NEXT recordRet;

END;
$BODY$
LANGUAGE plpgsql
VOLATILE;
ALTER FUNCTION apicredit.calcular_fecha_pago_por_negocio(CHARACTER VARYING, CHARACTER VARYING)
    OWNER TO postgres;
