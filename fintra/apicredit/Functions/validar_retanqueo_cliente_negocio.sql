-- Function: apicredit.validar_retanqueo_cliente(INTEGER, CHARACTER VARYING)

-- DROP FUNCTION apicredit.validar_retanqueo_cliente(INTEGER, CHARACTER VARYING);

CREATE OR REPLACE FUNCTION apicredit.validar_retanqueo_cliente_negocio(_identificacion INTEGER, _negocio CHARACTER VARYING)
    RETURNS SETOF RECORD AS
$BODY$
DECLARE
    /************************************************************************************
        AUTOR: JUAN DAVID BERMUDEZ CELEDON
        FECHA: 26-06-2019
        DESCRIPCION: Función para obtener los créditos vigentes asociados a un cliente
                    y validar que cumple con las politicas de retanqueo
    ************************************************************************************/
    _negocios_vigentes_record RECORD;
    _altura_mora              VARCHAR;
    _porcentaje_pagado        NUMERIC(3, 2);
    _plazo_negocio            INTEGER;
    _cuotas_pendientes        INTEGER;
    _unidad_negocio           INTEGER = 1;
    _valor_saldo_negocio      NUMERIC;

BEGIN

    FOR _negocios_vigentes_record IN
        SELECT sp.identificacion :: VARCHAR AS nit,
               sp.nombre :: VARCHAR AS nombre_cliente,
               sa.numero_solicitud :: INTEGER AS numero_solicitud_origen,
               n.cod_neg :: VARCHAR AS negocio,
               n.f_desem ::DATE AS fecha_desembolso,
               coalesce(ccf.fecha_ult_pago, '-') AS fecha_ult_pago,
               n.vr_negocio :: NUMERIC AS valor_negocio,
               0.00 :: NUMERIC AS valor_saldo_a_retanquear,
               0.00::NUMERIC AS valor_saldo_total,
               COALESCE(ccf.valor_preaprobado, 0) AS valor_preaprobado,
               current_date :: DATE AS fecha_vencimiento_ultima_cuota,
               COALESCE(ccf.clasificacion, 'SIN_CLASIFICAR') :: VARCHAR AS tipo_cliente,
               'N' :: VARCHAR AS politica,
               FALSE :: BOOLEAN AS mora
        FROM con.factura f
                 INNER JOIN negocios n ON n.cod_neg = f.negasoc AND f.nit = n.cod_cli
                 INNER JOIN solicitud_aval sa ON sa.cod_neg = n.cod_neg
                 INNER JOIN solicitud_persona sp ON sp.numero_solicitud = sa.numero_solicitud AND SP.tipo = 'S'
                 INNER JOIN rel_unidadnegocio_convenios ruc ON n.id_convenio = ruc.id_convenio
                 INNER JOIN unidad_negocio un ON ruc.id_unid_negocio = un.id
                 LEFT JOIN prov_convenio pc ON pc.nit_proveedor = n.nit_tercero AND pc.id_convenio = n.id_convenio
                 LEFT JOIN (SELECT a.*
                            FROM administrativo.clasificacion_clientes_fintracredit a
                                     INNER JOIN (SELECT max(periodo) AS periodo, id_unidad_negocio
                                                 FROM administrativo.clasificacion_clientes_fintracredit
                                                 WHERE id_unidad_negocio = _unidad_negocio
                                                   AND reg_status = ''
                                                 GROUP BY id_unidad_negocio) b ON (a.periodo = b.periodo AND a.id_unidad_negocio = b.id_unidad_negocio)
                            WHERE reg_status = '') ccf ON ccf.cedula_deudor = sp.identificacion AND ccf.id_unidad_negocio = un.id AND n.cod_neg = ccf.negasoc
        WHERE f.nit = _identificacion
          AND n.cod_neg = _negocio
          AND f.tipo_documento = 'FAC'
          AND f.valor_saldo > 0
          AND f.reg_status = ''
          AND n.estado_neg = 'T'
          AND CURRENT_DATE - n.fecha_negocio :: DATE > 60
          AND un.id = _unidad_negocio
        GROUP BY sp.identificacion, nombre_cliente, numero_solicitud_origen, negocio, n.f_desem,
                 valor_negocio, ccf.clasificacion, ccf.fecha_ult_pago, valor_preaprobado
        ORDER BY n.f_desem DESC
        LOOP
            -- Se valida que el estado de la cartera sea corriente, de lo contrario, no cumple para renovación
            _altura_mora := (SELECT eg_altura_mora_periodo(_negocios_vigentes_record.negocio, 0, 8, 0));


            RAISE INFO 'negocio % - Altura mora %', _negocios_vigentes_record.negocio, _altura_mora;

            _valor_saldo_negocio := coalesce((SELECT SUM(valor_saldo) FROM con.factura WHERE negasoc = _negocios_vigentes_record.negocio AND tipo_documento = 'FAC' AND reg_status = ''), 0.00);
            _negocios_vigentes_record.valor_saldo_total := _valor_saldo_negocio;


            IF _altura_mora ILIKE '%CORRIENTE%'
            THEN
                --Calcula el número de cuotas del crédito
                _plazo_negocio := (SELECT count(*)
                                   FROM documentos_neg_aceptado
                                   WHERE cod_neg = _negocios_vigentes_record.negocio
                                     AND reg_status = '');

                --Calcula el porcentaje pagado de las cuotas
                _porcentaje_pagado := (SELECT (count(num_doc_fen) :: DECIMAL / _plazo_negocio)
                                       FROM con.factura
                                       WHERE negasoc = _negocios_vigentes_record.negocio
                                         AND tipo_documento = 'FAC'
                                         AND substring(documento, length(documento) - 1) != '00'
                                         AND valor_saldo = 0
                                         AND reg_status = '');

                --Calcula las cuotas pendientes por pagar
                _cuotas_pendientes := (SELECT count(num_doc_fen)
                                       FROM con.factura
                                       WHERE negasoc = _negocios_vigentes_record.negocio
                                         AND tipo_documento = 'FAC'
                                         AND substring(documento, length(documento) - 1) != '00'
                                         AND valor_saldo > 0
                                         AND reg_status = '');
                --                 RAISE INFO 'plazo % - porcentaje pagado % - cuotas pendientes %', _plazo_negocio, _porcentaje_pagado, _cuotas_pendientes;

                --Se valida que el porcentaje pagado sea mínimo del 50% para renovación
                IF _porcentaje_pagado >= 0.5
                THEN
                    --VALIDACION POLÍTICA DE RETANQUEO
                    IF ((_plazo_negocio BETWEEN 6 AND 8) AND _cuotas_pendientes <= 3) OR
                       ((_plazo_negocio BETWEEN 9 AND 13) AND _cuotas_pendientes <= 4) OR
                       ((_plazo_negocio >= 14) AND _cuotas_pendientes <= 5)
                    THEN
                        _negocios_vigentes_record.politica := 'R';

                        -- CALCULAR SALDO A RETANQUEAR
                        _negocios_vigentes_record.valor_saldo_a_retanquear := (SELECT sum(k_capital) + sum(i_interes) + sum(ca_cuota_admin) + sum(saldo_cat) + sum(k_capital_aval) + sum(i_interes_aval)
                                                                               FROM (SELECT negasoc,
                                                                                            sum(saldo_capital) AS k_capital,
                                                                                            CASE WHEN estado_cartera = 'D-FUTUROS' THEN 0.00 ELSE sum(saldo_interes) END AS i_interes,
                                                                                            CASE WHEN estado_cartera = 'D-FUTUROS' THEN 0.00 ELSE sum(saldo_cuota_admin) END AS ca_cuota_admin,
                                                                                            sum(saldo_cat) AS saldo_cat,
                                                                                            sum(saldo_capital_aval) AS k_capital_aval,
                                                                                            CASE WHEN estado_cartera = 'D-FUTUROS' THEN 0.00 ELSE sum(saldo_interes_aval) END AS i_interes_aval,
                                                                                            estado_cartera
                                                                                     FROM apicredit.liquidacion_saldos_negocio_microcredito(_negocios_vigentes_record.negocio)
                                                                                              AS saldo (negasoc CHARACTER VARYING, documento CHARACTER VARYING, nit CHARACTER VARYING, num_doc_fen CHARACTER VARYING, valor_factura NUMERIC,
                                                                                                        saldo_capital NUMERIC, saldo_interes NUMERIC, saldo_cuota_admin NUMERIC, saldo_cat NUMERIC, saldo_capital_aval NUMERIC,
                                                                                                        saldo_interes_aval NUMERIC, valor_abono_cuota NUMERIC, valor_saldo_cuota NUMERIC,
                                                                                                        fecha_vencimiento DATE, dias_mora INTEGER, estado_cartera CHARACTER VARYING
                                                                                                       )
                                                                                     WHERE estado_cartera NOT IN ('A-VENCIDO_SIN_SALDO')
                                                                                     GROUP BY negasoc, estado_cartera) AS t
                                                                               GROUP BY negasoc);
                    END IF;
                END IF;
            ELSE
                _negocios_vigentes_record.mora := TRUE;
            END IF;

            PERFORM *
            FROM apicredit.pre_solicitudes_creditos
            WHERE negocio_origen = _negocios_vigentes_record.negocio
              AND estado_sol = 'P'
              AND etapa BETWEEN 1 AND 8;

            IF found THEN
                _negocios_vigentes_record.politica := 'X';
            END IF;

            RETURN NEXT _negocios_vigentes_record;
        END LOOP;
    RETURN;
END ;
$BODY$
    LANGUAGE plpgsql
    VOLATILE;
ALTER FUNCTION apicredit.validar_retanqueo_cliente_negocio(INTEGER, CHARACTER VARYING)
    OWNER TO postgres;