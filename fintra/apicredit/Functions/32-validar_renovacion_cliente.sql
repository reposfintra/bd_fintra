-- Function: apicredit.validar_renovacion_cliente(INTEGER)

-- DROP FUNCTION apicredit.validar_renovacion_cliente(INTEGER);

CREATE OR REPLACE FUNCTION apicredit.validar_renovacion_cliente(_identificacion INTEGER)
    RETURNS SETOF RECORD AS $BODY$
DECLARE
    /************************************************************************************
        AUTOR: JUAN DAVID BERMUDEZ CELEDON
        FECHA: 13-11-2018
        DESCRIPCION: Función para obtener los créditos vigentes asociados a un cliente
                    y validar que cumple con las politicas de renovación y tiempo
    ************************************************************************************/
    _negocios_vigentes_record RECORD;
    _altura_mora              VARCHAR;
    _proxima_fecha_pago       VARCHAR;
    _cumple_tiempo            CHAR;
    _porcentaje_pagado        NUMERIC(3, 2);
    _plazo_negocio            INTEGER;
    _cuotas_pendientes        INTEGER;
    _unidad_negocio_array     INTEGER [] = '{2, 8, 31}';
    _valor_saldo_negocio      NUMERIC:= 0.00; 
    _auxEstudiante 			  VARCHAR:='';
    
BEGIN
	
    FOR _negocios_vigentes_record IN
									    SELECT sp.identificacion :: VARCHAR AS nit,
									           sp.nombre :: VARCHAR AS nombre_cliente,
									           sa.numero_solicitud :: INTEGER AS numero_solicitud_origen,
									           n.cod_neg :: VARCHAR AS negocio,
									           n.f_desem ::DATE as fecha_dembolso,
									           coalesce(ccf.fecha_ult_pago,'-') as fecha_ult_pago,
									           n.vr_negocio :: NUMERIC AS valor_negocio,
									           0.00 :: NUMERIC AS valor_saldo_a_renovar,
									           0.00::numeric as valor_saldo_total,
									           COALESCE(ccf.valor_preaprobado,0) as valor_preaprobado,
									           current_date :: DATE AS fecha_vencimiento_ultima_cuota,
									           coalesce(se.identificacion, 'No encontrado'):: VARCHAR AS identificacion_estudiante,
									           coalesce(se.nombre, 'No encontrado') :: VARCHAR AS nombre_estudiante,
									           coalesce(upper(pc.nombre_afiliado), 'No encontrado') :: VARCHAR AS universidad,
									           FALSE :: BOOLEAN AS cumple_renovacion,
									           COALESCE(ccf.clasificacion, 'SIN_CLASIFICAR') :: VARCHAR AS tipo_cliente,
									           'N' :: VARCHAR AS politica,
									           FALSE :: BOOLEAN AS mora									          
									    FROM con.factura f
									             INNER JOIN negocios n ON n.cod_neg = f.negasoc AND f.nit = n.cod_cli
									             INNER JOIN solicitud_aval sa ON sa.cod_neg = n.cod_neg
									             INNER JOIN solicitud_persona sp ON sp.numero_solicitud = sa.numero_solicitud AND SP.tipo = 'S'
									             INNER JOIN rel_unidadnegocio_convenios ruc ON n.id_convenio = ruc.id_convenio
									             INNER JOIN unidad_negocio un ON ruc.id_unid_negocio = un.id
									             LEFT JOIN solicitud_persona se ON se.numero_solicitud = sa.numero_solicitud AND se.tipo = 'E'
									             LEFT JOIN prov_convenio pc on pc.nit_proveedor=n.nit_tercero and pc.id_convenio=n.id_convenio
									             LEFT JOIN (SELECT a.*
									                        FROM administrativo.clasificacion_clientes_fintracredit a
									                        INNER JOIN (SELECT max(periodo) AS periodo, id_unidad_negocio
									                                    FROM administrativo.clasificacion_clientes_fintracredit
									                                    WHERE id_unidad_negocio = ANY (_unidad_negocio_array) and reg_status=''
									                                    GROUP BY id_unidad_negocio) b ON (a.periodo = b.periodo AND a.id_unidad_negocio = b.id_unidad_negocio)
									                         where reg_status='') ccf ON ccf.cedula_deudor = sp.identificacion AND ccf.id_unidad_negocio = un.id and n.cod_neg=ccf.negasoc
									    WHERE f.nit = _identificacion
									      AND f.tipo_documento = 'FAC'
									      AND f.valor_saldo > 0
									      AND f.reg_status = ''
									      AND n.estado_neg = 'T'
												AND CURRENT_DATE - n.fecha_negocio :: DATE > 60
									      AND un.id = ANY (_unidad_negocio_array)
									    GROUP BY sp.identificacion, nombre_cliente, numero_solicitud_origen, negocio, n.f_desem, 
									    			valor_negocio, nombre_estudiante, universidad, ccf.clasificacion,ccf.fecha_ult_pago,valor_preaprobado,se.identificacion
									    order by n.f_desem desc
    LOOP
        -- Se valida que el estado de la cartera sea corriente, de lo contrario, no cumple para renovación
        _altura_mora := (SELECT eg_altura_mora_periodo(_negocios_vigentes_record.negocio, 0, 8, 0));
        
       
        RAISE INFO 'negocio % - Altura mora %', _negocios_vigentes_record.negocio, _altura_mora;
        
        _valor_saldo_negocio:=coalesce((select SUM(valor_saldo) from con.factura where negasoc=_negocios_vigentes_record.negocio and tipo_documento='FAC' and reg_status=''),0.00); 
        _negocios_vigentes_record.valor_saldo_total:=_valor_saldo_negocio;
      

        IF _altura_mora ILIKE '%CORRIENTE%'
        THEN
            --Calcula el número de cuotas del crédito
            _plazo_negocio := (SELECT count(*) FROM documentos_neg_aceptado WHERE cod_neg = _negocios_vigentes_record.negocio
                                                                              AND reg_status = '');

            --Calcula el porcentaje pagado de las cuotas
            _porcentaje_pagado := (SELECT (count(num_doc_fen) :: DECIMAL / _plazo_negocio)
                                   FROM con.factura
                                   WHERE negasoc = _negocios_vigentes_record.negocio
                                     AND tipo_documento = 'FAC'
                                     AND substring(documento, length(documento) - 1) != '00'
                                     AND valor_saldo = 0
                                     AND reg_status = '');

            --Calcula las cuotas pendientes por pagar
            _cuotas_pendientes := (SELECT count(num_doc_fen)
                                   FROM con.factura
                                   WHERE negasoc = _negocios_vigentes_record.negocio
                                     AND tipo_documento = 'FAC'
                                     AND substring(documento, length(documento) - 1) != '00'
                                     AND valor_saldo > 0
                                     AND reg_status = '');
            RAISE INFO 'plazo % - porcentaje pagado % - cuotas pendientes %', _plazo_negocio, _porcentaje_pagado, _cuotas_pendientes;

            --Se valida que el porcentaje pagado sea mínimo del 50% para renovación
            IF _porcentaje_pagado >= 0.5
            THEN
                --VALIDACION NUEVA POLÍTICA DE RENOVACIÓN
                IF (_plazo_negocio <= 8 AND _cuotas_pendientes > 2) OR
                   ((_plazo_negocio BETWEEN 9 AND 14) AND _cuotas_pendientes > 3) OR
                   ((_plazo_negocio >= 15) AND _cuotas_pendientes > 4)
                THEN
                    _negocios_vigentes_record.cumple_renovacion := TRUE;
                    _negocios_vigentes_record.politica = 'R';

                    -- CALCULAR SALDO A RENOVAR
                    _negocios_vigentes_record.valor_saldo_a_renovar := (SELECT sum(k_capital) + sum(i_interes) + sum(ca_cuota_admin)
                                                                      FROM (SELECT negasoc,
                                                                                   sum(saldo_capital) AS k_capital,
                                                                                   CASE WHEN estado_cartera = 'D-FUTUROS' THEN 0.00 ELSE sum(saldo_interes) END AS i_interes,
                                                                                   CASE WHEN estado_cartera = 'D-FUTUROS' THEN 0.00 ELSE sum(saldo_cuota_admin) END AS ca_cuota_admin,
                                                                                   estado_cartera
                                                                            FROM apicredit.liquidacion_saldos_negocios(_negocios_vigentes_record.negocio)
                                                                                     AS saldo (negasoc CHARACTER VARYING, documento CHARACTER VARYING, nit CHARACTER VARYING, num_doc_fen CHARACTER VARYING, valor_factura NUMERIC, saldo_capital NUMERIC, saldo_interes NUMERIC, saldo_cuota_admin NUMERIC, valor_abono_cuota NUMERIC, valor_saldo_cuota NUMERIC, fecha_vencimiento DATE, dias_mora INTEGER, estado_cartera CHARACTER VARYING)
                                                                            WHERE estado_cartera NOT IN ('A-VENCIDO_SIN_SALDO')
                                                                            GROUP BY negasoc, estado_cartera) AS t
                                                                      GROUP BY negasoc);

                --- VALIDACIÓN ANTIGUA POLÍTICA DE TIEMPO
                ELSIF (_plazo_negocio <= 8 AND _cuotas_pendientes <= 2) OR
                      ((_plazo_negocio BETWEEN 9 AND 14) AND _cuotas_pendientes <= 3) OR
                      ((_plazo_negocio >= 15) AND _cuotas_pendientes <= 4)
                    THEN
                        --Calcula la próxima fecha de pago del nuevo crédito y valida si cumple con la politica de tiempo
                        SELECT fecha_pago, tiempo INTO _proxima_fecha_pago, _cumple_tiempo
                        FROM apicredit.calcular_fecha_pago_por_negocio(_negocios_vigentes_record.nit, _negocios_vigentes_record.negocio) AS F (fecha_pago VARCHAR, tiempo VARCHAR);

                        IF _cumple_tiempo = 'S' THEN
                            _negocios_vigentes_record.fecha_vencimiento_ultima_cuota := _proxima_fecha_pago;
                            _negocios_vigentes_record.politica := 'T';
                        END IF;
                        
                        --SI EL NEGOCIO NO TIENE CLASIFICACION Y EL SALDO ES CERO NO SE MUESTRA EN LA ITERACION. 
                        IF (_negocios_vigentes_record.tipo_cliente ='SIN_CLASIFICAR' and _valor_saldo_negocio=0.00) THEN 
				        	CONTINUE;
				        END IF;
				        
						--FILTRO DE NEGOCIOS REPETIDOS POR ESTUDIANTE (EMPANADA)
				        IF (_valor_saldo_negocio=0.00) THEN 
				        	if (_auxEstudiante='') then
				        	  _auxEstudiante:=_negocios_vigentes_record.identificacion_estudiante;
				        	elsif(_auxEstudiante=_negocios_vigentes_record.identificacion_estudiante)then 
				        	  continue;
				        	else 
				        		_auxEstudiante:=_negocios_vigentes_record.identificacion_estudiante;
				        	end if;				        	
				        END IF;
				        
                END IF;
            END IF;
        ELSE        	
           	_negocios_vigentes_record.mora := TRUE;  
       END IF;

			 PERFORM *
		   FROM apicredit.pre_solicitudes_creditos
			 WHERE negocio_origen = _negocios_vigentes_record.negocio	AND estado_sol = 'P'	AND etapa BETWEEN 1 AND 8;

			 IF found THEN
				 _negocios_vigentes_record.politica := 'X';
			 END IF;
       
       RETURN NEXT _negocios_vigentes_record;
    END LOOP;
    RETURN;
END;
$BODY$
LANGUAGE plpgsql
VOLATILE;
ALTER FUNCTION apicredit.validar_renovacion_cliente(INTEGER)
    OWNER TO postgres;