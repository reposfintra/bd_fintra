-- Function: apicredit.anular_negocio_mirco_retanqueo(integer, character varying)

-- DROP FUNCTION apicredit.anular_negocio_mirco_retanqueo(integer, character varying);

CREATE OR REPLACE FUNCTION apicredit.anular_negocio_mirco_retanqueo(_numero_solicitud INTEGER, _usuario CHARACTER VARYING)
    RETURNS CHARACTER VARYING AS
$BODY$
DECLARE
    /********************************************************************
    * Autor: Edgar Gonzalez Mendoza                                     *
    * Fecha: 2018-10-11                                                 *
    * Descripcion: Crea las notas de ajuste, resta las cartera y anula  *
    *              los diferidos futuros cuando se renueva un negocio  	*
    ********************************************************************/

    facturas_negocio_record RECORD;
    _facturas_cartera       RECORD;
    _banco_record           RECORD;
    numero_ingreso_if       CHARACTER VARYING := '';
    numero_ingreso_cm       CHARACTER VARYING := '';
    numero_ingreso_iv       CHARACTER VARYING := '';
    numero_ingreso_fg       CHARACTER VARYING := '';
    _negocio_a_renovar      CHARACTER VARYING := '';
    _items_mi               INTEGER           := 0;
    _items_cm               INTEGER           := 0;
    _items_fg               INTEGER           := 0;
    _items_iv               INTEGER           := 0;
    saldo_factura_ingreso   NUMERIC           := 0;
    diferidos_array         VARCHAR(20)[];
    _cuentas_fiducia        VARCHAR(10)[]     := '{16252135,16252136,11050701,XXXXXXXX,XXXXXXXX}';

BEGIN
    SELECT INTO _negocio_a_renovar negocio FROM apicredit.liquidacion_saldos_ca WHERE numero_solicitud = _numero_solicitud GROUP BY negocio;
    PERFORM * FROM tem.backup_cartera_renovacion_credito_edu WHERE negasoc = _negocio_a_renovar;
    IF NOT FOUND THEN
        INSERT INTO tem.backup_cartera_renovacion_credito_edu(reg_status, dstrct, tipo_documento, documento, nit, codcli, concepto,
                                                              fecha_factura, fecha_vencimiento, fecha_ultimo_pago, fecha_impresion,
                                                              descripcion, observacion, valor_factura, valor_abono, valor_saldo,
                                                              valor_facturame, valor_abonome, valor_saldome, valor_tasa, moneda,
                                                              cantidad_items, forma_pago, agencia_facturacion, agencia_cobro,
                                                              zona, clasificacion1, clasificacion2, clasificacion3, transaccion,
                                                              transaccion_anulacion, fecha_contabilizacion, fecha_anulacion,
                                                              fecha_contabilizacion_anulacion, base, last_update, user_update,
                                                              creation_date, creation_user, fecha_probable_pago, flujo, rif,
                                                              cmc, usuario_anulo, formato, agencia_impresion, periodo, valor_tasa_remesa,
                                                              negasoc, num_doc_fen, obs, pagado_fenalco, corficolombiana, tipo_ref1,
                                                              ref1, tipo_ref2, ref2, dstrct_ultimo_ingreso, tipo_documento_ultimo_ingreso,
                                                              num_ingreso_ultimo_ingreso, item_ultimo_ingreso, fec_envio_fiducia,
                                                              nit_enviado_fiducia, tipo_referencia_1, referencia_1, tipo_referencia_2,
                                                              referencia_2, tipo_referencia_3, referencia_3, nc_traslado, fecha_nc_traslado,
                                                              tipo_nc, numero_nc, factura_traslado, factoring_formula_aplicada,
                                                              nit_endoso, devuelta, fc_eca, fc_bonificacion, indicador_bonificacion,
                                                              fi_bonificacion, endoso_fenalco, endoso_fiducia, causacion_int_ms,
                                                              fecha_causacion_int_ms, procesado)
        SELECT reg_status,
               dstrct,
               tipo_documento,
               documento,
               nit,
               codcli,
               concepto,
               fecha_factura,
               fecha_vencimiento,
               fecha_ultimo_pago,
               fecha_impresion,
               descripcion,
               observacion,
               valor_factura,
               valor_abono,
               valor_saldo,
               valor_facturame,
               valor_abonome,
               valor_saldome,
               valor_tasa,
               moneda,
               cantidad_items,
               forma_pago,
               agencia_facturacion,
               agencia_cobro,
               zona,
               clasificacion1,
               clasificacion2,
               clasificacion3,
               transaccion,
               transaccion_anulacion,
               fecha_contabilizacion,
               fecha_anulacion,
               fecha_contabilizacion_anulacion,
               base,
               last_update,
               user_update,
               creation_date,
               creation_user,
               fecha_probable_pago,
               flujo,
               rif,
               cmc,
               usuario_anulo,
               formato,
               agencia_impresion,
               periodo,
               valor_tasa_remesa,
               negasoc,
               num_doc_fen,
               obs,
               pagado_fenalco,
               corficolombiana,
               tipo_ref1,
               ref1,
               tipo_ref2,
               ref2,
               dstrct_ultimo_ingreso,
               tipo_documento_ultimo_ingreso,
               num_ingreso_ultimo_ingreso,
               item_ultimo_ingreso,
               fec_envio_fiducia,
               nit_enviado_fiducia,
               tipo_referencia_1,
               referencia_1,
               tipo_referencia_2,
               referencia_2,
               tipo_referencia_3,
               referencia_3,
               nc_traslado,
               fecha_nc_traslado,
               tipo_nc,
               numero_nc,
               factura_traslado,
               factoring_formula_aplicada,
               nit_endoso,
               devuelta,
               fc_eca,
               fc_bonificacion,
               indicador_bonificacion,
               fi_bonificacion,
               endoso_fenalco,
               endoso_fiducia,
               causacion_int_ms,
               fecha_causacion_int_ms,
               procesado
        FROM con.factura
        WHERE negasoc = _negocio_a_renovar
          AND reg_status = ''
          AND tipo_documento = 'FAC';
    END IF;

    -- 1. REVERSION DE INTERESES Y CUOTAS DE ADMINISTRACION.

    FOR facturas_negocio_record IN
        SELECT n.cod_cli AS nit_cliente,
               n.cod_neg AS negocio,
               f.codcli AS cod_cli,
               f.documento,
               lsc.num_doc_fen AS cuota,
               f.fecha_vencimiento,
               lsc.estado_cartera,
               lsc.dias_mora,
               f.valor_saldo,
               lsc.saldo_interes AS interes_a_cobrar,
               lsc.saldo_cuota_admin AS cuota_admon_a_cobrar,
               lsc.saldo_cat AS cat_a_cobrar,
               lsc.interes_aval::NUMERIC AS interes_a_aval,
               cmc3.cuenta AS cuenta_cartera,
               CASE WHEN COALESCE(ing.endosado, 'N') = 'S' THEN _cuentas_fiducia[1] ELSE COALESCE(cmc.cuenta, 'NULL') END AS cuenta_interes,
               CASE WHEN COALESCE(ing2.endosado, 'N') = 'S' THEN _cuentas_fiducia[2] ELSE COALESCE(cmc2.cuenta, 'NULL') END AS cuenta_cuota_admin,
               CASE WHEN COALESCE(ing3.endosado, 'N') = 'S' THEN _cuentas_fiducia[4] ELSE COALESCE(cmc4.cuenta, 'NULL') END AS cuenta_cat,
               CASE WHEN COALESCE(ing4.endosado, 'N') = 'S' THEN _cuentas_fiducia[5] ELSE COALESCE(cmc5.cuenta, 'NULL') END AS cuenta_iv,
               COALESCE(ing.cod, '-') AS diferido_mi,
               COALESCE(ing2.cod, '-') AS diferido_cm,
               COALESCE(ing3.cod, '-') AS diferido_ca,
               COALESCE(ing4.cod, '-') AS diferido_iv,
               ing.endosado
        FROM apicredit.liquidacion_saldos_ca lsc
                 INNER JOIN con.factura f ON lsc.documento = f.documento
                 INNER JOIN negocios n ON n.cod_neg = f.negasoc AND n.cod_cli = f.nit
                 INNER JOIN con.cmc_doc cmc3 ON (cmc3.cmc = f.cmc AND cmc3.tipodoc = f.tipo_documento)
                 LEFT JOIN ing_fenalco ing ON (ing.codneg = lsc.negocio AND ing.tipodoc = 'MI' AND ing.cuota = lsc.num_doc_fen)
                 LEFT JOIN ing_fenalco ing2 ON (ing2.codneg = lsc.negocio AND ing2.tipodoc = 'CM' AND ing2.cuota = lsc.num_doc_fen)
                 LEFT JOIN ing_fenalco ing3 ON (ing3.codneg = lsc.negocio AND ing3.tipodoc = 'CA' AND ing3.cuota = lsc.num_doc_fen)
                 LEFT JOIN ing_fenalco ing4 ON (ing4.codneg = lsc.negocio AND ing4.tipodoc = 'IV' AND ing4.cuota = lsc.num_doc_fen)
                 LEFT JOIN con.cmc_doc cmc ON (cmc.cmc = ing.cmc AND ing.tipodoc = cmc.tipodoc)
                 LEFT JOIN con.cmc_doc cmc2 ON (cmc2.cmc = ing2.cmc AND ing2.tipodoc = cmc2.tipodoc)
                 LEFT JOIN con.cmc_doc cmc4 ON (cmc4.cmc = ing3.cmc AND ing3.tipodoc = cmc4.tipodoc)
                 LEFT JOIN con.cmc_doc cmc5 ON (cmc5.cmc = ing4.cmc AND ing4.tipodoc = cmc5.tipodoc)
        WHERE lsc.numero_solicitud = _numero_solicitud
          AND lsc.estado_cartera IN ('D-FUTUROS')
          AND f.reg_status = ''
          AND f.tipo_documento = 'FAC'
          AND f.valor_saldo > 0
        ORDER BY f.documento

        LOOP

            --ANULACION DE LOS INTERESES FUTUROS
            IF facturas_negocio_record.interes_a_cobrar > 0 AND facturas_negocio_record.cuenta_interes != 'NULL' THEN

                --inserta las cabeceras de los ingresos.
                IF _items_mi = 0 THEN

                    --OBTENIE EL NUMERO DE INGRESO PARA IA DE INTERESES
                    numero_ingreso_if := get_lcod('ICAC');
                    saldo_factura_ingreso := facturas_negocio_record.valor_saldo;

                    --INSERTA LA CABECERA DEL INGRESO INTERESES
                    INSERT INTO con.ingreso (dstrct, tipo_documento, num_ingreso, codcli,
                                             nitcli, concepto, tipo_ingreso, fecha_consignacion, fecha_ingreso,
                                             branch_code, bank_account_no, codmoneda, agencia_ingreso, descripcion_ingreso,
                                             vlr_ingreso, vlr_ingreso_me, vlr_tasa, fecha_tasa, cant_item, creation_user, creation_date,
                                             base, cuenta, tipo_referencia_1, referencia_1, referencia_2)
                    VALUES ('FINV', 'ICA', numero_ingreso_if, facturas_negocio_record.cod_cli,
                            facturas_negocio_record.nit_cliente, 'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
                            '', '', 'PES', 'OP', 'REVERSION INTERESES - RETANQUEO DE CREDITOS MICRO.',
                            0.00, 0.00, '1.000000', CURRENT_DATE, _items_mi, _usuario, CURRENT_TIMESTAMP,
                            'COL', facturas_negocio_record.cuenta_interes, 'NEG', facturas_negocio_record.negocio, '');

                END IF;

                _items_mi := _items_mi + 1;
                saldo_factura_ingreso := saldo_factura_ingreso - facturas_negocio_record.interes_a_cobrar;

                --INSERTA EL DETALLE DEL INGRESO INTERESES
                INSERT INTO con.ingreso_detalle (dstrct, tipo_documento, num_ingreso, item,
                                                 nitcli, valor_ingreso, valor_ingreso_me, factura,
                                                 fecha_factura, tipo_doc, documento, creation_user,
                                                 creation_date, base, cuenta, descripcion,
                                                 valor_tasa, saldo_factura, tipo_referencia_2, referencia_2,
                                                 tipo_referencia_1, referencia_1)
                VALUES ('FINV', 'ICA', numero_ingreso_if, _items_mi,
                        facturas_negocio_record.nit_cliente, facturas_negocio_record.interes_a_cobrar, facturas_negocio_record.interes_a_cobrar, facturas_negocio_record.documento,
                        CURRENT_DATE, 'FAC', facturas_negocio_record.documento, _usuario,
                        CURRENT_TIMESTAMP, 'COL', facturas_negocio_record.cuenta_cartera, 'REVERSION INTERESES - RETANQUEO DE CREDITOS MICRO.',
                        '1.0000000000', saldo_factura_ingreso, 'MI', facturas_negocio_record.diferido_mi, 'NEG', facturas_negocio_record.negocio);

                -- RESTA EL SALDO DEL INTERES A LA CARTERA.
                UPDATE con.factura
                SET valor_abono      = (valor_abono + facturas_negocio_record.interes_a_cobrar),
                    valor_saldo      = (valor_saldo - facturas_negocio_record.interes_a_cobrar),
                    valor_abonome    = (valor_abonome + facturas_negocio_record.interes_a_cobrar),
                    valor_saldome    = (valor_saldome - facturas_negocio_record.interes_a_cobrar),
                    user_update      = _usuario,
                    fecha_ultimo_pago= CURRENT_DATE,
                    last_update      = CURRENT_TIMESTAMP
                WHERE documento = facturas_negocio_record.documento
                  AND tipo_documento = 'FAC'
                  AND reg_status = '';

                --AGREGAMOS LOS DIFERIDOS DE ITERESES PARA ANULARLOS
                diferidos_array := array_append(diferidos_array, facturas_negocio_record.diferido_mi);

            END IF;

            --ANULACION DE LAS CUAOTAS DE ADMINISTRACION FUTURAS
            IF facturas_negocio_record.cuota_admon_a_cobrar > 0 AND facturas_negocio_record.cuenta_cuota_admin != 'NULL' THEN

                IF _items_cm = 0 THEN

                    --OBTENIE EL NUMERO DE INGRESO PARA IA DE CUOTAS DE ADMINISTRACION
                    numero_ingreso_cm := get_lcod('ICAC');

                    --INSERTA LA CABECERA DEL INGRESO INTERESES
                    INSERT INTO con.ingreso (dstrct, tipo_documento, num_ingreso, codcli,
                                             nitcli, concepto, tipo_ingreso, fecha_consignacion, fecha_ingreso,
                                             branch_code, bank_account_no, codmoneda, agencia_ingreso, descripcion_ingreso,
                                             vlr_ingreso, vlr_ingreso_me, vlr_tasa, fecha_tasa, cant_item, creation_user, creation_date,
                                             base, cuenta, tipo_referencia_1, referencia_1, referencia_2)
                    VALUES ('FINV', 'ICA', numero_ingreso_cm, facturas_negocio_record.cod_cli,
                            facturas_negocio_record.nit_cliente, 'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
                            '', '', 'PES', 'OP', 'REVERSION CUOTAS ADMINISTRACION - RETANQUEO DE CREDITOS MICRO.',
                            0.00, 0.00, '1.000000', CURRENT_DATE, _items_cm, _usuario, CURRENT_TIMESTAMP,
                            'COL', facturas_negocio_record.cuenta_cuota_admin, 'NEG', facturas_negocio_record.negocio, '');

                END IF;

                _items_cm := _items_cm + 1;
                saldo_factura_ingreso := saldo_factura_ingreso - facturas_negocio_record.cuota_admon_a_cobrar;

                --INSERTA EL DETALLE DEL INGRESO PARA LAS CUOTAS DE ADMINISTRACION.
                INSERT INTO con.ingreso_detalle (dstrct, tipo_documento, num_ingreso, item,
                                                 nitcli, valor_ingreso, valor_ingreso_me, factura,
                                                 fecha_factura, tipo_doc, documento, creation_user,
                                                 creation_date, base, cuenta, descripcion,
                                                 valor_tasa, saldo_factura, tipo_referencia_2, referencia_2,
                                                 tipo_referencia_1, referencia_1)
                VALUES ('FINV', 'ICA', numero_ingreso_cm, _items_cm,
                        facturas_negocio_record.nit_cliente, facturas_negocio_record.cuota_admon_a_cobrar, facturas_negocio_record.cuota_admon_a_cobrar, facturas_negocio_record.documento,
                        CURRENT_DATE, 'FAC', facturas_negocio_record.documento, _usuario,
                        CURRENT_TIMESTAMP, 'COL', facturas_negocio_record.cuenta_cartera, 'REVERSION CUOTAS ADMINISTRACION - RETANQUEO DE CREDITOS MICRO.',
                        '1.0000000000', saldo_factura_ingreso, 'CM', facturas_negocio_record.diferido_cm, 'NEG', facturas_negocio_record.negocio);

                -- RESTA EL SALDO DE LA CUOTA DE ADMINISTRACION A LA CARTERA.
                UPDATE con.factura
                SET valor_abono      = (valor_abono + facturas_negocio_record.cuota_admon_a_cobrar),
                    valor_saldo      = (valor_saldo - facturas_negocio_record.cuota_admon_a_cobrar),
                    valor_abonome    = (valor_abonome + facturas_negocio_record.cuota_admon_a_cobrar),
                    valor_saldome    = (valor_saldome - facturas_negocio_record.cuota_admon_a_cobrar),
                    user_update      = _usuario,
                    fecha_ultimo_pago= CURRENT_DATE,
                    last_update      = CURRENT_TIMESTAMP
                WHERE documento = facturas_negocio_record.documento
                  AND tipo_documento = 'FAC'
                  AND reg_status = '';

                --AGREGAMOS LOS DIFERIDOS CUOTA DE ADMINISTRACION PARA ANULARLOS
                diferidos_array := array_append(diferidos_array, facturas_negocio_record.diferido_cm);

            END IF;

            IF facturas_negocio_record.cat_a_cobrar > 0 AND facturas_negocio_record.cuenta_cat != 'NULL' THEN

                --ACTUALIZAMOS LA FECHA DE DOCUMENTO DEL CAT PARA QUE CONTABILICE EN EL PERIODO DEL DESEMBOLSO.
                UPDATE ing_fenalco
                SET fecha_doc=current_date,
                    user_update = _usuario,
                    last_update = CURRENT_TIMESTAMP
                WHERE codneg = facturas_negocio_record.negocio
                  AND cod = facturas_negocio_record.diferido_ca
                  AND (periodo = '' OR periodo IS NULL)
                  AND procesado_dif != 'S'
                  AND cuota = facturas_negocio_record.cuota;

            END IF;

            --ANULACION DEL INTERES DEL AVAL DEL DIFERIDO
            IF facturas_negocio_record.interes_a_aval > 0 AND facturas_negocio_record.cuenta_iv != 'NULL' THEN

                IF _items_iv = 0 THEN

                    --OBTENIE EL NUMERO DE INGRESO PARA IA DE CUOTAS DE ADMINISTRACION
                    numero_ingreso_iv := get_lcod('ICAC');

                    --INSERTA LA CABECERA DEL INGRESO INTERESES
                    INSERT INTO con.ingreso (dstrct, tipo_documento, num_ingreso, codcli,
                                             nitcli, concepto, tipo_ingreso, fecha_consignacion, fecha_ingreso,
                                             branch_code, bank_account_no, codmoneda, agencia_ingreso, descripcion_ingreso,
                                             vlr_ingreso, vlr_ingreso_me, vlr_tasa, fecha_tasa, cant_item, creation_user, creation_date,
                                             base, cuenta, tipo_referencia_1, referencia_1, referencia_2)
                    VALUES ('FINV', 'ICA', numero_ingreso_iv, facturas_negocio_record.cod_cli,
                            facturas_negocio_record.nit_cliente, 'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
                            '', '', 'PES', 'OP', 'REVERSION INTERES AVAL - RETANQUEO DE CREDITOS MICRO.',
                            0.00, 0.00, '1.000000', CURRENT_DATE, _items_iv, _usuario, CURRENT_TIMESTAMP,
                            'COL', facturas_negocio_record.cuenta_iv, 'NEG', facturas_negocio_record.negocio, '');

                END IF;

                _items_iv := _items_iv + 1;
                saldo_factura_ingreso := saldo_factura_ingreso - facturas_negocio_record.interes_a_aval;

                --INSERTA EL DETALLE DEL INGRESO PARA LAS CUOTAS DE ADMINISTRACION.
                INSERT INTO con.ingreso_detalle (dstrct, tipo_documento, num_ingreso, item,
                                                 nitcli, valor_ingreso, valor_ingreso_me, factura,
                                                 fecha_factura, tipo_doc, documento, creation_user,
                                                 creation_date, base, cuenta, descripcion,
                                                 valor_tasa, saldo_factura, tipo_referencia_2, referencia_2,
                                                 tipo_referencia_1, referencia_1)
                VALUES ('FINV', 'ICA', numero_ingreso_iv, _items_iv,
                        facturas_negocio_record.nit_cliente, facturas_negocio_record.interes_a_aval, facturas_negocio_record.interes_a_aval, facturas_negocio_record.documento,
                        CURRENT_DATE, 'FAC', facturas_negocio_record.documento, _usuario,
                        CURRENT_TIMESTAMP, 'COL', facturas_negocio_record.cuenta_cartera, 'REVERSION INTERES AVAL - RETANQUEO DE CREDITOS MICRO.',
                        '1.0000000000', saldo_factura_ingreso, 'IV', facturas_negocio_record.diferido_iv, 'NEG', facturas_negocio_record.negocio);

                -- RESTA EL SALDO DE LA CUOTA DE ADMINISTRACION A LA CARTERA.
                UPDATE con.factura
                SET valor_abono      = (valor_abono + facturas_negocio_record.interes_a_aval),
                    valor_saldo      = (valor_saldo - facturas_negocio_record.interes_a_aval),
                    valor_abonome    = (valor_abonome + facturas_negocio_record.interes_a_aval),
                    valor_saldome    = (valor_saldome - facturas_negocio_record.interes_a_aval),
                    user_update      = _usuario,
                    fecha_ultimo_pago= CURRENT_DATE,
                    last_update      = CURRENT_TIMESTAMP
                WHERE documento = facturas_negocio_record.documento
                  AND tipo_documento = 'FAC'
                  AND reg_status = '';

                --AGREGAMOS LOS DIFERIDOS CUOTA DE ADMINISTRACION PARA ANULARLOS
                diferidos_array := array_append(diferidos_array, facturas_negocio_record.diferido_iv);

            END IF;

            --ANULAR LOS DIFERIDOS INTERESES, CUOTAS DE ADMINISTRACION E INTERES AVAL.
            UPDATE ing_fenalco
            SET reg_status  = 'A',
                user_update = _usuario,
                last_update = CURRENT_TIMESTAMP
            WHERE codneg = facturas_negocio_record.negocio
              AND cod = ANY (diferidos_array)
              AND (periodo = '' OR periodo IS NULL)
              AND procesado_dif != 'S'
              AND cuota = facturas_negocio_record.cuota;

            diferidos_array := NULL;

        END LOOP;

    IF numero_ingreso_if != '' THEN
        --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE INTERESES.
        UPDATE con.ingreso
        SET vlr_ingreso=(SELECT sum(idet.valor_ingreso) FROM con.ingreso_detalle idet WHERE idet.num_ingreso = numero_ingreso_if),
            vlr_ingreso_me=(SELECT sum(idet.valor_ingreso) FROM con.ingreso_detalle idet WHERE idet.num_ingreso = numero_ingreso_if),
            cant_item =_items_mi
        WHERE num_ingreso = numero_ingreso_if
          AND tipo_documento = 'ICA';
    END IF;

    IF numero_ingreso_cm != '' THEN
        --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE LAS CUOTAS DE AMINISTRACION.
        UPDATE con.ingreso
        SET vlr_ingreso=(SELECT sum(idet.valor_ingreso) FROM con.ingreso_detalle idet WHERE idet.num_ingreso = numero_ingreso_cm),
            vlr_ingreso_me=(SELECT sum(idet.valor_ingreso) FROM con.ingreso_detalle idet WHERE idet.num_ingreso = numero_ingreso_cm),
            cant_item =_items_cm
        WHERE num_ingreso = numero_ingreso_cm
          AND tipo_documento = 'ICA';
    END IF;

    IF numero_ingreso_iv != '' THEN
        --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE LOS INTERES DEL AVAL FINANCIADO.
        UPDATE con.ingreso
        SET vlr_ingreso=(SELECT sum(idet.valor_ingreso) FROM con.ingreso_detalle idet WHERE idet.num_ingreso = numero_ingreso_iv),
            vlr_ingreso_me=(SELECT sum(idet.valor_ingreso) FROM con.ingreso_detalle idet WHERE idet.num_ingreso = numero_ingreso_iv),
            cant_item =_items_iv
        WHERE num_ingreso = numero_ingreso_iv
          AND tipo_documento = 'ICA';
    END IF;

    -- 2.) REVERSION DE LA CARTERA DEL CLIENTE.
    FOR _facturas_cartera IN
        SELECT f.nit AS nit_cliente,
               f.documento,
               f.valor_factura,
               f.valor_abono,
               f.valor_saldo,
               cmc.cuenta AS cuenta_cartera,
               f.negasoc AS negocio
        FROM con.factura f
                 INNER JOIN apicredit.liquidacion_saldos_ca lca ON lca.documento = f.documento AND f.num_doc_fen = lca.num_doc_fen AND lca.negocio = f.negasoc
                 INNER JOIN con.cmc_doc cmc ON cmc.cmc = f.cmc AND cmc.tipodoc = f.tipo_documento
                 INNER JOIN negocios n ON n.cod_neg = f.negasoc
        WHERE lca.estado_cartera IN ('C-CORRIENTE', 'D-FUTUROS')
          AND f.reg_status = ''
          AND f.tipo_documento = 'FAC'
          AND lca.numero_solicitud = _numero_solicitud
          AND f.negasoc = _negocio_a_renovar
          AND f.valor_saldo > 0
        LOOP

            IF _items_fg = 0 THEN

                numero_ingreso_fg := get_lcod('ICAC');
                SELECT INTO _banco_record branch_code, bank_account_no, codigo_cuenta FROM public.banco WHERE branch_code = 'CAJA RETANQ EDU' AND bank_account_no = 'RETANQUEO EDUCATIVO';

                --INSERTA LA CABECERA DEL INGRESO PARA CANCELAR LA CARTERA.
                INSERT INTO con.ingreso (dstrct, tipo_documento, num_ingreso, codcli,
                                         nitcli, concepto, tipo_ingreso, fecha_consignacion, fecha_ingreso,
                                         branch_code, bank_account_no, codmoneda, agencia_ingreso, descripcion_ingreso,
                                         vlr_ingreso, vlr_ingreso_me, vlr_tasa, fecha_tasa, cant_item, creation_user, creation_date,
                                         base, cuenta, tipo_referencia_1, referencia_1, referencia_2)
                VALUES ('FINV', 'ICA', numero_ingreso_fg, facturas_negocio_record.cod_cli,
                        _facturas_cartera.nit_cliente, 'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
                        _banco_record.branch_code, _banco_record.bank_account_no, 'PES', 'OP', 'REVERSION DE CAPITAL - RETANQUEO DE CREDITOS MICRO.',
                        0.00, 0.00, '1.000000', CURRENT_DATE, _items_fg, _usuario, CURRENT_TIMESTAMP,
                        'COL', _banco_record.codigo_cuenta, 'NEG', facturas_negocio_record.negocio, '');
            END IF;

            _items_fg := _items_fg + 1;

            --INSERTA EL DETALLE DEL INGRESO INTERESES
            INSERT INTO con.ingreso_detalle (dstrct, tipo_documento, num_ingreso, item,
                                             nitcli, valor_ingreso, valor_ingreso_me, factura,
                                             fecha_factura, tipo_doc, documento, creation_user,
                                             creation_date, base, cuenta, descripcion,
                                             valor_tasa, saldo_factura, tipo_referencia_2, referencia_2, tipo_referencia_1, referencia_1)
            VALUES ('FINV', 'ICA', numero_ingreso_fg, _items_fg,
                    _facturas_cartera.nit_cliente, _facturas_cartera.valor_saldo, _facturas_cartera.valor_saldo, _facturas_cartera.documento,
                    CURRENT_DATE, 'FAC', _facturas_cartera.documento, _usuario,
                    CURRENT_TIMESTAMP, 'COL', _facturas_cartera.cuenta_cartera, 'REVERSION DE CAPITAL - RETANQUEO DE CREDITOS MICRO.',
                    '1.0000000000', 0.00, '', '', 'NEG', _facturas_cartera.negocio);


            -- RESTA EL SALDO DE LA CUOTA DE CAPITAL A LA CARTERA.
            UPDATE con.factura
            SET valor_abono      = (valor_abono + _facturas_cartera.valor_saldo),
                valor_saldo      = (valor_saldo - _facturas_cartera.valor_saldo),
                valor_abonome    = (valor_abonome + _facturas_cartera.valor_saldo),
                valor_saldome    = (valor_saldome - _facturas_cartera.valor_saldo),
                user_update      = _usuario,
                fecha_ultimo_pago= CURRENT_DATE,
                last_update      = CURRENT_TIMESTAMP
            WHERE documento = _facturas_cartera.documento
              AND tipo_documento = 'FAC'
              AND reg_status = '';


        END LOOP;

    IF numero_ingreso_fg != '' AND _negocio_a_renovar != '' THEN
        --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE LAS CUOTAS DE CAPITAL.
        UPDATE con.ingreso
        SET vlr_ingreso=(SELECT sum(idet.valor_ingreso) FROM con.ingreso_detalle idet WHERE idet.num_ingreso = numero_ingreso_fg),
            vlr_ingreso_me=(SELECT sum(idet.valor_ingreso) FROM con.ingreso_detalle idet WHERE idet.num_ingreso = numero_ingreso_fg),
            cant_item =_items_fg
        WHERE num_ingreso = numero_ingreso_fg
          AND tipo_documento = 'ICA';

        RAISE NOTICE 'numero_ingreso_if : % numero_ingreso_cm : % numero_ingreso_fg : %',numero_ingreso_if,numero_ingreso_cm,numero_ingreso_fg;
        --VALIDACION ADICIONAL SI EL SALDO DE CARTERA ES MAYOR A CERO.
        IF (SELECT sum(valor_saldo) FROM con.factura WHERE negasoc = _negocio_a_renovar AND tipo_documento = 'FAC' AND reg_status = '') > 0 THEN
            _items_fg := 10 / 0;
        END IF;

    END IF;

    RETURN 'OK';
END;
$BODY$
    LANGUAGE plpgsql
    VOLATILE;
ALTER FUNCTION apicredit.anular_negocio_mirco_retanqueo(INTEGER, CHARACTER VARYING)
    OWNER TO postgres;