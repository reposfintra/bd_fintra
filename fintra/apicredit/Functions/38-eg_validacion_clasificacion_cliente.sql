-- Function: apicredit.eg_validacion_clasificacion_cliente(_indentificacion varchar);

-- DROP FUNCTION apicredit.eg_validacion_clasificacion_cliente(_indentificacion varchar);

CREATE OR REPLACE FUNCTION apicredit.eg_validacion_clasificacion_cliente(_indentificacion varchar)
  returns boolean AS
$BODY$
DECLARE

	_clasificacion_cliente RECORD;
	_validador boolean:=TRUE;
  
BEGIN
	
	 FOR _clasificacion_cliente  IN 
	 		SELECT negasoc,
	 			   cedula_deudor,
	 			   clasificacion 
	 		FROM administrativo.clasificacion_clientes_fintracredit 
	 	    WHERE cedula_deudor=_indentificacion AND periodo=(SELECT max(periodo) FROM administrativo.clasificacion_clientes_fintracredit)
	 LOOP
	   
		IF _clasificacion_cliente.clasificacion IN ('TIPO 4') THEN 
			_validador:=FALSE;
			exit;
		END IF;
	 
	 END LOOP;

    RETURN _validador;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION  apicredit.eg_validacion_clasificacion_cliente(_indentificacion varchar)
  OWNER TO postgres;
  
  