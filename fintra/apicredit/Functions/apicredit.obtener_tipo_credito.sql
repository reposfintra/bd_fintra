-- SELECT apicredit.obtener_tipo_credito(_identificacion BIGINT, _monto_solicitado NUMERIC);

-- DROP FUNCTION apicredit.obtener_tipo_credito(_identificacion BIGINT, _monto_solicitado NUMERIC);

CREATE OR REPLACE FUNCTION apicredit.obtener_tipo_credito(_identificacion BIGINT, _monto_solicitado NUMERIC)
    RETURNS CHARACTER VARYING AS
$body$
DECLARE
    /************************************************************************************
        AUTOR: JUAN DAVID BERMUDEZ CELEDON
        FECHA: 26-06-2019
        DESCRIPCION: Función para obtener el tipo de crédito para la nueva solicitud
                     de un cliente
    ************************************************************************************/
    _arr_tipo_credito          CHARACTER VARYING[] = ARRAY ['NUE', 'PRE', 'REN', 'PAR', 'MOR'];
    _ultimo_negocio            CHARACTER VARYING;
    _actividad_negocio         CHARACTER VARYING;
    _estado_negocio            CHAR(2);
    _plazo_negocio             INTEGER;
    _saldo_negocio             NUMERIC(11, 2);
    _porcentaje_pagado         NUMERIC(3, 2);
    _monto_preaprobado         NUMERIC(11, 2);
    _cuotas_pendientes         INTEGER;
    _periodo_preaprobado       INTEGER;
    _periodo_desembolso        INTEGER;
    _ultima_fecha_pago         DATE;
    _fecha_vencimiento_negocio DATE;
BEGIN
    --BUSCA CLIENTE EN EL SISTEMA Y LA INFORMACIÓN DEL ÚLTIMO NEGOCIO
    SELECT max(n.cod_neg) AS negocio,
           COALESCE(max(f.fecha_vencimiento), '0099-01-01'::DATE),
           estado_neg,
           actividad,
           nro_docs,
           COALESCE(sum(valor_saldo), 0),
           COALESCE(fupview.fecha, '0099-01-01'::DATE),
           to_char(n.f_desem, 'YYYYMM'):: INTEGER AS periodo_desembolso
           INTO _ultimo_negocio, _fecha_vencimiento_negocio, _estado_negocio, _actividad_negocio, _plazo_negocio, _saldo_negocio, _ultima_fecha_pago, _periodo_desembolso
    FROM negocios AS n
             LEFT JOIN con.factura AS f ON n.cod_neg = f.negasoc AND f.nit = n.cod_cli AND f.reg_status = ''
             LEFT JOIN fecha_ultimo_pago_view fupview ON (f.negasoc = fupview.negasoc)
    WHERE n.cod_neg = (SELECT cod_neg
                       FROM negocios
                       WHERE estado_neg IN ('L', 'T', 'V')
                         AND cod_cli = _identificacion :: VARCHAR
                         AND negocios.negocio_rel = ''
                         AND negocios.negocio_rel_seguro = ''
                         AND negocios.negocio_rel_gps = ''
                         AND negocios.cod_neg LIKE 'MC%'
                       ORDER BY negocios.creation_date DESC
                       LIMIT 1)
    GROUP BY estado_neg, actividad, nro_docs, fupview.fecha, periodo_desembolso
    ORDER BY negocio DESC;
--     RAISE NOTICE 'NEGOCIOS: %', _ultimo_negocio;
--     RAISE NOTICE 'ESTADO: %', _estado_negocio;
--     RAISE NOTICE 'SALDO: %', _saldo_negocio;
--     RAISE NOTICE 'PERIODO DESEMBOLSO: %', _periodo_desembolso;
--     RAISE NOTICE 'ULTIMA FECHA DE PAGO: %', _ultima_fecha_pago;
--     RAISE NOTICE 'ULTIMA FECHA DE VENCIMIENTO: %', _fecha_vencimiento_negocio;

    -- SI NO ENCONTRÓ REGISTROS ES CRÉDITO NUEVO
    IF _ultimo_negocio IS NULL THEN
        RETURN _arr_tipo_credito [ 1];
    ELSIF _saldo_negocio = 0 THEN
        IF _estado_negocio = 'V' THEN
            RETURN _arr_tipo_credito [ 4];
        ELSIF _ultima_fecha_pago < (current_date - INTERVAL '6 month') THEN
            RETURN _arr_tipo_credito [ 1];
        ELSE
            --BUSCA EL ÚLTIMO PERIODO PREAPROBADO
            SELECT valor_preaprobado, periodo INTO _monto_preaprobado, _periodo_preaprobado
            FROM administrativo.preaprobados_fintra_credit
            WHERE cedula_deudor = _identificacion
              AND periodo = to_char(current_date, 'YYYYMM')
              AND id_unidad_negocio = 1
            ORDER BY periodo DESC;

--             RAISE NOTICE 'MONTO APROBADO: %', _monto_preaprobado;
--             RAISE NOTICE 'PERIODO APROBADO: %', _periodo_preaprobado;

            IF _monto_preaprobado IS NOT NULL AND _monto_preaprobado >= _monto_solicitado THEN
                RETURN _arr_tipo_credito [ 2];
            ELSE
                RETURN _arr_tipo_credito [ 3];
            END IF;
        END IF;
    ELSE
        -- SI EL NEGOCIO ESTA POR FORMALIZAR O TRANSFERIDO

        -- VALIDA SI ESTA EN MORA
        IF CURRENT_DATE > (SELECT min(fecha_vencimiento) FROM con.factura WHERE negasoc = _ultimo_negocio AND valor_saldo > 0) THEN
            RETURN _arr_tipo_credito [5];
        END IF;

        --CALCULA EL PORCENTAJE PAGADO DE LAS CUOTAS
        SELECT count(num_doc_fen) INTO _cuotas_pendientes
        FROM con.factura
        WHERE negasoc = _ultimo_negocio
          AND tipo_documento = 'FAC'
          AND substring(documento, length(documento) - 1) != '00'
          AND valor_saldo > 0
          AND reg_status = '';

--         RAISE NOTICE 'PLAZO: %', _plazo_negocio;
--         RAISE NOTICE 'CUOTAS PENDIENTES: %', _cuotas_pendientes;

        -- CALCULA LAS CUOTAS PENDIENTES
        _porcentaje_pagado := 1 - (_cuotas_pendientes :: DECIMAL / _plazo_negocio);

--         RAISE NOTICE 'PORCENTAJE PAGADO: %', _porcentaje_pagado;

        --BUSCA EL ÚLTIMO PERIODO PREAPROBADO
        SELECT valor_preaprobado, periodo INTO _monto_preaprobado, _periodo_preaprobado
        FROM administrativo.preaprobados_fintra_credit
        WHERE cedula_deudor = _identificacion
          AND periodo = to_char(current_date, 'YYYYMM')
          AND id_unidad_negocio = 1
        ORDER BY periodo DESC;

--         RAISE NOTICE 'MONTO APROBADO: %', _monto_preaprobado;
--         RAISE NOTICE 'PERIODO APROBADO: %', _periodo_preaprobado;

        -- SI NO TIENE PREAPROBADO, O SI TIENE, Y EL MONTO SOLICITADO ES MAYOR AL PREAPROBADO PASA A VALIDAR POLITICA DE RENOVACION
        IF (_monto_preaprobado >= _monto_solicitado AND _periodo_desembolso < _periodo_preaprobado) THEN
            RETURN _arr_tipo_credito [ 2];---pre
        ELSE
            IF (_porcentaje_pagado >= 0.7 AND ((_plazo_negocio BETWEEN 6 AND 8 AND _cuotas_pendientes <= 2) OR
                                               ((_plazo_negocio BETWEEN 9 AND 13) AND _cuotas_pendientes <= 3) OR
                                               ((_plazo_negocio >= 14) AND _cuotas_pendientes <= 4))) THEN
                RETURN _arr_tipo_credito [ 3];--ren
            ELSE
                RETURN _arr_tipo_credito [ 4];--par
            END IF;
        END IF;
    END IF;
END;
$body$
    LANGUAGE plpgsql
    VOLATILE;