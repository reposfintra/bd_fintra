-- Function: apicredit.confirmacion_liquidacion_negocio_micro(_codneg varchar, _monto_credito numeric, num_cuota integer, _fecha_pago date, _departamento VARCHAR);

-- DROP FUNCTION apicredit.confirmacion_liquidacion_negocio_micro(_codneg varchar, _monto_credito numeric, _num_cuota integer, _fecha_pago date, _departamento VARCHAR);

CREATE OR REPLACE FUNCTION apicredit.confirmacion_liquidacion_negocio_micro(_codneg CHARACTER VARYING, _montocredito NUMERIC, _numerocuotas INTEGER, _fechapago DATE, _departamento CHARACTER VARYING)
    RETURNS SETOF apicredit.RS_CONFIRMAR_RENOVACION AS
$BODY$
DECLARE
    /************************************************************************************
        AUTOR: JUAN DAVID BERMUDEZ CELEDON
        FECHA: 26-06-2019
        DESCRIPCION: Función para mostrar la confirmación de la liquidación del nuevo
                     crédito de retanqueo
        Modificacion: Liquidacion saldos microcredito. egonzalez
    ************************************************************************************/
    _recordLiquidacionFactura RECORD;
    _planPagos                RECORD;
    _valorFianzaAval          RECORD;
    _totalNuevoCredito        NUMERIC;
    _codcli                   VARCHAR;
    _idConvenio               INTEGER;
    _baseSalarioMinimo        NUMERIC;
    _montoEstudioCredito      NUMERIC;
    _montoComisionDesembolso  NUMERIC;
    _montoDescuentoAval       NUMERIC;
    _montoAvalFinanciado      NUMERIC;
    rs                        apicredit.RS_CONFIRMAR_RENOVACION;

BEGIN

    SELECT INTO _recordLiquidacionFactura negasoc,
                                          sum(k_capital) AS saldo_capital,
                                          sum(i_interes) AS saldo_interes,
                                          sum(ca_cuota_admin) AS saldo_ca,
                                          sum(saldo_cat) AS saldo_cat,
                                          sum(k_capital_aval) AS saldo_capital_aval,
                                          sum(i_interes_aval) AS saldo_intres,
                                          sum(k_capital) + sum(i_interes) + sum(ca_cuota_admin) + sum(saldo_cat) + sum(k_capital_aval) + sum(i_interes_aval) AS total_renovacion
    FROM (
             SELECT negasoc,
                    sum(saldo_capital) AS k_capital,
                    CASE WHEN estado_cartera = 'D-FUTUROS' THEN 0.00 ELSE sum(saldo_interes) END AS i_interes,
                    CASE WHEN estado_cartera = 'D-FUTUROS' THEN 0.00 ELSE sum(saldo_cuota_admin) END AS ca_cuota_admin,
                    sum(saldo_cat) AS saldo_cat,
                    sum(saldo_capital_aval) AS k_capital_aval,
                    CASE WHEN estado_cartera = 'D-FUTUROS' THEN 0.00 ELSE sum(saldo_interes_aval) END AS i_interes_aval,
                    estado_cartera
             FROM apicredit.liquidacion_saldos_negocio_microcredito(_codneg::VARCHAR) AS saldo (negasoc CHARACTER VARYING, documento CHARACTER VARYING, nit CHARACTER VARYING, num_doc_fen CHARACTER VARYING, valor_factura NUMERIC,
                                                                                                saldo_capital NUMERIC, saldo_interes NUMERIC, saldo_cuota_admin NUMERIC, saldo_cat NUMERIC, saldo_capital_aval NUMERIC, saldo_interes_aval NUMERIC,
                                                                                                valor_abono_cuota NUMERIC, valor_saldo_cuota NUMERIC,
                                                                                                fecha_vencimiento DATE, dias_mora INTEGER, estado_cartera CHARACTER VARYING
                                                                                               )
             WHERE estado_cartera NOT IN ('A-VENCIDO_SIN_SALDO')
             GROUP BY negasoc, estado_cartera
         ) t
    GROUP BY negasoc;

    _totalNuevoCredito := _montoCredito;

    _codcli := COALESCE((SELECT cod_cli FROM negocios WHERE cod_neg = _recordLiquidacionFactura.negasoc AND estado_neg = 'T'), '');

    SELECT INTO _planPagos round(retorno.valor) AS valor_cuota
    FROM apicredit.eg_simulador_liquidacion_micro_fecha(_totalNuevoCredito, _numeroCuotas,
                                                        _fechaPago, 'CTFCPV'::VARCHAR, _departamento,
                                                        current_date, 'N'::VARCHAR, 0::INTEGER, 'SIMULAR', 0::INTEGER) AS retorno
    GROUP BY retorno.valor;

    ----Salario minimo
    _baseSalarioMinimo := (SELECT salario_minimo_mensual FROM salario_minimo WHERE ano = SUBSTRING(CURRENT_DATE, 1, 4));

    --Buscamos el convenio.
    _idConvenio := (SELECT id_convenio
                    FROM convenios
                    WHERE tipo = 'Microcredito'
                      AND id_convenio NOT IN (37, 41, 15, 40)
                      AND round(_montoCredito / _baseSalarioMinimo, 2) BETWEEN monto_minimo AND monto_maximo
                      AND agencia = _departamento);

    /* ****************************************
    * Calculo del valor de la fianza o aval   *
    *******************************************/
    SELECT INTO _valorFianzaAval CASE WHEN porcentaje_comision > 0 THEN round((_montoCredito * porcentaje_comision / 100) * (1 + porcentaje_iva / 100))
                                      ELSE round((_numeroCuotas * _montoCredito * valor_comision / 1000000) * (1 + porcentaje_iva / 100))
                                 END AS valor,
                                 cf.financiado,
                                 cf.porcentaje_aval_finan, -- porcentaje financiar
                                 cf.porcentaje_aval_clie   --porcentaje descontar
    FROM configuracion_factor_por_millon cf
             INNER JOIN unidad_negocio un ON cf.id_unidad_negocio = un.id
             INNER JOIN rel_unidadnegocio_convenios run ON (run.id_unid_negocio = un.id)
    WHERE id_unid_negocio IN ((SELECT id_unid_negocio FROM rel_unidadnegocio_procinterno WHERE id_proceso_interno = (SELECT id FROM proceso_interno WHERE descripcion = 'COBRANZA ESTRATEGICA')))
      AND id_convenio = _idConvenio
      AND cf.reg_status = ''
      AND _numeroCuotas BETWEEN plazo_inicial AND plazo_final;

    /************************************************
    * valida si es financiado el aval y si aplica   *
    ************************************************/
    IF _valorFianzaAval.financiado = 'S' AND _valorFianzaAval.valor > 0 THEN
        _montoAvalFinanciado := round(_valorFianzaAval.valor * (_valorFianzaAval.porcentaje_aval_finan / 100));
        _montoDescuentoAval := _valorFianzaAval.valor - _montoAvalFinanciado;
    ELSE
        _montoDescuentoAval := _valorFianzaAval.valor;
    END IF;

    -- Valor central de riesgo.
    SELECT dato INTO _montoEstudioCredito FROM tablagen WHERE table_type = 'VLRCENTRAL' AND table_code = 'MICRO';
    SELECT valor INTO _montoComisionDesembolso FROM comisiones_bancos WHERE anio = 2019 AND banco_transfer IN ('EFECTY', 'Superefectivo', 'BANCOLOMBIA');


    IF _recordLiquidacionFactura IS NOT NULL THEN
        rs.monto_solicitado := _montoCredito;
        rs.saldo_credito_anterior := _recordLiquidacionFactura.total_renovacion;
        rs.total_nuevo_credito := _totalNuevoCredito;
        rs.num_cuota := _numeroCuotas;
        rs.fecha_pago := _fechaPago::VARCHAR;
        rs.nueva_cuota_aproximada := _planpagos.valor_cuota;
        rs.nuevo_valor_aval := _montoDescuentoAval;
        rs.monto_estudio_credito := _montoEstudioCredito;
        rs.monto_comision_desembolso := _montoComisionDesembolso;
        rs.monto_desembolso := _montoCredito - _recordLiquidacionFactura.total_renovacion - _montoDescuentoAval - _montoEstudioCredito - _montoComisionDesembolso;
    END IF;
    RETURN NEXT rs;

END ;
$BODY$
    LANGUAGE plpgsql
    VOLATILE;
ALTER FUNCTION apicredit.confirmacion_liquidacion_negocio_micro(CHARACTER VARYING, NUMERIC, INTEGER, DATE, CHARACTER VARYING)
    OWNER TO postgres;