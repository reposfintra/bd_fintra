-- Function: apicredit.confirmacion_liquidacion_negocio(_codneg varchar, _monto_credito numeric, num_cuota integer, _fecha_pago date, _id_convenio integer);

-- DROP FUNCTION apicredit.confirmacion_liquidacion_negocio(_codneg varchar, _monto_credito numeric, _num_cuota integer, _fecha_pago date, _id_convenio integer);

CREATE OR REPLACE FUNCTION apicredit.confirmacion_liquidacion_negocio(_codneg varchar, _monto_credito numeric, _num_cuota integer, _fecha_pago date, _id_convenio integer)
  returns SETOF  apicredit.rs_confirmar_renovacion AS
$BODY$
declare

  _recordLiquidacionFactura record;
  _planPagos record;
  _total_nuevo_credito numeric;
  _codcli varchar;
  rs  apicredit.rs_confirmar_renovacion;
  
begin
	
	select into _recordLiquidacionFactura
		   negasoc,
		   sum(k_capital) as saldo_capital,
		   sum(i_interes) as saldo_interes,
		   sum(ca_cuota_admin) as saldo_ca,
		   sum(k_capital)+sum(i_interes)+sum(ca_cuota_admin) as total_renovacion
	from (																  
				select  
				        negasoc,
				        sum(saldo_capital) as k_capital,
				        case when estado_cartera ='D-FUTUROS' then 0.00 else sum(saldo_interes) end as i_interes,
				        case when estado_cartera ='D-FUTUROS' then 0.00 else  sum(saldo_cuota_admin) end as ca_cuota_admin,
				        estado_cartera
				 from apicredit.liquidacion_saldos_negocios(_codneg::varchar) as saldo(  
																					  negasoc character varying,
																					  documento character varying, 
																					  nit character varying, 
																					  num_doc_fen character varying,
																					  valor_factura numeric,
																					  saldo_capital numeric,
																					  saldo_interes numeric,
																					  saldo_cuota_admin numeric,
																					  valor_abono_cuota numeric,
																					  valor_saldo_cuota numeric,
																					  fecha_vencimiento date,
																					  dias_mora integer,
																					  estado_cartera character varying)
				where estado_cartera not IN ('A-VENCIDO_SIN_SALDO')																	  
				group by negasoc,estado_cartera
	)t 
	group by negasoc;
			
	_total_nuevo_credito:=_monto_credito+_recordLiquidacionFactura.total_renovacion;
	
	_codcli:=COALESCE((SELECT cod_cli FROM negocios where cod_neg=_recordLiquidacionFactura.negasoc and estado_neg='T'),'');
	
	select  into _planpagos
			round(retorno.valor) as valor_cuota,
		    retorno.valor_aval
    from  apicredit.eg_liquidador_creditos(_total_nuevo_credito::numeric, _num_cuota::integer, _fecha_pago::date, _id_convenio::integer,_codcli::varchar ) as retorno
    group by  
    retorno.valor,
    retorno.valor_aval;
    
    if _recordLiquidacionFactura is not null then 
    rs.monto_solicitado := _monto_credito;
    rs.saldo_credito_anterior := _recordLiquidacionFactura.total_renovacion;
    rs.total_nuevo_credito := _total_nuevo_credito;
    rs.num_cuota := _num_cuota; 
    rs.fecha_pago :=_fecha_pago::varchar; 
    rs.nueva_cuota_aproximada := _planpagos.valor_cuota;
    rs.nuevo_valor_aval := _planpagos.valor_aval;
    
    end if;
    return next rs;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apicredit.confirmacion_liquidacion_negocio(_codneg varchar, _monto_credito numeric, _num_cuota integer, _fecha_pago date, _id_convenio integer)
  OWNER TO postgres;
  
 