-- Function: apicredit.anular_ingresos_negocio_educativo_renovado(INTEGER, CHARACTER VARYING)

-- DROP FUNCTION apicredit.anular_ingresos_negocio_educativo_renovado(INTEGER, CHARACTER VARYING);

CREATE OR REPLACE FUNCTION apicredit.anular_ingresos_negocio_educativo_renovado(_numero_solicitud INTEGER, _usuario CHARACTER VARYING)
    RETURNS CHARACTER VARYING AS $$
DECLARE

    /********************************************************************
    * Autor: Juan David Bermudez                                        *
    * Fecha: 2018-10-11                                                 *
    * Descripcion: Crea las notas de ajuste, resta las cartera y anula  *
    *             los diferidos futuros cuando se renueva un negocio  	*
    * Modificado por: Edgar Gonzalez Mendoza						    *
    ********************************************************************/

    facturas_negocio_record RECORD;
    _facturas_cartera RECORD;
    _banco_record RECORD;
    _negocio_nuevo RECORD;
    numero_ingreso_if  CHARACTER VARYING := '';
    numero_ingreso_cm  CHARACTER VARYING := '';
    numero_ingreso_fg  CHARACTER VARYING := '';
    _negocio_a_renovar  CHARACTER VARYING := '';
    _items_if  INTEGER := 0;
    _items_cm  INTEGER := 0;
    _items_fg  INTEGER := 0;
    saldo_factura_ingreso NUMERIC:= 0;
    diferidos_array VARCHAR(20) [];
    _cuentas_fiducia VARCHAR(10) []:='{16252135,16252136,11050701}';    
    
BEGIN
	
	SELECT INTO _negocio_a_renovar negocio FROM apicredit.liquidacion_saldos_ca WHERE numero_solicitud=_numero_solicitud GROUP BY negocio;   
	PERFORM * FROM tem.backup_cartera_renovacion_credito_edu WHERE negasoc=_negocio_a_renovar;
	IF NOT FOUND THEN 	
		INSERT INTO tem.backup_cartera_renovacion_credito_edu(
	            reg_status, dstrct, tipo_documento, documento, nit, codcli, concepto, 
	            fecha_factura, fecha_vencimiento, fecha_ultimo_pago, fecha_impresion, 
	            descripcion, observacion, valor_factura, valor_abono, valor_saldo, 
	            valor_facturame, valor_abonome, valor_saldome, valor_tasa, moneda, 
	            cantidad_items, forma_pago, agencia_facturacion, agencia_cobro, 
	            zona, clasificacion1, clasificacion2, clasificacion3, transaccion, 
	            transaccion_anulacion, fecha_contabilizacion, fecha_anulacion, 
	            fecha_contabilizacion_anulacion, base, last_update, user_update, 
	            creation_date, creation_user, fecha_probable_pago, flujo, rif, 
	            cmc, usuario_anulo, formato, agencia_impresion, periodo, valor_tasa_remesa, 
	            negasoc, num_doc_fen, obs, pagado_fenalco, corficolombiana, tipo_ref1, 
	            ref1, tipo_ref2, ref2, dstrct_ultimo_ingreso, tipo_documento_ultimo_ingreso, 
	            num_ingreso_ultimo_ingreso, item_ultimo_ingreso, fec_envio_fiducia, 
	            nit_enviado_fiducia, tipo_referencia_1, referencia_1, tipo_referencia_2, 
	            referencia_2, tipo_referencia_3, referencia_3, nc_traslado, fecha_nc_traslado, 
	            tipo_nc, numero_nc, factura_traslado, factoring_formula_aplicada, 
	            nit_endoso, devuelta, fc_eca, fc_bonificacion, indicador_bonificacion, 
	            fi_bonificacion, endoso_fenalco, endoso_fiducia, causacion_int_ms, 
	            fecha_causacion_int_ms, procesado)	            
	   select 	reg_status, dstrct, tipo_documento, documento, nit, codcli, concepto, 
	            fecha_factura, fecha_vencimiento, fecha_ultimo_pago, fecha_impresion, 
	            descripcion, observacion, valor_factura, valor_abono, valor_saldo, 
	            valor_facturame, valor_abonome, valor_saldome, valor_tasa, moneda, 
	            cantidad_items, forma_pago, agencia_facturacion, agencia_cobro, 
	            zona, clasificacion1, clasificacion2, clasificacion3, transaccion, 
	            transaccion_anulacion, fecha_contabilizacion, fecha_anulacion, 
	            fecha_contabilizacion_anulacion, base, last_update, user_update, 
	            creation_date, creation_user, fecha_probable_pago, flujo, rif, 
	            cmc, usuario_anulo, formato, agencia_impresion, periodo, valor_tasa_remesa, 
	            negasoc, num_doc_fen, obs, pagado_fenalco, corficolombiana, tipo_ref1, 
	            ref1, tipo_ref2, ref2, dstrct_ultimo_ingreso, tipo_documento_ultimo_ingreso, 
	            num_ingreso_ultimo_ingreso, item_ultimo_ingreso, fec_envio_fiducia, 
	            nit_enviado_fiducia, tipo_referencia_1, referencia_1, tipo_referencia_2, 
	            referencia_2, tipo_referencia_3, referencia_3, nc_traslado, fecha_nc_traslado, 
	            tipo_nc, numero_nc, factura_traslado, factoring_formula_aplicada, 
	            nit_endoso, devuelta, fc_eca, fc_bonificacion, indicador_bonificacion, 
	            fi_bonificacion, endoso_fenalco, endoso_fiducia, causacion_int_ms, 
	            fecha_causacion_int_ms, procesado
	     from con.factura where negasoc=_negocio_a_renovar and reg_status='' and tipo_documento='FAC';	     
     END IF;
  
	-- 1. REVERSION DE INTERESES Y CUOTAS DE ADMINISTRACION.
    FOR facturas_negocio_record in
    
    							SELECT n.cod_cli AS nit_cliente,
                                          n.cod_neg AS negocio,
                                          f.codcli AS cod_cli,
                                          f.documento,
                                          lsc.num_doc_fen AS cuota,
                                          f.fecha_vencimiento,
                                          lsc.estado_cartera,
                                          lsc.dias_mora,
                                          f.valor_saldo,
                                          lsc.saldo_interes AS interes_a_cobrar,
                                          lsc.saldo_cuota_admin AS cuota_admon_a_cobrar,
                                          cmc3.cuenta as cuenta_cartera,
                                          CASE WHEN ing.endosado ='S' THEN  _cuentas_fiducia [1] ELSE cmc.cuenta  END AS cuenta_interes,
                                          CASE WHEN ing.endosado ='S' THEN  _cuentas_fiducia [2] ELSE cmc2.cuenta END AS cuenta_cuota_admin,
                                          ing.cod AS diferido_if,
                                          ing2.cod AS diferido_cm,
                                         ing.endosado
                                   FROM apicredit.liquidacion_saldos_ca lsc
                                            INNER JOIN con.factura f ON lsc.documento = f.documento 
                                            INNER JOIN negocios n ON n.cod_neg = f.negasoc AND n.cod_cli = f.nit
                                            INNER JOIN ing_fenalco ing ON (ing.codneg = lsc.negocio AND ing.tipodoc = 'IF' AND ing.cuota = lsc.num_doc_fen)
                                            INNER JOIN ing_fenalco ing2 ON (ing2.codneg = lsc.negocio AND ing2.tipodoc = 'CM' AND ing2.cuota = lsc.num_doc_fen)
                                            INNER JOIN con.cmc_doc cmc ON (cmc.cmc = ing.cmc AND ing.tipodoc = cmc.tipodoc)
                                            INNER JOIN con.cmc_doc cmc2 ON (cmc2.cmc = ing2.cmc AND ing2.tipodoc = cmc2.tipodoc)
                                            INNER JOIN con.cmc_doc cmc3 ON (cmc3.cmc = f.cmc AND cmc3.tipodoc=f.tipo_documento)
                                   WHERE lsc.numero_solicitud = _numero_solicitud 
                                     AND lsc.estado_cartera IN ('D-FUTUROS')
                                     AND f.reg_status = ''
                                     AND f.tipo_documento = 'FAC'
                                     AND f.valor_saldo > 0
                                   ORDER BY f.documento 
                                   
   	LOOP
                                
	     --ANULACION DE LOS INTERESES FUTUROS
        IF facturas_negocio_record.interes_a_cobrar > 0  THEN

        	--inserta las cabeceras de los ingresos. 
        	IF _items_if=0 THEN   
        	
 				--OBTENIE EL NUMERO DE INGRESO PARA IA DE INTERESES
	            numero_ingreso_if := get_lcod('ICAC'); 
	            saldo_factura_ingreso := facturas_negocio_record.valor_saldo;
	            
	            --INSERTA LA CABECERA DEL INGRESO INTERESES
	            INSERT INTO con.ingreso (dstrct,tipo_documento,num_ingreso,codcli,
	                                     nitcli,concepto,tipo_ingreso,fecha_consignacion,fecha_ingreso,
	                                     branch_code,bank_account_no,codmoneda,agencia_ingreso,descripcion_ingreso,
	                                     vlr_ingreso,vlr_ingreso_me,vlr_tasa,fecha_tasa,cant_item,creation_user, creation_date,
	                                     base,cuenta, tipo_referencia_1,referencia_1,referencia_2)
	           	 VALUES ('FINV', 'ICA', numero_ingreso_if, facturas_negocio_record.cod_cli,
	                    facturas_negocio_record.nit_cliente,'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
	                    '', '','PES','OP','REVERSION INTERESES - RENOVACION DE CREDITOS EDUC.',
	                    0.00, 0.00, '1.000000', CURRENT_DATE, _items_if, _usuario, CURRENT_TIMESTAMP,
	                    'COL', facturas_negocio_record.cuenta_interes, 'NEG', facturas_negocio_record.negocio, '' );
		                   
             END IF;

             _items_if:=_items_if+1;	
             saldo_factura_ingreso:=saldo_factura_ingreso-facturas_negocio_record.interes_a_cobrar;
          
	         --INSERTA EL DETALLE DEL INGRESO INTERESES
	         INSERT INTO con.ingreso_detalle (dstrct,tipo_documento,num_ingreso,item,
	                                             nitcli,valor_ingreso,valor_ingreso_me,factura,
	                                             fecha_factura,tipo_doc,documento,creation_user,
	                                             creation_date,base,cuenta,descripcion,
	                                             valor_tasa,saldo_factura,tipo_referencia_2, referencia_2 )
	            VALUES ('FINV', 'ICA', numero_ingreso_if,_items_if,
	                    facturas_negocio_record.nit_cliente, facturas_negocio_record.interes_a_cobrar, facturas_negocio_record.interes_a_cobrar, facturas_negocio_record.documento,
	                    CURRENT_DATE, 'FAC', facturas_negocio_record.documento,  _usuario, 
	                    CURRENT_TIMESTAMP, 'COL',facturas_negocio_record.cuenta_cartera,'REVERSION INTERESES - RENOVACION DE CREDITOS EDUC.',
	                    '1.0000000000', saldo_factura_ingreso,'IF',facturas_negocio_record.diferido_if);
	                    
            -- RESTA EL SALDO DEL INTERES A LA CARTERA.
	        UPDATE con.factura
		        SET valor_abono   = (valor_abono + facturas_negocio_record.interes_a_cobrar),
		            valor_saldo   = (valor_saldo - facturas_negocio_record.interes_a_cobrar),
		            valor_abonome = (valor_abonome + facturas_negocio_record.interes_a_cobrar),
		            valor_saldome = (valor_saldome - facturas_negocio_record.interes_a_cobrar),
		            user_update   = _usuario,
		            fecha_ultimo_pago= CURRENT_DATE,
		            last_update   = CURRENT_TIMESTAMP
	        WHERE documento = facturas_negocio_record.documento and tipo_documento='FAC';
	                    
			--AGREGAMOS LOS DIFERIDOS DE ITERESES PARA ANULARLOS
            diferidos_array := array_append(diferidos_array, facturas_negocio_record.diferido_if);
            
        END IF;
        
         --ANULACION DE LAS CUAOTAS DE ADMINISTRACION FUTURAS
        IF facturas_negocio_record.cuota_admon_a_cobrar > 0  then
        	
        	IF _items_cm=0 THEN 
        	
        			--OBTENIE EL NUMERO DE INGRESO PARA IA DE CUOTAS DE ADMINISTRACION
        		    numero_ingreso_cm := get_lcod('ICAC'); 
        		    
		            --INSERTA LA CABECERA DEL INGRESO INTERESES
		            INSERT INTO con.ingreso (dstrct,tipo_documento,num_ingreso,codcli,
		                                     nitcli,concepto,tipo_ingreso,fecha_consignacion,fecha_ingreso,
		                                     branch_code,bank_account_no,codmoneda,agencia_ingreso,descripcion_ingreso,
		                                     vlr_ingreso,vlr_ingreso_me,vlr_tasa,fecha_tasa,cant_item,creation_user, creation_date,
		                                     base,cuenta, tipo_referencia_1,referencia_1,referencia_2)
		           	 VALUES ('FINV', 'ICA', numero_ingreso_cm, facturas_negocio_record.cod_cli,
		                    facturas_negocio_record.nit_cliente,'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
		                    '', '','PES','OP','REVERSION CUOTAS ADMINISTRACION - RENOVACION DE CREDITOS EDUC.',
		                    0.00, 0.00, '1.000000', CURRENT_DATE, _items_cm, _usuario, CURRENT_TIMESTAMP,
		                    'COL', facturas_negocio_record.cuenta_cuota_admin, 'NEG', facturas_negocio_record.negocio, '' );
        	
        	END IF;
        	
        	_items_cm:=_items_cm+1;
        	saldo_factura_ingreso:=saldo_factura_ingreso-facturas_negocio_record.cuota_admon_a_cobrar;
        	
           --INSERTA EL DETALLE DEL INGRESO PARA LAS CUOTAS DE ADMINISTRACION.
	        INSERT INTO con.ingreso_detalle (dstrct,tipo_documento,num_ingreso,item,
	                                         nitcli,valor_ingreso,valor_ingreso_me,factura,
	                                         fecha_factura,tipo_doc,documento,creation_user,
	                                         creation_date,base,cuenta,descripcion,
	                                         valor_tasa,saldo_factura,tipo_referencia_2, referencia_2 )
	            VALUES ('FINV', 'ICA', numero_ingreso_cm,_items_cm,
	                    facturas_negocio_record.nit_cliente, facturas_negocio_record.cuota_admon_a_cobrar, facturas_negocio_record.cuota_admon_a_cobrar, facturas_negocio_record.documento,
	                    CURRENT_DATE, 'FAC', facturas_negocio_record.documento,  _usuario, 
	                    CURRENT_TIMESTAMP, 'COL',facturas_negocio_record.cuenta_cartera,'REVERSION CUOTAS ADMINISTRACION - RENOVACION DE CREDITOS EDUC.',
	                    '1.0000000000', saldo_factura_ingreso,'CM',facturas_negocio_record.diferido_cm);
	                    
	        -- RESTA EL SALDO DE LA CUOTA DE ADMINISTRACION A LA CARTERA.
	        UPDATE con.factura
		        SET valor_abono   = (valor_abono + facturas_negocio_record.cuota_admon_a_cobrar),
		            valor_saldo   = (valor_saldo - facturas_negocio_record.cuota_admon_a_cobrar),
		            valor_abonome = (valor_abonome + facturas_negocio_record.cuota_admon_a_cobrar),
		            valor_saldome = (valor_saldome - facturas_negocio_record.cuota_admon_a_cobrar),
		            user_update   = _usuario,
		            fecha_ultimo_pago= CURRENT_DATE,
		            last_update   = CURRENT_TIMESTAMP
	        WHERE documento = facturas_negocio_record.documento and tipo_documento='FAC';
	                    
	         --AGREGAMOS LOS DIFERIDOS CUOTA DE ADMINISTRACION PARA ANULARLOS
	         diferidos_array := array_append(diferidos_array, facturas_negocio_record.diferido_cm);       
        
        
        END IF;
        
        --ANULAR LOS DIFERIDOS INTERESES Y CUOTAS DE ADMINISTRACION.
        UPDATE ing_fenalco
        SET reg_status  = 'A',
            user_update = _usuario,
            last_update = CURRENT_TIMESTAMP
        WHERE codneg = facturas_negocio_record.negocio 
        AND cod = ANY (diferidos_array) AND  (periodo = '' OR periodo isnull)
        AND procesado_dif != 'S' AND cuota = facturas_negocio_record.cuota;
        
        
        diferidos_array := NULL;
        
    END LOOP;
    
    IF numero_ingreso_if != ''  THEN     
     --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE INTERESES.
		     UPDATE con.ingreso 
			    	SET vlr_ingreso=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_if),
			    	    vlr_ingreso_me=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_if),
			    	    cant_item =_items_if
			 WHERE num_ingreso=numero_ingreso_if AND tipo_documento='ICA';    
    END IF; 
    
	 IF numero_ingreso_cm !='' THEN 
	 --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE LAS CUOTAS DE AMINISTRACION.
	     UPDATE con.ingreso 
		    	SET vlr_ingreso=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_cm),
		    	    vlr_ingreso_me=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_cm),
		    	    cant_item =_items_cm
		 WHERE num_ingreso=numero_ingreso_cm AND tipo_documento='ICA';
	 END IF; 
	
	 -- 2.) REVERSION DE LA CARTERA DEL CLIENTE.
	 FOR _facturas_cartera IN 	 							 
							 SELECT
							 		f.nit AS nit_cliente,
							 		f.documento,
							 		f.valor_factura,
							 		f.valor_abono,
							 		f.valor_saldo,
							 		cmc.cuenta AS cuenta_cartera,							 	
							 		f.negasoc AS negocio	 		
							 FROM con.factura f
							 INNER JOIN apicredit.liquidacion_saldos_ca lca ON lca.documento=f.documento AND f.num_doc_fen=lca.num_doc_fen
							 INNER JOIN con.cmc_doc cmc ON cmc.cmc=f.cmc AND cmc.tipodoc=f.tipo_documento
							 INNER JOIN negocios n ON n.cod_neg=f.negasoc	 
							 WHERE lca.estado_cartera IN ('C-CORRIENTE','D-FUTUROS')
							 	AND f.reg_status=''
							 	AND f.tipo_documento='FAC'
							 	AND lca.numero_solicitud=_numero_solicitud
							 	AND f.valor_saldo > 0							 	
	 LOOP
	 
 			IF _items_fg=0 THEN 
 		
 				numero_ingreso_fg := get_lcod('ICAC');	 				
 				SELECT INTO _banco_record branch_code,bank_account_no,codigo_cuenta FROM public.banco WHERE branch_code='CAJA RETANQ EDU' AND bank_account_no='RETANQUEO EDUCATIVO';
 				
 				--INSERTA LA CABECERA DEL INGRESO PARA CANCELAR LA CARTERA.
	            INSERT INTO con.ingreso (dstrct,tipo_documento,num_ingreso,codcli,
               		                     nitcli,concepto,tipo_ingreso,fecha_consignacion,fecha_ingreso,
	                                     branch_code,bank_account_no,codmoneda,agencia_ingreso,descripcion_ingreso,
	                                     vlr_ingreso,vlr_ingreso_me,vlr_tasa,fecha_tasa,cant_item,creation_user, creation_date,
	                                     base,cuenta, tipo_referencia_1,referencia_1,referencia_2)
	           	 VALUES ('FINV', 'ICA', numero_ingreso_fg, facturas_negocio_record.cod_cli,
	                    _facturas_cartera.nit_cliente,'FE', 'C', CURRENT_DATE, CURRENT_TIMESTAMP,
	                    _banco_record.branch_code, _banco_record.bank_account_no,'PES','OP','REVERSION DE CAPITAL - RENOVACION DE CREDITOS EDUC.',
	                    0.00, 0.00, '1.000000', CURRENT_DATE, _items_fg, _usuario, CURRENT_TIMESTAMP,
	                    'COL', _banco_record.codigo_cuenta, 'NEG', facturas_negocio_record.negocio, '' );	 			
 			END IF;
	 			
 			_items_fg:=_items_fg+1;
 			
 			 --INSERTA EL DETALLE DEL INGRESO INTERESES
	         INSERT INTO con.ingreso_detalle (dstrct,tipo_documento,num_ingreso,item,
	                                             nitcli,valor_ingreso,valor_ingreso_me,factura,
	                                             fecha_factura,tipo_doc,documento,creation_user,
	                                             creation_date,base,cuenta,descripcion,
	                                             valor_tasa,saldo_factura,tipo_referencia_2, referencia_2 )
	            VALUES ('FINV', 'ICA', numero_ingreso_fg,_items_fg,
	                    _facturas_cartera.nit_cliente, _facturas_cartera.valor_saldo, _facturas_cartera.valor_saldo, _facturas_cartera.documento,
	                    CURRENT_DATE, 'FAC', _facturas_cartera.documento,  _usuario, 
	                    CURRENT_TIMESTAMP, 'COL',_facturas_cartera.cuenta_cartera,'REVERSION DE CAPITAL - RENOVACION DE CREDITOS EDUC.',
	                    '1.0000000000', 0.00,'','');
	                    
	                    
	        -- RESTA EL SALDO DE LA CUOTA DE CAPITAL A LA CARTERA.
	        UPDATE con.factura
		        SET valor_abono   = (valor_abono + _facturas_cartera.valor_saldo),
		            valor_saldo   = (valor_saldo - _facturas_cartera.valor_saldo),
		            valor_abonome = (valor_abonome + _facturas_cartera.valor_saldo),
		            valor_saldome = (valor_saldome - _facturas_cartera.valor_saldo),
		            user_update   = _usuario,
		            fecha_ultimo_pago= CURRENT_DATE,
		            last_update   = CURRENT_TIMESTAMP
	        WHERE documento =  _facturas_cartera.documento and tipo_documento='FAC';
	        
	 
	 END LOOP;
	 
	 IF numero_ingreso_fg !='' AND _negocio_a_renovar != '' THEN  
		  --SE ACTUALIZA CABECERA DEL INGRESO PARA LA REVERSION DE LAS CUOTAS DE CAPITAL.
	     UPDATE con.ingreso 
		    	SET vlr_ingreso=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_fg),
		    	    vlr_ingreso_me=(select sum(idet.valor_ingreso) from con.ingreso_detalle idet where idet.num_ingreso=numero_ingreso_fg),
		    	    cant_item =_items_fg
		 WHERE num_ingreso=numero_ingreso_fg AND tipo_documento='ICA';
		 
		 RAISE NOTICE 'numero_ingreso_if : % numero_ingreso_cm : % numero_ingreso_fg : %',numero_ingreso_if,numero_ingreso_cm,numero_ingreso_fg;
		 --VALIDACION ADICIONAL SI EL SALDO DE CARTERA ES MAYOR A CERO.
		 IF (SELECT  sum(valor_saldo) FROM  con.factura WHERE  negasoc=_negocio_a_renovar AND  tipo_documento='FAC' AND  reg_status='' ) > 0 THEN 	    
		 	_items_fg:=10/0;	 	
		 END IF;
		 
		 --crear nota para cancelar cxp temporal al cliente	 		  
		 SELECT INTO _negocio_nuevo  ng.cod_cli, ng.cod_neg,cxp.documento,upper(pe.nombre) AS nombre, COALESCE(pr.nit,'-') AS nit 
		    FROM solicitud_aval sa
			INNER JOIN negocios ng ON ng.cod_neg=sa.cod_neg
			INNER JOIN solicitud_persona pe ON (pe.numero_solicitud=sa.numero_solicitud AND pe.tipo='S')
			INNER JOIN fin.cxp_doc cxp ON cxp.documento_relacionado=ng.cod_neg AND cxp.tipo_documento ='FAP' AND cxp.proveedor=ng.cod_cli
			LEFT JOIN proveedor pr ON pr.nit=cxp.proveedor 
		 WHERE sa.numero_solicitud= _numero_solicitud
		 AND cxp.documento LIKE 'RN%' 
         AND cxp.reg_status='' ;
		
		IF _negocio_nuevo IS NOT NULL  THEN
		
			--CREAMOS EL PROVEEDOR SI NO EXISTE 
	       IF _negocio_nuevo.nit='-' THEN
	       
	       		INSERT INTO proveedor(
				   reg_status, dstrct, nit, id_mims, payment_name, branch_code, 
				    bank_account_no, agency_id, last_update, user_update, creation_date, 
				    creation_user, tipo_doc, banco_transfer, suc_transfer, tipo_cuenta, 
				    no_cuenta, codciu_cuenta, clasificacion, gran_contribuyente, 
				    agente_retenedor, autoret_rfte, autoret_iva, autoret_ica, hc, 
				    plazo, base, cedula_cuenta, nombre_cuenta, concept_code, cmc, 
				    tipo_pago, nit_beneficiario, aprobado, fecha_aprobacion, usuario_aprobacion, 
				    ret_pago, cod_fenalco, tasa_fenalco, cliente_fenalco, cliente_captacion, 
				    tasa_captacion, frecuencia_captacion, custodiacheque, remesa, 
				    dtsp, afiliado, sede, regimen, nit_afiliado)
			    VALUES ('', 'FINV',  _negocio_nuevo.cod_cli, '',_negocio_nuevo.nombre, 'CAJA RETANQ EDU', 
				    'RETANQUEO EDUCATIVO', '', '0099-01-01 00:00:00'::timestamp without time zone, '', NOW(), 
				    _usuario, '', '', '','',
				    '0000000000', '', 'PN', 'N', 
				    'N', 'N', 'N', 'N', '', 
				    1, 'COL', _negocio_nuevo.cod_cli,_negocio_nuevo.nombre, '01', '01', 
				    'T',  _negocio_nuevo.cod_cli, 'N', '0099-01-01 00:00:00'::timestamp without time zone, '', 
				    'N', '', 0, false, false, 
				    0, 'Mensual', 1450, 0, 
				    '', 'N', 'N', '', '');				

	       END IF; 
			
			--CABECERA DE NOTA CREDITO 
			INSERT INTO FIN.CXP_DOC (PROVEEDOR, TIPO_DOCUMENTO, DOCUMENTO, DESCRIPCION, AGENCIA,
	            					HANDLE_CODE, BANCO, SUCURSAL, VLR_NETO, VLR_SALDO, VLR_NETO_ME,
	            					VLR_SALDO_ME, TASA, CREATION_DATE, CREATION_USER,  BASE,
	            					MONEDA_BANCO, FECHA_DOCUMENTO, FECHA_VENCIMIENTO, CLASE_DOCUMENTO_REL,
	          						MONEDA,TIPO_DOCUMENTO_REL, DOCUMENTO_RELACIONADO,  DSTRCT,CLASE_DOCUMENTO,TIPO_REFERENCIA_1,
	          						REFERENCIA_1, TIPO_REFERENCIA_2,REFERENCIA_2)          						
	          						
	         SELECT 
	               PROVEEDOR, 'NC'::VARCHAR AS TIPO_DOCUMENTO, DOCUMENTO||'-1','NOTA CREDITO CANCELANDO SALDO POR PAGAR AL CLIENTE DE RENOVACION '||DOCUMENTO::VARCHAR AS DESCRIPCION, AGENCIA,
			       HANDLE_CODE, BANCO, SUCURSAL, VLR_SALDO, VLR_SALDO, VLR_SALDO,
				   VLR_SALDO, TASA, CURRENT_TIMESTAMP AS CREATION_DATE, _usuario AS  CREATION_USER,  BASE,
				   MONEDA_BANCO, CURRENT_DATE AS FECHA_DOCUMENTO, CURRENT_DATE AS FECHA_VENCIMIENTO, 'FAP'::VARCHAR AS CLASE_DOCUMENTO_REL,
				   MONEDA,'FAP'::VARCHAR AS TIPO_DOCUMENTO_REL, DOCUMENTO AS DOCUMENTO_RELACIONADO,  DSTRCT,'4' AS CLASE_DOCUMENTO, 'FACT'::VARCHAR AS TIPO_REFERENCIA_1,
				   DOCUMENTO AS REFERENCIA_1,'NEG'::VARCHAR AS TIPO_REFERENCIA_2, DOCUMENTO_RELACIONADO::VARCHAR  AS referencia_2
	         FROM FIN.CXP_DOC 
	         WHERE DOCUMENTO_RELACIONADO = _negocio_nuevo.cod_neg 
	         AND TIPO_DOCUMENTO='FAP' 
	         AND DOCUMENTO LIKE 'RN%' 
	         AND REG_STATUS='' 
	         AND DOCUMENTO= _negocio_nuevo.documento
	         AND PROVEEDOR= _negocio_nuevo.cod_cli;
	         
	         --DATALLE NOTA CREDITO.         
	         INSERT INTO FIN.CXP_ITEMS_DOC(PROVEEDOR,  TIPO_DOCUMENTO,   DOCUMENTO,  ITEM,  DESCRIPCION,  
	         								VLR,   VLR_ME,  CODIGO_CUENTA,  CREATION_DATE, CREATION_USER,  BASE,  DSTRCT, REFERENCIA_1)
		      SELECT 
		               PROVEEDOR, 'NC'::VARCHAR AS TIPO_DOCUMENTO, DOCUMENTO||'-1','1','NOTA CREDITO CANCELANDO SALDO POR PAGAR AL CLIENTE DE RENOVACION '||DOCUMENTO::VARCHAR AS DESCRIPCION,
				       VLR_SALDO, VLR_SALDO, _cuentas_fiducia [3] AS CODIGO_CUENTA, CURRENT_TIMESTAMP AS CREATION_DATE, _usuario AS CREATION_USER, BASE, DSTRCT, DOCUMENTO_RELACIONADO AS  REFERENCIA_1
		         FROM FIN.CXP_DOC 
		         WHERE DOCUMENTO_RELACIONADO = _negocio_nuevo.cod_neg 
		         AND TIPO_DOCUMENTO='FAP' 
		         AND DOCUMENTO LIKE 'RN%' 
		         AND REG_STATUS='' 
		         AND DOCUMENTO= _negocio_nuevo.documento
		         AND PROVEEDOR= _negocio_nuevo.cod_cli;
		         
		      --ACTULIZAMOS LOS SALDOS DE LA CXP ORIGINAL
		      UPDATE fin.cxp_doc 
		      	SET vlr_saldo=0.00,
		      	    vlr_saldo_me=0.00,
		      	    vlr_total_abonos=vlr_total_abonos+vlr_saldo,
		      	    vlr_total_abonos_me=vlr_total_abonos_me+vlr_saldo
		      WHERE documento=_negocio_nuevo.documento 
		      AND tipo_documento='FAP' AND reg_status='' AND proveedor=_negocio_nuevo.cod_cli ;         
        
		END IF; 		  
     
	 END IF ;

    RETURN 'OK';
END;
$$
LANGUAGE plpgsql
VOLATILE;
ALTER FUNCTION apicredit.anular_ingresos_negocio_educativo_renovado(INTEGER, CHARACTER VARYING)
    OWNER TO postgres;
