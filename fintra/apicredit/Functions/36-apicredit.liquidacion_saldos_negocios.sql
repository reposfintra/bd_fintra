﻿-- Function: apicredit.liquidacion_saldos_negocios(_codneg text);

-- DROP FUNCTION apicredit.liquidacion_saldos_negocios(_codneg text);

CREATE OR REPLACE FUNCTION apicredit.liquidacion_saldos_negocios(_codneg text)
  returns SETOF record AS
$BODY$
declare

  _recordNegocios record;
  _detalleSaldoFacturas record;
  
begin
	
   if (eg_altura_mora_periodo(_codneg, 0, 8, 0) !='1- CORRIENTE')then 
     	    return ;   
   end if; 
 
   for _recordNegocios in 
						select 
						      fac.negasoc::varchar,
						      fac.documento::varchar,
						      fac.nit::varchar,
						      fac.num_doc_fen::varchar,
						      fac.valor_factura::numeric,
						      0.00::numeric as saldo_capital,
						      0.00::numeric as saldo_interes,
						      0.00::numeric as saldo_cuota_admin,
						      fac.valor_abono::numeric as valor_abono_cuota,
						      fac.valor_saldo::numeric as valor_saldo_cuota,
						      fac.fecha_vencimiento::date,
						      (now()::date-fac.fecha_vencimiento::date)::integer as dias_mora,
						      case when fac.valor_saldo=0.00 and  (now()::date-fac.fecha_vencimiento::date) > 0 then 'A-VENCIDO_SIN_SALDO'
						           when fac.valor_saldo >0.00 and (now()::date-fac.fecha_vencimiento::date) > 0 then 'B-VENCIDO_CON_SALDO'
						           when fac.valor_saldo >= 0.00 and (now()::date-fac.fecha_vencimiento::date) between -30 and 0 then 'C-CORRIENTE'
						           when fac.valor_saldo >= 0.00 and  (now()::date-fac.fecha_vencimiento::date) < -30 then 'D-FUTUROS'
						      end::varchar as estado_cartera
						from con.factura fac 
						inner join negocios neg on (neg.cod_neg=fac.negasoc and neg.cod_cli=fac.nit)
						where negasoc = _codneg 
						and fac.reg_status='' 
						and fac.tipo_documento='FAC'
						and neg.estado_neg='T'
						and substring(fac.documento,8,2) !='00'
						order by fac.fecha_vencimiento
	loop
				
				
				select into _detalleSaldoFacturas * from eg_detalle_saldo_facturas_fe_fc(_recordNegocios.negasoc::varchar, _recordNegocios.num_doc_fen::integer);
				
				_recordNegocios.saldo_capital:=_detalleSaldoFacturas.saldo_capital;
				_recordNegocios.saldo_interes:=_detalleSaldoFacturas.saldo_interes;
				_recordNegocios.saldo_cuota_admin:=_detalleSaldoFacturas.saldo_cuota_manejo;				
				
				
			   RETURN NEXT _recordNegocios;
	end loop;

    

END;
$BODY$
LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apicredit.liquidacion_saldos_negocios(_codneg text)
  OWNER TO postgres;
  
  