-- Function: apicredit.liquidacion_saldos_negocio_microcredito(_codneg text);

-- DROP FUNCTION apicredit.liquidacion_saldos_negocio_microcredito(_codneg text);

CREATE OR REPLACE FUNCTION apicredit.liquidacion_saldos_negocio_microcredito(_codneg TEXT)
    RETURNS SETOF RECORD AS
$BODY$
DECLARE
    _recordNegocios       RECORD;
    _detalleSaldoFacturas RECORD;
BEGIN
    IF (eg_altura_mora_periodo(_codneg, 0, 8, 0) != '1- CORRIENTE') THEN
        RETURN ;
    END IF;

    FOR _recordNegocios IN
        SELECT fac.negasoc::VARCHAR,
               fac.documento::VARCHAR,
               fac.nit::VARCHAR,
               fac.num_doc_fen::VARCHAR,
               fac.valor_factura::NUMERIC,
               0.00::NUMERIC AS saldo_capital,
               0.00::NUMERIC AS saldo_interes,
               0.00::NUMERIC AS saldo_cuota_admin,
               0.00::NUMERIC AS saldo_cat,
               0.00::NUMERIC AS saldo_capital_aval,
               0.00::NUMERIC AS saldo_interes_aval,
               fac.valor_abono::NUMERIC AS valor_abono_cuota,
               fac.valor_saldo::NUMERIC AS valor_saldo_cuota,
               fac.fecha_vencimiento::DATE,
               (now()::DATE - fac.fecha_vencimiento::DATE)::INTEGER AS dias_mora,
               CASE WHEN fac.valor_saldo = 0.00 AND (now()::DATE - fac.fecha_vencimiento::DATE) > 0                THEN 'A-VENCIDO_SIN_SALDO'
                    WHEN fac.valor_saldo > 0.00 AND (now()::DATE - fac.fecha_vencimiento::DATE) > 0                THEN 'B-VENCIDO_CON_SALDO'
                    WHEN fac.valor_saldo >= 0.00 AND (now()::DATE - fac.fecha_vencimiento::DATE) BETWEEN -30 AND 0 THEN 'C-CORRIENTE'
                    WHEN fac.valor_saldo >= 0.00 AND (now()::DATE - fac.fecha_vencimiento::DATE) < -30             THEN 'D-FUTUROS'
               END::VARCHAR AS estado_cartera
        FROM con.factura fac
                 INNER JOIN negocios neg ON (neg.cod_neg = fac.negasoc AND neg.cod_cli = fac.nit)
        WHERE negasoc = _codneg
          AND fac.reg_status = ''
          AND fac.tipo_documento = 'FAC'
          AND neg.estado_neg = 'T'
          AND substring(fac.documento, 8, 2) != '00'
        ORDER BY fac.fecha_vencimiento
        LOOP
            SELECT INTO _detalleSaldoFacturas * FROM eg_detalle_saldo_facturas_mc(_recordNegocios.negasoc::VARCHAR, _recordNegocios.num_doc_fen::INTEGER);

            _recordNegocios.saldo_capital := _detalleSaldoFacturas.saldo_capital;
            _recordNegocios.saldo_interes := _detalleSaldoFacturas.saldo_interes;
            _recordNegocios.saldo_cuota_admin := _detalleSaldoFacturas.saldo_cuota_manejo;
            _recordNegocios.saldo_cat := _detalleSaldoFacturas.saldo_cat;
            _recordNegocios.saldo_capital_aval := _detalleSaldoFacturas.saldo_capital_aval;
            _recordNegocios.saldo_interes_aval := _detalleSaldoFacturas.saldo_interes_aval;

            RETURN NEXT _recordNegocios;
        END LOOP;
END;
$BODY$
    LANGUAGE plpgsql
    VOLATILE;
ALTER FUNCTION apicredit.liquidacion_saldos_negocio_microcredito(TEXT)
    OWNER TO postgres;