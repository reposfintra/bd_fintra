-- Type: apicredit.rs_confirmar_renovacion

-- DROP TYPE apicredit.rs_confirmar_renovacion;

CREATE TYPE apicredit.rs_confirmar_renovacion AS
    (monto_solicitado NUMERIC,
    saldo_credito_anterior NUMERIC,
    total_nuevo_credito NUMERIC,
    num_cuota INTEGER,
    fecha_pago CHARACTER VARYING,
    nueva_cuota_aproximada NUMERIC,
    nuevo_valor_aval NUMERIC,
    monto_desembolso NUMERIC,
    monto_estudio_credito NUMERIC,
    monto_comision_desembolso NUMERIC);
ALTER TYPE apicredit.rs_confirmar_renovacion
    OWNER TO postgres;
