-- Table: tem.pago_masivo__

-- DROP TABLE tem.pago_masivo__;

CREATE TABLE tem.pago_masivo__
(
  negasoc character varying(20) NOT NULL,
  nitcli character varying(15) NOT NULL,
  unidad_negocio integer,
  tipo_pago character varying(10) NOT NULL,
  fecha_pago date,
  banco character varying(15) NOT NULL,
  sucursal character varying(30) NOT NULL,
  valor_aplicar_neto moneda,
  usuario character varying(30) NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem.pago_masivo__
  OWNER TO postgres;
COMMENT ON TABLE tem.pago_masivo__
  IS 'TABLA TEMPORAL PARA PAGOS MASIVOS';
  
 ALTER TABLE tem.pago_masivo__  ADD COLUMN procesado character varying(1) NOT NULL DEFAULT 'N';
 
 ALTER TABLE tem.pago_masivo__  ADD COLUMN fecha_aplicacion timestamp NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp;
 
 ALTER TABLE tem.pago_masivo__  ADD COLUMN creation_date timestamp NOT NULL DEFAULT now()::timestamp;
 
 ALTER TABLE tem.pago_masivo__  ADD COLUMN num_ingreso character varying(50) NOT NULL DEFAULT '';
  
 ALTER TABLE tem.pago_masivo__  ADD COLUMN id serial NOT NULL PRIMARY KEY;
 
 ALTER TABLE tem.pago_masivo__  ADD COLUMN lote_pago character varying(50) NOT NULL DEFAULT '';

 ALTER TABLE tem.pago_masivo__  ADD COLUMN comentario character varying(300) NOT NULL DEFAULT '';
 
  ALTER TABLE tem.pago_masivo__  ADD COLUMN orden integer;
 
ALTER TABLE tem.pago_masivo__  ADD COLUMN logica_aplicacion character VARYING(8) NOT NULL DEFAULT '';

ALTER TABLE tem.pago_masivo__  ADD COLUMN extracto character VARYING(12) NOT NULL DEFAULT '';

ALTER TABLE tem.pago_masivo__  ADD COLUMN negocio_cargue character VARYING(20) NOT NULL DEFAULT '';

ALTER TABLE tem.pago_masivo__  ADD COLUMN aplica_refinanciacion character VARYING (1)  NOT NULL DEFAULT 'N'::character VARYING;

ALTER TABLE tem.pago_masivo__  ADD COLUMN aplica_refinanciacion character VARYING (1)  NOT NULL DEFAULT 'N'::character VARYING;

ALTER TABLE tem.pago_masivo__  ADD COLUMN lote_cargue character VARYING(50) NOT NULL DEFAULT '';

ALTER TABLE tem.pago_masivo__  ADD COLUMN usuario_cierre_plan_al_dia character VARYING(30) NOT NULL DEFAULT '';

ALTER TABLE tem.pago_masivo__  ADD COLUMN fecha_cierre_plan_al_dia timestamp NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp;
 
 create  index  indx_lote_pago on tem.pago_masivo__ (procesado,lote_pago); 
  
  

ALTER TABLE tem.pago_masivo__ ALTER COLUMN logica_aplicacion TYPE varchar(20) USING logica_aplicacion::varchar;