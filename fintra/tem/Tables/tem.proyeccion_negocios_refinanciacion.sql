-- Table: tem.proyeccion_negocios_refinanciacion

-- DROP TABLE tem.proyeccion_negocios_refinanciacion;

CREATE TABLE tem.proyeccion_negocios_refinanciacion
(
  documento character varying(10),
  cod_neg character varying(15),
  item integer,
  fecha date,
  capital moneda,
  interes moneda,
  cat moneda,
  cuota_manejo moneda,
  valor_cuota_sin_aplicacion moneda,
  valor_cuota moneda,
  dias_vencidos integer,
  estado text,
  color character varying(1),
  aplicado character varying(1)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tem.proyeccion_negocios_refinanciacion
  OWNER TO postgres;