-- DROP TABLE tem.nomina_empleados

CREATE TABLE tem.nomina_empleados (
		id              SERIAL PRIMARY KEY,
		periodo         VARCHAR(6)   NOT NULL,
		centro_origen   VARCHAR(20),
		tercero_origen  VARCHAR(50),
		nombre          VARCHAR(200) NOT NULL,
		porcentaje      VARCHAR(100),
		centro_destino  VARCHAR(20),
		tercero_destino VARCHAR(100)
);

ALTER TABLE tem.nomina_empleados
		ADD COLUMN dstrct VARCHAR(6) DEFAULT '';

ALTER TABLE tem.nomina_empleados
		ADD COLUMN reg_status VARCHAR(1) DEFAULT '';

ALTER TABLE tem.nomina_empleados
		ADD COLUMN creation_user VARCHAR(15) DEFAULT '';

ALTER TABLE tem.nomina_empleados
		ADD COLUMN creation_date TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP;
