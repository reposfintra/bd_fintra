-- DROP TABLE tem.conceptos_nomina;

CREATE TABLE tem.conceptos_nomina (
		id     SERIAL PRIMARY KEY,
		cuenta VARCHAR(20),
		nombre VARCHAR(100)
);

ALTER TABLE tem.conceptos_nomina
		ADD COLUMN dstrct VARCHAR(6) DEFAULT '';

ALTER TABLE tem.conceptos_nomina
		ADD COLUMN reg_status VARCHAR(1) DEFAULT '';

ALTER TABLE tem.conceptos_nomina
		ADD COLUMN creation_user VARCHAR(15) DEFAULT '';

ALTER TABLE tem.conceptos_nomina
		ADD COLUMN creation_date TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP;
