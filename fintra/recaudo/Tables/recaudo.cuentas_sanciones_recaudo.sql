-- Table: recaudo.cuentas_sanciones_recaudo

-- DROP TABLE recaudo.cuentas_sanciones_recaudo;

CREATE TABLE recaudo.cuentas_sanciones_recaudo
(
  id serial, 
  reg_status character varying(1) NOT NULL  DEFAULT '',
  unidad_negocio integer, 
  descripcion character varying(50) NOT NULL  DEFAULT '',
  cuenta character varying(50) NOT NULL  DEFAULT '',  
  tipo_sancion character varying(5) NOT NULL  DEFAULT '',  
  referencia_1 character varying(5) NOT NULL  DEFAULT '',  
  referencia_2 character varying(5) NOT NULL  DEFAULT '',  
  referencia_3 character varying(5) NOT NULL  DEFAULT '',  
  referencia_4 character varying(5) NOT NULL  DEFAULT '',  
  creation_date timestamp NOT NULL DEFAULT now(),
  creation_user  character varying(30) NOT NULL  DEFAULT ''
)
WITH (
  OIDS=FALSE
);
ALTER TABLE recaudo.cuentas_sanciones_recaudo
  OWNER TO postgres;
COMMENT ON TABLE recaudo.cuentas_sanciones_recaudo
  IS 'TABLA PARA LA CONFIGURACION DE CUENTAS CONTABLES DE SANCIONES';
  
  
  INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 1, 'GASTO DE COBRANZA', 'I010130014170', 'GAC', 
            '', '', '', '', NOW(), 
            'EDGARGM87');
            
  INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 1, 'INTERES DE MORA', 'I010130014170', 'IXM', 
            '', '', '', '', NOW(), 
            'EDGARGM87');
            
  INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 2, 'GASTO DE COBRANZA', 'I010140014205', 'GAC', 
            '', '', '', '', NOW(), 
            'EDGARGM87'); 
            
   INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 2, 'INTERES DE MORA', 'I010140014170', 'IXM', 
            '', '', '', '', NOW(), 
            'EDGARGM87');
            
    INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 8, 'GASTO DE COBRANZA', 'I010140014205', 'GAC', 
            '', '', '', '', NOW(), 
            'EDGARGM87'); 
            
   INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 8, 'INTERES DE MORA', 'I010140014170', 'IXM', 
            '', '', '', '', NOW(), 
            'EDGARGM87');
            
 INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 22, 'GASTO DE COBRANZA', 'I010140014205', 'GAC', 
            '', '', '', '', NOW(), 
            'EDGARGM87'); 
            
 INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 22, 'INTERES DE MORA', 'I010140014170', 'IXM', 
            '', '', '', '', NOW(), 
            'EDGARGM87');
           
   INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 30, 'GASTO DE COBRANZA', 'I010140014205', 'GAC', 
            '', '', '', '', NOW(), 
            'EDGARGM87'); 
            
   INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 30, 'INTERES DE MORA', 'I010140014170', 'IXM', 
            '', '', '', '', NOW(), 
            'EDGARGM87');
            
    INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 31, 'GASTO DE COBRANZA', 'I010140014205', 'GAC', 
            '', '', '', '', NOW(), 
            'EDGARGM87'); 
            
   INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 31, 'INTERES DE MORA', 'I010140014170', 'IXM', 
            '', '', '', '', NOW(), 
            'EDGARGM87');
            
   INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 3, 'GASTO DE COBRANZA', 'I010140014205', 'GAC', 
            '', '', '', '', NOW(), 
            'EDGARGM87'); 
            
   INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 3, 'INTERES DE MORA', 'I010140014170', 'IXM', 
            '', '', '', '', NOW(), 
            'EDGARGM87');
            
      INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 9, 'GASTO DE COBRANZA', 'I010140014205', 'GAC', 
            '', '', '', '', NOW(), 
            'EDGARGM87'); 
            
   INSERT INTO recaudo.cuentas_sanciones_recaudo(
            reg_status, unidad_negocio, descripcion, cuenta, tipo_sancion, 
            referencia_1, referencia_2, referencia_3, referencia_4, creation_date, 
            creation_user)
    VALUES ('', 9, 'INTERES DE MORA', 'I010140014170', 'IXM', 
            '', '', '', '', NOW(), 
            'EDGARGM87');
            
   -------------------------------------------------------------------------- 

            
            
