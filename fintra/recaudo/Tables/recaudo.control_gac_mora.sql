-- Table: recaudo.control_gac_mora

-- DROP TABLE recaudo.control_gac_mora;

CREATE TABLE recaudo.control_gac_mora
(
  id serial,  
  negasoc character varying(20) NOT NULL  DEFAULT '',
  documento character varying(30) NOT NULL  DEFAULT '',
  unidad_negocio integer,
  fecha_pago date,
  dias_mora integer,
  banco character varying(15) NOT NULL  DEFAULT '',
  sucursal character varying(30) NOT NULL  DEFAULT '',
  valor_aplicado moneda  DEFAULT 0.00,
  tipo_pago character varying(5) NOT NULL DEFAULT '',
  creation_date timestamp NOT NULL DEFAULT now()
)
WITH (
  OIDS=FALSE
);
ALTER TABLE recaudo.control_gac_mora
  OWNER TO postgres;
COMMENT ON TABLE recaudo.control_gac_mora
  IS 'TABLA PARA CONTROLAR GASTOS DE COBRANZA Y MORA.';
  
  
 ALTER TABLE recaudo.control_gac_mora  ADD COLUMN lote_pago character varying(50) NOT NULL DEFAULT '';

ALTER TABLE recaudo.control_gac_mora  ADD COLUMN logica_aplicacion character varying(50) NOT NULL DEFAULT '';
 
 ALTER TABLE recaudo.control_gac_mora  ADD COLUMN tipo_abono character varying(1) NOT NULL DEFAULT '';

 ALTER TABLE recaudo.control_gac_mora  ADD COLUMN valor_dto moneda  DEFAULT 0.00;
 
 