-- Function: recaudo.motor_clasificacion(character varying, character varying)

-- DROP FUNCTION recaudo.motor_clasificacion(character varying, character varying);

CREATE OR REPLACE FUNCTION recaudo.motor_clasificacion(_lote_pago character varying, _useraplicacion character varying)
  RETURNS text AS
$BODY$

DECLARE

/**
 *@descripcion:  
 *
 *@autor:JZAPATA
 *@Fecha:2019-05-21
 */

_retorno text:='ERROR';
extracto_ record;
_clasificacion varchar:='CEDULA';
_tem_pago_masivo record;
_cartera record;
_planDia varchar:='';

BEGIN
	
	/**
	 *RECORREMOS EL LOTE DE PAGO A PROCESAR
	 */
	for _tem_pago_masivo in select *, eg_saldo_cartera_negocio(negasoc) as valor_saldo from tem.pago_masivo__ where lote_pago= _lote_pago and procesado='N'

	loop
	
		_clasificacion:='CEDULA';
	
		--1) VALIDAR EL TIPO DE CLASIFICACION
		select 
	         INTO extracto_ * 
	    from 
	            recibo_oficial_pago 
	    where 
	           id = _tem_pago_masivo.extracto and 
	           id_unidad_negocio=_tem_pago_masivo.unidad_negocio and 
	           cedula=_tem_pago_masivo.nitcli and 
	           negocio=_tem_pago_masivo.negasoc and 
	           _tem_pago_masivo.valor_aplicar_neto=total;--SE REALIZA CAMBIO, 2019-05-24, SE CAMBIA DE MAYOR O IGUAL, A IGUAL 
	          
	          
	    if(extracto_.periodo_rop=to_char(_tem_pago_masivo.fecha_pago,'YYYYMM'))THEN
	    	_clasificacion:='EXTRACTO';
	    END IF;
	    
	   --2) VALIDAR EL SALDO DE LA REFERENCIA
	
		if(_tem_pago_masivo.valor_saldo<=0) then
		
			_clasificacion:='CEDULA';
			
			--2.1) BUSCAMOS EL NEGOCIO MAS VENCIDO SEGUN LA IDENTIFICACION
			select into _cartera
				negasoc, 
				sum(valor_saldo) as saldo,
				max(current_date-fecha_vencimiento) as max_dias_mora
			from 
				con.factura 
			where 
				reg_status='' and 
				tipo_documento='FAC' and 
				negasoc!='' and 
				valor_saldo>0 and 
				nit=_tem_pago_masivo.nitcli 
			group by 
				negasoc
			order by 
				max_dias_mora desc
			limit 1;
		
			if(_cartera is not null)then
				
				update 
					tem.pago_masivo__ 
				set 
					negocio_cargue=negasoc, 
					negasoc=_cartera.negasoc 
				where 
					id=_tem_pago_masivo.id and 
					lote_pago=_lote_pago;
				
				_clasificacion:='CEDULA';
				
			end if;
		
		end if;
	
	    --3.) Validamos si el negocio es refinanciado o reestructurado
	    perform * from administrativo.control_refinanciacion_negocios where cod_neg=_tem_pago_masivo.negasoc and estado='P';
	    if found then 
	    	_clasificacion:='PLAN_AL_DIA';
	    end if;
	
		update 
			tem.pago_masivo__ 
		set 
			logica_aplicacion=_clasificacion
		where 
			id=_tem_pago_masivo.id and 
			lote_pago=_lote_pago;
		
	end loop;
	
	--Calsifica los planaes al dia.
	_planDia:=recaudo.agrupa_pagos_pladia(_userAplicacion);

return 'OK';
  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION recaudo.motor_clasificacion(character varying, character varying)
  OWNER TO postgres;
