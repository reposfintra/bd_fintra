-- Function: recaudo.pagos_masivos_manuales_v3(character varying, _lote_pago character varying, _logica_aplicacion character varying)

-- DROP FUNCTION recaudo.pagos_masivos_manuales_v3(character varying, _lote_pago character varying, _logica_aplicacion character varying);

CREATE OR REPLACE FUNCTION recaudo.pagos_masivos_manuales_v3(_usuario character varying, _lote_pago character varying, _logica_aplicacion character varying)
 RETURNS text AS
$BODY$

DECLARE

_pagos_manuales record;
_facturas record;
_intereses record;
_valor_interes_x_mora NUMERIC := 0;
_valor_gasto_cobranza NUMERIC :=0;
_valor_saldo_factura NUMERIC :=0;
_saldo_aplicar_ic NUMERIC :=0;
_valor_abono_fact NUMERIC :=0;
_cont int :=0;
_num_ingreso varchar ;
_sw boolean;
_diff NUMERIC :=0;
_dto_sanciones record ;
_cta_gac VARCHAR :='';
_cta_ixm VARCHAR :='';
_cta_ajuste varchar :='';
_tipo_abono varchar :='';
_dto_gac NUMERIC :=0;
_dto_ixm NUMERIC :=0;
_dias_vencidos INTEGER:=0;


BEGIN
	
   <<labelfact1>>
	FOR _pagos_manuales IN       
				SELECT
				    id,
				    c.codcli,
				    negasoc,
				    nitcli,
				    unidad_negocio,
				    tipo_pago,
				    fecha_pago,
				    banco,
				    sucursal,
				    valor_aplicar_neto,
				    usuario,
				    procesado,
				    lote_pago AS lt_pago,
				    logica_aplicacion as log_apl
				FROM  tem.pago_masivo__  pm 
					INNER JOIN cliente c ON c.nit=pm.nitcli  
				WHERE procesado='N' AND lote_pago =_lote_pago and logica_aplicacion=_logica_aplicacion --AND negasoc='MC14340'
				ORDER BY pm.orden
	LOOP

		IF _pagos_manuales.fecha_pago::date > current_date  THEN
			--MARCAMOS EL CONTROL COMO PAGO ERRADO 
		    UPDATE tem.pago_masivo__ SET procesado='E', num_ingreso='',comentario='FECHA DE PAGO MAYOR A HOY' WHERE negasoc=_pagos_manuales.negasoc AND  id=_pagos_manuales.id;
			CONTINUE;
		END IF;
	
		--0.) Raalizar Backup antes de aplicar el pago.
		INSERT INTO con.facturas_bk(reg_status,dstrct,tipo_documento,documento,nit,
					 codcli,concepto,fecha_factura,fecha_vencimiento,fecha_ultimo_pago,descripcion, 
					valor_factura,valor_abono,valor_saldo,cmc,periodo,negasoc,num_doc_fen, lote_pago, logica_aplicacion)
				SELECT reg_status,dstrct,tipo_documento,documento,nit,
					codcli,concepto,fecha_factura,fecha_vencimiento,fecha_ultimo_pago,descripcion, 
					valor_factura,valor_abono,valor_saldo,cmc,periodo,negasoc,num_doc_fen,_pagos_manuales.lt_pago, _pagos_manuales.log_apl
				FROM con.factura 
				WHERE negasoc= _pagos_manuales.negasoc;   	
       
		--0.1) SETEAMOS NUEVAMENTE LOS VALORES DE INTERESES X MORA, GASTOS DE COBRANZA
		_num_ingreso := get_lcod ('INGC');
	    _valor_interes_x_mora := 0;
	    _valor_gasto_cobranza := 0;
		_saldo_aplicar_ic:=_pagos_manuales.valor_aplicar_neto;
		_cont :=1;   
		_sw:=false;		
	   		
		RAISE NOTICE '==============================================';
		RAISE NOTICE '0.) Numero Ingreso: % Unidad negocio : %',_num_ingreso, _pagos_manuales.unidad_negocio;
		RAISE NOTICE '1.) Saldo Inicial Pago: %',_saldo_aplicar_ic;
		
		--1.1) LOOP PARA ITERAR LAS CUOTAS DE LA CARTERA.
		<<labelcartera>>
	        FOR _facturas IN       
	            SELECT
	                fra.tipo_documento,
	                fra.documento,
	                fra.nit,
	                fra.codcli,
	                cmc.cuenta,
	                fra.fecha_factura,
	                fra.fecha_vencimiento,
	                (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento) AS dias_vencidos,
	                CASE WHEN (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento) BETWEEN 11 AND 30 THEN 0.05
	                WHEN (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento) BETWEEN 31 AND 60 THEN 0.1
	                WHEN (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento) >60 THEN 0.2
	                ELSE 0
	                END AS porc_mora,
	                fra.descripcion,
	                fra.valor_factura,
	                fra.valor_abono,
	                fra.valor_saldo,
	                fra.periodo,
	                fra.negasoc,
	                CASE  WHEN (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento::date) > 0 THEN 'A-VENCIDO_CON_SALDO'
					      WHEN (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento::date) BETWEEN -30 AND 0 THEN 'B-CORRIENTE'
					      WHEN (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento::date) < -30 THEN 'C-FUTUROS'
					END::varchar AS estado_cartera
	            FROM
	              con.factura fra
	            INNER JOIN con.cmc_doc cmc on(cmc.cmc=fra.cmc and cmc.tipodoc=fra.tipo_documento)
	            LEFT JOIN conceptos_facturacion cf ON cf.prefijo=substring(fra.documento,1,2) 
	            WHERE
	                fra.dstrct = 'FINV'
	                AND fra.valor_saldo > 0
	                AND fra.reg_status = ''
	                AND fra.negasoc = _pagos_manuales.negasoc
	                AND fra.tipo_documento in ('FAC','NDC')
	                AND substring(fra.documento,1,2) NOT IN ('CP','FF','DF')
	            ORDER BY fecha_vencimiento, cf.prioridad_pago asc, documento DESC	      
	        LOOP  
	           
	        RAISE NOTICE 'Factura de pago: %',_facturas;
			--Inicializamos el valor saldo de la cuota a pagar.	
	        _valor_saldo_factura := _facturas.valor_saldo; 
	        _dto_ixm:=0.00;
	        _dto_gac:=0.00;	
	       
	       -- IF  _facturas.estado_cartera IN ('A-VENCIDO_CON_SALDO','B-CORRIENTE') THEN 
	       
				IF  _facturas.dias_vencidos >0 AND _pagos_manuales.unidad_negocio NOT IN (22) THEN  --IF FACTURAS VENCIDAS 
		          	   
					--1.) CALCULO DEL GASTO DE COBRANZA
					SELECT INTO _dto_sanciones fecha_pago AS ultimo_pago,valor_aplicado, tipo_abono 
					FROM recaudo.control_gac_mora  
					WHERE documento=_facturas.documento AND negasoc=_facturas.negasoc AND tipo_pago= 'GAC'
					AND fecha_pago = (SELECT max(fecha_pago) FROM recaudo.control_gac_mora f WHERE f.documento=_facturas.documento AND f.negasoc=_facturas.negasoc AND f.tipo_pago= 'GAC' );	
				
					_dias_vencidos:=_facturas.dias_vencidos;
					IF _dto_sanciones IS NOT NULL THEN
					    
						RAISE NOTICE '_dto_sanciones : % ', _dto_sanciones;				
						IF _dto_sanciones.tipo_abono ='P' THEN 				    
					        _dto_gac := _dto_sanciones.valor_aplicado;
					    ELSE 
					    	_dias_vencidos := _pagos_manuales.fecha_pago-_dto_sanciones.ultimo_pago;
					    END IF;				
											
						IF _dias_vencidos  BETWEEN 11 AND 30 THEN   _facturas.porc_mora := 0.05;
						ELSIF _dias_vencidos  BETWEEN 31 AND 60 THEN 	_facturas.porc_mora := 0.1;
						ELSIF _dias_vencidos >60 THEN _facturas.porc_mora := 0.2;
						ELSE _facturas.porc_mora := 0.0;
						END IF;		
						
						RAISE NOTICE '_facturas.porc_mora : % ', _facturas.porc_mora;					
						
					END IF; 
	
					--1.1) CALCULAMOS LOS GASTOS DE COBRANZA SI ESTA VENCIDO DE LA FACTURA	
					_tipo_abono :='T';		       
					_valor_gasto_cobranza := round(_facturas.valor_saldo * _facturas.porc_mora)-_dto_gac;
					RAISE NOTICE '_valor_gasto_cobranza: %',_valor_gasto_cobranza;
					IF (_saldo_aplicar_ic-_valor_gasto_cobranza) < 0 THEN 				
						_valor_gasto_cobranza :=_saldo_aplicar_ic;
						_saldo_aplicar_ic:=0.00;	
					    _tipo_abono :='P';
					ELSE 
						_saldo_aplicar_ic:=_saldo_aplicar_ic-_valor_gasto_cobranza;      
					END IF; 
	
					--1.2) VALIDAMOS EL VALOR DE GASTO DE COBRANZA
					IF _valor_gasto_cobranza>0 THEN
	
					 
						_cta_gac:=COALESCE((SELECT cuenta FROM recaudo.cuentas_sanciones_recaudo 
								WHERE tipo_sancion='GAC' AND unidad_negocio=_pagos_manuales.unidad_negocio),'SIN CUENTA');		            				
	
						RAISE NOTICE 'Factura : % _valor_gasto_cobranza : %',_FACTURAS.DOCUMENTO,_valor_gasto_cobranza;
						
						INSERT INTO con.ingreso_detalle(dstrct,tipo_documento,num_ingreso,item,nitcli,valor_ingreso,valor_ingreso_me,factura,tipo_doc,documento,
										creation_user,creation_date,base,cuenta,descripcion,
										valor_tasa,saldo_factura,tipo_referencia_1,referencia_1)
									VALUES  ('FINV','ING',_num_ingreso,_cont,_facturas.nit,_valor_gasto_cobranza,_valor_gasto_cobranza,_facturas.documento,
										_facturas.tipo_documento,_facturas.documento,_usuario,now(),'COL',_cta_gac,'AJUSTE SALDO AL INGRESO GASTO COBRANZA NRO. '||_num_ingreso,
										 1,_facturas.valor_saldo,'NEG',_facturas.negasoc);
	
						--INSERT POR LA DIFERENCIA DEL G.A.C A DESCONTAR EN EL PROXIMO PAGO				
						INSERT INTO recaudo.control_gac_mora(negasoc, documento, unidad_negocio, fecha_pago, dias_mora, 
										    banco, sucursal, valor_aplicado, tipo_pago,lote_pago,tipo_abono,valor_dto,logica_aplicacion)
									VALUES (_facturas.negasoc, _facturas.documento, _pagos_manuales.unidad_negocio, _pagos_manuales.fecha_pago, _dias_vencidos, 
										_pagos_manuales.banco, _pagos_manuales.sucursal, _valor_gasto_cobranza, 'GAC',_lote_pago,_tipo_abono,_dto_gac,_logica_aplicacion);
										
						_cont=_cont+1;				    
						_valor_gasto_cobranza:=0;
					    
					END IF;
	
					RAISE notice '1.) Saldo despues de G.C.A : %',_saldo_aplicar_ic;
	
					-- 2.) CALCULO DEL INTERES DE MORA.
	
					--2.1 VALIDAMOS QUE TODAVIA HAY PLATA PARA APLICAR EL PAGO DE INTERES DE MORA
					IF _saldo_aplicar_ic >0 THEN    
	
						_dto_sanciones := NULL;
	
						--2.2) OBTENEMOS EL % DE INTERES A COBRAR POR MORA
						SELECT INTO _intereses 
							c.tasa_usura,
							c.id_convenio
						FROM negocios AS n
						INNER JOIN convenios AS c ON (c.id_convenio=n.id_convenio)
						where n.cod_neg =_facturas.negasoc;
						
						IF _intereses IS NOT NULL THEN --Inicio record null
						
							SELECT INTO _dto_sanciones fecha_pago AS ultimo_pago,valor_aplicado,tipo_abono 
							FROM recaudo.control_gac_mora  
							WHERE documento=_facturas.documento AND negasoc=_facturas.negasoc AND tipo_pago='IXM'
							AND fecha_pago = (SELECT max(fecha_pago) FROM recaudo.control_gac_mora f WHERE f.documento=_facturas.documento AND f.negasoc=_facturas.negasoc AND f.tipo_pago= 'IXM');	
							
							_dias_vencidos:=_facturas.dias_vencidos;
							IF _dto_sanciones IS NOT NULL THEN			    
																		
							    IF  _dto_sanciones.tipo_abono ='P' THEN 				    
							    	_dto_ixm := _dto_sanciones.valor_aplicado;	
							    ELSE 
							    	_dias_vencidos := _pagos_manuales.fecha_pago-_dto_sanciones.ultimo_pago;	
							    END IF;
							   
							END IF; 
									    
							--2.3) CALCULAMOS EL VALOR DEL INTERES POR MORA DE LA FACTURA
							 _tipo_abono :='T';		
							_valor_interes_x_mora := round((((_facturas.valor_saldo * _intereses.tasa_usura)/100 ) / 30) * _dias_vencidos)-_dto_ixm;
							RAISE NOTICE 'ixm : % _dto_sanciones: % (_saldo_aplicar_ic-_valor_interes_x_mora)) : %',(round((((_facturas.valor_saldo * _intereses.tasa_usura)/100 ) / 30)*_dias_vencidos)),_dto_sanciones, (_saldo_aplicar_ic-_valor_interes_x_mora);
							IF (_saldo_aplicar_ic-_valor_interes_x_mora) < 0 THEN 
	
								_valor_interes_x_mora :=_saldo_aplicar_ic; 
								_saldo_aplicar_ic:=0.00;
							    _tipo_abono :='P';		
	
							ELSE 
								_saldo_aplicar_ic:=_saldo_aplicar_ic-_valor_interes_x_mora;      
							END IF; 
	
							--2.4) INSERTAMOS LOS INTERESES POR MORA
							IF _valor_interes_x_mora>0 THEN 
	
								_cta_ixm:=COALESCE((SELECT cuenta FROM recaudo.cuentas_sanciones_recaudo 
											WHERE tipo_sancion='IXM' AND unidad_negocio=_pagos_manuales.unidad_negocio),'SIN CUENTA');
	
								RAISE NOTICE 'Factura : % _valor_interes_x_mora : %',_FACTURAS.DOCUMENTO,_valor_interes_x_mora;
	
								INSERT INTO CON.INGRESO_DETALLE(DSTRCT,TIPO_DOCUMENTO,NUM_INGRESO,ITEM,NITCLI,VALOR_INGRESO,VALOR_INGRESO_ME,FACTURA,
												TIPO_DOC,DOCUMENTO,CREATION_USER,CREATION_DATE,BASE,CUENTA,DESCRIPCION,
												VALOR_TASA,SALDO_FACTURA,TIPO_REFERENCIA_1,REFERENCIA_1)
											VALUES ('FINV','ING',_NUM_INGRESO,_CONT,_FACTURAS.NIT,_VALOR_INTERES_X_MORA,_VALOR_INTERES_X_MORA,_FACTURAS.DOCUMENTO,
												_FACTURAS.TIPO_DOCUMENTO,_FACTURAS.DOCUMENTO,_USUARIO,NOW(),'COL',_cta_ixm,'AJUSTE SALDO AL INGRESO INTERES X MORA NRO. '||_NUM_INGRESO,
												1,_FACTURAS.VALOR_SALDO,'NEG',_FACTURAS.NEGASOC);
	
																	
								--INSERT POR LA DIFERENCIA DEL INTERES A DESCONTAR EN EL PROXIMO PAGO
								INSERT INTO recaudo.control_gac_mora(negasoc, documento, unidad_negocio, fecha_pago, dias_mora, 
															 banco, sucursal, valor_aplicado, tipo_pago,lote_pago,tipo_abono,valor_dto,logica_aplicacion)
									VALUES (_facturas.negasoc, _facturas.documento, _pagos_manuales.unidad_negocio, _pagos_manuales.fecha_pago, _dias_vencidos, 
										_pagos_manuales.banco, _pagos_manuales.sucursal, _valor_interes_x_mora, 'IXM',_lote_pago,_tipo_abono,_dto_ixm,_logica_aplicacion);
	
								_cont=_cont+1;
								_valor_interes_x_mora:=0;
							END IF;
						END IF; --fin record null
					 END IF;
	
					RAISE notice '2.) Saldo despues de IxM : %',_saldo_aplicar_ic;
				
				END IF; --fin facturas vencidas 
	
				
				--3.0) VALIDAMOS SI HAY SALDO PARA APLICAR A LA CARTERA.	 
				IF _saldo_aplicar_ic >0 THEN
	
					--3.1) VALIDAMOS EL VALOR DE LA APLICACION DE LA CARTERA.
					IF _saldo_aplicar_ic >= _valor_saldo_factura THEN 
							    
						_saldo_aplicar_ic := _saldo_aplicar_ic-_valor_saldo_factura;
									
					ELSIF  _saldo_aplicar_ic < _valor_saldo_factura THEN 
						_valor_saldo_factura := _saldo_aplicar_ic;
						_saldo_aplicar_ic :=0.00;			          	    		
					END IF;
					
					RAISE notice '4.0) Pago Cartera  : %',_valor_saldo_factura;
					RAISE notice '4.1) Saldo despues de aplicar cartera  : %',_saldo_aplicar_ic;
	
					--3.2) APLICAMOS EL PAGO A LA CARTERA DE LA CUOTA ITERADA     
					IF _valor_saldo_factura>0 THEN 							       	
									
						INSERT INTO con.ingreso_detalle(dstrct,tipo_documento,num_ingreso,item,nitcli,valor_ingreso,valor_ingreso_me,factura,
										tipo_doc,documento,creation_user,creation_date,base,cuenta,descripcion,
										valor_tasa,saldo_factura,tipo_referencia_1,referencia_1)
									VALUES('FINV','ING',_num_ingreso,_cont,_facturas.nit,_valor_saldo_factura,_valor_saldo_factura,_facturas.documento,
										_facturas.tipo_documento,_facturas.documento,_usuario,now(),'COL',_facturas.cuenta,_facturas.descripcion,
										1,(_facturas.valor_saldo-_valor_saldo_factura),'NEG',_facturas.negasoc);
								    
								    
						UPDATE con.factura 
							SET valor_abono=valor_abono+_valor_saldo_factura,
								valor_abonome=valor_abonome+_valor_saldo_factura ,
								valor_saldo=valor_saldo-_valor_saldo_factura, 
								valor_saldome=valor_saldome-_valor_saldo_factura,
								fecha_ultimo_pago=current_date, 
								last_update=now() 
						WHERE documento=_facturas.documento
						and tipo_documento='FAC'
						and negasoc=_facturas.negasoc
						and reg_status='' ;						                
									
						_cont:= _cont+1;							       
					END IF ;
				END IF; 
				
				IF _saldo_aplicar_ic=0.00  THEN 
					EXIT labelcartera;
				END IF;
			
			--ELSIF _facturas.estado_cartera IN ('C-FUTUROS') THEN
			
				 --BUSCAMOS EL SALDO DE LA FACTURA POR CONCEPTOS SEGUN LOGICA DE APLICACION.
			--	 RAISE NOTICE 'Pagos a cuotas futuras segun logica de aplicacion. En construccion. :)';
				
				 --select into _detalleSaldoFacturas * from eg_detalle_saldo_facturas_fe_fc(_recordNegocios.negasoc::varchar, _recordNegocios.num_doc_fen::integer);
			
		--	END IF; --Fin facturas vencidas y corriente.
	
	     END LOOP labelcartera;    
			 
		 --4.0) AJUSTE DE SALDO
		 IF _saldo_aplicar_ic > 0 THEN 
		 
			IF _pagos_manuales.unidad_negocio=1 AND _saldo_aplicar_ic <= 20000 THEN
				_cta_ajuste:='I010130014225';
			ELSIF _pagos_manuales.unidad_negocio=21 AND _saldo_aplicar_ic <= 20000 THEN
				_cta_ajuste:='I010300014225';
			ELSIF  _saldo_aplicar_ic <= 20000 THEN
				_cta_ajuste:='I010140014225';
			ELSE		
				_cta_ajuste :='23809504';
			END IF; 			
			
			INSERT INTO con.ingreso_detalle (dstrct,tipo_documento,num_ingreso,item,nitcli,valor_ingreso,valor_ingreso_me,factura,tipo_doc,documento,
							creation_user,creation_date,base,cuenta,descripcion,valor_tasa,saldo_factura,tipo_referencia_1,referencia_1)				    
						VALUES('FINV','ING',_num_ingreso,_cont,_facturas.nit,_saldo_aplicar_ic,_saldo_aplicar_ic,'','','',
							_usuario,now(),'COL',_cta_ajuste,'AJUSTE SALDO',1,0.00,'NEG',_pagos_manuales.negasoc);		
		 END IF; 
			 
			 
		--5.0) INSERT A CABECERA
		INSERT INTO con.ingreso(dstrct, tipo_documento, num_ingreso, codcli, nitcli, concepto, tipo_ingreso, fecha_consignacion, fecha_ingreso, branch_code, bank_account_no,
					codmoneda, agencia_ingreso, descripcion_ingreso, vlr_ingreso, vlr_ingreso_me, vlr_tasa, fecha_tasa, cant_item,
					creation_user, creation_date, user_update, last_update, base,tipo_referencia_2,referencia_2,periodo,
					tipo_referencia_1,referencia_1)
				VALUES('FINV', 'ING', _num_ingreso, _pagos_manuales.codcli, _pagos_manuales.nitcli, 'FE', 'C', _pagos_manuales.fecha_pago, now()::date, 
				case when _pagos_manuales.banco='CORRESPONSALES' then 'CORRESPONSALES ' else _pagos_manuales.banco end, _pagos_manuales.sucursal,
				'PES', 'OP', 'RECAUDO '||_pagos_manuales.banco, _pagos_manuales.valor_aplicar_neto,  _pagos_manuales.valor_aplicar_neto,1, NOW()::date,(_cont-1),_usuario,now(), _usuario,now(), 'COL','TIPAG',_pagos_manuales.tipo_pago,'000000',
				'NEG',_pagos_manuales.negasoc);
					
			
		--5.1) Actualizamos como aplizado
		UPDATE tem.pago_masivo__ SET procesado='S', fecha_aplicacion=now()::timestamp, num_ingreso=_num_ingreso WHERE negasoc=_pagos_manuales.negasoc AND id=_pagos_manuales.id;

		--5.2) VALIDAR QUE EL INGREOS ESTE CUADRADO CABECERA CON DETALLE SI CUADRA ACTUALIZAS LA TABLA PAGOS MANUALES SI NO MARCAS EL REGISTRO COMO ERROR.
		_diff := COALESCE((SELECT SUM(valor_ingreso)-ing.vlr_ingreso AS valida 
							FROM con.ingreso_detalle idet
							INNER JOIN con.ingreso ing ON (ing.num_ingreso=idet.num_ingreso AND ing.nitcli=idet.nitcli )
							WHERE ing.num_ingreso=_num_ingreso
							GROUP BY  ing.vlr_ingreso),0) ;
							
		IF _diff !=0 THEN 
		   
			DELETE FROM con.ingreso WHERE num_ingreso=_num_ingreso;
			DELETE FROM con.ingreso_detalle WHERE num_ingreso=_num_ingreso;
			
			--restablece el saldo de la fatura segun la tabla de backup.
			UPDATE con.factura 
			SET valor_abono=t.valor_abono,
				valor_abonome=t.valor_abono ,
				valor_saldo=t.valor_saldo,
				valor_saldome=t.valor_saldo,
				fecha_ultimo_pago='0099-01-01 00:00:00'::timestamp, 
				last_update=now() 
			FROM (SELECT bk.documento,
					 bk.tipo_documento,
					 bk.negasoc,
					 bk.nit,
					 bk.num_doc_fen,
					 bk.valor_abono,
					 bk.valor_saldo 
				FROM con.facturas_bk bk 
				WHERE bk.negasoc=_pagos_manuales.negasoc AND bk.reg_status='' AND lote_pago=_lote_pago and bk.logica_aplicacion=_logica_aplicacion) t
			WHERE con.factura.negasoc=t.negasoc 
			AND con.factura.tipo_documento=t.tipo_documento 
			AND con.factura.num_doc_fen=t.num_doc_fen
			AND con.factura.tipo_documento='FAC'
			AND con.factura.negasoc= _pagos_manuales.negasoc   
			AND con.factura.reg_status='' ;
			
			
		    --MARCAMOS EL BACKUP COMO ANULADO.
		   UPDATE con.facturas_bk SET reg_status='A' WHERE negasoc=_pagos_manuales.negasoc AND reg_status='' AND lote_pago=_lote_pago and logica_aplicacion=_logica_aplicacion;
		       
		   --MARCAMOS EL CONTROL COMO PAGO ERRADO 
		   UPDATE tem.pago_masivo__ SET procesado='E', num_ingreso='',comentario='VALOR CABECERA DIFERENTE A VALOR DETALLADO.' WHERE negasoc=_pagos_manuales.negasoc AND  id=_pagos_manuales.id;
		 
		END IF;		         
		      
	END LOOP labelfact1;
	
            
return 'OK';
  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION recaudo.pagos_masivos_manuales_v3(character varying, character varying, character varying)
  OWNER TO postgres;