
--DROP FUNCTION  recaudo.agrupa_pagos_pladia(_userAplicacion CHARACTER VARYING);
--DROP FUNCTION recaudo.agrupa_pagos_pladia();
CREATE OR REPLACE FUNCTION recaudo.agrupa_pagos_pladia(_userAplicacion CHARACTER VARYING)
  RETURNS text AS
$BODY$

DECLARE
  _recordpagos record;
  _det_pagos record;
  _iterador INTEGER:=1;
  _lotepago varchar;
  _orden integer:=1;
BEGIN
		--consolidas todos los pagos d eplan al dia 
		_lotepago:=get_lcod('LOTEDIA');
		FOR _recordpagos IN 
							SELECT  
							    negasoc,
							    nitcli,	
							    sum(pm.valor_aplicar_neto) AS valor_aplicar_neto,
							    ctrl.valor_a_pagar AS valor_negociacion,
							    (sum(pm.valor_aplicar_neto)::NUMERIC-ctrl.valor_a_pagar) AS diff_pago,	   
							    logica_aplicacion as log_apl,
							    ctrl.id_rop,
							    ctrl.key_ref,
							    PM.procesado,
							    count(*) AS num_pago
							FROM  tem.pago_masivo__  pm 
								INNER JOIN cliente c ON c.nit=pm.nitcli 
								INNER JOIN administrativo.control_refinanciacion_negocios ctrl ON ctrl.cod_neg=pm.negasoc AND ctrl.estado='P'
							WHERE  logica_aplicacion='PLAN_AL_DIA'  AND pm.procesado='N' --AND  negasoc='MC14654'  
							GROUP BY 
							negasoc,
							nitcli,	
							log_apl,
							ctrl.id_rop,
							ctrl.key_ref,
							PM.procesado,
							ctrl.valor_a_pagar
							ORDER BY negasoc
		LOOP
			--si el pago es menor salta al siguiente negocio.
		  	IF _recordpagos.diff_pago <-1001 THEN 
		  		CONTINUE;		  	
		  	END IF; 
		  	_iterador:=1;
			FOR _det_pagos IN SELECT
								    pm.id,
								    c.codcli,
								    negasoc,
								    nitcli,
								    unidad_negocio,
								    tipo_pago,
								    fecha_pago,
								    banco,
								    sucursal,
								    valor_aplicar_neto::numeric,
								    ctrl.valor_a_pagar AS valor_negociacion,
								    (valor_aplicar_neto::NUMERIC-ctrl.valor_a_pagar) AS diff_pago,
								    usuario,
								    procesado,
								    lote_pago AS lt_pago,		   
								    logica_aplicacion as log_apl,
								    ctrl.id_rop,
								    ctrl.key_ref
								FROM  tem.pago_masivo__  pm 
									INNER JOIN cliente c ON c.nit=pm.nitcli 
									INNER JOIN administrativo.control_refinanciacion_negocios ctrl ON ctrl.cod_neg=pm.negasoc AND ctrl.estado='P'
								WHERE procesado='N' AND  logica_aplicacion='PLAN_AL_DIA' AND pm.negasoc=_recordpagos.negasoc AND  ctrl.key_ref=_recordpagos.key_ref
								ORDER BY pm.id,pm.orden
			 LOOP
			 		
			 		UPDATE tem.pago_masivo__ set lote_cargue=lote_pago, lote_pago= _lotepago , orden=_orden, usuario=_userAplicacion
			 		WHERE logica_aplicacion='PLAN_AL_DIA'  AND procesado='N' AND negasoc=_det_pagos.negasoc AND id=_det_pagos.id;
			 		
			 		IF _recordpagos.num_pago=_iterador THEN 
				 		 UPDATE tem.pago_masivo__ SET aplica_refinanciacion='S'
				 			WHERE logica_aplicacion='PLAN_AL_DIA'  AND procesado='N' AND negasoc=_det_pagos.negasoc  AND id=_det_pagos.id;
			 		END IF;			 		
			 		_iterador:=_iterador+1;
			 		_orden:=_orden+1;
			 END LOOP;
			 
			
		END LOOP;
		
		RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION recaudo.agrupa_pagos_pladia(_userAplicacion CHARACTER VARYING)
  OWNER TO postgres;
  
--se ejecuta cuando se aplica pagos y cuando entren  
SELECT recaudo.agrupa_pagos_pladia('AMAJARRES');

SELECT * FROM con.ingreso WHERE num_ingreso IN ('IC361649','IA540331');

MC17025

--0.)PASO 0 
SELECT * FROM tem.pago_masivo__ WHERE negasoc='MC17025' AND lote_pago='LD00011';
SELECT * FROM administrativo.control_refinanciacion_negocios WHERE key_ref='PAA0000243';

SELECT num_doc_fen,documento,negasoc,fecha_vencimiento,valor_abono,valor_saldo,valor_abonome,valor_saldome,* FROM con.foto_cartera WHERE negasoc='MC17784' AND periodo_lote='201906' ORDER BY fecha_vencimiento;
SELECT num_doc_fen,documento,negasoc,fecha_vencimiento,valor_abono,valor_saldo,* FROM con.factura WHERE negasoc='MC17784' ORDER BY fecha_vencimiento;

UPDATE con.foto_cartera  SET valor_saldo=0.00 , valor_saldome=0.00 , valor_abono=538243.00 , valor_abonome=538243.00
WHERE negasoc='MC17784' AND periodo_lote='201906' AND documento IN ('MC1493001','MC1493002');

538243.00

--IA540399
--IC362929

--IA540398
--IC362928

SELECT * FROM recibo_oficial_pago WHERE id=361123;

--.1)RESTABLECER SALDS DE CARTERA SEGUN LA TABLA con.factura_bk usando el lote de pago(el lote esta en la tabla tem.pago_masivo__ )
SELECT * FROM con.facturas_bk WHERE lote_pago='LD00052' AND negasoc='MC17784' ORDER BY fecha_vencimiento;
SELECT * FROM con.factura WHERE negasoc='MC13930' ORDER BY fecha_vencimiento;
--2.) update la monda .

UPDATE con.factura 
			SET valor_abono=t.valor_abono,
				valor_abonome=t.valor_abono ,
				valor_saldo=t.valor_saldo,
				valor_saldome=t.valor_saldo,
				fecha_ultimo_pago=t.fecha_ultimo_pago::timestamp, 
				last_update=now() 
			FROM (SELECT bk.documento,
					 bk.tipo_documento,
					 bk.negasoc,
					 bk.nit,
					 bk.num_doc_fen,
					 bk.fecha_ultimo_pago,
					 bk.valor_abono,
					 bk.valor_saldo 
				FROM con.facturas_bk bk 
				WHERE bk.negasoc='MC13930' AND bk.reg_status='' AND lote_pago='LD00052' and bk.logica_aplicacion='PLAN_AL_DIA') t
			WHERE con.factura.negasoc=t.negasoc 
			AND con.factura.tipo_documento=t.tipo_documento 
			AND con.factura.num_doc_fen=t.num_doc_fen
			AND con.factura.tipo_documento='FAC'
			AND con.factura.negasoc='MC13930'
			AND con.factura.reg_status='' ;

--3.)se borran las facturas que se crean adicionales 
SELECT * FROM administrativo.control_refinanciacion_negocios WHERE key_ref='PAA0000226'; --cod_neg='MC13930';
SELECT * FROM administrativo.proyeccion_refinanciacion_negocios WHERE key_ref='PAA0000226' AND color='S';

SELECT * FROM con.factura WHERE negasoc='MC13930' ORDER BY fecha_vencimiento; --VALIDACION
SELECT * FROM con.foto_cartera WHERE negasoc='MC13930' AND periodo_lote='201906' ORDER  BY fecha_vencimiento; --VALIDACIONES 
SELECT * FROM con.factura_detalle WHERE documento='MC1201313';

--4.) ELIMINAR NOTAS

SELECT * FROM con.ingreso WHERE num_ingreso IN ('IA540398','IC362928');
SELECT * FROM con.ingreso_detalle WHERE num_ingreso IN ('IA540398','IC362928');

--5.) actulizar otras cosas
--UPDATE negocios set refinanciado='N' WHERE cod_neg='MC13930';
--UPDATE administrativo.control_refinanciacion_negocios SET estado='A' WHERE key_ref='PAA0000226' ;
