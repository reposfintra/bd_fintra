-- Function: recaudo.pagos_masivos_manuales_x_refinanciacion(_usuario character varying, _lote_pago character varying, _logica_aplicacion character VARYING);

-- DROP FUNCTION recaudo.pagos_masivos_manuales_x_refinanciacion(_usuario character varying, _lote_pago character varying, _logica_aplicacion character VARYING);

CREATE OR REPLACE FUNCTION recaudo.pagos_masivos_manuales_x_refinanciacion(_usuario character varying, _lote_pago character varying, _logica_aplicacion character VARYING)
  RETURNS text AS
$BODY$

DECLARE

 _pagos_manuales record; 
 _facturas record;
 _facturas_saldo record;
 _saldo_aplicar_ic NUMERIC :=0;
 _cont int :=0;
 _diff NUMERIC :=0;
 _num_ingreso VARCHAR;
 _valor_saldo_factura NUMERIC:=0.00;
 _plandia varchar:='';


BEGIN
	
	FOR  _pagos_manuales IN 
							SELECT
							    pm.id,
							    c.codcli,
							    negasoc,
							    nitcli,
							    unidad_negocio,
							    tipo_pago,
							    fecha_pago,
							    banco,
							    sucursal,
							    valor_aplicar_neto::numeric,
							    ctrl.valor_a_pagar AS valor_negociacion,
							    (valor_aplicar_neto::NUMERIC-ctrl.valor_a_pagar) AS diff_pago,
							    usuario,
							    procesado,
							    lote_pago AS lt_pago,		   
							    logica_aplicacion as log_apl,
							    ctrl.id_rop,
							    ctrl.key_ref,
							    aplica_refinanciacion
							FROM  tem.pago_masivo__  pm 
								INNER JOIN cliente c ON c.nit=pm.nitcli 
								INNER JOIN administrativo.control_refinanciacion_negocios ctrl ON ctrl.cod_neg=pm.negasoc AND ctrl.estado='P'
							WHERE procesado='N' AND  logica_aplicacion='PLAN_AL_DIA' AND lote_pago=_lote_pago 
							ORDER BY pm.orden
	LOOP
	        --SE VALIDA LA FECHA DE PAGO 
			IF _pagos_manuales.fecha_pago::date > current_date  THEN
			    UPDATE tem.pago_masivo__ SET procesado='E', num_ingreso='',comentario='FECHA DE PAGO MAYOR A HOY' WHERE lote_pago= _pagos_manuales.log_apl AND  negasoc=_pagos_manuales.negasoc AND  id=_pagos_manuales.id;
				CONTINUE;
			END IF;
	
		    --SE APLICA EL PAGO SEGUN FACTURAS DEL EXTRACTO.
			IF _pagos_manuales.valor_aplicar_neto > 0 THEN 
				
				--0.) Raalizar Backup antes de aplicar el pago.
				INSERT INTO con.facturas_bk(reg_status,dstrct,tipo_documento,documento,nit,
											codcli,concepto,fecha_factura,fecha_vencimiento,fecha_ultimo_pago,descripcion, 
											valor_factura,valor_abono,valor_saldo,cmc,periodo,negasoc,num_doc_fen, lote_pago, logica_aplicacion)
					SELECT reg_status,dstrct,tipo_documento,documento,nit,
						   codcli,concepto,fecha_factura,fecha_vencimiento,fecha_ultimo_pago,descripcion, 
						   valor_factura,valor_abono,valor_saldo,cmc,periodo,negasoc,num_doc_fen,_pagos_manuales.lt_pago, _pagos_manuales.log_apl
					FROM con.factura 
					WHERE negasoc= _pagos_manuales.negasoc; 
				
				 _saldo_aplicar_ic:=_pagos_manuales.valor_aplicar_neto;
			   	 _num_ingreso := get_lcod ('INGC');
				 _cont :=1; 
				
				RAISE NOTICE '_num_ingreso : %',_num_ingreso;
				
				 FOR _facturas IN
				 				  select
									fra.nit,
									fra.codcli,
									fra.documento,
									fra.tipo_documento,
									cmc.cuenta,
									fra.fecha_factura,
									fra.fecha_vencimiento,
									fra.descripcion,
									dr.valor_concepto,
									fra.valor_factura,
									fra.valor_abono,
									fra.valor_saldo,
									fra.periodo,
									fra.negasoc,
									cr.prefijo
								from 
								    detalle_rop dr 
								inner join 
									recibo_oficial_pago rop on(rop.id=dr.id_rop)
								inner join 
									conceptos_recaudo cr on(cr.id=dr.id_conceptos_recaudo)
								left join 
									tem.negocios_esquema_viejo e on e.cod_neg =rop.negocio
								inner join 
									con.factura fra on(fra.nit=rop.cedula and fra.negasoc = dr.negocio and fra.num_doc_fen=dr.cuota AND substring(fra.documento,1,2)=cr.prefijo )
								inner JOIN 
									con.cmc_doc cmc on(cmc.cmc=fra.cmc and cmc.tipodoc=fra.tipo_documento)			
								where 
									fra.reg_status = ''  AND 
									fra.tipo_documento in ('FAC','NDC') and
									fra.valor_saldo > 0 and
									dr.id_rop=_pagos_manuales.id_rop and 
									fra.negasoc=_pagos_manuales.negasoc AND 
									dr.cuota!=0 AND
									fra.negasoc !='' and
									substring(fra.documento,1,2) NOT IN ('CP','FF','DF') AND 
									fra.valor_saldo > 0
								ORDER BY fra.fecha_vencimiento, fra.documento DESC
				 
				 LOOP 
				 
				    _valor_saldo_factura := _facturas.valor_saldo; 
				    --3.0) VALIDAMOS QUE TENGAMOS PLATA PARA APLICAR
				    IF _saldo_aplicar_ic > 0 THEN 
						--3.1) VALIDAMOS EL VALOR DE LA APLICACION DE LA CARTERA.
						IF _saldo_aplicar_ic >= _valor_saldo_factura THEN 
								    
							_saldo_aplicar_ic := _saldo_aplicar_ic-_valor_saldo_factura;
										
						ELSIF  _saldo_aplicar_ic < _valor_saldo_factura THEN 
							_valor_saldo_factura := _saldo_aplicar_ic;
							_saldo_aplicar_ic :=0.00;			          	    		
						END IF;
						
						RAISE notice '4.0) Pago Cartera  : %',_valor_saldo_factura;
						RAISE notice '4.1) Saldo despues de aplicar cartera  : %',_saldo_aplicar_ic;
		
						--3.2) APLICAMOS EL PAGO A LA CARTERA DE LA CUOTA ITERADA     
						IF _valor_saldo_factura>0 THEN 							       	
										
							INSERT INTO con.ingreso_detalle(dstrct,tipo_documento,num_ingreso,item,nitcli,valor_ingreso,valor_ingreso_me,factura,
											tipo_doc,documento,creation_user,creation_date,base,cuenta,descripcion,
											valor_tasa,saldo_factura,tipo_referencia_1,referencia_1)
										VALUES('FINV','ING',_num_ingreso,_cont,_facturas.nit,_valor_saldo_factura,_valor_saldo_factura,_facturas.documento,
											_facturas.tipo_documento,_facturas.documento,_usuario,now(),'COL',_facturas.cuenta,_facturas.descripcion,
											1,(_facturas.valor_saldo-_valor_saldo_factura),'NEG',_facturas.negasoc);
										
							UPDATE con.factura 
								SET valor_abono=valor_abono+_valor_saldo_factura,
									valor_abonome=valor_abonome+_valor_saldo_factura ,
									valor_saldo=valor_saldo-_valor_saldo_factura, 
									valor_saldome=valor_saldome-_valor_saldo_factura,
									fecha_ultimo_pago=current_date, 
									last_update=now() 
							WHERE documento=_facturas.documento
							and tipo_documento='FAC'
							and negasoc=_facturas.negasoc
							and reg_status='' ;	
						
							_cont:= _cont+1;							       
						END IF ; -- _valor_saldo_factura>0 
				   END IF; --_saldo_aplicar_ic > 0
				END LOOP;
		END IF; --fin _record_pago.valor_aplicar_neto > 0
		
		--3.)
		IF _saldo_aplicar_ic > 0 THEN 
		
			FOR _facturas_saldo in
	    
							    SELECT
								    tipo_documento,
								    documento,
								    nit,
								    codcli,
								    cmc.cuenta,
								    fecha_factura,
								    fecha_vencimiento,
								    (_pagos_manuales.fecha_pago::date-fecha_vencimiento) AS dias_vencidos,
								    CASE WHEN (_pagos_manuales.fecha_pago::date-fecha_vencimiento) BETWEEN 11 AND 30 THEN 0.05
								    WHEN (_pagos_manuales.fecha_pago::date-fecha_vencimiento) BETWEEN 31 AND 60 THEN 0.1
								    WHEN (_pagos_manuales.fecha_pago::date-fecha_vencimiento) >60 THEN 0.2
								    ELSE 0
								    END AS porc_mora,
								    descripcion,
								    valor_factura,
								    valor_abono,
								    valor_saldo,
								    periodo,
								    negasoc,
								    CASE  WHEN (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento::date) > 0 THEN 'A-VENCIDO_CON_SALDO'
									      WHEN (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento::date) BETWEEN -30 AND 0 THEN 'B-CORRIENTE'
									      WHEN (_pagos_manuales.fecha_pago::date-fra.fecha_vencimiento::date) < -30 THEN 'C-FUTUROS'
									END::varchar AS estado_cartera
								FROM
								  con.factura fra
								INNER JOIN con.cmc_doc cmc on(cmc.cmc=fra.cmc and cmc.tipodoc=fra.tipo_documento)
								WHERE
								    fra.dstrct = 'FINV'
								    AND fra.valor_saldo > 0
								    AND fra.reg_status = ''
								    AND fra.negasoc = _pagos_manuales.negasoc
								    AND fra.tipo_documento in ('FAC','NDC')
								    AND substring(fra.documento,1,2) NOT IN ('CP','FF','DF')
								ORDER BY fecha_vencimiento, documento DESC	
								
			LOOP
								
					_valor_saldo_factura := _facturas_saldo.valor_saldo; 
				
					IF _saldo_aplicar_ic >= _valor_saldo_factura THEN 
								    
						_saldo_aplicar_ic := _saldo_aplicar_ic-_valor_saldo_factura;
									
					ELSIF  _saldo_aplicar_ic < _valor_saldo_factura THEN 
						_valor_saldo_factura := _saldo_aplicar_ic;
						_saldo_aplicar_ic :=0.00;			          	    		
					END IF;
				
					INSERT INTO con.ingreso_detalle(dstrct,tipo_documento,num_ingreso,item,nitcli,valor_ingreso,valor_ingreso_me,factura,
									tipo_doc,documento,creation_user,creation_date,base,cuenta,descripcion,
									valor_tasa,saldo_factura,tipo_referencia_1,referencia_1)
								VALUES('FINV','ING',_num_ingreso,_cont,_facturas_saldo.nit,_valor_saldo_factura,_valor_saldo_factura,_facturas_saldo.documento,
									_facturas_saldo.tipo_documento,_facturas_saldo.documento,_usuario,now(),'COL',_facturas_saldo.cuenta,_facturas_saldo.descripcion,
									1,(_facturas_saldo.valor_saldo-_valor_saldo_factura),'NEG',_facturas_saldo.negasoc);
							    
					UPDATE con.factura 
						SET valor_abono=valor_abono+_valor_saldo_factura,
							valor_abonome=valor_abonome+_valor_saldo_factura ,
							valor_saldo=valor_saldo-_valor_saldo_factura, 
							valor_saldome=valor_saldome-_valor_saldo_factura,
							fecha_ultimo_pago=current_date, 
							last_update=now() 
					WHERE documento=_facturas_saldo.documento
					and tipo_documento='FAC'
					and negasoc=_facturas_saldo.negasoc
					and reg_status='' ;						                
								
					IF _saldo_aplicar_ic=0.00  THEN 
						exit;
					END IF;							
					_cont:= _cont+1;	
					
			END LOOP;
			
	   END IF;
		
	   --5.0) INSERT A CABECERA
	   INSERT INTO con.ingreso(dstrct, tipo_documento, num_ingreso, codcli, nitcli, concepto, tipo_ingreso, fecha_consignacion, fecha_ingreso, branch_code, bank_account_no,
					codmoneda, agencia_ingreso, descripcion_ingreso, vlr_ingreso, vlr_ingreso_me, vlr_tasa, fecha_tasa, cant_item,
					creation_user, creation_date, user_update, last_update, base,tipo_referencia_2,referencia_2,periodo,
					tipo_referencia_1,referencia_1,nro_extracto)
				VALUES('FINV', 'ING', _num_ingreso, _pagos_manuales.codcli, _pagos_manuales.nitcli, 'FE', 'C', _pagos_manuales.fecha_pago, now()::date, 
				case when _pagos_manuales.banco='CORRESPONSALES' then 'CORRESPONSALES ' else _pagos_manuales.banco end, _pagos_manuales.sucursal,
				'PES', 'OP', 'RECAUDO PLAN AL DIA '||_pagos_manuales.banco, _pagos_manuales.valor_aplicar_neto,  _pagos_manuales.valor_aplicar_neto,1, NOW()::date,(_cont-1),_usuario,now(), _usuario,now(), 'COL','TIPAG',_pagos_manuales.tipo_pago,'000000',
				'NEG',_pagos_manuales.negasoc,_pagos_manuales.id_rop);
					
	
		--5.1) Actualizamos como aplizado
		UPDATE tem.pago_masivo__ SET procesado='S', fecha_aplicacion=now()::timestamp, num_ingreso=_num_ingreso WHERE negasoc=_pagos_manuales.negasoc AND id=_pagos_manuales.id;

		--5.2) VALIDAR QUE EL INGREOS ESTE CUADRADO CABECERA CON DETALLE SI CUADRA ACTUALIZAS LA TABLA PAGOS MANUALES SI NO MARCAS EL REGISTRO COMO ERROR.
		_diff := COALESCE((SELECT SUM(valor_ingreso)-ing.vlr_ingreso AS valida 
							FROM con.ingreso_detalle idet
							INNER JOIN con.ingreso ing ON (ing.num_ingreso=idet.num_ingreso AND ing.nitcli=idet.nitcli )
							WHERE ing.num_ingreso=_num_ingreso
							GROUP BY  ing.vlr_ingreso),0) ;
						
						
		IF _diff !=0 THEN 
		   
			DELETE FROM con.ingreso WHERE num_ingreso=_num_ingreso;
			DELETE FROM con.ingreso_detalle WHERE num_ingreso=_num_ingreso;
			
			--restablece el saldo de la fatura segun la tabla de backup.
			UPDATE con.factura 
			SET valor_abono=t.valor_abono,
				valor_abonome=t.valor_abono ,
				valor_saldo=t.valor_saldo,
				valor_saldome=t.valor_saldo,
				fecha_ultimo_pago=t.fecha_ultimo_pago::timestamp, 
				last_update=now() 
			FROM (SELECT bk.documento,
					 bk.tipo_documento,
					 bk.negasoc,
					 bk.nit,
					 bk.num_doc_fen,
					 bk.fecha_ultimo_pago,
					 bk.valor_abono,
					 bk.valor_saldo 
				FROM con.facturas_bk bk 
				WHERE bk.negasoc=_pagos_manuales.negasoc AND bk.reg_status='' AND lote_pago=_lote_pago and bk.logica_aplicacion=_logica_aplicacion) t
			WHERE con.factura.negasoc=t.negasoc 
			AND con.factura.tipo_documento=t.tipo_documento 
			AND con.factura.num_doc_fen=t.num_doc_fen
			AND con.factura.tipo_documento='FAC'
			AND con.factura.negasoc= _pagos_manuales.negasoc   
			AND con.factura.reg_status='' ;
			
			
		    --MARCAMOS EL BACKUP COMO ANULADO.
		   UPDATE con.facturas_bk SET reg_status='A' WHERE negasoc=_pagos_manuales.negasoc AND reg_status='' AND lote_pago=_lote_pago and logica_aplicacion=_logica_aplicacion;
		       
		   --MARCAMOS EL CONTROL COMO PAGO ERRADO 
		   UPDATE tem.pago_masivo__ SET procesado='E', num_ingreso='',comentario='VALOR CABECERA DIFERENTE A VALOR DETALLADO.' WHERE negasoc=_pagos_manuales.negasoc AND  id=_pagos_manuales.id;
		
	    ELSIF _diff=0 AND _pagos_manuales.aplica_refinanciacion='S' THEN 
			_plandia:=administrativo.aplicar_plan_aldia(_usuario::character VARYING, _pagos_manuales.negasoc::character VARYING,  _pagos_manuales.key_ref::character VARYING);		
			RAISE NOTICE 'PLAN AL DIA %',_plandia;
		END IF;		
	
	END LOOP;
	
return 'OK';
  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION recaudo.pagos_masivos_manuales_x_refinanciacion(character varying, character varying, character varying)
  OWNER TO postgres;
 

--SELECT recaudo.pagos_masivos_manuales_x_refinanciacion('AMANJARRES'::character varying, 'LD00014'::character varying, 'PLAN_AL_DIA'::character varying);
