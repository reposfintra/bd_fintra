-- DROP FUNCTION recaudo.cierre_plan_al_dia();


CREATE OR REPLACE FUNCTION recaudo.cierre_plan_al_dia(_usuario character varying)
  RETURNS text AS
$BODY$

DECLARE
  _recordpagos record;
  _det_pagos record; 
BEGIN
		--consultamos los pagos que esten marcados como plan al dia y que no esten procesados
		FOR _recordpagos IN 
		
			SELECT  
			    negasoc,
			    nitcli,	
			    sum(pm.valor_aplicar_neto) AS valor_aplicar_neto,
			    ctrl.valor_a_pagar AS valor_negociacion,
			    (sum(pm.valor_aplicar_neto)::NUMERIC-ctrl.valor_a_pagar) AS diff_pago,	   
			    logica_aplicacion as log_apl,
			    ctrl.id_rop,
			    ctrl.key_ref,
			    PM.procesado,
			    count(*) AS num_pago
			FROM  tem.pago_masivo__  pm 
				INNER JOIN cliente c ON c.nit=pm.nitcli 
				INNER JOIN administrativo.control_refinanciacion_negocios ctrl ON ctrl.cod_neg=pm.negasoc AND ctrl.estado='P'
			WHERE  
				logica_aplicacion='PLAN_AL_DIA'  AND pm.procesado='N' --AND  negasoc='MC14654'  
			GROUP BY 
				negasoc,
				nitcli,	
				log_apl,
				ctrl.id_rop,
				ctrl.key_ref,
				PM.procesado,
				ctrl.valor_a_pagar
			ORDER BY negasoc
			
		LOOP
			--si el pago es menor actualizamos el motor de clasificacion
		  IF _recordpagos.diff_pago <-1001 THEN 
		  
				  FOR _det_pagos IN SELECT
										    pm.id,
										    c.codcli,
										    negasoc,
										    nitcli,
										    unidad_negocio,
										    tipo_pago,
										    fecha_pago,
										    banco,
										    sucursal,
										    valor_aplicar_neto::numeric,
										    ctrl.valor_a_pagar AS valor_negociacion,
										    (valor_aplicar_neto::NUMERIC-ctrl.valor_a_pagar) AS diff_pago,
										    usuario,
										    procesado,
										    lote_pago AS lt_pago,		   
										    logica_aplicacion as log_apl,
										    ctrl.id_rop,
										    ctrl.key_ref
										FROM  tem.pago_masivo__  pm 
											INNER JOIN cliente c ON c.nit=pm.nitcli 
											INNER JOIN administrativo.control_refinanciacion_negocios ctrl ON ctrl.cod_neg=pm.negasoc AND ctrl.estado='P'
										WHERE procesado='N' AND  logica_aplicacion='PLAN_AL_DIA' AND pm.negasoc=_recordpagos.negasoc AND  ctrl.key_ref=_recordpagos.key_ref
										ORDER BY pm.id,pm.orden
					 LOOP
					 		
					 	update 
					 		tem.pago_masivo__ 
					 	set 
					 		logica_aplicacion='CEDULA', 
					 		usuario_cierre_plan_al_dia=_usuario, 
					 		fecha_cierre_plan_al_dia=now() 
					 	where 
					 		id=_det_pagos.id and 
					 		logica_aplicacion='PLAN_AL_DIA';
					 		
					 END LOOP;
		  	  	
		  END IF; 
		  	
		END LOOP;
		
RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION recaudo.cierre_plan_al_dia(character varying)
  OWNER TO postgres;
  
--select recaudo.cierre_plan_al_dia()