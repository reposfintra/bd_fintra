-- Function: recaudo.pagos_masivos(character varying, _lote_pago character varying, _logica_aplicacion character varying)

-- DROP FUNCTION recaudo.pagos_masivos(character varying, _lote_pago character varying, _logica_aplicacion character varying); 

CREATE OR REPLACE FUNCTION recaudo.pagos_masivos(_usuario character varying, _lote_pago character varying, _logica_aplicacion character varying)
 RETURNS text AS
$BODY$

DECLARE

/**
 *@descripcion: Funcion encargada de tomar la decision 
 *del pago masivo, extracto o por cedula
 *@autor:JZAPATA
 *@Fecha:2019-05-13
 */

_retorno text:='ERROR';

BEGIN
	
	if(_logica_aplicacion='EXTRACTO')then
		_retorno:=recaudo.pagos_masivos_manuales_x_extracto(_usuario, _lote_pago, _logica_aplicacion);
	ELSIF(_logica_aplicacion='CEDULA')then
		_retorno:=recaudo.pagos_masivos_manuales_v3(_usuario, _lote_pago, _logica_aplicacion);
	ELSIF _logica_aplicacion='PLAN_AL_DIA' AND substring(_lote_pago,1,2)='LD' THEN 
	    _retorno:=recaudo.pagos_masivos_manuales_x_refinanciacion(_usuario, _lote_pago, _logica_aplicacion);
	end if;

	raise notice '_retorno:%',_retorno;
	            
return 'OK';
  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION recaudo.pagos_masivos(character varying, character varying, character varying)
  OWNER TO postgres;
  
 
--select recaudo.pagos_masivos('JZAPATA', 'LT00092', 'EXTRACTO')