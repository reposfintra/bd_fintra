--DROP FUNCTION business_intelligence.dv_consolidado_gestiones_cartera() ;

CREATE OR REPLACE FUNCTION business_intelligence.dv_consolidado_gestiones_cartera()
  RETURNS varchar AS
$BODY$

DECLARE
  	InfoGeneral record;
  	periodRecord record;
	UltimoPago record;
	ClienteInfo record;
	GestionInfo record;
  
  _periodo varchar ;
  _years integer;
  _mes integer;
  _id integer;
  
BEGIN
   
--  truncate TABLE business_intelligence.consolidado_gestiones_cartera; 

	delete from business_intelligence.consolidado_gestiones_cartera where periodo_lote = replace(substring(now(),1,7),'-','');
     
  FOR periodRecord in
  
      SELECT substring(valor,1,4)::integer AS YEAR , 
          substring(valor,5,2)::integer AS MONTH, 
          valor AS PERIOD,
          CASE WHEN substring(valor,5,2)::integer =12 THEN ((substring(valor,1,4)::integer+1)||'01')::integer 
          ELSE valor::integer+1 END AS next_period     
         FROM con.periodos_foto 
         where valor= replace(substring(now(),1,7),'-','')         
         --valor::integer>=201904 ORDER BY valor::integer
         
     LOOP
  
      _periodo:=periodRecord.PERIOD;
   _years := periodRecord.YEAR;
   _mes :=periodRecord.MONTH;
      _id :=(SELECT min(id) FROM con.foto_cartera WHERE periodo_lote=_periodo AND ano=_years AND mes=_mes);
     
      -- raise notice 'periodRecord.period: %',periodRecord.period;
  
   FOR InfoGeneral IN 
   
     select
			f.nit::varchar as cedula,
			''::varchar as nombre_cliente,
			n.cod_neg::varchar as negocio,
			substring(max(f.fecha_vencimiento)::varchar,9)::numeric as dia_vencimiento,
			case when  substring(replace(max(fupview.fecha::date),'-',''),1,6) = f.periodo_lote 
					then fupview.fecha::date 
					else '0099-01-01' end as fecha_ult_pag_vs_foto,
			case when  substring(replace(max(fupview.fecha::date),'-',''),1,6)::varchar = f.periodo_lote 
					then substring(replace(max(fupview.fecha::date),'-',''),1,6) 
					else '009901'::varchar end as periodo_ult_pag_vs_foto,
			0::numeric as valor_ultimo_pago,	
			 ''::varchar as altura_mora_actual,
			fo.creation_user::varchar as agente, 
			''::varchar as agente_campo,
			fo.creation_date::date as fecha_gestion,
			substring(fo.creation_date from 12 for 5)::varchar as hora_gestion,
			substring(replace(max(fo.creation_date::date),'-',''),1,6)::varchar as periodo_gestion,
			''::varchar as tipo_gestion,
			''::varchar as  desc_tipo_gestion,
			f.periodo_lote::varchar as periodo_lote,
			''::varchar as estado_gestion,
			'FACTURA_OBSERVACION' as tabla
		from con.foto_cartera f
		inner join negocios n on n.cod_neg=f.negasoc
		LEFT JOIN fecha_ultimo_pago_view fupview ON (n.cod_neg=fupview.negasoc )
		inner join con.factura_observacion  fo on fo.documento=f.documento and replace(substring(fo.creation_date,1,7),'-','')= f.periodo_lote
		where periodo_lote = _periodo
		and f.reg_status = ''
		AND f.ano=_years
        AND f.mes=_mes
        AND f.id > _id 
		group by f.nit, nombre_cliente, n.cod_neg, fo.creation_user, fupview.fecha,	f.periodo_lote, f.negasoc, --agente_campo, 
		fo.creation_date::date,substring(fo.creation_date from 12 for 5),tabla
		union ALL
		select
			f.nit::varchar as cedula,
			''::varchar as nombre_cliente,
			n.cod_neg::varchar as negocio,
			substring(max(f.fecha_vencimiento)::varchar,9)::numeric as dia_vencimiento,
			case when  substring(replace(max(fupview.fecha::date),'-',''),1,6) = f.periodo_lote 
					then fupview.fecha::date 
					else '0099-01-01' end as fecha_ult_pag_vs_foto,
			case when  substring(replace(max(fupview.fecha::date),'-',''),1,6)::varchar = f.periodo_lote 
					then substring(replace(max(fupview.fecha::date),'-',''),1,6) 
					else '009901'::varchar end as periodo_ult_pag_vs_foto,
			0::numeric as valor_ultimo_pago,	
			''::varchar as altura_mora_actual,
			fo.creation_user::varchar as agente, 
			''::varchar as agente_campo,
			fo.creation_date::date as fecha_gestion,
			substring(fo.creation_date from 12 for 5)::varchar as hora_gestion,
			substring(replace(max(fo.creation_date::date),'-',''),1,6)::varchar as periodo_gestion,
			''::varchar as tipo_gestion,
			''::varchar as  desc_tipo_gestion,
			f.periodo_lote::varchar as periodo_lote,
			''::varchar as estado_gestion,
			'COMPROMISO_PAGO' as tabla
		from con.foto_cartera f
		inner join negocios n on n.cod_neg=f.negasoc
		LEFT JOIN fecha_ultimo_pago_view fupview ON (n.cod_neg=fupview.negasoc )
		inner join con.compromiso_pago_cartera  fo on fo.negocio=f.negasoc and replace(substring(fo.creation_date,1,7),'-','')= f.periodo_lote
		where periodo_lote = _periodo
		and f.reg_status = ''
		AND f.ano=_years
        AND f.mes=_mes
        AND f.id > _id
		group by f.nit, nombre_cliente, n.cod_neg, fo.creation_user, fupview.fecha,	f.periodo_lote, f.negasoc, --agente_campo, 
		fo.creation_date::date,substring(fo.creation_date from 12 for 5),tabla
		
   LOOP 
  
          
    InfoGeneral.altura_mora_actual := eg_altura_mora_periodo(InfoGeneral.negocio,InfoGeneral.periodo_lote::integer,1,0);
    
    InfoGeneral.agente_campo := coalesce((select agente_campo from con.foto_cartera where periodo_lote = _periodo and negasoc = InfoGeneral.negocio and agente_campo !='' group by agente_campo),''); 
        
    
    --raise notice 'InfoGeneral.negocio: %',InfoGeneral.negocio;
    
		SELECT INTO ClienteInfo nomcli,direccion,ciudad,case when telefono is null then '0' else telefono end as telefono,telcontacto FROM cliente WHERE nit = InfoGeneral.cedula;
		InfoGeneral.nombre_cliente = ClienteInfo.nomcli;
		
		SELECT INTO UltimoPago sum(valor_abono)  as valor_abono
		from con.factura where negasoc = InfoGeneral.negocio
		and fecha_ultimo_pago = (select max(fecha_ultimo_pago) from con.factura where negasoc = InfoGeneral.negocio);
		InfoGeneral.valor_ultimo_pago = UltimoPago.valor_abono;

		
		IF(InfoGeneral.tabla = 'FACTURA_OBSERVACION') then 
		
        SELECT INTO GestionInfo fo.creation_date::Date as fecha_gestion, tipo_gestion, t.dato, substring(max(fo.creation_date::date)::varchar,9) as dia_gestion,
        			coalesce((case when substring(max(fo.creation_date::date)::varchar,9)::numeric > InfoGeneral.dia_vencimiento then 'Gestion vencida'
        				when substring(max(fo.creation_date::date)::varchar,9)::numeric < InfoGeneral.dia_vencimiento then 'Gestion anticipada'
        				when substring(max(fo.creation_date::date)::varchar,9)::numeric = InfoGeneral.dia_vencimiento then 'Gestion al dia'
        				else 'No tiene gestion' end),'')::varchar as estado_gestion
		from con.factura_observacion fo
		inner join tablagen t on t.table_code=fo.tipo_gestion and t.table_type = 'TIPOGEST'
		where documento in (select documento from con.factura where negasoc= InfoGeneral.negocio) 
		and replace(substring(fo.creation_date,1,7),'-','')= InfoGeneral.periodo_lote
		and fo.creation_date::date=InfoGeneral.fecha_gestion 
		group by fo.creation_date::date, tipo_gestion, t.dato;
		InfoGeneral.tipo_gestion = GestionInfo.tipo_gestion;
		InfoGeneral.desc_tipo_gestion = GestionInfo.dato;
		InfoGeneral.estado_gestion = GestionInfo.estado_gestion;
		
		else
		
		SELECT INTO GestionInfo fo.creation_date::Date as fecha_gestion, t.dato as tipo_gestion, t.dato, 
					substring(max(fo.creation_date::date)::varchar,9) as dia_gestion,
        			coalesce((case when substring(max(fo.creation_date::date)::varchar,9)::numeric > InfoGeneral.dia_vencimiento then 'Gestion vencida'
        				when substring(max(fo.creation_date::date)::varchar,9)::numeric < InfoGeneral.dia_vencimiento then 'Gestion anticipada'
        				when substring(max(fo.creation_date::date)::varchar,9)::numeric = InfoGeneral.dia_vencimiento then 'Gestion al dia'
        				else 'No tiene gestion' end),'')::varchar as estado_gestion
		from con.compromiso_pago_cartera fo
		inner join tablagen t on t.table_code=19 and t.table_type = 'TIPOGEST'
		where negocio = InfoGeneral.negocio 
		and replace(substring(fo.creation_date,1,7),'-','')= InfoGeneral.periodo_lote
		and fo.creation_date::date=InfoGeneral.fecha_gestion 
		group by fo.creation_date::date, tipo_gestion, t.dato;
		InfoGeneral.tipo_gestion = GestionInfo.tipo_gestion;
		InfoGeneral.desc_tipo_gestion = GestionInfo.dato;
		InfoGeneral.estado_gestion = GestionInfo.estado_gestion;
		
		end if;
		
		
      
       INSERT INTO business_intelligence.consolidado_gestiones_cartera(cedula, nombre_cliente, negocio, dia_vencimiento, fecha_ult_pag_vs_foto, periodo_ult_pag_vs_foto, valor_ultimo_pago, 
       altura_mora_actual, agente, agente_campo, fecha_gestion, periodo_gestion, tipo_gestion, desc_tipo_gestion, periodo_lote, estado_gestion)                     
       VALUES (InfoGeneral.cedula, InfoGeneral.nombre_cliente, InfoGeneral.negocio, InfoGeneral.dia_vencimiento, InfoGeneral.fecha_ult_pag_vs_foto, InfoGeneral.periodo_ult_pag_vs_foto, 
       InfoGeneral.valor_ultimo_pago, InfoGeneral.altura_mora_actual, InfoGeneral.agente, InfoGeneral.agente_campo,InfoGeneral.fecha_gestion, InfoGeneral.periodo_gestion, 
       InfoGeneral.tipo_gestion, InfoGeneral.desc_tipo_gestion, InfoGeneral.periodo_lote, InfoGeneral.estado_gestion);
    
    
    
         
   END LOOP;
  
 END LOOP;

    RETURN  'OK';

END;$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION business_intelligence.dv_consolidado_gestiones_cartera()
  OWNER TO postgres;
 
  
--SELECT business_intelligence.dv_consolidado_gestiones_cartera();


---SELECT count(*) FROM business_intelligence.consolidado_gestiones_cartera;  --36737


--DROP TABLE business_intelligence.consolidado_gestiones_cartera ;
/*
CREATE TABLE business_intelligence.consolidado_gestiones_cartera 
   (id serial,
    cedula character varying,
    nombre_cliente character varying,
    negocio character varying,
    dia_vencimiento numeric,
    fecha_ult_pag_vs_foto date,
    periodo_ult_pag_vs_foto character varying,
    valor_ultimo_pago numeric,
    altura_mora_actual character varying,
    agente character varying,
    agente_campo character varying,
    fecha_gestion date,
    periodo_gestion character varying,
    tipo_gestion character varying,
    desc_tipo_gestion character varying,
    periodo_lote character varying,
    estado_gestion character varying
    ) ;
ALTER TABLE business_intelligence.consolidado_gestiones_cartera
  OWNER TO postgres;
  */