-- FUNCTION: CON.INTERFAZ_MICROCREDITO_NOTA_SUPEREFECTIVO_APOTEOSYS()

-- DROP FUNCTION CON.INTERFAZ_MICROCREDITO_NOTA_SUPEREFECTIVO_APOTEOSYS();

CREATE OR REPLACE FUNCTION CON.INTERFAZ_MICROCREDITO_NOTA_SUPEREFECTIVO_APOTEOSYS()
  RETURNS TEXT AS
$BODY$

DECLARE

 /************************************************
  *DESCRIPCION: ESTA FUNCION BUSCA TODAS LAS NOTAS QUE CRUZAN LAS R0 A SUPEREFECTIVO PARA LOS NEGOCIOS DE MICROCREDITO
  *CREA EL ASIENTO CONTABLE PARA CADA EGRESO.
  *AUTOR:=@DVALENCIA
  *FECHA CREACION:=2018-11-15
  *DESCRIPCION DE CAMBIOS Y FECHA
  ************************************************/

NC_SUPEREFECTIVO RECORD;
INFOITEMS_ RECORD;
INFOCLIENTE RECORD;
LONGITUD NUMERIC;
SECUENCIA_GEN INTEGER;
SECUENCIA_INT INTEGER:= 1;
CUENTAS_ VARCHAR[] := '{23051104,13809502}';
FECHADOC_ VARCHAR:= '';
MCTYPE CON.TYPE_INSERT_MC;
SW TEXT:='N';
VALIDACIONES TEXT;

SALDOINGRESO NUMERIC:=0.00;
VLR_CREDITO NUMERIC:=0.00;

BEGIN
	/**SACAMOS EL LISTADO DE NEGOCIOS*/
	FOR NC_SUPEREFECTIVO IN
		
			SELECT ING.PERIODO, 
					ING.TIPO_DOCUMENTO, 
					ING.NUM_INGRESO,
					ING.VLR_INGRESO,
					ING.NITCLI, 
					ING.DESCRIPCION_INGRESO,
					ING.FECHA_INGRESO as creation_date, 
					ING.FECHA_INGRESO as fecha_vencimiento 
			FROM CON.INGRESO ING
			INNER JOIN CON.INGRESO_DETALLE IDET ON ING.NUM_INGRESO=IDET.NUM_INGRESO AND ING.TIPO_DOCUMENTO=IDET.TIPO_DOCUMENTO
			WHERE ING.NITCLI = '890104964' 
			AND ING.TIPO_DOCUMENTO = 'ICA' 
			--AND ING.CMC = 'SU'
			AND ING.PERIODO =REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','')  
			AND COALESCE(IDET.PROCESADO_ICA,'N') = 'N'
			--and ING.num_ingreso in ('IA538175')
			and ING.reg_status = ''
			and IDET.reg_status = ''
			GROUP BY ING.PERIODO, 
					ING.TIPO_DOCUMENTO, 
					ING.NUM_INGRESO, 
					ING.NITCLI, 
					ING.DESCRIPCION_INGRESO,
					ING.FECHA_INGRESO, 
					ING.FECHA_CONSIGNACION,
					ING.VLR_INGRESO

		--SELECT CON.INTERFAZ_MICROCREDITO_NOTA_SUPEREFECTIVO_APOTEOSYS();

	LOOP
		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.INTERFAZ_SECUENCIA_ICA_APOTEOSYS');


		SECUENCIA_INT:=1;

		MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT';
		MCTYPE.MC_____CODIGO____TD_____B := 'ICRN';
		MCTYPE.MC_____CODIGO____CD_____B := 'IASU';
		MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN; --SECUENCIA GENERAL

		/**BUSCAMOS LA COMPLETA QUE CONFORMARA EL ASIENTO CONTABLE PARA MANDARLO A APOTEOSYS*/
		FOR INFOITEMS_ IN
			(
			SELECT
				CXP.TIPO_DOCUMENTO,
				CXP.DOCUMENTO,
				CXP.PROVEEDOR as NIT,
				CXP.DESCRIPCION,
				CXP.VLR_NETO AS VALOR_DEB,
				0::NUMERIC AS VALOR_CREDT,
				CUENTAS_[1]AS CUENTA,
				'-' AS CC,
				(SELECT DOCUMENTO_RELACIONADO FROM FIN.CXP_DOC WHERE DOCUMENTO=CXP.DOCUMENTO AND TIPO_DOCUMENTO = 'FAP') AS REFERENCIA
			FROM FIN.CXP_DOC CXP
			INNER JOIN FIN.CXP_ITEMS_DOC CXPI ON CXPI.DOCUMENTO=CXP.DOCUMENTO AND CXPI.TIPO_DOCUMENTO=CXP.TIPO_DOCUMENTO
			--INNER JOIN FIN.CXP_DOC CXPN ON (CXPN.DOCUMENTO_RELACIONADO=CXP.DOCUMENTO_RELACIONADO AND CXPN.DOCUMENTO LIKE 'MP%')			
			WHERE CXP.DOCUMENTO IN (select tem.explode_array((SELECT string_to_array(regexp_replace(descripcion_ingreso, E'[\n\r]+', ',', 'g'), ',') AS arr FROM con.ingreso WHERE num_ingreso = NC_SUPEREFECTIVO.NUM_INGRESO)))
			AND CXP.DOCUMENTO ILIKE 'CXP%'
			AND CXP.TIPO_DOCUMENTO = 'NC'
			UNION ALL
			SELECT 
				'NC' AS TIPO_DOCUMENTO,
				DOCUMENTO,
				NITCLI as NIT,
				DESCRIPCION,
				0.00 AS VALOR_DEB,	
				--SUM(MC_____DEBMONLOC_B) AS VALOR_CREDT,
				VALOR_INGRESO AS VALOR_CREDT,
				CUENTAS_[2]AS CUENTA,
				MC_____CODIGO____CU_____B AS CC,
				NUM_INGRESO AS REFERENCIA
			FROM CON.INGRESO_DETALLE ID
			INNER JOIN apoteosys.MC_FENALCO____ MC ON MC.MC_____NUMDOCSOP_B = ID.DOCUMENTO
			WHERE NUM_INGRESO = NC_SUPEREFECTIVO.NUM_INGRESO 
			AND TIPO_DOCUMENTO = 'ICA'
			--and MC.procesado = 'S'
			GROUP BY DOCUMENTO, NITCLI, DESCRIPCION,MC_____CODIGO____CU_____B, NUM_INGRESO, VALOR_INGRESO
			)
		LOOP
		
			/**BUSCAMOS LA INFORMACION DEL CLIENTE*/
			SELECT INTO INFOCLIENTE
				(CASE
				WHEN TIPO_DOC ='CED' THEN 'CC'
				WHEN TIPO_DOC ='RIF' THEN 'CE'
				WHEN TIPO_DOC ='NIT' THEN 'NIT' ELSE
				'NIT' END) AS TIPO_DOC,
				(CASE
				WHEN GRAN_CONTRIBUYENTE ='N' AND AGENTE_RETENEDOR ='N' THEN 'RCOM'
				WHEN GRAN_CONTRIBUYENTE ='N' AND AGENTE_RETENEDOR ='S' THEN 'RCAU'
				WHEN GRAN_CONTRIBUYENTE ='S' AND AGENTE_RETENEDOR ='N' THEN 'GCON'
				WHEN GRAN_CONTRIBUYENTE ='S' AND AGENTE_RETENEDOR ='S' THEN 'GCAU'
				ELSE 'RCOM' END) AS CODIGO,
				(CASE
				WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				ELSE '08001' END) AS CODIGOCIU,
				(D.NOMBRE1||' '||D.NOMBRE2) AS NOMBRE_CORTO,
				(D.APELLIDO1||' '||D.APELLIDO2) AS APELLIDOS,
				*
			FROM NIT D
			LEFT JOIN PROVEEDOR PROV ON PROV.NIT=D.CEDULA
			LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
			WHERE CEDULA =  INFOITEMS_.NIT;
			
--			NC_SUPEREFECTIVO.VLR_INGRESO :=  NC_SUPEREFECTIVO.VLR_INGRESO-INFOITEMS_.VALOR_CREDT;
--				
--				--RAISE NOTICE 'NC_SUPEREFECTIVO.VLR_INGRESO : % INFOITEMS_.VALOR_DEB : % INFOITEMS_.VALOR_CREDT : %  iteracion : %',NC_SUPEREFECTIVO.VLR_INGRESO,INFOITEMS_.VALOR_DEB,INFOITEMS_.VALOR_CREDT,SECUENCIA_INT;			
--				VLR_CREDITO:=0.00;	
--				if ( INFOITEMS_.VALOR_CREDT >0 and INFOITEMS_.VALOR_DEB = 0.00) then
--				
--					IF(NC_SUPEREFECTIVO.VLR_INGRESO=0.00)then
--						--RAISE NOTICE 'Supuestamente aqui y salta: %';			
--						CONTINUE;						
--					ELSIF (NC_SUPEREFECTIVO.VLR_INGRESO > 0.00) THEN 
--							VLR_CREDITO = INFOITEMS_.VALOR_CREDT;
--					ELSE
--							VLR_CREDITO = INFOITEMS_.VALOR_CREDT+NC_SUPEREFECTIVO.VLR_INGRESO;
--							NC_SUPEREFECTIVO.VLR_INGRESO = 0.00;
--					END IF;
--				end if;	
	

			IF(INFOITEMS_.TIPO_DOCUMENTO IN ('FAC','EGR','FAP','ING','NC') AND CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N',INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,'', 6)='S')THEN
				MCTYPE.MC_____FECHEMIS__B = NC_SUPEREFECTIVO.CREATION_DATE; --FECHA CREACION
				MCTYPE.MC_____FECHVENC__B = NC_SUPEREFECTIVO.FECHA_VENCIMIENTO; --FECHA VENCIMIENTO
			ELSE
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';
			END IF;
			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(NC_SUPEREFECTIVO.CREATION_DATE,1,7),'-','') = NC_SUPEREFECTIVO.PERIODO THEN NC_SUPEREFECTIVO.CREATION_DATE::DATE ELSE CON.SP_FECHA_CORTE_MES(SUBSTRING(NC_SUPEREFECTIVO.PERIODO,1,4), SUBSTRING(NC_SUPEREFECTIVO.PERIODO,5,2)::INT)::DATE END;
			MCTYPE.MC_____FECHA_____B := CASE WHEN (NC_SUPEREFECTIVO.CREATION_DATE::DATE > FECHADOC_::DATE AND REPLACE(SUBSTRING(NC_SUPEREFECTIVO.CREATION_DATE,1,7),'-','') = NC_SUPEREFECTIVO.PERIODO)  THEN NC_SUPEREFECTIVO.CREATION_DATE::DATE ELSE FECHADOC_::DATE END;
			MCTYPE.MC_____SECUINTE__DCD____B := SECUENCIA_INT;--SECUENCIA INTERNA
			MCTYPE.MC_____SECUINTE__B := SECUENCIA_INT;--SECUENCIA INTERNA
			MCTYPE.MC_____REFERENCI_B := INFOITEMS_.REFERENCIA;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING( NC_SUPEREFECTIVO.PERIODO,1,4)::INT;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING( NC_SUPEREFECTIVO.PERIODO,5,2)::INT;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF';
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,'', 1);
			if (INFOITEMS_.CC ='-') THEN
						MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,'', 2);
			else 
						MCTYPE.MC_____CODIGO____CU_____B := INFOITEMS_.CC;
			end if;
			
			MCTYPE.MC_____IDENTIFIC_TERCER_B := CASE WHEN CHAR_LENGTH(INFOITEMS_.NIT)>9 AND INFOCLIENTE.TIPO_DOC='NIT' THEN SUBSTR(INFOITEMS_.NIT,1,9) ELSE INFOITEMS_.NIT END;
			MCTYPE.MC_____DEBMONORI_B := 0;
			MCTYPE.MC_____CREMONORI_B := 0;
			MCTYPE.MC_____DEBMONLOC_B := INFOITEMS_.VALOR_DEB::NUMERIC;
			MCTYPE.MC_____CREMONLOC_B := INFOITEMS_.VALOR_CREDT::NUMERIC;
			
--			IF(INFOITEMS_.VALOR_CREDT > 0.00 and INFOITEMS_.VALOR_DEB =0.00) THEN 
--						MCTYPE.MC_____CREMONLOC_B := VLR_CREDITO::NUMERIC;
--			END IF;
			
			MCTYPE.MC_____INDTIPMOV_B := 4;
			MCTYPE.MC_____INDMOVREV_B := 'N';
			MCTYPE.MC_____OBSERVACI_B := INFOITEMS_.DESCRIPCION;
			MCTYPE.MC_____FECHORCRE_B := NC_SUPEREFECTIVO.CREATION_DATE::TIMESTAMP;
			MCTYPE.MC_____AUTOCREA__B := 'COREFINTRA';
			MCTYPE.MC_____FEHOULMO__B := NC_SUPEREFECTIVO.CREATION_DATE::TIMESTAMP;
			MCTYPE.MC_____AUTULTMOD_B := '';
			MCTYPE.MC_____VALIMPCON_B := 0;
			MCTYPE.MC_____NUMERO_OPER_B := '';
			MCTYPE.TERCER_CODIGO____TIT____B := INFOCLIENTE.TIPO_DOC;
			MCTYPE.TERCER_NOMBCORT__B := INFOCLIENTE.NOMBRE_CORTO;
			MCTYPE.TERCER_NOMBEXTE__B := INFOCLIENTE.NOMBRE;
			MCTYPE.TERCER_APELLIDOS_B := INFOCLIENTE.APELLIDOS;
			MCTYPE.TERCER_CODIGO____TT_____B := INFOCLIENTE.CODIGO;
			MCTYPE.TERCER_DIRECCION_B := INFOCLIENTE.DIRECCION;
			MCTYPE.TERCER_CODIGO____CIUDAD_B := INFOCLIENTE.CODIGOCIU;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(INFOCLIENTE.TELEFONO)>15 THEN SUBSTR(INFOCLIENTE.TELEFONO,1,15) ELSE INFOCLIENTE.TELEFONO END;
			MCTYPE.TERCER_TIPOGIRO__B := 1;
			MCTYPE.TERCER_CODIGO____EF_____B := '';
			MCTYPE.TERCER_SUCURSAL__B := '';
			MCTYPE.TERCER_NUMECUEN__B := '';
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N',INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,'', 3);

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,'', 4)='S')THEN
				MCTYPE.MC_____NUMDOCSOP_B := INFOITEMS_.DOCUMENTO;
			ELSE
				MCTYPE.MC_____NUMDOCSOP_B := '';
			END IF;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,'', 5)::INT=1)THEN
				MCTYPE.MC_____NUMEVENC__B := 1;--NUMERO DE CUOTAS
			ELSE
				MCTYPE.MC_____NUMEVENC__B := NULL;
			END IF;

			--FUNCION PARA INSERTAR EL REGISTRO EN LA TABLA TEMPORAL DE FINTRA
			--RAISE NOTICE 'SW %',SW||' '||MCTYPE.MC_____NUMERO____B;
			SW:=CON.SP_INSERT_TABLE_MC_RECAUDO____(MCTYPE);
			SECUENCIA_INT :=SECUENCIA_INT+1;
			
			

		END LOOP;

		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		--IF CON.SP_VALIDACIONES(MCTYPE,'RECAUDO') ='N' THEN
		--	SW = 'N';
		--	CONTINUE;
		--END IF;

		
		IF(SW = 'S')then
			
			UPDATE
				CON.INGRESO_DETALLE
			SET
				PROCESADO_ICA='S'
			WHERE
				TIPO_DOCUMENTO=NC_SUPEREFECTIVO.TIPO_DOCUMENTO AND NUM_INGRESO=NC_SUPEREFECTIVO.NUM_INGRESO AND REG_STATUS = '';-- and DOCUMENTO = INFOITEMS_.DOCUMENTO;
				
				RAISE NOTICE 'SW %',SW||' '||NC_SUPEREFECTIVO.NUM_INGRESO;
		
		END IF;

		--SECUENCIA_INT:=1;

	END LOOP;
	
	--DELETE FROM CON.MC_RECAUDO____ WHERE  MC_____DEBMONLOC_B= 0 AND  MC_____CREMONLOC_B= 0;	
	
RETURN 'OK';

END;
$BODY$
  LANGUAGE PLPGSQL VOLATILE;
ALTER FUNCTION CON.INTERFAZ_MICROCREDITO_NOTA_SUPEREFECTIVO_APOTEOSYS()
  OWNER TO POSTGRES;