-- Function: con.interfaz_occidente_pago()

-- DROP FUNCTION con.interfaz_occidente_pago()

CREATE OR REPLACE FUNCTION con.interfaz_occidente_pago()
  RETURNS text AS
$BODY$


DECLARE
	r_factura record;
	r_registros record;
	infocliente record;
	SECUENCIA_GEN INTEGER;
	FECHADOC_ TEXT:='';
	MCTYPE CON.TYPE_INSERT_MC;
	SW TEXT:='N';
	CONSEC INTEGER:=1;	
	_ARRAYFECHAS varchar[];
	documento_ text:='';
--SELECT con.interfaz_occidente_pago()
--select * from con.mc_recaudo____ where procesado='N' AND mc_____codigo____pf_____b='2019' AND mc_____numero____period_b=2 and mc_____codigo____cd_____b='ICOC'
--delete from con.mc_fenalco____  where procesado='N' and mc_____codigo____cd_____b='CCOC'
BEGIN

	FOR r_factura IN

		SELECT 
			a.tipo_documento,
			a.num_ingreso,
			b.cuenta as cuenta_r,
			c.codigo_cuenta as cuenta_banco,
			a.nitcli,
			a.periodo,
			a.fecha_ingreso as fecha
		FROM 
			con.ingreso a 
		inner join
			con.ingreso_detalle b on(b.tipo_documento=a.tipo_documento and b.num_ingreso=a.num_ingreso)
		inner join
			banco c on(c.branch_code=a.branch_code and c.bank_account_no=a.bank_account_no)
		where 
			a.reg_status='' and 
			a.tipo_documento='ING' and 
			a.branch_code='BANCO OCCIDENTE' and 
			a.bank_account_no='CTE 802027144' and
			a.periodo=replace(substring(current_date,1,7),'-','') and 
			a.descripcion_ingreso ilike 'PAGO DE RECAUDOS%' and 
			coalesce(b.procesado_ica,'N')='N'
		group by
			a.tipo_documento,
			a.num_ingreso,
			b.cuenta,
			c.codigo_cuenta,
			a.nitcli,
			a.periodo,
			a.fecha_ingreso
		order by
			num_ingreso

/**
select negasoc, * from con.factura where documento='CA80627'
select * from documentos_neg_aceptado where cod_neg='MC07989'
select * from con.mc_recaudo____ where procesado='N'
update con.mc_recaudo____ set procesado='P' where procesado='N' and mc_____codigo____cd_____b!='ICOC'
*/		
	LOOP

		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.INTERFAZ_SECUENCIA_R_APOTEOSYS');
		
		_ARRAYFECHAS:=((select array(select 
					(substring(a.descripcion, strpos(a.descripcion,'.')-4, 4)||'-'||substring(a.descripcion, strpos(a.descripcion,'.')-7, 2)||'-'||substring(a.descripcion, strpos(a.descripcion,'.')-10, 2))::date as fecha
				from 
					con.factura a
				where 
					a.tipo_documento='FAC' and 
					a.documento ilike 'R0%' and
					a.documento in(select 
									factura 
									from 
									con.ingreso_detalle 
									where 
									tipo_documento='ING' and 
									num_ingreso=r_factura.num_ingreso)
								)
					)::varchar[]);

		FOR r_registros IN

			(select 
				a.tipo_documento,
				a.num_ingreso,
				a.codcli,
				--a.nitcli,
				r_factura.nitcli as nitcli,
				a.branch_code,
				a.bank_account_no,
				r_factura.cuenta_banco as cuenta,
				b.valor_ingreso as valor_debito,
				0 as valor_credito,
				--b.factura,
				r_factura.num_ingreso as doc_sop,
				c.negasoc,
				e.agencia,
				a.descripcion_ingreso as descripcion,
				con.interfaz_obtener_centro_costo_x_ingreso(a.num_ingreso,1) as cc,
				a.fecha_consignacion
			from 
				con.ingreso a
			inner join
				con.ingreso_detalle b on(b.tipo_documento=a.tipo_documento and b.num_ingreso=a.num_ingreso)
			left join
				con.factura c on(c.tipo_documento=b.tipo_doc and c.documento=b.documento)
			left join 
				negocios d on(d.cod_neg=c.negasoc)
			left join
				convenios e on(e.id_convenio=d.id_convenio)
			left join
				con.cmc_doc f on(f.tipodoc=c.tipo_documento and f.cmc=c.cmc)
			where 
				a.tipo_documento='ING' and 
				a.num_ingreso ilike 'IC%' and 
				a.fecha_consignacion = any (_ARRAYFECHAS) and 
				a.branch_code='CORRESPONSALES ' and 
				a.bank_account_no='BCO OCCIDENTE' 
			group by 
				a.tipo_documento,
				a.num_ingreso,
				a.codcli,
				a.nitcli,
				a.branch_code,
				a.bank_account_no,
				b.valor_ingreso,
				b.factura,
				f.cuenta,
				c.negasoc,
				e.agencia,
				a.descripcion_ingreso,
				a.fecha_consignacion)
			union all
			(select 
				a.tipo_documento,
				a.num_ingreso,
				a.codcli,
				--a.nitcli,
				r_factura.nitcli as nitcli,
				a.branch_code,
				a.bank_account_no,
				r_factura.cuenta_r as cuenta,
				0 as valor_debito,
				b.valor_ingreso as valor_credito,
				--b.factura,
				b.factura as doc_sop,
				c.negasoc,
				e.agencia,
				a.descripcion_ingreso as descripcion,
				con.interfaz_obtener_centro_costo_x_ingreso(a.num_ingreso,1) as cc,
				a.fecha_consignacion
			from 
				con.ingreso a
			inner join
				con.ingreso_detalle b on(b.tipo_documento=a.tipo_documento and b.num_ingreso=a.num_ingreso)
			left join
				con.factura c on(c.tipo_documento=b.tipo_doc and c.documento=b.documento)
			left join 
				negocios d on(d.cod_neg=c.negasoc)
			left join
				convenios e on(e.id_convenio=d.id_convenio)
			left join
				con.cmc_doc f on(f.tipodoc=c.tipo_documento and f.cmc=c.cmc)
			where 
				a.tipo_documento='ING' and 
				a.num_ingreso ilike 'IC%' and 
				a.fecha_consignacion = any (_ARRAYFECHAS) and 
				a.branch_code='CORRESPONSALES ' and 
				a.bank_account_no='BCO OCCIDENTE' 
			group by 
				a.tipo_documento,
				a.num_ingreso,
				a.codcli,
				a.nitcli,
				a.branch_code,
				a.bank_account_no,
				b.valor_ingreso,
				b.factura,
				f.cuenta,
				c.negasoc,
				e.agencia,
				a.descripcion_ingreso,
				a.fecha_consignacion)
				
		loop
		
			select INTO INFOCLIENTE
				(CASE
				WHEN tipo_iden ='CED' THEN 'CC'
				WHEN tipo_iden ='RIF' THEN 'CE'
				WHEN tipo_iden ='NIT' THEN 'NIT' ELSE
				'CC' END) as TERCER_CODIGO____TIT____B,
				(CASE
				WHEN tipo_iden in  ('RIF','NIT') THEN 'RCOM'  -->regimen comun
				WHEN tipo_iden in  ('CED')  THEN 'RSCP'
				else 'RSCP'
				END) as TERCER_CODIGO____TT_____B,
				(CASE
				WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				ELSE '08001' END) as TERCER_CODIGO____CIUDAD_B,
				(D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
				(D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
				d.nombre as TERCER_NOMBEXTE__B,
				d.direccion as TERCER_DIRECCION_B,
				d.telefono as TERCER_TELEFONO1_B
			from  NIT D --ON(D.CEDULA=prov.NIT)
			LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
			where cedula = r_registros.nitcli;
			
			documento_:='';
			
			if(r_registros.valor_credito=0)THEN
				documento_:=r_registros.doc_sop;
			ELSE
				documento_:=(select 
					a.documento
				from 
					con.factura a
				where 
					a.tipo_documento='FAC' and 
					a.documento ilike 'R0%' and
					a.documento in(select 
									factura 
									from 
									con.ingreso_detalle 
									where 
									tipo_documento='ING' and 
									num_ingreso=r_factura.num_ingreso)
					and (substring(a.descripcion, strpos(a.descripcion,'.')-4, 4)||'-'||substring(a.descripcion, strpos(a.descripcion,'.')-7, 2)||'-'||substring(a.descripcion, strpos(a.descripcion,'.')-10, 2))::date=r_registros.fecha_consignacion);
			END IF;

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(r_factura.fecha::DATE,1,7),'-','')=r_factura.PERIODO THEN r_factura.fecha::DATE ELSE CON.SP_FECHA_CORTE_MES(SUBSTRING(r_factura.PERIODO,1,4),SUBSTRING(r_factura.PERIODO,5,2)::INT)::DATE END ;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_OCCI', r_registros.TIPO_DOCUMENTO, r_registros.CUENTA,'', 6)='S')then
				MCTYPE.MC_____FECHEMIS__B = r_factura.fecha::DATE;
				--MCTYPE.MC_____FECHEMIS__B = recordNeg.CREATION_DATE::DATE;
				MCTYPE.MC_____FECHVENC__B = FECHADOC_::DATE;
			else
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';

			end if;

			MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
			MCTYPE.MC_____CODIGO____TD_____B := 'INGN' ;
			MCTYPE.MC_____CODIGO____CD_____B := 'ICOC';
			MCTYPE.MC_____SECUINTE__DCD____B := CONSEC  ;
			MCTYPE.MC_____FECHA_____B := FECHADOC_::DATE  ;
			MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN  ;
			MCTYPE.MC_____SECUINTE__B := CONSEC  ;
			MCTYPE.MC_____REFERENCI_B := r_factura.num_ingreso;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING(r_factura.periodo,1,4)::INT;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING(r_factura.periodo,5,2)::INT;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF' ;
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_OCCI', r_registros.TIPO_DOCUMENTO, r_registros.CUENTA,'', 1)  ;
			MCTYPE.MC_____CODIGO____CU_____B := r_registros.cc;
			MCTYPE.MC_____IDENTIFIC_TERCER_B :=  CASE WHEN CHAR_LENGTH(r_registros.nitcli)>10 THEN SUBSTR(r_registros.nitcli,1,10) ELSE r_registros.nitcli END;
			MCTYPE.MC_____DEBMONORI_B := 0  ;
			MCTYPE.MC_____CREMONORI_B := 0 ;
			MCTYPE.MC_____DEBMONLOC_B := r_registros.VALOR_DEBITO::NUMERIC  ;
			MCTYPE.MC_____CREMONLOC_B := r_registros.VALOR_CREDITO::NUMERIC  ;
			MCTYPE.MC_____INDTIPMOV_B := 4  ;
			MCTYPE.MC_____INDMOVREV_B := 'N'  ;
			MCTYPE.MC_____OBSERVACI_B := r_registros.DESCRIPCION ||'- Ingreso: '||r_registros.num_ingreso;
			MCTYPE.MC_____FECHORCRE_B := r_factura.fecha::TIMESTAMP  ;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN'  ;
			MCTYPE.MC_____FEHOULMO__B := r_factura.fecha::TIMESTAMP  ;
			MCTYPE.MC_____AUTULTMOD_B := ''  ;
			MCTYPE.MC_____VALIMPCON_B := 0  ;
			MCTYPE.MC_____NUMERO_OPER_B := r_factura.num_ingreso;
			MCTYPE.TERCER_CODIGO____TIT____B := infocliente.TERCER_CODIGO____TIT____B  ;
			MCTYPE.TERCER_NOMBCORT__B := infocliente.TERCER_NOMBCORT__B  ;
			MCTYPE.TERCER_NOMBEXTE__B := CASE WHEN CHAR_LENGTH(infocliente.TERCER_NOMBEXTE__B)>64 THEN SUBSTR(infocliente.TERCER_NOMBEXTE__B,1,64) ELSE infocliente.TERCER_NOMBEXTE__B END;
			MCTYPE.TERCER_APELLIDOS_B := infocliente.TERCER_APELLIDOS_B  ;
			MCTYPE.TERCER_CODIGO____TT_____B := infocliente.TERCER_CODIGO____TT_____B  ;
			MCTYPE.TERCER_DIRECCION_B := CASE WHEN CHAR_LENGTH(infocliente.TERCER_DIRECCION_B)>64 THEN SUBSTR(infocliente.TERCER_DIRECCION_B,1,64) ELSE infocliente.TERCER_DIRECCION_B END;
			MCTYPE.TERCER_CODIGO____CIUDAD_B := infocliente.TERCER_CODIGO____CIUDAD_B  ;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(infocliente.TERCER_TELEFONO1_B)>15 THEN SUBSTR(infocliente.TERCER_TELEFONO1_B,1,15) ELSE infocliente.TERCER_TELEFONO1_B END;
			MCTYPE.TERCER_TIPOGIRO__B := 1 ;
			MCTYPE.TERCER_CODIGO____EF_____B := ''  ;
			MCTYPE.TERCER_SUCURSAL__B := ''  ;
			MCTYPE.TERCER_NUMECUEN__B := ''  ;
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_OCCI', r_registros.TIPO_DOCUMENTO, r_registros.CUENTA,'', 3);
			--MCTYPE.MC_____NUMDOCSOP_B := REC_OS.NUMERO_OPERACION;
			MCTYPE.MC_____NUMEVENC__B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_OCCI', r_registros.TIPO_DOCUMENTO, r_registros.CUENTA,'', 5)::INT;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_OCCI', r_registros.TIPO_DOCUMENTO, r_registros.CUENTA,'', 4)='S')then
				MCTYPE.MC_____NUMDOCSOP_B :=documento_;
			else
				MCTYPE.MC_____NUMDOCSOP_B := '';
			end if;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_OCCI', r_registros.TIPO_DOCUMENTO, r_registros.CUENTA,'', 5)::int=1)then
				MCTYPE.MC_____NUMEVENC__B := 1;
			else
				MCTYPE.MC_____NUMEVENC__B := null;
			end if;

			raise notice 'MCTYPE: %', MCTYPE;

			-- Insertamos en la tabla de Apoteosys
			--FUNCION QUE TRANSACCION POR TIPO DE DOCUMENTO EN TABLA TEMPORAL EN FINTRA.
			SW:=CON.sp_insert_table_mc_recaudo____(MCTYPE);
			CONSEC:=CONSEC+1;
		END LOOP;

		CONSEC:=1;
		---------------------------------------------------------------------------

		--------------Revision de la transaccion-----------------
		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE, 'INGRESO_SE') ='N' THEN
			SW='N';

			--BORRAMOS EL COMPROBANTE DE EXT
			DELETE FROM CON.mc_recaudo____
			WHERE MC_____NUMERO____B = SECUENCIA_GEN AND MC_____CODIGO____CONTAB_B = 'FINT'
			 AND MC_____CODIGO____TD_____B = 'INGN' AND  MC_____CODIGO____CD_____B = 'ICOC';

			CONTINUE;
		END IF;

		-- ACTUALIZAMOS EL CAMPO DE APOTEOSYS DE LA CABECERA DEL CRéDITO PARA INDICAR QUE YA SE ENVíO
		IF(SW='S')THEN

			UPDATE
				con.ingreso_detalle
			SET
				procesado_ica='S'
			WHERE
				TIPO_DOCUMENTO=r_factura.tipo_documento and
				num_ingreso=r_factura.num_ingreso;

			SW:='N';
		END IF;

		CONSEC:=1;
		---------------------------------------------------------------

	END LOOP;

	RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION con.interfaz_occidente_pago()
  OWNER TO postgres;
