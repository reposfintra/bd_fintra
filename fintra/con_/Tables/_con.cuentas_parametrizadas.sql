-- Table: con.cuentas_parametrizadas

-- DROP TABLE con.cuentas_parametrizadas;

CREATE TABLE con.cuentas_parametrizadas
(
  cuenta character varying(10) NOT NULL DEFAULT ''::character varying primary key,
  linea character varying(20) NOT NULL DEFAULT ''::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE con.cuentas_parametrizadas
  OWNER TO postgres;
GRANT ALL ON TABLE con.cuentas_parametrizadas TO postgres;

