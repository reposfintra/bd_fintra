-- Table: con.traslado_negocios_renovados

-- DROP TABLE con.traslado_negocios_renovados;

CREATE TABLE con.traslado_negocios_renovados
(
  id serial NOT NULL,
  cod_neg character varying  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE con.traslado_negocios_renovados
  OWNER TO postgres;

alter table con.traslado_negocios_renovados add column capital_reversado varchar(1) not null default 'N';
alter table con.traslado_negocios_renovados add column if_reversado varchar(1) not null default 'N';
alter table con.traslado_negocios_renovados add column cm_reversado varchar(1) not null default 'N';