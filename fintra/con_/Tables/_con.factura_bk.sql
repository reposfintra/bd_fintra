-- Table: con.facturas_bk

-- DROP TABLE con.facturas_bk;

CREATE TABLE con.facturas_bk
(
  reg_status character varying(1),
  dstrct character varying(4),
  tipo_documento character varying(5),
  documento character varying(10),
  nit character varying(15),
  codcli character varying(10),
  concepto character varying(8),
  fecha_factura date,
  fecha_vencimiento date,
  fecha_ultimo_pago date,
  descripcion text,
  valor_factura moneda,
  valor_abono moneda,
  valor_saldo moneda,
  cmc character varying(6),
  periodo character varying(6),
  negasoc character varying(15),
  num_doc_fen character varying(20)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE con.facturas_bk
  OWNER TO postgres;
  
alter table con.facturas_bk add column creation_date timestamp default now();

alter table con.facturas_bk add column lote_pago character varying (50) not null  default '';

alter table con.facturas_bk add column logica_aplicacion character varying (50) not null  default '';

create  index  referencia_indx on con.ingreso_detalle (reg_status,referencia_1,tipo_referencia_1); 



