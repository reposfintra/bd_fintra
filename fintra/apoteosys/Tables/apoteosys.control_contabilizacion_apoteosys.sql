-- Table: apoteosys.control_contabilizacion_apoteosys

-- DROP TABLE apoteosys.control_contabilizacion_apoteosys;

create table apoteosys.control_contabilizacion_apoteosys (
id serial primary key,
reg_status character varying(1) NOT NULL DEFAULT ''::character varying,
dstrct character varying(4) NOT NULL DEFAULT ''::character varying,
descripcion  character varying(60) NOT NULL DEFAULT ''::character varying,
empresa  character varying(30) NOT NULL DEFAULT ''::character varying,
creation_date timestamp without time zone NOT NULL DEFAULT now(),
creation_user character varying(15) NOT NULL DEFAULT ''::character varying,
last_update timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
user_update character varying(20) NOT NULL DEFAULT ''::character varying
);