-- Function: apoteosys.interfaz_ias_micro_reversion_cm()

-- DROP FUNCTION apoteosys.interfaz_ias_micro_reversion_cm();

CREATE OR REPLACE FUNCTION apoteosys.interfaz_ias_micro_reversion_cm()
  RETURNS text AS
$BODY$

DECLARE

 /************************************************
  *DESCRIPCION: ESTA FUNCION BUSCA LAS NOTAS DE AJUSTE DE PAGO TOTAL DEUDA
  *AUTOR:=@DVALENCIA
  *FECHA CREACION:=2019-02-28
  *LAST_UPDATE
  *DESCRIPCION DE CAMBIOS Y FECHA
  ************************************************/

DOCUMENTOS_ RECORD;
INFOITEMS_ RECORD;
--INFOCLIENTE RECORD;
LONGITUD numeric;
SECUENCIA_GEN INTEGER;
SECUENCIA_INT integer:= 1;
FECHADOC_ VARCHAR:= '';
MCTYPE CON.TYPE_INSERT_MC;
SW TEXT:='N';
validaciones text;



BEGIN
	/**SACAMOS EL LISTADO DE NEGOCIOS*/
	FOR DOCUMENTOS_ in
	
			SELECT 
				I.PERIODO, 
				I.NITCLI,  
				I.tipo_documento,
				I.NUM_INGRESO, 
				I.VLR_INGRESO,
				I.BRANCH_CODE,
				I.BANK_ACCOUNT_NO, 
				ID.PROCESADO_ICA,
				CO.agencia,
				N.cod_neg,i.descripcion_ingreso,
				case when i.descripcion_ingreso like '%PAGO TOTAL%' then 'ACPT'
					else 'ACPA' end as concepto
 			FROM CON.INGRESO I
			INNER JOIN CON.INGRESO_DETALLE ID ON ID.NUM_INGRESO = I.NUM_INGRESO
			inner join CON.FACTURA F on F.documento=ID.documento
			inner join negocios N on N.COD_NEG=F.negasoc
			inner join convenios CO on CO.id_convenio=N.id_convenio
			INNER JOIN REL_UNIDADNEGOCIO_CONVENIOS RUC ON (CO.ID_CONVENIO = RUC.ID_CONVENIO)
			INNER JOIN UNIDAD_NEGOCIO UNEG ON (UNEG.ID = RUC.ID_UNID_NEGOCIO)
			where I.PERIODO = REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','') 
			--I.PERIODO BETWEEN '201901' AND '201905' 
			and id.tipo_documento = 'ICA' and  i.tipo_documento = 'ICA' and 
			    I.BRANCH_CODE = '' and I.BANK_ACCOUNT_NO = '' and UNEG.ID= 1
				and i.cuenta in ('27059803','27059813','I090130014134')
				--AND I.NUM_INGRESO in ('IA539333','IA539352','IA539367')
				AND COALESCE(ID.PROCESADO_ICA,'N') = 'N'
				and id. reg_status = '' and i.reg_status = ''
				and (i.descripcion_ingreso like '%PAGO TOTAL%' or i.descripcion_ingreso like '%ANTICIPADO%'  or i.descripcion_ingreso like '%INTICIPAGO%')
			GROUP BY I.PERIODO, I.NITCLI, I.tipo_documento, I.NUM_INGRESO, I.BRANCH_CODE, I.BANK_ACCOUNT_NO, ID.PROCESADO_ICA,I.VLR_INGRESO, CO.agencia, N.cod_neg,i.descripcion_ingreso
				
				--select * from apoteosys.interfaz_ias_micro_reversion_cm();

	LOOP
		
		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.INTERFAZ_SECUENCIA_ICA_APOTEOSYS');
		
		
		--raise notice 'paso: %',DOCUMENTOS_.NEG_ANTERIOR;

		MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
		MCTYPE.MC_____CODIGO____TD_____B := 'ICRN' ;
		MCTYPE.MC_____CODIGO____CD_____B := DOCUMENTOS_.concepto;
		MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN  ; --SECUENCIA GENERAL
		SECUENCIA_INT:=1;


		/**BUSCAMOS LA COMPLETA QUE CONFORMARA EL ASIENTO CONTABLE PARA MANDARLO A APOTEOSYS*/
		FOR INFOITEMS_ IN
				
		(select i.cuenta,	    
					   if.cod as  documento, 
					   'ICA' AS tipo_documento,
					   CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE i.nitcli END AS NIT,
					   id.valor_ingreso as valor_deb,
					   0::numeric as valor_credt,
					   i.num_ingreso ||': '|| i.descripcion_ingreso as descripcion,
					   i.creation_date::DATE,
					   i.creation_date::DATE AS fecha_vencimiento,
					   replace(substring(i.creation_date,1,7),'-','') as periodo,	   
					   (CASE
						WHEN tipo_iden ='CED' THEN 'CC'
						WHEN tipo_iden ='RIF' THEN 'CE'
						WHEN tipo_iden ='NIT' THEN 'NIT' ELSE
						'CC' END) as tipo_doc,
						(CASE
						WHEN tipo_iden in  ('RIF','NIT') THEN 'RCOM'  -->regimen comun
						WHEN tipo_iden in  ('CED')  THEN 'RSCP'
						else 'RSCP'
						END) as codigo,
						(CASE
						WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
						ELSE '08001' END) as codigociu,
						(D.NOMBRE1||' '||D.NOMBRE2) AS nombre_corto,
						(D.APELLIDO1||' '||D.APELLIDO2) AS apellidos,
						D.NOMBRE,
						D.DIRECCION,
						D.TELEFONO
				from con.ingreso i
				inner join con.ingreso_detalle id on id.num_ingreso=i.num_ingreso
				inner join con.factura f on f.documento=id.documento
				inner join ing_fenalco if on if.codneg=f.negasoc and if.cuota=f.num_doc_fen and if.tipodoc = 'CM'
				left join NIT D ON(D.CEDULA=i.nitcli)
				LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
				LEFT JOIN CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=i.nitcli)
				where i.num_ingreso = DOCUMENTOS_.NUM_INGRESO and f.documento like 'MC%'  --and if.reg_status !=''
				and id.reg_status = ''
				)
				union all
				(select id.cuenta,	    
					   id.documento, 
					   'ICA' AS tipo_documento,
					   CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE i.nitcli END AS NIT,
					   0::numeric as valor_deb,
					   id.valor_ingreso as valor_credt,
					   i.num_ingreso ||': '|| i.descripcion_ingreso as descripcion,
					   i.creation_date::DATE,
					   i.creation_date::DATE AS fecha_vencimiento,
					   replace(substring(i.creation_date,1,7),'-','') as periodo,	   
					   (CASE
						WHEN tipo_iden ='CED' THEN 'CC'
						WHEN tipo_iden ='RIF' THEN 'CE'
						WHEN tipo_iden ='NIT' THEN 'NIT' ELSE
						'CC' END) as tipo_doc,
						(CASE
						WHEN tipo_iden in  ('RIF','NIT') THEN 'RCOM'  -->regimen comun
						WHEN tipo_iden in  ('CED')  THEN 'RSCP'
						else 'RSCP'
						END) as codigo,
						(CASE
						WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
						ELSE '08001' END) as codigociu,
						(D.NOMBRE1||' '||D.NOMBRE2) AS nombre_corto,
						(D.APELLIDO1||' '||D.APELLIDO2) AS apellidos,
						D.NOMBRE,
						D.DIRECCION,
						D.TELEFONO
				from con.ingreso i
				inner join con.ingreso_detalle id on id.num_ingreso=i.num_ingreso
				inner join con.factura f on f.documento=id.documento
				left join NIT D ON(D.CEDULA=i.nitcli)
				LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
				LEFT JOIN CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=i.nitcli)
				where  i.num_ingreso = DOCUMENTOS_.NUM_INGRESO  and f.documento like 'MC%'
				and id.reg_status = ''
				)
				
		LOOP
		
			IF(INFOITEMS_.tipo_documento in ('FAC','FAP','NEG','IF','CM','ICA') AND CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_MICRO',INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, DOCUMENTOS_.agencia, 6)='S')THEN
				MCTYPE.MC_____FECHVENC__B = INFOITEMS_.fecha_vencimiento; --fecha vencimiento
				if (INFOITEMS_.fecha_vencimiento < INFOITEMS_.creation_date)then /** se valida si la fecha de vencimeinto es menor a la de creacion*/
					MCTYPE.MC_____FECHEMIS__B = INFOITEMS_.fecha_vencimiento; --fecha creacion
				else
					MCTYPE.MC_____FECHEMIS__B = INFOITEMS_.creation_date; --fecha creacion
				end if;
			ELSE
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';
			END IF;

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(INFOITEMS_.creation_date,1,7),'-','') = INFOITEMS_.periodo THEN INFOITEMS_.creation_date::DATE ELSE con.sp_fecha_corte_mes(SUBSTRING(INFOITEMS_.periodo,1,4), SUBSTRING(INFOITEMS_.periodo,5,2)::INT)::DATE END;
			MCTYPE.MC_____FECHA_____B := FECHADOC_;
			MCTYPE.MC_____SECUINTE__DCD____B := SECUENCIA_INT;--secuencia interna
			MCTYPE.MC_____SECUINTE__B := SECUENCIA_INT;--secuencia interna
			MCTYPE.MC_____REFERENCI_B := DOCUMENTOS_.COD_NEG;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING( INFOITEMS_.periodo,1,4)::INT;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING( INFOITEMS_.periodo,5,2)::INT;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF';
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_MICRO', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, DOCUMENTOS_.agencia, 1);
			MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_MICRO', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, DOCUMENTOS_.agencia, 2);
			MCTYPE.MC_____IDENTIFIC_TERCER_B := INFOITEMS_.NIT;
			MCTYPE.MC_____DEBMONORI_B := 0;
			MCTYPE.MC_____CREMONORI_B := 0;
			MCTYPE.MC_____DEBMONLOC_B := INFOITEMS_.valor_deb::NUMERIC;
			MCTYPE.MC_____CREMONLOC_B := INFOITEMS_.valor_credt::NUMERIC;
			MCTYPE.MC_____INDTIPMOV_B := 4;
			MCTYPE.MC_____INDMOVREV_B := 'N';
			MCTYPE.MC_____OBSERVACI_B := INFOITEMS_.descripcion || ' MICROCREDITO';
			MCTYPE.MC_____FECHORCRE_B := INFOITEMS_.creation_date::TIMESTAMP;
			MCTYPE.MC_____AUTOCREA__B := 'COREFINTRA';
			MCTYPE.MC_____FEHOULMO__B := INFOITEMS_.creation_date::TIMESTAMP;
			MCTYPE.MC_____AUTULTMOD_B := '';
			MCTYPE.MC_____VALIMPCON_B := 0;
			MCTYPE.MC_____NUMERO_OPER_B := '';
			MCTYPE.TERCER_CODIGO____TIT____B := INFOITEMS_.tipo_doc;
			MCTYPE.TERCER_NOMBCORT__B := SUBSTR(INFOITEMS_.nombre_corto,1,32);
			MCTYPE.TERCER_NOMBEXTE__B := SUBSTR (INFOITEMS_.nombre,1,64);
			MCTYPE.TERCER_APELLIDOS_B := SUBSTR(INFOITEMS_.apellidos,1,32);
			MCTYPE.TERCER_CODIGO____TT_____B := INFOITEMS_.codigo;
			MCTYPE.TERCER_DIRECCION_B := SUBSTR(INFOITEMS_.direccion,1,64);
			MCTYPE.TERCER_CODIGO____CIUDAD_B := INFOITEMS_.codigociu;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(INFOITEMS_.telefono)>15 THEN SUBSTR(INFOITEMS_.telefono,1,15) ELSE INFOITEMS_.telefono END;
			MCTYPE.TERCER_TIPOGIRO__B := 1;
			MCTYPE.TERCER_CODIGO____EF_____B := '';
			MCTYPE.TERCER_SUCURSAL__B := '';
			MCTYPE.TERCER_NUMECUEN__B := '';
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_MICRO',INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, DOCUMENTOS_.agencia, 3);
			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_MICRO', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, DOCUMENTOS_.agencia, 4)='S')THEN

				MCTYPE.MC_____NUMDOCSOP_B := INFOITEMS_.documento;
			ELSE
				MCTYPE.MC_____NUMDOCSOP_B := '';
			END IF;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('RECAUDO_MICRO', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, DOCUMENTOS_.agencia, 5)::INT=1)THEN
				MCTYPE.MC_____NUMEVENC__B := 1;
			ELSE
				MCTYPE.MC_____NUMEVENC__B := NULL;
			END IF;

			--FUNCION PARA INSERTAR EL REGISTRO EN LA TABLA TEMPORAL DE FINTRA
			SW:=CON.SP_INSERT_TABLE_MC_RECAUDO____(MCTYPE);
			SECUENCIA_INT :=SECUENCIA_INT+1;
 
			--raise notice 'valor_deb: % valor_credt: %',INFOITEMS_.valor_deb,INFOITEMS_.valor_credt;
		END LOOP;

		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE,'RECAUDO') ='N' THEN
			SW = 'N';
			CONTINUE;
		END IF;

		if(SW = 'S')then
			--SELECT * FROM OPAV.SL_TRASLADO_FACTURAS_APOTEOSYS
			UPDATE
				con.ingreso_detalle
			SET
				PROCESADO_ICA='S'
			WHERE
				TIPO_DOCUMENTO=DOCUMENTOS_.tipo_documento and
				num_ingreso=DOCUMENTOS_.num_ingreso;
		end if;

	END LOOP;

RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apoteosys.interfaz_ias_micro_reversion_cm()
  OWNER TO postgres;
