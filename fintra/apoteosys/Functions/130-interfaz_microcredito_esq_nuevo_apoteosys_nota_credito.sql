-- Function: apoteosys.interfaz_microcredito_esq_nuevo_nota_credito()

-- DROP FUNCTION apoteosys.interfaz_microcredito_esq_nuevo_nota_credito();

CREATE OR REPLACE FUNCTION apoteosys.interfaz_microcredito_esq_nuevo_nota_credito()
  RETURNS text AS
$BODY$

DECLARE

 /************************************************
  *DESCRIPCION: ESTA FUNCION BUSCA TODOS LO NEGOCIOS MICROCREDITOS DEL ESQUEMA NUEVO Y
  *CREA EL ASIENTO CONTABLE DEL EGRESO GENERADO.
  *AUTOR:=@MMEDINA
  *FECHA CREACION:=2017-07-18
  *LAST_UPDATE
  *DESCRIPCION DE CAMBIOS Y FECHA
  ************************************************/

NEGOCIO_ RECORD;
INFOITEMS_ RECORD;
INFOCLIENTE RECORD;
LONGITUD NUMERIC;
SECUENCIA_GEN INTEGER;
SECUENCIA_INT INTEGER:= 1;
CUENTAS_ATL VARCHAR[] := '{23051102, 23050129}';
CUENTAS_COR VARCHAR[] := '{23051106, 23050129}';
CUENTAS_DEF VARCHAR[] := '{}';
FECHADOC_ VARCHAR:= '';
MCTYPE CON.TYPE_INSERT_MC;
SW TEXT:='N';
VALIDACIONES TEXT;


BEGIN
	/**SACAMOS EL LISTADO DE NEGOCIOS*/
	FOR NEGOCIO_ in
	
		SELECT
			NEG.COD_NEG,
			CXP.DOCUMENTO as DOC_CXP,
			NEG.COD_CLI as CLIENTE,
			NEG.FECHA_NEGOCIO,
			CONV.AGENCIA,
			NEG.PERIODO,
			F_DESEM::DATE as F_DESEM,
			PROCESADO_MC,
			SP_UNEG_NEGOCIO_NAME(NEG.COD_NEG) AS UNIDAD_NEGOCIO
		FROM NEGOCIOS NEG
		INNER JOIN CONVENIOS CONV ON (CONV.ID_CONVENIO = NEG.ID_CONVENIO)
		INNER JOIN REL_UNIDADNEGOCIO_CONVENIOS RUC ON (CONV.ID_CONVENIO = RUC.ID_CONVENIO)
		INNER JOIN UNIDAD_NEGOCIO UNEG ON (UNEG.ID = RUC.ID_UNID_NEGOCIO)
		inner join FIN.CXP_DOC CXP on CXP.documento_relacionado=NEG.cod_neg
		WHERE NEG.ESTADO_NEG in ('T','L') AND UNEG.ID = '1'
		AND PROCESADO_MC = 'S'
		and procesado_egreso ='P'
		AND COD_NEG NOT IN (SELECT NEGOCIO_REESTRUCTURACION FROM REL_NEGOCIOS_REESTRUCTURACION )
		AND COD_NEG NOT IN (SELECT COD_NEG FROM TEM.NEGOCIOS_FACTURACION_OLD)
		and CXP.documento not in (select documento from apoteosys.traslado_facturas_apoteosys)
		AND REPLACE(SUBSTRING(NEG.F_DESEM,1,7),'-','')=REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','')
		--AND COD_NEG='MC16006'
		--and cxp.documento='MP17481'
		--AND NEG.PERIODO ='201809'
		AND COD_NEG LIKE 'MC%'
		AND DOCUMENTO ILIKE 'MP%'
		and CXP.referencia_2 = ''
		ORDER BY CONV.AGENCIA
		
		--select apoteosys.interfaz_microcredito_esq_nuevo_nota_credito()

	LOOP
		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.INTERFAZ_SECUENCIA_OPER_APOTEOSYS');
		--SELECT INTO SECUENCIA_GEN NEXTVAL('CON.INTERFAZ_SECUENCIA_OPER_APOTEOSYS');

		/**SEGUN LA AGENCIA ASIGNAMOS EL ARRAY DE CUENTAS*/
		IF(NEGOCIO_.AGENCIA = 'ATL')THEN
			CUENTAS_DEF:= CUENTAS_ATL;
		ELSIF(NEGOCIO_.AGENCIA IN ('COR', 'SUC'))THEN
			CUENTAS_DEF:= CUENTAS_COR;
		END IF;
		
		SECUENCIA_INT:=1;
		--RAISE NOTICE 'AGENCIA %,CUENTAS_DEF: %',NEGOCIO_.AGENCIA,CUENTAS_DEF;

		/**BUSCAMOS LA INFORMACION DEL CLIENTE*/

		MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT';
		MCTYPE.MC_____CODIGO____TD_____B := 'CXPN';
		MCTYPE.MC_____CODIGO____CD_____B := 'NCMP';
		MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN; --SECUENCIA GENERAL

		/**BUSCAMOS LA INFORMACION COMPLETA QUE CONFORMARA EL ASIENTO CONTABLE PARA MANDARLO A APOTEOSYS*/
		FOR INFOITEMS_ IN
			
			(SELECT
				'NC DESCUENTOS' AS DESCR,
				substring(CXPN.DOCUMENTO,9) AS NUM_DOC_FEN,
				CXP.documento_relacionado AS DOCUMENTO,
				CXP.TIPO_DOCUMENTO,
				NEGOCIO_.CLIENTE AS NIT, ---NEG.COD_CLI
				0::NUMERIC AS VALOR_DEB,
				CXP.VLR_NETO AS VALOR_CREDT,
				CXP.DOCUMENTO || ': ' || CXP.DESCRIPCION as DESCRIPCION,
				NEGOCIO_.F_DESEM AS CREATION_DATE,
				NEGOCIO_.F_DESEM AS FECHA_VENCIMIENTO,
				REPLACE(SUBSTRING(NEGOCIO_.F_DESEM,1,7),'-','') AS PERIODO,
				CUENTAS_DEF[2]AS CUENTA
			FROM FIN.CXP_DOC CXP
			inner join FIN.CXP_ITEMS_DOC CXPN on CXPN.DOCUMENTO=CXP.documento
			WHERE CXP.documento_relacionado = NEGOCIO_.DOC_CXP
			AND CXP.DOCUMENTO ILIKE 'MP%'
			AND CXP.REG_STATUS = ''
			AND CXP.PERIODO != ''
			and CXP.DESCRIPCION not in ('NC - DESCUENTO POR CHEQUE','NC - VIDA DEUDORES','NC - DEUDORES') 
			AND CXPN.DESCRIPCION not in ('NC - DESCUENTO POR CHEQUE','NC - VIDA DEUDORES','NC - DEUDORES')
			group by CXPN.DOCUMENTO,CXP.documento_relacionado, cxp.tipo_documento, vlr_neto, cxp.descripcion,  cxp.documento 
			order by CXP.DOCUMENTO)	
			UNION all
			(SELECT
				'CXP CLIENTES MICRO ATLÁNTICO - NC COMISION' AS DESCR,
				substring(CXPN.DOCUMENTO,9) AS NUM_DOC_FEN,
				CXP.documento_relacionado AS DOCUMENTO,
				CXP.TIPO_DOCUMENTO,
				NEGOCIO_.CLIENTE AS NIT, ---NEG.COD_CLI
				CXP.VLR_NETO AS VALOR_DEB,
				0::NUMERIC AS VALOR_CREDT,
				CXP.DOCUMENTO || ': ' || CXP.DESCRIPCION as DESCRIPCION,
				NEGOCIO_.F_DESEM AS CREATION_DATE,
				NEGOCIO_.F_DESEM AS FECHA_VENCIMIENTO,
				REPLACE(SUBSTRING(NEGOCIO_.F_DESEM,1,7),'-','') AS PERIODO,
				CUENTAS_DEF[1]AS CUENTA
			FROM FIN.CXP_DOC CXP
			inner join FIN.CXP_ITEMS_DOC CXPN on CXPN.DOCUMENTO=CXP.documento
			WHERE CXP.documento_relacionado = NEGOCIO_.DOC_CXP
			AND CXP.DOCUMENTO ILIKE 'MP%'
			AND CXP.REG_STATUS = ''
			AND CXP.PERIODO != ''
			and CXP.DESCRIPCION not in ('NC - DESCUENTO POR CHEQUE','NC - VIDA DEUDORES','NC - DEUDORES') 
			AND CXPN.DESCRIPCION not in ('NC - DESCUENTO POR CHEQUE','NC - VIDA DEUDORES','NC - DEUDORES')
			group by CXPN.DOCUMENTO,CXP.documento_relacionado, cxp.tipo_documento, vlr_neto, cxp.descripcion,  cxp.documento 
			order by CXP.DOCUMENTO
			)
			
		loop
			RAISE NOTICE 'CUENTA %,DOCUMENTO %, DESCR: %',INFOITEMS_.CUENTA,INFOITEMS_.DOCUMENTO, INFOITEMS_.DESCRIPCION;
			IF (INFOITEMS_.VALOR_DEB !=0 AND  INFOITEMS_.VALOR_CREDT =0) OR (INFOITEMS_.VALOR_DEB =0 AND INFOITEMS_.VALOR_CREDT !=0) THEN

				SELECT INTO INFOCLIENTE
				(CASE
				WHEN TIPO_DOC ='CED' THEN 'CC'
				WHEN TIPO_DOC ='RIF' THEN 'CE'
				WHEN TIPO_DOC ='NIT' THEN 'NIT' ELSE
				'CC' END) AS TIPO_DOC,
				(CASE
				WHEN GRAN_CONTRIBUYENTE ='N' AND AGENTE_RETENEDOR ='N' THEN 'RCOM'
				WHEN GRAN_CONTRIBUYENTE ='N' AND AGENTE_RETENEDOR ='S' THEN 'RCAU'
				WHEN GRAN_CONTRIBUYENTE ='S' AND AGENTE_RETENEDOR ='N' THEN 'GCON'
				WHEN GRAN_CONTRIBUYENTE ='S' AND AGENTE_RETENEDOR ='S' THEN 'GCAU'
				ELSE 'PNAL' END) AS CODIGO,
				(CASE
				WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				ELSE '08001' END) AS CODIGOCIU,
				(D.NOMBRE1||' '||D.NOMBRE2) AS NOMBRE_CORTO,
				(D.APELLIDO1||' '||D.APELLIDO2) AS APELLIDOS,
				*
				FROM PROVEEDOR PROV
				LEFT JOIN NIT D ON(D.CEDULA=PROV.NIT)
				LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
				WHERE NIT = INFOITEMS_.NIT;


				IF(INFOITEMS_.TIPO_DOCUMENTO IN ('FAC','FAP','NEG','NC') AND CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N',INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,NEGOCIO_.AGENCIA, 6)='S')THEN
					MCTYPE.MC_____FECHVENC__B = INFOITEMS_.FECHA_VENCIMIENTO; --FECHA VENCIMIENTO
						IF (INFOITEMS_.FECHA_VENCIMIENTO < INFOITEMS_.CREATION_DATE)THEN /** SE VALIDA SI LA FECHA DE VENCIMEINTO ES MENOR A LA DE CREACION*/
							MCTYPE.MC_____FECHEMIS__B = INFOITEMS_.FECHA_VENCIMIENTO; --FECHA CREACION
						ELSE
							--RAISE NOTICE 'SIMPRE DEBE ENTRA EN ESTA CONDICION PARA FECHA DE EMISION';
							MCTYPE.MC_____FECHEMIS__B = INFOITEMS_.CREATION_DATE; --FECHA CREACION
						END IF;
				ELSE
					MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
					MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';
				END IF;

				--FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(INFOITEMS_.CREATION_DATE,1,7),'-','') = INFOITEMS_.PERIODO THEN INFOITEMS_.CREATION_DATE::DATE ELSE CON.SP_FECHA_CORTE_MES(SUBSTRING(INFOITEMS_.PERIODO,1,4), SUBSTRING(INFOITEMS_.PERIODO,5,2)::INT)::DATE END;
				MCTYPE.MC_____FECHA_____B := INFOITEMS_.CREATION_DATE;--CASE WHEN (INFOITEMS_.CREATION_DATE::DATE > FECHADOC_::DATE AND REPLACE(SUBSTRING(INFOITEMS_.CREATION_DATE,1,7),'-','') = INFOITEMS_.PERIODO)  THEN INFOITEMS_.CREATION_DATE::DATE ELSE FECHADOC_::DATE END;
				MCTYPE.MC_____SECUINTE__DCD____B := SECUENCIA_INT;--SECUENCIA INTERNA
				MCTYPE.MC_____SECUINTE__B := SECUENCIA_INT;--SECUENCIA INTERNA
				MCTYPE.MC_____REFERENCI_B := NEGOCIO_.COD_NEG;
				MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING( INFOITEMS_.PERIODO,1,4)::INT;
				MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING( INFOITEMS_.PERIODO,5,2)::INT;
				MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF';
				MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,NEGOCIO_.AGENCIA, 1);
				MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,NEGOCIO_.AGENCIA, 2);
				MCTYPE.MC_____IDENTIFIC_TERCER_B := CASE WHEN CHAR_LENGTH(INFOITEMS_.NIT)>9 AND INFOCLIENTE.TIPO_DOC='NIT' THEN SUBSTR(INFOITEMS_.NIT,1,9) ELSE INFOITEMS_.NIT END;
				MCTYPE.MC_____DEBMONORI_B := 0;
				MCTYPE.MC_____CREMONORI_B := 0;
				MCTYPE.MC_____DEBMONLOC_B := INFOITEMS_.VALOR_DEB::NUMERIC;
				MCTYPE.MC_____CREMONLOC_B := INFOITEMS_.VALOR_CREDT::NUMERIC;
				MCTYPE.MC_____INDTIPMOV_B := 4;
				MCTYPE.MC_____INDMOVREV_B := 'N';
				MCTYPE.MC_____OBSERVACI_B := INFOITEMS_.DESCRIPCION;
				MCTYPE.MC_____FECHORCRE_B := INFOITEMS_.CREATION_DATE::TIMESTAMP;
				MCTYPE.MC_____AUTOCREA__B := 'ADMIN';
				MCTYPE.MC_____FEHOULMO__B := INFOITEMS_.CREATION_DATE::TIMESTAMP;
				MCTYPE.MC_____AUTULTMOD_B := '';
				MCTYPE.MC_____VALIMPCON_B := 0;
				MCTYPE.MC_____NUMERO_OPER_B := '';
				MCTYPE.TERCER_CODIGO____TIT____B := INFOCLIENTE.TIPO_DOC;
				MCTYPE.TERCER_NOMBCORT__B := INFOCLIENTE.NOMBRE_CORTO;
				MCTYPE.TERCER_NOMBEXTE__B := INFOCLIENTE.NOMBRE;
				MCTYPE.TERCER_APELLIDOS_B := INFOCLIENTE.APELLIDOS;
				MCTYPE.TERCER_CODIGO____TT_____B := INFOCLIENTE.CODIGO;
				MCTYPE.TERCER_DIRECCION_B := INFOCLIENTE.DIRECCION;
				MCTYPE.TERCER_CODIGO____CIUDAD_B := INFOCLIENTE.CODIGOCIU;
				MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(INFOCLIENTE.TELEFONO)>15 THEN SUBSTR(INFOCLIENTE.TELEFONO,1,15) ELSE INFOCLIENTE.TELEFONO END;
				MCTYPE.TERCER_TIPOGIRO__B := 1;
				MCTYPE.TERCER_CODIGO____EF_____B := '';
				MCTYPE.TERCER_SUCURSAL__B := '';
				MCTYPE.TERCER_NUMECUEN__B := '';
				MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N',INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,NEGOCIO_.AGENCIA, 3);

				IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,NEGOCIO_.AGENCIA, 4)='S')THEN --->OJO

					MCTYPE.MC_____NUMDOCSOP_B := INFOITEMS_.DOCUMENTO; -->FACTURA
				ELSE
					MCTYPE.MC_____NUMDOCSOP_B := '';
				END IF;

				IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.TIPO_DOCUMENTO, INFOITEMS_.CUENTA,NEGOCIO_.AGENCIA, 5)::INT=1)THEN
					MCTYPE.MC_____NUMEVENC__B := 1;
				ELSE
					MCTYPE.MC_____NUMEVENC__B := NULL;
				END IF;

				--FUNCION PARA INSERTAR EL REGISTRO EN LA TABLA TEMPORAL DE FINTRA
				RAISE NOTICE 'SW %',SW||' '||NEGOCIO_.COD_NEG;

					SW:=apoteosys.SP_INSERT_TABLE_MC_MICRO____(MCTYPE);
					SECUENCIA_INT :=SECUENCIA_INT+1;

			END IF;

		END LOOP;

		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE,'MICROCREDITO') ='N' THEN
			--SW = 'N';
			--CONTINUE;
		END IF;

		IF(SW = 'S')THEN
			INSERT INTO apoteosys.traslado_facturas_apoteosys(cod_neg, tipo_documento, documento, agencia, periodo, fecha_negocio, unidad_negocio)
			VALUES (NEGOCIO_.COD_NEG, 'NC', NEGOCIO_.DOC_CXP, NEGOCIO_.AGENCIA, NEGOCIO_.PERIODO, NEGOCIO_.FECHA_NEGOCIO, NEGOCIO_.UNIDAD_NEGOCIO);
		END IF;

		--SECUENCIA_INT:=1;

	END LOOP;
RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apoteosys.interfaz_microcredito_esq_nuevo_nota_credito()
  OWNER TO postgres;
  
  --SELECT apoteosys.interfaz_microcredito_esq_nuevo_nota_credito()
  
  
