-- Function: apoteosys.interfaz_microcredito_refinanciacion_ia_plan_dia()

-- DROP FUNCTION apoteosys.interfaz_microcredito_refinanciacion_ia_plan_dia();

CREATE OR REPLACE FUNCTION apoteosys.interfaz_microcredito_refinanciacion_ia_plan_dia()
  RETURNS text AS
$BODY$

DECLARE

 /************************************************
  *DESCRIPCION: ESTA FUNCION BUSCA TODOS LO NEGOCIOS EDUCATIVOS CON AVALES INCLUIDOS.
  *AUTOR:=@DVALENCIA
  *FECHA CREACION:=2018-12 -17
  *LAST_UPDATE
  *DESCRIPCION DE CAMBIOS Y FECHA
  ************************************************/

NEGOCIO_ RECORD;
INFOITEMS_ RECORD;
--INFOCLIENTE RECORD;
LONGITUD numeric;
SECUENCIA_GEN INTEGER;
SECUENCIA_INT integer:= 1;
FECHADOC_ VARCHAR:= '';
MCTYPE CON.TYPE_INSERT_MC;
SW TEXT:='N';
validaciones text;

BEGIN
	/**SACAMOS EL LISTADO DE NEGOCIOS*/
	FOR NEGOCIO_ in
	
				SELECT  NEG.COD_NEG,
						NEG.COD_CLI,
						NEG.FECHA_NEGOCIO,
						CONV.AGENCIA,
						UNEG.DESCRIPCION AS UNIDAD_NEG,
						REPLACE(SUBSTRING(NEG.F_DESEM,1,7),'-','') AS PERIODO_DESEM,
						neg.ESTADO_NEG,
						ctrl.creation_date::date as  fecha_refinanciacion,
						REPLACE(SUBSTRING(ctrl.creation_date,1,7),'-','') AS PERIODO_REFINANCIACION,
						ctrl.key_ref as lote,
						ctrl.nota_ajuste						
				FROM NEGOCIOS NEG
				INNER JOIN CONVENIOS CONV ON (CONV.ID_CONVENIO = NEG.ID_CONVENIO)
				INNER JOIN REL_UNIDADNEGOCIO_CONVENIOS RUC ON (CONV.ID_CONVENIO = RUC.ID_CONVENIO)
				INNER JOIN UNIDAD_NEGOCIO UNEG ON (UNEG.ID = RUC.ID_UNID_NEGOCIO)
				inner join administrativo.control_refinanciacion_negocios ctrl on ctrl.cod_neg=neg.cod_neg
				WHERE NEG.ESTADO_NEG IN ('T') AND UNEG.ID = '1'
				AND neg.PROCESADO_MC = 'S'
				AND NEG.NEGOCIO_REL = ''
				and ctrl.procesado_apot = 'N'
				--AND NEG.COD_NEG NOT IN (SELECT NEGOCIO_REESTRUCTURACION FROM REL_NEGOCIOS_REESTRUCTURACION)
				AND REPLACE(SUBSTRING(ctrl.creation_date,1,7),'-','')=REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','')
				and ctrl.key_ref not in (select documento from apoteosys.traslado_facturas_apoteosys )
				and ctrl.nota_ajuste != ''
				and ctrl.estado = 'S'
				--AND REPLACE(SUBSTRING(ctrl.creation_date,1,7),'-','')=  '201808'		
				--AND NEG.COD_NEG IN ('MC17412','MC17471','MC17572','MC18331','MC17701','MC17750','MC17124','MC17629','MC17025')
				
				--select apoteosys.interfaz_microcredito_refinanciacion_ia_plan_dia();
				


	LOOP
		
		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.INTERFAZ_SECUENCIA_OPER_APOTEOSYS');
		
		raise notice 'paso: %',NEGOCIO_.COD_NEG;

		MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
		MCTYPE.MC_____CODIGO____TD_____B := 'OPER' ;
		MCTYPE.MC_____CODIGO____CD_____B := 'PLDI';
		MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN  ; --SECUENCIA GENERAL
		SECUENCIA_INT:=1;


		/**BUSCAMOS LA COMPLETA QUE CONFORMARA EL ASIENTO CONTABLE PARA MANDARLO A APOTEOSYS*/
		FOR INFOITEMS_ IN
				
		(select i.cuenta,	    
					   id.documento, 
					   'FAC' AS tipo_documento,
					   CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE i.nitcli END AS NIT,
					   0::numeric as valor_deb,
					   id.valor_ingreso as valor_credt,
					   i.num_ingreso ||': '|| id.descripcion as descripcion,
					   i.creation_date::DATE,
					   i.creation_date::DATE AS fecha_vencimiento,
					   replace(substring(i.creation_date,1,7),'-','') as periodo,	   
					   (CASE
						WHEN tipo_iden ='CED' THEN 'CC'
						WHEN tipo_iden ='RIF' THEN 'CE'
						WHEN tipo_iden ='NIT' THEN 'NIT' ELSE
						'CC' END) as tipo_doc,
						(CASE
						WHEN tipo_iden in  ('RIF','NIT') THEN 'RCOM'  -->regimen comun
						WHEN tipo_iden in  ('CED')  THEN 'RSCP'
						else 'RSCP'
						END) as codigo,
						(CASE
						WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
						ELSE '08001' END) as codigociu,
						(D.NOMBRE1||' '||D.NOMBRE2) AS nombre_corto,
						(D.APELLIDO1||' '||D.APELLIDO2) AS apellidos,
						D.NOMBRE,
						D.DIRECCION,
						D.TELEFONO
				from con.ingreso i
				inner join con.ingreso_detalle id on id.num_ingreso=i.num_ingreso
				left join NIT D ON(D.CEDULA=i.nitcli)
				LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
				LEFT JOIN CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=i.nitcli)
				where i.num_ingreso = NEGOCIO_.nota_ajuste				
				)
				union all				
				(SELECT
						cmc.CUENTA,
						f.documento,
						'FAC' AS tipo_documento,
						CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE f.nit END AS NIT,						
						valor_factura as valor_deb,
						0::numeric as valor_credt,
						'NUEVA FACTURA PLAN AL DIA' as DESCRIPCION,
						f.creation_date::date,
						f.fecha_vencimiento::DATE,
						replace(substring(f.creation_date,1,7),'-','') as periodo,
						(CASE
						WHEN tipo_iden ='CED' THEN 'CC'
						WHEN tipo_iden ='RIF' THEN 'CE'
						WHEN tipo_iden ='NIT' THEN 'NIT' ELSE
						'CC' END) as tipo_doc,
						(CASE
						WHEN tipo_iden in  ('RIF','NIT') THEN 'RCOM'  -->regimen comun
						WHEN tipo_iden in  ('CED')  THEN 'RSCP'
						else 'RSCP'
						END) as codigo,
						(CASE
						WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
						ELSE '08001' END) as codigociu,
						(D.NOMBRE1||' '||D.NOMBRE2) AS nombre_corto,
						(D.APELLIDO1||' '||D.APELLIDO2) AS apellidos,
						D.NOMBRE,
						D.DIRECCION,
						D.TELEFONO
				from con.factura f
				inner join con.cmc_doc cmc on cmc.cmc=f.cmc
				inner join negocios neg on ( neg.cod_neg = f.negasoc)
				left join NIT D ON(D.CEDULA=f.nit)
				LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
				LEFT JOIN CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=f.nit)
				WHERE f.documento in (select (SUBSTRING(documento,1,LENGTH(documento)-2)||case when ITEM<10 then '0'||ITEM else ITEM::varchar end ) as documento
	    							  from administrativo.proyeccion_refinanciacion_negocios where cod_neg=NEGOCIO_.cod_neg and color='S')				
				and f.reg_status = '' and cmc.tipodoc = 'FAC'				
				)
				
				
		LOOP
		
			IF(INFOITEMS_.tipo_documento in ('FAC','FAP','NEG','IF','CM','ICA') AND CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N',INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 6)='S')THEN
				MCTYPE.MC_____FECHVENC__B = INFOITEMS_.fecha_vencimiento; --fecha vencimiento
				if (INFOITEMS_.fecha_vencimiento < INFOITEMS_.creation_date)then /** se valida si la fecha de vencimeinto es menor a la de creacion*/
					MCTYPE.MC_____FECHEMIS__B = INFOITEMS_.fecha_vencimiento; --fecha creacion
				else
					MCTYPE.MC_____FECHEMIS__B = INFOITEMS_.creation_date; --fecha creacion
				end if;
			ELSE
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';
			END IF;

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(INFOITEMS_.creation_date,1,7),'-','') = INFOITEMS_.periodo THEN INFOITEMS_.creation_date::DATE ELSE con.sp_fecha_corte_mes(SUBSTRING(INFOITEMS_.periodo,1,4), SUBSTRING(INFOITEMS_.periodo,5,2)::INT)::DATE END;
			MCTYPE.MC_____FECHA_____B := FECHADOC_;
			MCTYPE.MC_____SECUINTE__DCD____B := SECUENCIA_INT;--secuencia interna
			MCTYPE.MC_____SECUINTE__B := SECUENCIA_INT;--secuencia interna
			MCTYPE.MC_____REFERENCI_B := NEGOCIO_.COD_NEG;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING( INFOITEMS_.periodo,1,4)::INT;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING( INFOITEMS_.periodo,5,2)::INT;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF';
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 1);
			MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 2);
			MCTYPE.MC_____IDENTIFIC_TERCER_B := INFOITEMS_.NIT;
			MCTYPE.MC_____DEBMONORI_B := 0;
			MCTYPE.MC_____CREMONORI_B := 0;
			MCTYPE.MC_____DEBMONLOC_B := INFOITEMS_.valor_deb::NUMERIC;
			MCTYPE.MC_____CREMONLOC_B := INFOITEMS_.valor_credt::NUMERIC;
			MCTYPE.MC_____INDTIPMOV_B := 4;
			MCTYPE.MC_____INDMOVREV_B := 'N';
			MCTYPE.MC_____OBSERVACI_B := INFOITEMS_.descripcion || ' '||NEGOCIO_.UNIDAD_NEG;
			MCTYPE.MC_____FECHORCRE_B := INFOITEMS_.creation_date::TIMESTAMP;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN';
			MCTYPE.MC_____FEHOULMO__B := INFOITEMS_.creation_date::TIMESTAMP;
			MCTYPE.MC_____AUTULTMOD_B := '';
			MCTYPE.MC_____VALIMPCON_B := 0;
			MCTYPE.MC_____NUMERO_OPER_B := '';
			MCTYPE.TERCER_CODIGO____TIT____B := INFOITEMS_.tipo_doc;
			MCTYPE.TERCER_NOMBCORT__B := SUBSTR(INFOITEMS_.nombre_corto,1,32);
			MCTYPE.TERCER_NOMBEXTE__B := SUBSTR (INFOITEMS_.nombre,1,64);
			MCTYPE.TERCER_APELLIDOS_B := SUBSTR(INFOITEMS_.apellidos,1,32);
			MCTYPE.TERCER_CODIGO____TT_____B := INFOITEMS_.codigo;
			MCTYPE.TERCER_DIRECCION_B := SUBSTR(INFOITEMS_.direccion,1,64);
			MCTYPE.TERCER_CODIGO____CIUDAD_B := INFOITEMS_.codigociu;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(INFOITEMS_.telefono)>15 THEN SUBSTR(INFOITEMS_.telefono,1,15) ELSE INFOITEMS_.telefono END;
			MCTYPE.TERCER_TIPOGIRO__B := 1;
			MCTYPE.TERCER_CODIGO____EF_____B := '';
			MCTYPE.TERCER_SUCURSAL__B := '';
			MCTYPE.TERCER_NUMECUEN__B := '';
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N',INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 3);
			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 4)='S')THEN

				MCTYPE.MC_____NUMDOCSOP_B := INFOITEMS_.documento;
			ELSE
				MCTYPE.MC_____NUMDOCSOP_B := '';
			END IF;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 5)::INT=1)THEN
				MCTYPE.MC_____NUMEVENC__B := 1;
			ELSE
				MCTYPE.MC_____NUMEVENC__B := NULL;
			END IF;

			--FUNCION PARA INSERTAR EL REGISTRO EN LA TABLA TEMPORAL DE FINTRA
			SW:=apoteosys.SP_INSERT_TABLE_MC_MICRO____(MCTYPE);
			SECUENCIA_INT :=SECUENCIA_INT+1;
 
			--raise notice 'valor_deb: % valor_credt: %',INFOITEMS_.valor_deb,INFOITEMS_.valor_credt;
		END LOOP;

		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE,'MICROCREDITO') ='N' THEN
			SW = 'N';
			CONTINUE;
		END IF;

		if(SW = 'S')then
			INSERT INTO apoteosys.traslado_facturas_apoteosys(cod_neg, tipo_documento, documento, agencia, periodo, fecha_negocio, unidad_negocio)
			VALUES (NEGOCIO_.COD_NEG, 'REF', NEGOCIO_.LOTE, NEGOCIO_.AGENCIA, NEGOCIO_.PERIODO_REFINANCIACION, NEGOCIO_.FECHA_NEGOCIO, NEGOCIO_.UNIDAD_NEG);
			
			UPDATE administrativo.control_refinanciacion_negocios SET procesado_apot = 'S' WHERE COD_NEG = NEGOCIO_.COD_NEG and key_ref=NEGOCIO_.lote and nota_ajuste= NEGOCIO_.nota_ajuste and ESTADO = 'S' ;
			
			update con.ingreso_detalle set PROCESADO_ICA='S' where TIPO_DOCUMENTO='ICA' and	num_ingreso=NEGOCIO_.nota_ajuste;
		end if;

		 
		
	END LOOP;

RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apoteosys.interfaz_microcredito_refinanciacion_ia_plan_dia()
  OWNER TO postgres;
