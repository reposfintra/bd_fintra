-- Function: apoteosys.interfaz_educativo_fintra_reestructuracion_anulaCM_credito_anterior()

-- DROP FUNCTION apoteosys.interfaz_educativo_fintra_reestructuracion_anulaCM_credito_anterior();

CREATE OR REPLACE FUNCTION apoteosys.interfaz_educativo_fintra_reestructuracion_anulaCM_credito_anterior()
  RETURNS text AS
$BODY$

DECLARE

 /************************************************
  *DESCRIPCION: ESTA FUNCION BUSCA TODOS LO NEGOCIOS EDUCATIVOS CON AVALES INCLUIDOS.
  *AUTOR:=@DVALENCIA
  *FECHA CREACION:=2018-12 -17
  *LAST_UPDATE
  *DESCRIPCION DE CAMBIOS Y FECHA
  ************************************************/

NEGOCIO_ RECORD;
INFOITEMS_ RECORD;
--INFOCLIENTE RECORD;
LONGITUD numeric;
SECUENCIA_GEN INTEGER;
SECUENCIA_INT integer:= 1;
FECHADOC_ VARCHAR:= '';
MCTYPE CON.TYPE_INSERT_MC;
SW TEXT:='N';
validaciones text;

BEGIN
	/**SACAMOS EL LISTADO DE NEGOCIOS*/
	FOR NEGOCIO_ in
	
				SELECT  NEG.COD_NEG as NEG_ANTERIOR,
						NEG.COD_CLI,
						NEG.FECHA_NEGOCIO,
						CONV.AGENCIA,
						UNEG.DESCRIPCION AS UNIDAD_NEG,
						REPLACE(SUBSTRING(NEG.F_DESEM,1,7),'-','') AS PERIODO_DESEM,
						neg.ESTADO_NEG as estado_neg_ant,
						i.num_ingreso as ia_reversion,
						nn.cod_neg as NEG_RENOVADO,
						Nn.F_DESEM as f_desem_neg_renov,
						nn.ESTADO_NEG as estado_neg_renov,
						nn.periodo						
				FROM NEGOCIOS NEG
				INNER JOIN CONVENIOS CONV ON (CONV.ID_CONVENIO = NEG.ID_CONVENIO)
				INNER JOIN REL_UNIDADNEGOCIO_CONVENIOS RUC ON (CONV.ID_CONVENIO = RUC.ID_CONVENIO)
				INNER JOIN UNIDAD_NEGOCIO UNEG ON (UNEG.ID = RUC.ID_UNID_NEGOCIO)
				inner join rel_negocios_reestructuracion nr on nr.negocio_base=neg.cod_neg
				inner join negocios nn on nn.cod_neg=nr.negocio_reestructuracion
				inner join con.factura f on f.negasoc=neg.cod_neg
				left join con.ingreso_detalle id on id.documento = f.documento
				inner join con.ingreso i on i.num_ingreso=id.num_ingreso 
				WHERE NEG.ESTADO_NEG IN ('T') AND UNEG.ID = '31'
				and Nn.procesado_mc ='N'
				--and neg.cod_neg = 'FE03152'
				--and i.num_ingreso = 'IA538457'
				and nn.estado_neg = 'T'
				and id.tipo_documento= 'ICA'
				and i.descripcion_ingreso = 'AJUSTE CM FIDUCIA POR REESTRUCTURACION'
				AND REPLACE(SUBSTRING(Nn.creation_date,1,7),'-','')=REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','')
				and neg.cod_neg  in (select cod_neg from con.traslado_negocios_renovados where capital_reversado = 'S' and if_reversado = 'S'  and cm_reversado = '-')
				group by neg.cod_neg, neg.cod_cli, neg.fecha_negocio, conv.agencia, uneg.descripcion, neg.f_desem,neg.estado_neg, nn.cod_neg, nn.f_desem, nn.estado_neg, nn.periodo,i.num_ingreso
			
				--AND REPLACE(SUBSTRING(Nr.F_DESEM,1,7),'-','') = '201808'		
				--AND NEG.COD_NEG ='FE00982'
				
				--select  apoteosys.interfaz_educativo_fintra_reestructuracion_anulaCM_credito_anterior();

	LOOP
		
		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.INTERFAZ_SECUENCIA_ICA_APOTEOSYS');
		
		raise notice 'paso: %',NEGOCIO_.NEG_ANTERIOR;

		MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
		MCTYPE.MC_____CODIGO____TD_____B := 'ICRN' ;
		MCTYPE.MC_____CODIGO____CD_____B := 'ACMR';
		MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN  ; --SECUENCIA GENERAL
		SECUENCIA_INT:=1;


		/**BUSCAMOS LA COMPLETA QUE CONFORMARA EL ASIENTO CONTABLE PARA MANDARLO A APOTEOSYS*/
		FOR INFOITEMS_ IN
				
		(SELECT
						i.CUENTA,
						case when if.cod is null then (select cod from ing_fenalco 
														where codneg = (select con.interfaz_obtener_centro_costo_x_ingreso(i.num_ingreso,2)) 
														and tipodoc = 'CM' and reg_status = 'A' order by cod limit 1)
					    	else if.cod end as documento,
						'ICA' AS tipo_documento,
						CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE i.nitcli END AS NIT,
						--proveedor,
						id.valor_ingreso as valor_deb,
						0::numeric as valor_credt,
						i.num_ingreso ||': '|| i.descripcion_ingreso as descripcion,
						i.creation_date::DATE,
					   i.creation_date::DATE AS fecha_vencimiento,
					   replace(substring(i.creation_date,1,7),'-','') as periodo,	
						(CASE
						WHEN tipo_iden ='CED' THEN 'CC'
						WHEN tipo_iden ='RIF' THEN 'CE'
						WHEN tipo_iden ='NIT' THEN 'NIT' ELSE
						'CC' END) as tipo_doc,
						(CASE
						WHEN tipo_iden in  ('RIF','NIT') THEN 'RCOM'  -->regimen comun
						WHEN tipo_iden in  ('CED')  THEN 'RSCP'
						else 'RSCP'
						END) as codigo,
						(CASE
						WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
						ELSE '08001' END) as codigociu,
						(D.NOMBRE1||' '||D.NOMBRE2) AS nombre_corto,
						(D.APELLIDO1||' '||D.APELLIDO2) AS apellidos,
						D.NOMBRE,
						D.DIRECCION,
						D.TELEFONO
				from con.ingreso i
				inner join con.ingreso_detalle id on id.num_ingreso=i.num_ingreso
				left join con.factura f on f.documento=id.documento
				left join NIT D ON(D.CEDULA=i.nitcli)
				LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
				LEFT JOIN CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=i.nitcli)
				left join ing_fenalco if on if.codneg=f.negasoc and if.cuota=f.num_doc_fen and if.tipodoc = 'CM'
				where i.num_ingreso =NEGOCIO_.ia_reversion and i.descripcion_ingreso = 'AJUSTE CM FIDUCIA POR REESTRUCTURACION'
				)
				union ALL
				(select id.cuenta,   
					   id.documento, 
					   'ICA' AS tipo_documento,
					   CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE i.nitcli END AS NIT,
					   0::numeric as valor_deb,
					   id.valor_ingreso as valor_credt,
					   i.num_ingreso ||': '|| i.descripcion_ingreso,
					   i.creation_date::DATE,
					   i.creation_date::DATE AS fecha_vencimiento,
					   replace(substring(i.creation_date,1,7),'-','') as periodo,	   
					   (CASE
						WHEN tipo_iden ='CED' THEN 'CC'
						WHEN tipo_iden ='RIF' THEN 'CE'
						WHEN tipo_iden ='NIT' THEN 'NIT' ELSE
						'CC' END) as tipo_doc,
						(CASE
						WHEN tipo_iden in  ('RIF','NIT') THEN 'RCOM'  -->regimen comun
						WHEN tipo_iden in  ('CED')  THEN 'RSCP'
						else 'RSCP'
						END) as codigo,
						(CASE
						WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
						ELSE '08001' END) as codigociu,
						(D.NOMBRE1||' '||D.NOMBRE2) AS nombre_corto,
						(D.APELLIDO1||' '||D.APELLIDO2) AS apellidos,
						D.NOMBRE,
						D.DIRECCION,
						D.TELEFONO
				from con.ingreso i
				inner join con.ingreso_detalle id on id.num_ingreso=i.num_ingreso
				left join con.factura f on f.documento=id.documento
				left join NIT D ON(D.CEDULA=i.nitcli)
				LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
				LEFT JOIN CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=i.nitcli)
				where i.num_ingreso =NEGOCIO_.ia_reversion
				--and i.periodo !=''
				and i.descripcion_ingreso = 'AJUSTE CM FIDUCIA POR REESTRUCTURACION'
				)
				
		LOOP
		
			IF(INFOITEMS_.tipo_documento in ('FAC','FAP','NEG','IF','CM','ICA') AND CON.OBTENER_HOMOLOGACION_APOTEOSYS('EDUCATIVO_FINT',INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 6)='S')THEN
				MCTYPE.MC_____FECHVENC__B = INFOITEMS_.fecha_vencimiento; --fecha vencimiento
				if (INFOITEMS_.fecha_vencimiento < INFOITEMS_.creation_date)then /** se valida si la fecha de vencimeinto es menor a la de creacion*/
					MCTYPE.MC_____FECHEMIS__B = INFOITEMS_.fecha_vencimiento; --fecha creacion
				else
					MCTYPE.MC_____FECHEMIS__B = INFOITEMS_.creation_date; --fecha creacion
				end if;
			ELSE
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';
			END IF;

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(INFOITEMS_.creation_date,1,7),'-','') = INFOITEMS_.periodo THEN INFOITEMS_.creation_date::DATE ELSE con.sp_fecha_corte_mes(SUBSTRING(INFOITEMS_.periodo,1,4), SUBSTRING(INFOITEMS_.periodo,5,2)::INT)::DATE END;
			MCTYPE.MC_____FECHA_____B := FECHADOC_;
			MCTYPE.MC_____SECUINTE__DCD____B := SECUENCIA_INT;--secuencia interna
			MCTYPE.MC_____SECUINTE__B := SECUENCIA_INT;--secuencia interna
			MCTYPE.MC_____REFERENCI_B := NEGOCIO_.NEG_ANTERIOR;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING( INFOITEMS_.periodo,1,4)::INT;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING( INFOITEMS_.periodo,5,2)::INT;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF';
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('EDUCATIVO_FINT', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 1);
			MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('EDUCATIVO_FINT', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 2);
			MCTYPE.MC_____IDENTIFIC_TERCER_B := INFOITEMS_.NIT;
			MCTYPE.MC_____DEBMONORI_B := 0;
			MCTYPE.MC_____CREMONORI_B := 0;
			MCTYPE.MC_____DEBMONLOC_B := INFOITEMS_.valor_deb::NUMERIC;
			MCTYPE.MC_____CREMONLOC_B := INFOITEMS_.valor_credt::NUMERIC;
			MCTYPE.MC_____INDTIPMOV_B := 4;
			MCTYPE.MC_____INDMOVREV_B := 'N';
			MCTYPE.MC_____OBSERVACI_B := INFOITEMS_.descripcion || ' '||NEGOCIO_.UNIDAD_NEG;
			MCTYPE.MC_____FECHORCRE_B := INFOITEMS_.creation_date::TIMESTAMP;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN';
			MCTYPE.MC_____FEHOULMO__B := INFOITEMS_.creation_date::TIMESTAMP;
			MCTYPE.MC_____AUTULTMOD_B := '';
			MCTYPE.MC_____VALIMPCON_B := 0;
			MCTYPE.MC_____NUMERO_OPER_B := '';
			MCTYPE.TERCER_CODIGO____TIT____B := INFOITEMS_.tipo_doc;
			MCTYPE.TERCER_NOMBCORT__B := SUBSTR(INFOITEMS_.nombre_corto,1,32);
			MCTYPE.TERCER_NOMBEXTE__B := SUBSTR (INFOITEMS_.nombre,1,64);
			MCTYPE.TERCER_APELLIDOS_B := SUBSTR(INFOITEMS_.apellidos,1,32);
			MCTYPE.TERCER_CODIGO____TT_____B := INFOITEMS_.codigo;
			MCTYPE.TERCER_DIRECCION_B := SUBSTR(INFOITEMS_.direccion,1,64);
			MCTYPE.TERCER_CODIGO____CIUDAD_B := INFOITEMS_.codigociu;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(INFOITEMS_.telefono)>15 THEN SUBSTR(INFOITEMS_.telefono,1,15) ELSE INFOITEMS_.telefono END;
			MCTYPE.TERCER_TIPOGIRO__B := 1;
			MCTYPE.TERCER_CODIGO____EF_____B := '';
			MCTYPE.TERCER_SUCURSAL__B := '';
			MCTYPE.TERCER_NUMECUEN__B := '';
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('EDUCATIVO_FINT',INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 3);
			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('EDUCATIVO_FINT', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 4)='S')THEN

				MCTYPE.MC_____NUMDOCSOP_B := INFOITEMS_.documento;
			ELSE
				MCTYPE.MC_____NUMDOCSOP_B := '';
			END IF;

			IF(CON.OBTENER_HOMOLOGACION_APOTEOSYS('EDUCATIVO_FINT', INFOITEMS_.tipo_documento, INFOITEMS_.CUENTA, NEGOCIO_.agencia, 5)::INT=1)THEN
				MCTYPE.MC_____NUMEVENC__B := 1;
			ELSE
				MCTYPE.MC_____NUMEVENC__B := NULL;
			END IF;

			--FUNCION PARA INSERTAR EL REGISTRO EN LA TABLA TEMPORAL DE FINTRA
			SW:=apoteosys.SP_INSERT_TABLE_MC_FENALCO____(MCTYPE);
			SECUENCIA_INT :=SECUENCIA_INT+1;
 
			--raise notice 'valor_deb: % valor_credt: %',INFOITEMS_.valor_deb,INFOITEMS_.valor_credt;
		END LOOP;

		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE,'EDUCATIVO') ='N' THEN
			SW = 'N';
			CONTINUE;
		END IF;

		if(SW = 'S')then
			--SELECT * FROM OPAV.SL_TRASLADO_FACTURAS_APOTEOSYS
			update  con.traslado_negocios_renovados set cm_reversado = 'S'  where cod_neg = NEGOCIO_.NEG_ANTERIOR and capital_reversado = 'S' and if_reversado = 'S'  and cm_reversado = '-';
			
			UPDATE	con.ingreso_detalle	SET	PROCESADO_ICA='S' where	TIPO_DOCUMENTO=INFOITEMS_.tipo_documento and num_ingreso=NEGOCIO_.ia_reversion;
		end if;

	END LOOP;

RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apoteosys.interfaz_educativo_fintra_reestructuracion_anulaCM_credito_anterior()
  OWNER TO postgres;
