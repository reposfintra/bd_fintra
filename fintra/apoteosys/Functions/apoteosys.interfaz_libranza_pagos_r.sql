-- Function: apoteosys.interfaz_libranza_pagos_r();

-- DROP FUNCTION apoteosys.interfaz_libranza_pagos_r();

CREATE OR REPLACE FUNCTION apoteosys.interfaz_libranza_pagos_r()
  RETURNS text AS
$BODY$

DECLARE
	r_ingreso record;
	r_asiento record;
	recordNeg record;
	INFOCLIENTE record;
	SECUENCIA_GEN INTEGER;
	FECHADOC_ TEXT:='';
	MCTYPE CON.TYPE_INSERT_MC;
	SW TEXT:='N';
	CONSEC INTEGER:=1;
	_PROCESOHOM TEXT := 'RECAUDO_LIBRA';

--select apoteosys.interfaz_libranza_pagos_r()
--select * from con.mc_recaudo____  where procesado='N' and MC_____CODIGO____PF_____B='2019' and MC_____NUMERO____PERIOD_B IN(3)

BEGIN

	FOR r_ingreso IN

		SELECT 
			a.tipo_documento,
			a.num_ingreso,
			b.cuenta as cuenta_r,
			c.codigo_cuenta as cuenta_banco,
			a.nitcli,
			a.periodo,
			a.descripcion_ingreso,
			a.fecha_ingreso as fecha,
			'ATL'::varchar as agencia
		FROM 
			con.ingreso a 
		inner join
			con.ingreso_detalle b on(b.tipo_documento=a.tipo_documento and b.num_ingreso=a.num_ingreso)
		inner join
			banco c on(c.branch_code=a.branch_code and c.bank_account_no=a.bank_account_no)
		where 
			a.reg_status='' and 
			a.tipo_documento='ING' and 
			a.periodo=replace(substring(current_date,1,7),'-','') and 
			a.descripcion_ingreso ilike 'PAGO%CONVENIO%LIBRANZ%' and 
			--b.factura='R0037880'
			coalesce(b.procesado_ica,'N')='N'
		group by
			a.tipo_documento,
			a.num_ingreso,
			b.cuenta,
			c.codigo_cuenta,
			a.nitcli,
			a.periodo,
			a.descripcion_ingreso,
			a.fecha_ingreso
		order by
			num_ingreso
		--limit 1

	LOOP

		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.interfaz_secuencia_bingreso_apoteosys');

		FOR r_asiento IN

			select 
				a.tipo_documento,
				a.num_ingreso,
				a.creation_date,
				d.descripcion_ingreso,
				'R'||c.codigo_cuenta as cuenta,
				a.num_ingreso as documento,
				a.valor_ingreso as valor_debito, 
				0 as valor_credito,
				a.nitcli,
				'1' as item
			from 
				con.ingreso_detalle a 
			inner join
				con.ingreso d on(d.tipo_documento=a.tipo_documento and d.num_ingreso=a.num_ingreso)
			inner join
				banco c on(c.branch_code=d.branch_code and c.bank_account_no=d.bank_account_no)
			where 
				a.reg_status='' and 
				a.tipo_documento='ING' and 
				a.num_ingreso=r_ingreso.num_ingreso
			union all
			select 
				a.tipo_documento,
				a.num_ingreso,
				a.creation_date,
				d.descripcion_ingreso, 
				'R'||a.cuenta,
				A.factura,
				0 as valor_debito, 
				a.valor_ingreso as valor_credito,
				a.nitcli,
				'2' as item
			from 
				con.ingreso_detalle a 
			inner join
				con.ingreso d on(d.tipo_documento=a.tipo_documento and d.num_ingreso=a.num_ingreso)
			where 
				a.reg_status='' and 
				a.tipo_documento='ING' and 
				a.num_ingreso=r_ingreso.num_ingreso

		LOOP

			SELECT INTO INFOCLIENTE
				'NIT' AS TIPO_DOC,
				'GCON' AS CODIGO,
				(CASE
				WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
				ELSE '08001' END) AS CODIGOCIU,
				NOMCLI AS NOMBRE_CORTO,
				NOMCLI AS  NOMBRE,
				'' AS APELLIDOS,
				DIRECCION,
				TELEFONO
			FROM CLIENTE CL
			LEFT JOIN CIUDAD E ON(E.CODCIU=CL.CIUDAD)
			WHERE CL.NIT =  r_asiento.nitcli;

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(R_ASIENTO.CREATION_DATE::DATE,1,7),'-','')=R_INGRESO.PERIODO THEN R_ASIENTO.CREATION_DATE::DATE ELSE CON.SP_FECHA_CORTE_MES(SUBSTRING(R_INGRESO.PERIODO,1,4),SUBSTRING(R_INGRESO.PERIODO,5,2)::INT)::DATE END ;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 6)='S')then
				MCTYPE.MC_____FECHEMIS__B = FECHADOC_::DATE;
				--MCTYPE.MC_____FECHEMIS__B = recordNeg.CREATION_DATE::DATE;
				MCTYPE.MC_____FECHVENC__B = FECHADOC_::DATE;
			else
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';

			end if;

			MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
			MCTYPE.MC_____CODIGO____TD_____B := 'INGN' ;
			MCTYPE.MC_____CODIGO____CD_____B := 'ICRL';
			MCTYPE.MC_____SECUINTE__DCD____B := CONSEC  ;
			MCTYPE.MC_____FECHA_____B := FECHADOC_::DATE  ;
			MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN  ;
			MCTYPE.MC_____SECUINTE__B := CONSEC  ;
			MCTYPE.MC_____REFERENCI_B := r_ingreso.num_ingreso;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING(r_ingreso.PERIODO,1,4)::INT;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING(r_ingreso.PERIODO,5,2)::INT;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF' ;
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 1)  ;
			MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 2)  ;
			MCTYPE.MC_____IDENTIFIC_TERCER_B := case when char_length(R_ASIENTO.nitcli)>9 then substring(R_ASIENTO.nitcli,1,9) else R_ASIENTO.nitcli end;
			MCTYPE.MC_____DEBMONORI_B := 0  ;
			MCTYPE.MC_____CREMONORI_B := 0 ;
			MCTYPE.MC_____DEBMONLOC_B := R_ASIENTO.VALOR_DEBITO::NUMERIC  ;
			MCTYPE.MC_____CREMONLOC_B := R_ASIENTO.VALOR_CREDITO::NUMERIC  ;
			MCTYPE.MC_____INDTIPMOV_B := 4  ;
			MCTYPE.MC_____INDMOVREV_B := 'N'  ;
			MCTYPE.MC_____OBSERVACI_B := r_ingreso.num_ingreso||' - '||r_ingreso.descripcion_ingreso;
			MCTYPE.MC_____FECHORCRE_B := R_ASIENTO.CREATION_DATE::TIMESTAMP  ;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN'  ;
			MCTYPE.MC_____FEHOULMO__B := R_ASIENTO.CREATION_DATE::TIMESTAMP  ;
			MCTYPE.MC_____AUTULTMOD_B := ''  ;
			MCTYPE.MC_____VALIMPCON_B := 0  ;
			MCTYPE.MC_____NUMERO_OPER_B := R_ASIENTO.num_ingreso;
			MCTYPE.TERCER_CODIGO____TIT____B := INFOCLIENTE.TIPO_DOC;
			MCTYPE.TERCER_NOMBCORT__B := SUBSTRING(INFOCLIENTE.NOMBRE_CORTO,1,32);
			MCTYPE.TERCER_NOMBEXTE__B := SUBSTRING(INFOCLIENTE.NOMBRE,1,64);
			MCTYPE.TERCER_APELLIDOS_B := INFOCLIENTE.APELLIDOS;
			MCTYPE.TERCER_CODIGO____TT_____B := INFOCLIENTE.CODIGO;
			MCTYPE.TERCER_DIRECCION_B := CASE WHEN CHAR_LENGTH(INFOCLIENTE.DIRECCION)>64 THEN SUBSTR(INFOCLIENTE.DIRECCION,1,64) ELSE INFOCLIENTE.DIRECCION END;
			MCTYPE.TERCER_CODIGO____CIUDAD_B := INFOCLIENTE.CODIGOCIU;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(INFOCLIENTE.TELEFONO)>15 THEN SUBSTR(INFOCLIENTE.TELEFONO,1,15) ELSE INFOCLIENTE.TELEFONO END;
			MCTYPE.TERCER_TIPOGIRO__B := 1 ;
			MCTYPE.TERCER_CODIGO____EF_____B := ''  ;
			MCTYPE.TERCER_SUCURSAL__B := ''  ;
			MCTYPE.TERCER_NUMECUEN__B := ''  ;
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 3);
			--MCTYPE.MC_____NUMDOCSOP_B := REC_OS.NUMERO_OPERACION;
			MCTYPE.MC_____NUMEVENC__B := CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 5)::INT;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 4)='S')then
				MCTYPE.MC_____NUMDOCSOP_B := R_ASIENTO.DOCUMENTO;
			else
				MCTYPE.MC_____NUMDOCSOP_B := '';
			end if;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS(_PROCESOHOM, R_ASIENTO.TIPO_DOCUMENTO, R_ASIENTO.CUENTA,R_INGRESO.AGENCIA, 5)::int=1)then
				MCTYPE.MC_____NUMEVENC__B := 1;
			else
				MCTYPE.MC_____NUMEVENC__B := null;
			end if;

			raise notice 'MCTYPE: %', MCTYPE;

			-- Insertamos en la tabla de Apoteosys
			--FUNCION QUE TRANSACCION POR TIPO DE DOCUMENTO EN TABLA TEMPORAL EN FINTRA.
			SW:=CON.SP_INSERT_TABLE_MC_RECAUDO____(MCTYPE);
			CONSEC:=CONSEC+1;
		END LOOP;

		CONSEC:=1;
		---------------------------------------------------------------------------

		--------------Revision de la transaccion-----------------
		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		IF CON.SP_VALIDACIONES(MCTYPE, 'RECAUDO') ='N' THEN
			SW='N';

			--BORRAMOS EL COMPROBANTE DE EXT
			DELETE FROM CON.mc_recaudo____
			WHERE MC_____NUMERO____B = SECUENCIA_GEN AND MC_____CODIGO____CONTAB_B = 'FINT'
			 AND MC_____CODIGO____TD_____B = 'INGN' AND  MC_____CODIGO____CD_____B = 'ICRL';

			CONTINUE;
		END IF;

		-- ACTUALIZAMOS EL CAMPO DE APOTEOSYS DE LA CABECERA DEL CRéDITO PARA INDICAR QUE YA SE ENVíO
		IF(SW='S')THEN

			 UPDATE
				con.ingreso_detalle
			SET
				PROCESADO_ICA='S'
			WHERE
				TIPO_DOCUMENTO=r_ingreso.tipo_documento and
				num_ingreso=r_ingreso.num_ingreso;

			SW:='N';
		END IF;

		CONSEC:=1;
		---------------------------------------------------------------

	END LOOP;

	RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apoteosys.interfaz_libranza_pagos_r()
  OWNER TO postgres;
