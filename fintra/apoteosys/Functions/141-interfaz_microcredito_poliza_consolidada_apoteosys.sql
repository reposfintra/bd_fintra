-- Function: apoteosys.interfaz_microcredito_poliza_consolidada_apoteosys()

-- DROP FUNCTION apoteosys.interfaz_microcredito_poliza_consolidada_apoteosys();

CREATE OR REPLACE FUNCTION apoteosys.interfaz_microcredito_poliza_consolidada_apoteosys()
  RETURNS text AS
$BODY$

DECLARE

 _CXPCONSOLIDADA RECORD;
 _RECORDCONTABLE RECORD;
SECUENCIA_GEN INTEGER;
FECHADOC_ TEXT:='';
MCTYPE CON.TYPE_INSERT_MC;
SW TEXT:='N';
CONSEC INTEGER:=1;


BEGIN
	--BUSCAMOS LAS CXP CONSOLIDADAS POLIZA PENDIENTES POR PROCESAR
	FOR _CXPCONSOLIDADA IN (

				SELECT  CXP.DOCUMENTO,
						CXP.PROVEEDOR,
						CXP.VLR_NETO,
						CXP.DESCRIPCION,
						--ARRAY(SELECT REFERENCIA_1 FROM FIN.CXP_ITEMS_DOC WHERE DOCUMENTO= CXP.DOCUMENTO /* AND F.REG_STATUS=''*/) AS POLIZAS_RELACIONADAS,
						CXP.FECHA_DOCUMENTO::DATE,
						CXP.FECHA_VENCIMIENTO::DATE AS FECHA_VENCIMIENTO,
						CXP.creation_date::DATE,
						CXP.PERIODO		 
				FROM FIN.CXP_DOC CXP 
				INNER JOIN FIN.CXP_ITEMS_DOC CID ON CID.DOCUMENTO=CXP.DOCUMENTO and cid.tipo_documento=cxp.tipo_documento
				INNER JOIN FIN.CXP_DOC CP ON CP.DOCUMENTO=CID.REFERENCIA_1 and cp.tipo_documento=cid.tipo_documento
				WHERE CXP.DOCUMENTO ILIKE 'PLD%' --AND CXP.DOCUMENTO in ('PLD00014')--,'PLD00014','PLD00015','PLD00016')
				AND CXP.HANDLE_CODE='PZ' 
				AND CXP.DOCUMENTO ILIKE 'PLD%'
				AND CXP.TIPO_DOCUMENTO='FAP' --ORDER BY CXP.DOCUMENTO
				AND CXP.DOCUMENTO NOT IN (SELECT DOCUMENTO FROM apoteosys.TRASLADO_FACTURAS_APOTEOSYS)
				and CXP.periodo =REPLACE(SUBSTRING(CURRENT_DATE,1,7),'-','')
				--and CXP.periodo ='201905'
				and cxp.reg_status = ''
				GROUP BY  CXP.DOCUMENTO,
						CXP.PROVEEDOR,
						CXP.VLR_NETO,
						CXP.DESCRIPCION,
						CXP.FECHA_DOCUMENTO,
						CXP.FECHA_VENCIMIENTO,
						CXP.PERIODO,
						CXP.creation_date

	) LOOP
		
		--select apoteosys.interfaz_microcredito_poliza_consolidada_apoteosys();
		RAISE NOTICE 'recordCxpPoliza : %',_CXPCONSOLIDADA;

		SELECT INTO SECUENCIA_GEN NEXTVAL('CON.INTERFAZ_SECUENCIA_CXP_APOTEOSYS');

		--CREAMOS LA TRANSACCION CONTABLE POR NEGOCIO PARA PODER RELACIONAR TODO AL NEGOCIO.

		FOR _RECORDCONTABLE IN (
		SELECT
						T.PERIODO,
						T.PROVEEDOR,
						T.TIPO_DOCUMENTO,
						T.DOCUMENTO,
						T.DESCRIPCION,
						T.REFERENCIA,
						T.HANDLE_CODE,
						T.CTA_PASIVO,
						T.CTA_DEBITO_APO,
						T.FECHA_DOCUMENTO,
						T.FECHA_VENCIMIENTO,
						T.CREATION_DATE::DATE,
						T.VALOR_DEBITO,
						T.VALOR_CREDITO,
						T.TERCER_CODIGO____TIT____B,
						T.TERCER_DIGICHEQ__B,
						T.TERCER_NOMBCORT__B,
						T.TERCER_APELLIDOS_B,
						T.TERCER_NOMBEXTE__B,
						T.TERCER_CODIGO____TT_____B,
						T.TERCER_DIRECCION_B,
						T.TERCER_CODIGO____CIUDAD_B,
						T.TERCER_TELEFONO1_B,
						T.AGENCIA
					FROM (
							SELECT
								CXP.PERIODO,
								CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE SUBSTRING(CXP.PROVEEDOR,1,9) END AS PROVEEDOR,
								CXP.TIPO_DOCUMENTO,
								CXP.DOCUMENTO,
								CXP.DOCUMENTO AS REFERENCIA,
								--CP.DOCUMENTO||': '||CXP.DESCRIPCION AS DESCRIPCION,
								NP.DESCRIPCION|| ' : ' ||CP.DESCRIPCION AS DESCRIPCION,
								CXP.HANDLE_CODE,
								CID.CODIGO_CUENTA AS CTA_PASIVO,
								CMC.CUENTA AS CTA_DEBITO_APO,
								CON.AGENCIA,
								_CXPCONSOLIDADA.FECHA_DOCUMENTO::DATE AS FECHA_DOCUMENTO,
								_CXPCONSOLIDADA.FECHA_VENCIMIENTO::DATE AS FECHA_VENCIMIENTO,
								_CXPCONSOLIDADA.CREATION_DATE::DATE AS CREATION_DATE,
								0.00::NUMERIC AS VALOR_DEBITO,
								CID.VLR AS VALOR_CREDITO,
								(CASE
								 WHEN D.TIPO_IDEN='CED' THEN 'CC'
								 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
								 WHEN D.TIPO_IDEN='' THEN 'CC'
								 WHEN D.TIPO_IDEN='NIT' THEN 'NIT'
								 ELSE
								 'CC' END) AS TERCER_CODIGO____TIT____B,
								 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
								 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
								 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
								 D.NOMBRE AS TERCER_NOMBEXTE__B,
								 (CASE
								 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
								 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
								 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
								 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
								 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
								 D.DIRECCION AS TERCER_DIRECCION_B,
								 (CASE
								 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
								 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
								 D.TELEFONO AS TERCER_TELEFONO1_B
							FROM FIN.CXP_DOC CXP 
							INNER JOIN FIN.CXP_ITEMS_DOC CID ON CID.DOCUMENTO=CXP.DOCUMENTO and cid.tipo_documento=cxp.tipo_documento
							INNER JOIN FIN.CXP_DOC CP ON CP.DOCUMENTO=CID.REFERENCIA_1  and cp.tipo_documento=cid.tipo_documento
							INNER JOIN CON.CMC_DOC CMC ON (CMC.CMC=CP.HANDLE_CODE AND CMC.TIPODOC='FAP')
							INNER JOIN NEGOCIOS N ON N.COD_NEG=CP.DOCUMENTO_RELACIONADO
							INNER JOIN CONVENIOS CON ON CON.ID_CONVENIO=N.ID_CONVENIO
							LEFT JOIN PROVEEDOR C ON(C.NIT=CXP.PROVEEDOR)
							LEFT JOIN NIT D ON(D.CEDULA=C.NIT)
							LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
							LEFT JOIN CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=CXP.PROVEEDOR)
							INNER JOIN ADMINISTRATIVO.NUEVAS_POLIZAS NP ON NP.ID=CP.REFERENCIA_1
							WHERE CXP.DOCUMENTO ILIKE 'PLD%'  
							AND CXP.DOCUMENTO = _CXPCONSOLIDADA.DOCUMENTO
							AND CXP.HANDLE_CODE='PZ' 
							AND CXP.REG_STATUS = ''
							AND CXP.PERIODO != ''
							AND CXP.TIPO_DOCUMENTO='FAP' --ORDER BY CXP.DOCUMENTO
							UNION ALL
							SELECT
								CXP.PERIODO,
								CASE WHEN HT.NIT_APOTEOSYS IS NOT NULL THEN HT.NIT_APOTEOSYS ELSE SUBSTRING(CXP.PROVEEDOR,1,9) END AS PROVEEDOR,
								CXP.TIPO_DOCUMENTO,
								CXP.DOCUMENTO,
								CXP.DOCUMENTO_RELACIONADO,
								--CP.DOCUMENTO||': '||CXP.DESCRIPCION AS DESCRIPCION,
								NP.DESCRIPCION|| ' : ' ||CXP.DESCRIPCION,
								--CXPDET.REFERENCIA_2 AS REFERENCIA,
								CXP.HANDLE_CODE,
								CIP.CODIGO_CUENTA AS CTA_PASIVO,
								CMC.CUENTA AS CTA_DEBITO_APO,
								CON.AGENCIA,
								CXP.FECHA_DOCUMENTO::DATE,
								CXP.FECHA_VENCIMIENTO::DATE,
								CXP.CREATION_DATE::DATE,
								CXP.VLR_NETO AS VALOR_DEBITO,
								0.00::NUMERIC AS VALOR_CREDITO,
								(CASE WHEN D.TIPO_IDEN='CED' THEN 'CC'
								 WHEN D.TIPO_IDEN='RIF' THEN 'CE'
								 WHEN D.TIPO_IDEN='' THEN 'CC'
								 WHEN D.TIPO_IDEN='NIT' THEN 'NIT'
								 ELSE
								 'CC' END) AS TERCER_CODIGO____TIT____B,
								 C.DIGITO_VERIFICACION AS TERCER_DIGICHEQ__B,
								 (D.NOMBRE1||' '||D.NOMBRE2) AS TERCER_NOMBCORT__B,
								 (D.APELLIDO1||' '||D.APELLIDO2) AS TERCER_APELLIDOS_B,
								 D.NOMBRE AS TERCER_NOMBEXTE__B,
								 (CASE
								 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='N' THEN 'RCOM'
								 WHEN C.GRAN_CONTRIBUYENTE='N' AND C.AGENTE_RETENEDOR='S' THEN 'RCAU'
								 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='N' THEN 'GCON'
								 WHEN C.GRAN_CONTRIBUYENTE='S' AND C.AGENTE_RETENEDOR='S' THEN 'GCAU'
								 ELSE 'PNAL' END) AS TERCER_CODIGO____TT_____B,
								 D.DIRECCION AS TERCER_DIRECCION_B,
								 (CASE
								 WHEN E.CODIGO_DANE2!='' THEN E.CODIGO_DANE2
								 ELSE '08001' END) AS TERCER_CODIGO____CIUDAD_B,
								 D.TELEFONO AS TERCER_TELEFONO1_B	 
							FROM FIN.CXP_DOC CXP
							INNER JOIN FIN.CXP_ITEMS_DOC CIP ON CIP.REFERENCIA_1=CXP.DOCUMENTO  and cip.tipo_documento=cxp.tipo_documento
							INNER JOIN FIN.CXP_DOC CP ON CP.DOCUMENTO=CIP.DOCUMENTO and cp.tipo_documento=cip.tipo_documento
							INNER JOIN CON.CMC_DOC CMC ON (CMC.CMC=CXP.HANDLE_CODE AND CMC.TIPODOC='FAP')
							INNER JOIN NEGOCIOS N ON N.COD_NEG=CXP.DOCUMENTO_RELACIONADO
							INNER JOIN CONVENIOS CON ON CON.ID_CONVENIO=N.ID_CONVENIO
							LEFT JOIN PROVEEDOR C ON(C.NIT=CXP.PROVEEDOR)
							LEFT JOIN NIT D ON(D.CEDULA=C.NIT)
							LEFT JOIN CIUDAD E ON(E.CODCIU=D.CODCIU)
							LEFT JOIN CON.HOMOLOGA_TERCEROS HT ON(HT.NIT_FINTRA=CXP.PROVEEDOR)
							INNER JOIN ADMINISTRATIVO.NUEVAS_POLIZAS NP ON NP.ID=CXP.REFERENCIA_1
							WHERE  CXP.DOCUMENTO ILIKE 'PL%' --AND CIP.DOCUMENTO = 'PLD00001'
							AND CIP.DOCUMENTO ILIKE 'PLD%'
							AND CXP.REG_STATUS = ''
							AND CXP.PERIODO != ''
							AND CXP.TIPO_DOCUMENTO ='FAP'
							AND CP.DOCUMENTO = _CXPCONSOLIDADA.DOCUMENTO
						)T
					ORDER BY REFERENCIA,VALOR_CREDITO,VALOR_DEBITO
					)

		LOOP
			RAISE NOTICE '_RECORDCONTABLE: %',_RECORDCONTABLE;

			FECHADOC_ := CASE WHEN REPLACE(SUBSTRING(_CXPCONSOLIDADA.FECHA_DOCUMENTO::DATE,1,7),'-','')=_CXPCONSOLIDADA.PERIODO THEN _CXPCONSOLIDADA.FECHA_DOCUMENTO::DATE ELSE CON.SP_FECHA_CORTE_MES(SUBSTRING(_CXPCONSOLIDADA.PERIODO,1,4),SUBSTRING(_CXPCONSOLIDADA.PERIODO,5,2)::INT)::DATE END ;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', _RECORDCONTABLE.TIPO_DOCUMENTO, _RECORDCONTABLE.CTA_DEBITO_APO,_RECORDCONTABLE.AGENCIA, 6)='S')then
				MCTYPE.MC_____FECHEMIS__B = FECHADOC_::DATE;
				MCTYPE.MC_____FECHVENC__B = FECHADOC_::DATE;
			else
				MCTYPE.MC_____FECHEMIS__B='0099-01-01 00:00:00';
				MCTYPE.MC_____FECHVENC__B='0099-01-01 00:00:00';

			end if;

			------------------------------------------------------
			MCTYPE.MC_____CODIGO____CONTAB_B := 'FINT' ;
 			MCTYPE.MC_____CODIGO____TD_____B := 'CXPN' ;
 			MCTYPE.MC_____CODIGO____CD_____B := 'NDPZ';
			MCTYPE.MC_____SECUINTE__DCD____B := 0  ;
			MCTYPE.MC_____FECHA_____B := FECHADOC_::DATE  ;
			MCTYPE.MC_____NUMERO____B := SECUENCIA_GEN  ;
			MCTYPE.MC_____SECUINTE__B := CONSEC  ;
 			MCTYPE.MC_____REFERENCI_B := _RECORDCONTABLE.REFERENCIA;
			MCTYPE.MC_____CODIGO____PF_____B := SUBSTRING(_CXPCONSOLIDADA.PERIODO,1,4)::INT;
			MCTYPE.MC_____NUMERO____PERIOD_B := SUBSTRING(_CXPCONSOLIDADA.PERIODO,5,2)::INT;
			MCTYPE.MC_____CODIGO____PC_____B :=  'PUCF' ;
			MCTYPE.MC_____CODIGO____CPC____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', _RECORDCONTABLE.TIPO_DOCUMENTO, _RECORDCONTABLE.CTA_DEBITO_APO,_RECORDCONTABLE.AGENCIA, 1)  ;
			MCTYPE.MC_____CODIGO____CU_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', _RECORDCONTABLE.TIPO_DOCUMENTO, _RECORDCONTABLE.CTA_DEBITO_APO,_RECORDCONTABLE.AGENCIA, 2)  ;
			MCTYPE.MC_____IDENTIFIC_TERCER_B :=  CASE WHEN CHAR_LENGTH(_RECORDCONTABLE.PROVEEDOR)>10 THEN SUBSTR(_RECORDCONTABLE.PROVEEDOR,1,10) ELSE _RECORDCONTABLE.PROVEEDOR END;
			MCTYPE.MC_____DEBMONORI_B := 0  ;
			MCTYPE.MC_____CREMONORI_B := 0 ;
			MCTYPE.MC_____DEBMONLOC_B := _RECORDCONTABLE.VALOR_DEBITO::NUMERIC  ;
			MCTYPE.MC_____CREMONLOC_B := _RECORDCONTABLE.VALOR_CREDITO::NUMERIC  ;
			MCTYPE.MC_____INDTIPMOV_B := 4  ;
			MCTYPE.MC_____INDMOVREV_B := 'N'  ;
			MCTYPE.MC_____OBSERVACI_B := _RECORDCONTABLE.DESCRIPCION  ;
			MCTYPE.MC_____FECHORCRE_B := FECHADOC_::TIMESTAMP  ;
			MCTYPE.MC_____AUTOCREA__B := 'ADMIN'  ;
			MCTYPE.MC_____FEHOULMO__B := FECHADOC_::TIMESTAMP  ;
			MCTYPE.MC_____AUTULTMOD_B := ''  ;
			MCTYPE.MC_____VALIMPCON_B := 0  ;
			MCTYPE.MC_____NUMERO_OPER_B := _CXPCONSOLIDADA.DOCUMENTO;
			MCTYPE.TERCER_CODIGO____TIT____B := _RECORDCONTABLE.TERCER_CODIGO____TIT____B  ;
			MCTYPE.TERCER_NOMBCORT__B := _RECORDCONTABLE.TERCER_NOMBCORT__B  ;
			MCTYPE.TERCER_NOMBEXTE__B := CASE WHEN CHAR_LENGTH(_RECORDCONTABLE.TERCER_NOMBEXTE__B)>64 then SUBSTR(_RECORDCONTABLE.TERCER_NOMBEXTE__B,1,64) ELSE _RECORDCONTABLE.TERCER_NOMBEXTE__B END;
			MCTYPE.TERCER_APELLIDOS_B := _RECORDCONTABLE.TERCER_APELLIDOS_B  ;
			MCTYPE.TERCER_CODIGO____TT_____B := _RECORDCONTABLE.TERCER_CODIGO____TT_____B  ;
			MCTYPE.TERCER_DIRECCION_B := CASE WHEN CHAR_LENGTH(_RECORDCONTABLE.TERCER_DIRECCION_B)>64 then SUBSTR(_RECORDCONTABLE.TERCER_DIRECCION_B,1,64) ELSE _RECORDCONTABLE.TERCER_DIRECCION_B END;
			MCTYPE.TERCER_CODIGO____CIUDAD_B := _RECORDCONTABLE.TERCER_CODIGO____CIUDAD_B  ;
			MCTYPE.TERCER_TELEFONO1_B := CASE WHEN CHAR_LENGTH(_RECORDCONTABLE.TERCER_TELEFONO1_B)>15 THEN SUBSTR(_RECORDCONTABLE.TERCER_TELEFONO1_B,1,15) ELSE _RECORDCONTABLE.TERCER_TELEFONO1_B END;
			MCTYPE.TERCER_TIPOGIRO__B := 1 ;
			MCTYPE.TERCER_CODIGO____EF_____B := ''  ;
			MCTYPE.TERCER_SUCURSAL__B := ''  ;
			MCTYPE.TERCER_NUMECUEN__B := ''  ;
			MCTYPE.MC_____CODIGO____DS_____B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', _RECORDCONTABLE.TIPO_DOCUMENTO, _RECORDCONTABLE.CTA_DEBITO_APO,_RECORDCONTABLE.AGENCIA, 3);
			--MCTYPE.MC_____NUMDOCSOP_B := REC_OS.NUMERO_OPERACION;
			MCTYPE.MC_____NUMEVENC__B := CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', _RECORDCONTABLE.TIPO_DOCUMENTO, _RECORDCONTABLE.CTA_DEBITO_APO,_RECORDCONTABLE.AGENCIA, 5)::INT;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', _RECORDCONTABLE.TIPO_DOCUMENTO, _RECORDCONTABLE.CTA_DEBITO_APO,_RECORDCONTABLE.AGENCIA, 4)='S')then
				MCTYPE.MC_____NUMDOCSOP_B := _RECORDCONTABLE.DOCUMENTO;
			else
				MCTYPE.MC_____NUMDOCSOP_B := '';
			end if;

			if(CON.OBTENER_HOMOLOGACION_APOTEOSYS('MICROCREDITO_N', _RECORDCONTABLE.TIPO_DOCUMENTO, _RECORDCONTABLE.CTA_DEBITO_APO,_RECORDCONTABLE.AGENCIA, 5)::int=1)then
				MCTYPE.MC_____NUMEVENC__B := 1;
			else
				MCTYPE.MC_____NUMEVENC__B := null;
			end if;
			
			
			-- Insertamos en la tabla de Apoteosys
			--FUNCION QUE TRANSACCION POR TIPO DE DOCUMENTO EN TABLA TEMPORAL EN FINTRA.
			SW:=apoteosys.SP_INSERT_TABLE_MC_MICRO____(MCTYPE);
			CONSEC:=CONSEC+1;
			------------------------------------------------------

		END LOOP; --FIN LOOP DE LA TRANSACCION INTERNA

		CONSEC:=1;


		--VALIDAMOS VALORES DEBITOS Y CREDITOS DEL COMPROBANTE A TRASLADAR.
		 IF CON.SP_VALIDACIONES(MCTYPE, 'MICROCREDITO') ='N' THEN
			SW='N';

			--BORRAMOS EL COMPROBANTE DE EXT
			DELETE FROM apoteosys.mc_micro____
			WHERE MC_____NUMERO____B = SECUENCIA_GEN AND MC_____CODIGO____CONTAB_B = 'FINT'
			 AND MC_____CODIGO____TD_____B = 'CXPN' AND  MC_____CODIGO____CD_____B = 'NDPZ';

			CONTINUE;
		END IF;

		-- ACTUALIZAMOS EL CAMPO DE APOTEOSYS DE LA CABECERA DEL CRéDITO PARA INDICAR QUE YA SE ENVíO
		IF(SW = 'S')THEN
			INSERT INTO apoteosys.traslado_facturas_apoteosys(tipo_documento, documento, agencia, periodo, fecha_negocio, unidad_negocio)
			VALUES ('FAP', _CXPCONSOLIDADA.DOCUMENTO, 'ATL', _CXPCONSOLIDADA.PERIODO, _CXPCONSOLIDADA.FECHA_documento, 'MICROCREDITO');
		END IF;




	END LOOP; --FIN LOOP PRINCIPAL


RETURN 'OK';

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION apoteosys.interfaz_microcredito_poliza_consolidada_apoteosys()
  OWNER TO postgres;
  
  --select * from con.interfaz_microcredito_poliza_consolidada_apoteosys();
