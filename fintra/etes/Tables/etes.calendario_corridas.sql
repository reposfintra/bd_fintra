-- Table: etes.calendario_corridas;

-- DROP TABLE etes.calendario_corridas;

CREATE TABLE etes.calendario_corridas
(
  id serial NOT NULL,
  reg_status character varying(1) NOT NULL DEFAULT ''::character varying,
  dstrct character varying(4) NOT NULL DEFAULT 'FINV'::character varying,
  id_transportadora integer NOT NULL,
  fecha timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp,
  fecha_corrida timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp,
  fecha_pago_fintra timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp,
  creation_date timestamp without time zone NOT NULL DEFAULT now(),
  creation_user character varying(10) NOT NULL DEFAULT ''::character varying,
  last_update timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
  user_update character varying(10) NOT NULL DEFAULT ''::character varying,
  CONSTRAINT fk_id_cal_transp FOREIGN KEY (id_transportadora)
      REFERENCES etes.transportadoras (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE etes.calendario_corridas
  OWNER TO postgres;

