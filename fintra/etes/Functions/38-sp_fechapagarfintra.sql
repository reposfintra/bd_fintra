-- Function: etes.sp_fechapagarfintra(integer, character varying)

-- DROP FUNCTION etes.sp_fechapagarfintra(integer, character varying);

CREATE OR REPLACE FUNCTION etes.sp_fechapagarfintra(_idmanifiesto integer, _tipo_anticipo character varying)
  RETURNS SETOF record AS
$BODY$

DECLARE

	ManifiestoCarga record;
	_transportadora integer;

BEGIN


    IF _tipo_anticipo='A' THEN 

			_transportadora:=(SELECT a.id_transportadora FROM etes.manifiesto_carga mc
							   INNER JOIN etes.agencias a ON a.id=mc.id_agencia WHERE mc.id=_idmanifiesto);
	ELSE 
	        _transportadora:=(SELECT a.id_transportadora  FROM etes.manifiesto_reanticipos ra
							 INNER JOIN etes.manifiesto_carga mc ON mc.id=ra.id_manifiesto_carga
		 	 				 INNER JOIN etes.agencias a ON a.id=mc.id_agencia	
		 	 				 WHERE ra.id=_idmanifiesto);	 		
	END IF;
	
	FOR ManifiestoCarga IN
				SELECT 
					fecha_corrida::varchar AS fechacorrida,
					fecha_pago_fintra::varchar AS fechapagofintra
				FROM etes.calendario_corridas 
			   WHERE fecha::date=current_date AND id_transportadora=_transportadora
	LOOP

	--	ManifiestoCarga.fechacorrida = '2018-08-27 00:00:00'::date;
	--	ManifiestoCarga.fechapagofintra = '2018-08-31 00:00:00'::date;

		RETURN NEXT ManifiestoCarga;

	END LOOP;


END;

$BODY$
  LANGUAGE plpgsql VOLATILE;
ALTER FUNCTION etes.sp_fechapagarfintra(integer, character varying)
  OWNER TO postgres;
